<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Notification extends CI_Model {

		public function __construct() {
			$this->load->model(array('overtime', 'leave'));
			parent::__construct();
		}

		// --------------------------------------------------------------------------

		public function add($notification_type_id = null, $notification_id = null, $recipient_id = null, $message = null) {
			$data = array();

			 if(!is_null($notification_type_id) && !is_null($notification_id) && !is_null($recipient_id) && !is_null($message)) {

				$this->db->trans_start();
				$this->db->insert('notification', array(
					'id' => NULL,
					'notification_type_id' => $notification_type_id,
					'notification_id' => $notification_id,
					'employee_id' => ($recipient_id !== 0) ? $recipient_id : NULL,
					'sender_id' => $this->session->id,
					'message' => $message,
					'status' => 0,
					'date' => date('Y-m-d')
				));

				if($this->db->affected_rows()) {
					$id = $this->db->insert_id();
					
					$data = array(
						'id' => $id,
						'notification_type_id' => $notification_type_id,
						'notification_id' => $notification_id,
						'employee_id' => $recipient_id,
						'message' => $message,
						'status' => 0,
						'date' => date('Y-m-d')
					);
				}

				$this->db->trans_complete();

				return $data;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get($employee_id = null) {

			$notification_list = array();

			$this->db->select('
				`notification`.`id`,
				`notification_type_id`,
				`notification_id`,
				`message`,
				`notification`.`status`,
				`notification`.`date`,
				`leave_type`.`name` AS "leave_type"
			');
			$this->db->from('`notification`');
			$this->db->join('`leave`', '`notification`.`notification_id` = `leave`.`id`');
			$this->db->join('`leave_type`', '`leave`.`leave_type_id` = `leave_type`.`id`');
			$this->db->where('`notification`.`sender_id`', $employee_id);
			$this->db->where('`notification`.`notification_type_id`', 1);
			$this->db->where('`notification`.`status`', 0);
			$this->db->where('`leave`.`status` !=', 'pending');
			$this->db->where('`leave`.`status` !=', 'cancelled');
			$this->db->where('`leave`.`show`', 1);
			$leave_query = $this->db->get();


			$this->db->select('`notification`.`id`, `notification_type_id`, `notification_id`, `message`, `notification`.`status`, `notification`.`date`');
			$this->db->from('`notification`');
			$this->db->join('`overtime`', '`notification`.`notification_id` = `overtime`.`id`');
			$this->db->where('`notification`.`sender_id`', $employee_id);
			$this->db->where('`notification`.`notification_type_id`', 2);
			$this->db->where('`notification`.`status`', 0);
			$this->db->where('`overtime`.`status` !=', 'pending');
			$this->db->where('`overtime`.`status` !=', 'cancelled');
			$this->db->where('`overtime`.`show`', 1);
			$overtime_query = $this->db->get();

			if($leave_query->num_rows()) {
				$notification_list = array_merge($notification_list, $leave_query->result_array());
			}

			if($overtime_query->num_rows()) {
				$notification_list = array_merge($notification_list, $overtime_query->result_array());
			}

			if(count($notification_list)) {
				
				usort($notification_list, function($notification_a, $notification_b) {
			 		return $notification_a['id'] < $notification_b['id'];
				});

				return $notification_list;
			}else {
				return false;
			}
		}

		// --------------------------------------------------------------------------

		public function get_employee_overtime_notification($employee_id = null) {

			$notification_list = array();

			$this->db->select("
				`notification`.`id`,
				`notification_type_id`,
				`notification_id`, `message`,
				`notification`.`status`,
				`notification`.`seen_by`,
				`notification`.`date`,
				CONCAT(`employee`.`firstname`, ' ' , `employee`.`lastname`) AS 'employee_name'
			");
			$this->db->from('`notification`');
			$this->db->join('`overtime`', '`notification`.`notification_id` = `overtime`.`id`');
			$this->db->join('`employee`', '`overtime`.`employee_id` = `employee`.`id`');
			$this->db->where('`notification`.`employee_id`', $employee_id);
			$this->db->where('`notification`.`notification_type_id`', 2);
			$this->db->where('`notification`.`seen_by`', 0);
			$this->db->where('`overtime`.`show`', 1);
			$overtime_query = $this->db->get();

			if($overtime_query->num_rows()) {
				$notification_list = array_merge($notification_list, $overtime_query->result_array());
			}

			if(count($notification_list)) {
				
				usort($notification_list, function($notification_a, $notification_b) {
			 		return $notification_a['id'] < $notification_b['id'];
				});

				return $notification_list;
			}else {
				return false;
			}
		}

		// --------------------------------------------------------------------------

		public function get_employee_leave_notification() {

			$notification_list = array();

			$this->db->select("
				`notification`.`id`,
				`notification_type_id`,
				`notification_id`,
				`message`,
				`notification`.`status`,
				`notification`.`seen_by`,
				`notification`.`date`,
				`leave_type`.`name` AS 'leave_type',
				`leave`.`status`,
				`leave`.`id` AS 'leave_id',
				CONCAT(`employee`.`firstname`, ' ' , `employee`.`lastname`) AS 'employee_name'
			");

			$this->db->from('`notification`');
			$this->db->join('`leave`', '`notification`.`notification_id` = `leave`.`id`');
			$this->db->join('`employee`', '`leave`.`employee_id` = `employee`.`id`');
			$this->db->where('`leave`.`status` !=', 'cancelled');
			$this->db->join('`leave_type`', '`leave`.`leave_type_id` = `leave_type`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 1);
			$this->db->where('`notification`.`seen_by`', 0);
			$this->db->where('`leave`.`show`', 1);

			if($this->session->type == 'board_of_director') { $this->db->where('`leave`.`employee_id` !=', $this->session->id); }
			$overtime_query = $this->db->get();

			if($overtime_query->num_rows()) {
				$notification_list = array_merge($notification_list, $overtime_query->result_array());
			}

			if(count($notification_list)) {
				
				usort($notification_list, function($notification_a, $notification_b) {
			 		return $notification_a['id'] < $notification_b['id'];
				});

				return $notification_list;
			} else {
				return false;
			}
		}

		// --------------------------------------------------------------------------

		public function get_employee_leave_notification_all( $notification_id = null ) {

			$notification_list = array();

			$this->db->select("
				`notification`.`id`,
				`notification_type_id`,
				`notification_id`,
				`message`,
				`notification`.`status`,
				`notification`.`seen_by`,
				`notification`.`date`,
				`leave_type`.`name` AS 'leave_type',
				`leave`.`status`,
				`leave`.`id` AS 'leave_id',
				CONCAT(`employee`.`firstname`, ' ' , `employee`.`lastname`) AS 'employee_name'
			");

			$this->db->from('`notification`');
			$this->db->join('`leave`', '`notification`.`notification_id` = `leave`.`id`');
			$this->db->join('`employee`', '`leave`.`employee_id` = `employee`.`id`');
			$this->db->where('`leave`.`status` !=', 'cancelled');
			$this->db->join('`leave_type`', '`leave`.`leave_type_id` = `leave_type`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 1);
			$this->db->where('`leave`.`show`', 1);

			if(!is_null($notification_id)) {
				$this->db->where('`notification`.`id`', $notification_id);
			}

			if($this->session->type == 'board_of_director') {
				$this->db->where('`leave`.`employee_id` !=', $this->session->id);
			}
			
			$overtime_query = $this->db->get();

			if($overtime_query->num_rows()) {
				$notification_list = array_merge($notification_list, $overtime_query->result_array());
			}

			if(count($notification_list)) {
				
				usort($notification_list, function($notification_a, $notification_b) {
			 		return $notification_a['id'] < $notification_b['id'];
				});

				return $notification_list;
			} else {
				return false;
			}
		}

		// --------------------------------------------------------------------------

		public function count($employee_id = null) {
			
			$this->db->select('`notification`.`id`');
			$this->db->from('`notification`');
			$this->db->join('`leave`', '`notification`.`notification_id` = `leave`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 1);
			$this->db->where('`notification`.`status`', 0);
			$this->db->where('`leave`.`show`', 1);
			$this->db->where('`leave`.`status` !=', 'pending');
			$this->db->where('`leave`.`status` !=', 'cancelled');
			$this->db->where('`leave`.`employee_id`', $employee_id);
			$leave_count = $this->db->count_all_results();

			$this->db->select('`notification`.`id`');
			$this->db->from('`notification`');
			$this->db->join('`overtime`', '`notification`.`notification_id` = `overtime`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 2);
			$this->db->where('`notification`.`status`', 0);
			$this->db->where('`overtime`.`show`', 1);
			$this->db->where('`overtime`.`status` !=', 'pending');
			$this->db->where('`overtime`.`status` !=', 'cancelled');
			$this->db->where('`overtime`.`employee_id`', $employee_id);
			$overtime_count = $this->db->count_all_results();

			return $leave_count + $overtime_count;
		}

		// --------------------------------------------------------------------------

		public function count_employee_overtime_applications($employee_id = null) { // Counts overtime application of employee under this PM

			$this->db->select('`notification`.`id`');
			$this->db->from('`notification`');
			$this->db->join('`overtime`', '`notification`.`notification_id` = `overtime`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 2);
			$this->db->where('`notification`.`seen_by`', 0);
			$this->db->where('`overtime`.`show`', 1);
			$this->db->where('`overtime`.`pm_id`', $employee_id);
			$overtime_count = $this->db->count_all_results();

			return $overtime_count;
		}

		// --------------------------------------------------------------------------

		public function count_employee_leave_applications() { // Counts leave application of employee

			$this->db->select('`notification`.`id`');
			$this->db->from('`notification`');
			$this->db->join('`leave`', '`notification`.`notification_id` = `leave`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 1);
			$this->db->where('`notification`.`seen_by`', 0);

			if($this->session->type == 'board_of_director') {
				$this->db->where('`leave`.`employee_id` !=', $this->session->id);
			}

			$this->db->where('`leave`.`show`', 1);
			$this->db->where('`leave`.`status` !=', 'cancelled');
			$overtime_count = $this->db->count_all_results();

			return $overtime_count;
		}

		// --------------------------------------------------------------------------

		public function seen($employee_id = null, $notification_type_id = null, $table_pk = null) {

			if($notification_type_id == 1) {
				$table = 'leave';
			}else { $table = 'overtime'; }

			$this->db->select('`notification`.`id`, `notification`.`status`');
			$this->db->from("`$table`");
			$this->db->join('notification', "`$table`.`id` = `notification`.`notification_id`");
			$this->db->where('`notification`.`notification_type_id`', $notification_type_id);
			$this->db->where("`$table`.`id`", $table_pk);
			$this->db->where("`$table`.`employee_id`", $employee_id);
			$this->db->where("`$table`.`show`", 1);
			$query = $this->db->get();

			$notification = $query->result_array()[0];

			if($notification['status'] == 0) {

				if($notification_type_id == 1) {
					if(!$this->leave->is_pending( $table_pk )) {
						$this->db->update('`notification`', array('`status`' => 1), array('`id`' => $notification['id']));
						if($this->db->affected_rows()) { return true; }
					}
				}else {
					if(!$this->overtime->is_pending( $table_pk )) {
						$this->db->update('`notification`', array('`status`' => 1), array('`id`' => $notification['id']));
						if($this->db->affected_rows()) { return true; }
					}
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function ot_seen_by_hp($pm_id = null, $overtime_id = null) {

			$this->db->select('`notification`.`id`, `notification`.`seen_by`');
			$this->db->from("`overtime`");
			$this->db->join('notification', "`overtime`.`id` = `notification`.`notification_id`");
			$this->db->where("`notification`.`notification_type_id`", 2);
			$this->db->where("`overtime`.`id`", $overtime_id);
			$this->db->where("`overtime`.`pm_id`", $pm_id);
			$this->db->where("`overtime`.`show`", 1);
			$query = $this->db->get();

			$notification = $query->result_array()[0];

			if($notification['seen_by'] == 0) {
				$this->db->update('`notification`', array('`seen_by`' => 1), array('`id`' => $notification['id']));
				if($this->db->affected_rows()) { return true; }
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function leave_seen_by_hp($leave_id = null) {

			$this->db->select('`notification`.`id`, `notification`.`seen_by`');
			$this->db->from("`leave`");
			$this->db->join('`notification`', "`leave`.`id` = `notification`.`notification_id`");
			$this->db->where("`notification`.`notification_type_id`", 1);
			$this->db->where("`leave`.`id`", $leave_id);
			$this->db->where("`leave`.`show`", 1);
			$query = $this->db->get();

			$notification = $query->result_array()[0];

			if($notification['seen_by'] == 0) {
				$this->db->update('`notification`', array('`seen_by`' => 1), array('`id`' => $notification['id']));
				if($this->db->affected_rows()) { return true; }
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function feed($notification_type = null, $notification_id = null) {

			$request_feed = array();

			if($notification_type == 'leave') {
				$leave_requests = $this->leave->detailed_list($this->session->id, $notification_id);

				if($leave_requests !== false) {
					$request_feed = array_merge($request_feed, $leave_requests);
				}
			}

			if($notification_type == 'overtime') {
				$overtime_requests = $this->overtime->detailed_list($this->session->id, $notification_id);

				if($overtime_requests !== false) {
					$request_feed = array_merge($request_feed, $overtime_requests);
				}
			}

			if(count($request_feed) > 0) { return $request_feed; }

			return false;
		}

		// -------------------------------------------------------------------------------
	}