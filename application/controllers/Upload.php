<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Upload extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->library(array('upload', 'e_security', 'e_attendance'));
			$this->load->model(array('admin_db', 'employee', 'timelog'));
		}

		// -------------------------------------------------------------------------------------

		public function index() {

			$ui = NULL;

			switch ($this->session->type) {
				case 'regular_employee':
					$ui = 'regemp';
				break;

				case 'project_manager':
					$ui = 'pm';
				break;

				case 'board_of_director':
					$ui = 'bod';
				break;

				default:
					$ui = 'regemp';
				break;
			}
			
			$data = $this->employee->get_overall_info($this->session->id);
			$data['employee_position'] = $this->employee->get_position();
			$data['employee_skills'] = $this->employee->skills();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);

			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);

			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);

			$data['notification_count'] = $this->notification->count($this->session->id);
			$data['notification_list'] = $this->notification->get($this->session->id);
			$data['basic_info'] = $this->employee->get_overall_info($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $time = $this->timelog->count_hours($data['weekly_attendance']);
			$data['current_tab'] = 'change_picture';
			$data['page_title'] = 'Update Picture';

			$this->load->view($ui . '/header', $data);
			$this->load->view($ui . '/modal');
			$this->load->view($ui . '/nav');

			if(!empty($this->input->post('upload_picture'))) {

				if($this->upload->do_upload('picture')) {

					$info = array('picture' => $this->upload->data('file_name'));
					$img_width = intval($this->upload->data('image_width'))/2;
					$img_height = intval($this->upload->data('image_height'))/2;

					$config['width'] = $img_width;
					$config['height'] = $img_height;
					$config['source_image'] = base_url() . 'resources/dist/img/users/' . $info['picture'];

					$this->load->library('image_lib');
					$this->image_lib->initialize($config);
					$this->image_lib->clear();
					
					if ($this->image_lib->resize()) {
						if($this->employee->update_info($info)) {
							$data['success'] = TRUE;
							$this->load->view($ui . '/editprofile', $data);
						}else {
							$data['upload_error'] = "<p class='error'>Unable to update picture.</p>";
							$this->load->view($ui . '/editprofile', $data);
						}
					}else {
						$data['upload_error'] = $this->image_lib->display_errors('<p class="error">', '</p>');
						$this->load->view($ui . '/editprofile', $data);
					}

				}else {
					$data['upload_error'] = $this->upload->display_errors('<p class="error">', '</p>');
					$this->load->view($ui . '/editprofile', $data);
				}
			}else {
				$this->load->view($ui . '/editprofile');
			}

			$this->load->view($ui . '/footer');

		}

		// -------------------------------------------------------------------------------------

		public function admin_upload() {


			$employee_id = $this->input->post('employee_id');
			$this->e_security->only_allow('admin');
			$data = $this->employee->get_overall_info( $this->session->id );
			$data['employee_position'] = $this->employee->get_position();
			$data['employee_skills'] = $this->employee->skills( $employee_id );
			$data['employee_skill_id'] = $this->admin_db->get_employee_skill_id( $employee_id );
			$data['skill_list'] = $this->admin_db->get_skill();
			$data['position_list'] = $this->admin_db->get_position();
			$data['deactivation_detail'] = $this->admin_db->deactivation_detail( $employee_id );
			$data['project_manager_list'] = $this->admin_db->pm_list();
			$data['assigned_to_pm_id'] = $this->admin_db->assigned_to_pm_id($employee_id);
			$data['employee_detail'] = $this->admin_db->get_employee_info( $employee_id );
			$data['get_position_id'] = $this->admin_db->get_position_id( $employee_id );
			$data['employee_leave_detail'] = $this->admin_db->get_leave_detail( $employee_id );

			$this->load->view('admin/header', $data);
			$this->load->view('admin/modal');
			$this->load->view('admin/nav');

			if(!empty($this->input->post('upload_picture'))) {

				if($this->upload->do_upload('picture')) {

					$info = array('picture' => $this->upload->data('file_name'));
					$img_width = intval($this->upload->data('image_width'))/2;
					$img_height = intval($this->upload->data('image_height'))/2;

					$config['width'] = $img_width;
					$config['height'] = $img_height;
					$config['source_image'] = base_url() . 'resources/dist/img/users/' . $this->upload->data('file_name');

					$this->load->library('image_lib');
					$this->image_lib->initialize($config);
					$this->image_lib->clear();
					
					if ($this->image_lib->resize()) {
						if($this->employee->update_info($info, $employee_id)) {
							header('Location: ' . base_url('admin/employee-profile/' . $employee_id));
							$this->load->view('admin/employee_profile');
						} else { $this->load->view('admin/employee_profile'); }
					} else { $this->load->view('admin/employee_profile'); }
				} else { $this->load->view('admin/employee_profile'); }
			}else {
				$this->load->view('admin/employee_profile');
			}

			$this->load->view('admin/footer');

		}

		// -------------------------------------------------------------------------------------

}