
  <!-- Edit Overtime Log Modal -->
  <div class="modal" id="edit-overtime-log-modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Feb 21, 2016 <small>1st overtime</small></h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-xs-6">
                <label>Time In</label>
                  <input type="time" class="form-control">
              </div>
              <div class="form-group col-xs-6">
                <label>Time Out</label>
                  <input type="time" class="form-control" disabled>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" href="#update-log-prompt" data-toggle="modal">Save</button>
          </div>
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Edit Overtime Log Modal End -->



  <!-- Edit Regular Time Log Modal -->
  <div class="modal" id="edit-regular-time-log-modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><div id="timelog_modal_title"></div></h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-xs-6">
                <label>Time In</label>
                  <input type="time" class="form-control" id="timelog_edit_from" name="timelog_edit_from" />
              </div>
              <div class="form-group col-xs-6">
                <label>Time Out</label>
                  <input type="time" class="form-control" id="timelog_edit_to" name="timelog_edit_to" disabled />
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" href="#update-log-prompt" data-toggle="modal">Save</button>
          </div>
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- Edit Regular Time Log Modal End -->


    <!-- Modal For Update Log Prompt -->

    <div class="modal modal-default" id="update-log-prompt" tabindex="-1">
      <div class="modal-dialog modal-sm">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><i class="fa fa-warning"></i></h4>
            </div>
            <div class="modal-body text-centered">
              <h4 class="text-red">You are about to edit this time log.</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default">Confirm</button>
            </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  <!-- Modal For Update Log Prompt End -->

  <!-- Add Employee Modal -->
  <?= form_open('admin/add-employee', array('class' => 'add_employee')); ?>
  <div class="modal" id="add-employee-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Employee</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-xs-6">
                <label>First Name</label>
                  <input type="text" id="firstname" name="firstname" class="form-control" required>
              </div>
              <div class="form-group col-xs-6">
                <label>Last Name</label>
                  <input type="text" id="lastname" name="lastname" class="form-control" required>
              </div>
              <div class="form-group col-xs-6">
                <label>Birthdate</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                  <input type="date" class="form-control pull-right" id="birthdate" name="birthdate" required>
                </div>
              </div>
              <div class="form-group col-xs-6">
                <label>Gender</label>
                <div>
                  <select id="gender" required name="gender" class="form-control" required>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12">
                <label>Address</label>
                  <input type="text" id="address" name="address" class="form-control">
              </div>
              <div class="form-group col-xs-6">
                <label>Contact Number</label>
                  <input type="text" id="contact" name="contact" class="form-control">
              </div>
              <div class="form-group col-xs-6">
                <label>Email</label>
                  <input type="text" id="email" name="email" class="form-control">
              </div>
              <div class="form-group col-xs-6">
                <label>Position</label>
                  <input type="text" id="position" name="position" class="form-control" required>
              </div>
              <div class="form-group col-xs-6">
                <label>Type</label>
                <div>
                  <select id="type" name="type" class="form-control user-type-select">
                    <option value="regular_employee">Employee</option>
                    <option value="project_manager">Project Manager</option>
                    <option value="board_of_director">BOD</option>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-12">
                <label>Skills</label>                
                <select id="skills" name="skills" class="form-control select2" multiple="multiple" data-placeholder="Select..." style="width: 100%;">
                  <?php				  		
						foreach($all_skills as $skill){
							echo '<option value="'.$skill['id'].'">'.$skill['description'].'</option>';							
						}
					?>
                </select>
              </div><!-- /.form-group -->
              <div class="form-group col-xs-6">
                <label>Date Started</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                  <input type="date" class="form-control pull-right" id="startdate" name="startdate" required>
                </div>
              </div>
              <div id='project_manager_field' class="form-group col-xs-6">
                <label>Project Manager</label>
                <div>
                  <select id="projectmanager" name="projectmanager" class="form-control select2" multiple="multiple" data-placeholder="Select..." style="width: 100%;">
                    <?php 
						foreach($all_pm as $pm){
							echo '<option>'.$pm['firstname'].' '.$pm['lastname'].'</option>';
						}							
					?>
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-6">
                <label>Hourly Rate</label>
                  <input id="rate" name="rate" type="number" class="form-control" required>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <input type='submit' class='btn btn-primary' value='Confirm' />
          </div>
          <div id='add_employee_message'></div>
        </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <?= form_close(); ?>
  <!-- Add Employee Modal End -->
    














  <div class="modal" id="add-holiday-modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Holiday</h4>
          </div>
          
          <div class="modal-body">

            <div class="row">

              <div class="form-group col-xs-6">
                <label>Date</label>
                  <input type="date" id="holiday_date" class="form-control">
              </div>

              <div class="form-group col-xs-6">
                <label>Holiday</label>
                  <input type="text" id="holiday_name" class="form-control">
              </div>

              <div class="form-group col-xs-6">
                <label>Type</label>
                  <select id="holiday_type" class="form-control">
                    <option value="regular">Regular Holiday</option>
                    <option value="special">Special Holiday</option>
                  </select>
              </div>

              <div class="form-group col-xs-12">
                <div id='add_holiday_info'></div>
              </div>


            </div>

          </div>

          <div class="modal-footer">
              <button type="button" id='holiday_close_button' class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" id="add_holiday_button" class="btn btn-primary">Confirm</button>
          </div>

        </div>
       </div>
  </div>


















<!-- Edit Holiday Modal -->



<!--   <div class="modal" id="edit-holiday-modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><div id="edit_holiday_title"></div></h4>
          </div>
          <?php // form_open('admin/edit-holiday', array('class' => 'edit_holiday', 'id' => 'edit_holiday_form')); ?>
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-xs-6">
                <label>Date</label>
                  <input type="hidden" class="form-control" name="holiday_id_edit" id="holiday_id_edit">
                  <input type="date" class="form-control" name="holiday_date_edit" id="holiday_date_edit">
              </div>
              <div class="form-group col-xs-6">
                <label>Holiday</label>
                  <input type="text" class="form-control" name="holiday_name_edit" id="holiday_name_edit">
              </div>
              <div class="form-group col-xs-6">
                <label>Type</label>
                  <select class="form-control" name="holiday_type_edit" id="holiday_type_edit">
                    <option value="regular">Regular Holiday</option>
                    <option value="special">Special Holiday</option>
                  </select>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" onclick="refreshPage()" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" href="#edit-holiday-prompt" data-toggle="modal">Confirm</button>
          </div>
          <div id='edit_holiday_message'></div>
          <?php // form_close(); ?>
        </div>
       </div>
  </div> -->
  <!-- Edit Holiday Modal End -->



  
    <!-- Modal For Edit Holiday Prompt -->

    <div class="modal modal-default" id="edit-holiday-prompt" tabindex="-1">
      <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><i class="fa fa-warning"></i></h4>
            </div>
            <div class="modal-body text-centered">
              <h4 class="text-red"><div id="edit_holiday_name"></div></h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
              <input type="button" class="btn btn-primary" onclick="editHoliday(edit_holiday_form)" data-dismiss="modal" value="Confirm" />
            </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  <!-- Modal For Edit Holiday Prompt End -->
  
  <!-- Modal For Deactivate -->

    <div class="modal modal-default" id="employee-deactivate-prompt" tabindex="-1">
      <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><i class="fa fa-warning"></i></h4>
            </div>
            <div class="modal-body text-centered">
              <h4 class="text-red"><div id="activate_modal_text"></div></h4>
            </div>
            <?= form_open('admin/home', array('id' => 'activate_form')); ?>
            <div class="modal-body text-centered" id="reason_div">
              <div class="form-group">
                <textarea class="form-control" rows="3" placeholder="Your reason here..." id="deactivate_reason" name="deactivate_reason"></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>              
              <input type="hidden" name="modal_emp_function" id="modal_emp_function" />
              <input type="hidden" name="modal_emp_id" id="modal_emp_id"/>
              <button type="submit"  class="btn btn-default">Confirm</button>              
            </div>
            <?= form_close(); ?>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  <!-- Modal For Deactivate End -->


  <!-- Modal For Activate -->

    <div class="modal modal-default" id="employee-activate-prompt" tabindex="-1">
      <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><i class="fa fa-warning"></i></h4>
            </div>
            <div class="modal-body text-centered">
              <h4 class="text-red">You are about to activate Jason Roxas.</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-default">Confirm</button>
            </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  <!-- Modal For Activate End -->

  <!-- Modal For Remove -->

<!--     <div class="modal modal-default" id="holiday-remove-prompt" tabindex="-1">
      <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><i class="fa fa-warning"></i></h4>
            </div>
            <div class="modal-body text-centered">
              <h4 class="text-red"><div id="remove_holiday_name"></div></h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
              <?php // form_open('admin/holiday', array('class' => 'remove_holiday', 'id' => 'remove_holiday_form')); ?>  
              <input type="hidden" class="form-control" name="holiday_id_remove" id="holiday_id_remove">
              <input type="submit" class="btn btn-primary" value="Confirm" />
              <?php // form_close(); ?> 
            </div>
          </div>
      </div>
    </div> -->

  <!-- Modal For Remove End -->

<!-- Modal For Notification -->

    <div class="modal modal-default" id="admin-notification" tabindex="-1">
      <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="hideToast()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><i class="fa fa-warning"></i></h4>
            </div>
            <div class="modal-body text-centered">
              <h4 class="text-red"><div id="admin_notification_text">
              <?php
					if($param==1){
						echo 'Employee successfully created!';
					}
					elseif($param==2){
						echo 'Employee creation failed!';
					}
					elseif($param==3){
						echo 'Username already used. Employee creation failed!';
					}
					elseif($param==4){
						echo 'Please fill in the form';
					}
					elseif($param==5){
						echo 'Employee Activated';
					}
					elseif($param==6){
						echo 'Employee Deactivated';
					}
					elseif($param==7){
						echo 'Employee Update Failed!';
					}
					elseif($param==8){
						echo 'Holiday deleted!';
					}
					else{
					}
				?>
              </div></h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" onclick="hideToast()">Ok</button>
            </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  <!-- Modal For Notification End -->







  <!-- Modals End -->
  
  <?php
	  	if($param){
			echo "<script>$('#admin-notification').show();</script>";
		}
	  ?>




