<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Holiday extends CI_Model {

		public function __construct() {
			parent::__construct();
			$this->load->library(array('e_security', 'e_attendance'));
		}

		// --------------------------------------------------------------------------

		public function view() {
			// CHUCHUCHU
			$this->db->where('status !=', 0);
			// CHUCHUCHU
			$query = $this->db->get('holiday');
			return $query->result_array();
		}

		// --------------------------------------------------------------------------

		public function type($date = null) { # Returns the type of date and can also be use to check if holiday exist

			if($this->e_security->is_validDate($date)) {

				$today = $this->e_attendance->date_extract($date);

				$query = $this->db->get('holiday');

				if($query->num_rows()) {

					foreach ($query->result_array() as $index => $holiday) {
						$holiday_extract = $this->e_attendance->date_extract($holiday['date']);

						if(strcmp($today['month'], $holiday_extract['month']) === 0 && strcmp($today['day'], $holiday_extract['day']) === 0) {
							return $holiday['type'];
						}
					}
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_dates( $year = null ) {

			$holiday_dates = array_column(self::view(), 'date');
			$current_year_holidays = array();

			if($year !== NULL) {
				foreach ($holiday_dates as $index => $holiday) {
					$holiday_extract = $this->e_attendance->date_extract($holiday);

					$current_year_holidays[] = $year . '-' . $holiday_extract['month'] . '-' . $holiday_extract['day'];
				}
				
				return $current_year_holidays;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_dates_within($date_range = null) {  }

		// --------------------------------------------------------------------------
	}