<div class="employee-management-wrapper">
<div class="content-wrapper">

<section class="content">
<div class="row">
<div class="col-md-3">
<div class="box box-primary">
<div class="box-body box-profile">
<img class="profile-user-img img-responsive img-circle" src="<?= base_url('resources/dist/img/users/' . $picture); ?>" alt="User profile picture" style="width: 128px; height: 128px;">
<h3 class="profile-username text-center">Administrator</h3>

<ul class="list-group list-group-unbordered">
</ul>

</div>
</div>

</div>
<div class="col-md-9">
<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
<li class="active"><a href="#activity" data-toggle="tab">Change Password</a></li>
</ul>
<div class="tab-content">




                <div class='active tab-pane' id="change-password">

                    <?= form_open('user/change-password', array('class' => 'form-horizontal', 'id' => 'change_password')); ?>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Old Password</label>
                      <div class="col-sm-6">
                        <input type="password" name='oldpass' id='oldpass_field' class="form-control" onkeyup="checkpass(oldpass_field)" />
                      </div>
                      <div id='oldpass_info'></div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label">New Password</label>
                      <div class="col-sm-6">
                        <input type="password" name='newpass' id='newpass_field' class="form-control" onkeyup="checkpass(newpass_field)" />
                      </div>
                      <div id='newpass_info'></div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Confirm New Password</label>
                      <div class="col-sm-6">
                        <input type="password" name='confirmpass' id='confirmpass_field' class="form-control" onkeyup="checkpass(confirmpass_field)" />
                      </div>
                      <div id='repass_info'></div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label"></label>
                      <div class="col-sm-6">
                        <div id='changepass_info'></div>
                      </div>
                    </div>

                    <hr />

                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" id='changepass_button' class="btn btn-primary">Save</button>
                        <input type='hidden' value='<?= base_url();?>' id='base_url' />
                      </div>
                    </div>
                  </form>
                </div>





</div>
</div>
</div>
</div>


</section>
</div>
</div>