<div class="content-wrapper">
	<div class="container">
		<section class="content-header">
			<div class="content-wrapper">
				<section class="content">
					<div class="row">
						<div class="col-md-4 col-sm-8 col-xs-12">
							<div class="info-box">
								<span class="info-box-icon bg-blue"><i class="fa fa-clock-o"></i></span>
								<div class="info-box-content">
									<span class="info-box-text">Total Regular Hours</span>
									<span class="info-box-number" id='total_timelog_reghours'>
										<?php
											if($attendance_total_hours['regular'] !== NULL) {
												echo $attendance_total_hours['regular'];
											} else { echo '0'; }
										?>
									</span>
								</div>
							</div>
						</div>
					</div>

					<?= form_open('attendance/timelog-search', array('class' => 'timelog_search')); ?>
					<div class="row">
						<div class="form-group col-md-4 col-sm-4 col-xs-12">
							<label>From</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="date" class="form-control pull-right" id="from" />
							</div>
						</div>
						
						<div class="form-group col-md-4 col-sm-4 col-xs-12">
							<label>To</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="date" class="form-control pull-right" id="to" />
							</div>
						</div>
						
						<div class="form-group col-md-4 col-sm-4 col-xs-12">
							<label>Search</label>
							<div class="input-group">
								<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</div>
					<?= form_close(); ?>

					<div class="row">
						<div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-body">





									<table id='timelog_table' class="table table-hover">
										<thead>
											<tr>
												<td style='display: none;'></td>
												<th>Date</th>
												<th>Time In</th>
												<th>Time Out</th>
												<th>Total Time</th>
											</tr>
										</thead>
										
										<tbody>


<?php
										if(count($timelog)) {

											foreach ($timelog as $index => $log) {

												if($log['type'] == 'leave') {

													echo <<<LEAVE
														<tr>
															<td style='display: none;'>{$log['timestamp']}</td>
															<td>{$log['date']}</td>
															<td>--</td>
															<td>--</td>
															<td>{$log['total_time']} hrs <span class='label bg-navy'>Leave</span></td>
														</tr>
LEAVE;

												}else {

													$hr_word = ($log['hour'] > 1) ? ' hrs ' : ' hr ';
													$min_word = ($log['minute'] > 1) ? ' mins ' : ' min ';

													if($log['hour'] == 0 && $log['minute'] == 0) {
														$timelog_total_time = '<strong>NC</strong>';
													}elseif ($log['hour'] > 0 && $log['minute'] == 0) {
														$timelog_total_time = $log['hour'] . $hr_word;
													}elseif($log['hour'] == 0 && $log['minute'] > 0) {
														$timelog_total_time = $log['minute'] . ' ' . $min_word;
													}else {
														$timelog_total_time = $log['hour'] . $hr_word . $log['minute'] . $min_word;
													}

													if(!is_null($log['time_out'])) {
														echo <<<TIMELOG_COMPLETE

															<tr>
																<td style='display: none;'> {$log['timestamp']} </td>
																<td> {$log['timein_date']} </td>
																<td> {$log['time_in']} </td>
																<td> {$log['time_out']} </td>
																<td> $timelog_total_time </td>
															</tr>
TIMELOG_COMPLETE;
													}else {

														echo <<<TIMELOG_INCOMPLETE

															<tr>
																<td></td>
																<td style='display: none;'> {$log['timestamp']} </td>
																<td> {$log['timein_date']} </td>
																<td> {$log['time_in']} </td>
																<td> -- </td>
																<td> $timelog_total_time </td>
															</tr>
TIMELOG_INCOMPLETE;

													}

												}
											} // End of foreach

										}else {
											echo "
												<tr>
													<td style='display: none;'></td>
													<td><p class='error'>Log is empty</p></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											";
										}

?>


										<!-- *************************************************************************************** -->

										</tbody>
									</table>








								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</section>
	</div>
</div>