<div id='leave_modal'>

<?php

	$pending_count = 0;
	if($my_request_feed !== false) {
		foreach ($my_request_feed as $index => $req) {
			
		if($req['status'] == 'pending') {
			$pending_count = $pending_count + 1;
			$approved_by = '--';
		} elseif($req['status'] == 'cancelled') {
			$approved_by = '--';
		} else {
			$approved_by = $req['manager_name'] . ' on ' . nice_date($req['date_updated'], 'M d, Y');
		}

		$table_pk = $req['table_pk'];
		$from = nice_date($req['start_date'], 'M d, Y');
		$to = nice_date($req['end_date'], 'M d, Y');
		$date_filed = nice_date($req['date_filed'], 'M d, Y');

		if($req['notification_type_id'] == 1) {

			$leave_type = $req['leave_type'];


			echo <<<LEAVE_REQ

				  <div class="modal" id="leave_request_$table_pk" tabindex="-1">
				    <div class="modal-dialog">
				      <div class="modal-content">

				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				          <h3 class="modal-title">$leave_type</h3>
				        </div>

				          <div class="modal-body">
				            <div class="row">
				              <div class="form-group col-xs-4">
				                <h4>From: $from</h4>
				              </div>
				              
				              <div class="form-group col-xs-4">
				                <h4>To: $to</h4>
				              </div>
				            </div>
				          </div>

				          <div class="modal-footer">
				            <h5 class="pull-right text-red">Noted by: $approved_by</h5>
				            <h5 class="pull-left text-red">Date Filed: $date_filed</h5>
				          </div>
				      </div>
				    </div>
				  </div>
LEAVE_REQ;

			}
		}
	}
?>

</div>

<div class="content-wrapper">
	<div class="container">
		<section class="content-header">
		</section>

		<section class="content">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-aqua">
						<span class="info-box-icon"><i class="fa fa-feed"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Request Feed</span>
							
							<div class="progress">
								<div class="progress-bar" style="width: 100%"></div>
							</div>
							
							<span class="progress-description">
								<?php
									if($pending_count == 0) {
										echo 'No Pending Request';
									}elseif($pending_count == 1) {
										echo '1 Pending Request';
									}else {
										echo $pending_count . ' Pending Requests';
									}
								?>
							</span>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body">

							<table id="reqfeed_table" class="table table-hover">
								<thead>
									<tr>
										<th style='display: none;'></th>
										<th>Date Filed</th>
										<th>Type</th>
										<th>Status</th>
										<th>Noted By</th>
										<th>Action</th>
									</tr>
								</thead>
								
								<tbody>



<?php

	if($my_request_feed !== false) {

		foreach ($my_request_feed as $index => $request) {

			$date_filed = nice_date($request['date_filed'], 'M d, Y');

			if($request['notification_type_id'] == 1) { $request_type = 'Leave'; }
			else { $request_type = 'Overtime'; }

			$tr_id = 'tr_' . strtolower($request_type) . '_request_' . $request['table_pk'];

			if($request['seen_status'] == 0 && $request['status'] != 'pending' && $request['status'] != 'cancelled') {
				$seen_status = 'class="bg-gray odd"';
			}else { $seen_status = ''; }

			$request_status = function($request_status) {
				$html_form = null;

				switch ($request_status) {
					case 'approved':
						$html_form = '<span class="label label-success">Approved</span>';
					break;

					case 'pending':
						$html_form = '<span class="label label-warning">Pending</span>';
					break;

					case 'declined':
						$html_form = '<span class="label label-danger">Declined</span>';
					break;

					case 'cancelled':
						$html_form = '<span class="label label-danger">Cancelled</span>';
					break;

				}

				return $html_form;
			};

			if($request['date_updated'] !== NULL) {
				$date_updated = nice_date($request['date_updated'], 'M d, Y');
			}

			if($request['status'] === 'pending' || $request['status'] == 'cancelled') {
				$noted_by = '--';
			}else {
				$noted_by = $request['manager_name'] . ' on ' . $date_updated;
			}
?>


		<tr <?= $seen_status . " id='$tr_id'"; ?> >
			<td style='display: none;'> <?= $request['timestamp']; ?> </td>
			<td> <?= $date_filed; ?> </td>
			<td> <?= $request['leave_type']; ?> </td>
			<td class='request_status'> <?= $request_status($request['status']); ?> </td>
			<td> <?= $noted_by; ?> </td>
			<td>
				<a class="btn btn-primary btn-sm" onclick="seen_request(<?php echo $request['notification_type_id'] . ', ' . $request['table_pk']; ?>);" href="#<?= strtolower($request_type) . '_request_' . $request['table_pk']; ?>" data-toggle="modal">View</a>
				<?php
					if($request['status'] == 'pending') {
						if($request['notification_type_id'] == 1) {
							echo "<a class='btn btn-danger btn-sm' id='cancel_button' onclick='cancel_leave_request(" . $request['table_pk'] . ");'>Cancel</a>";
						}else { echo "<a class='btn btn-danger btn-sm' id='cancel_button' onclick='cancel_overtime_request(" . $request['table_pk'] . ");'>Cancel</a>"; }
					}
				?>
			</td>
		</tr>


<?php
		}

	} else {
		echo "
			<tr>
				<td style='display: none;'></td>
				<td> <p class='error'>No record.</p> </td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		";
	}
?>


								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>