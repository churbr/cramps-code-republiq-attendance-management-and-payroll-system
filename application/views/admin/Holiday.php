<div class="modal" id="edit-holiday-modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id='holiday_edit_title'></h4>
      </div>

      <div class="modal-body">
        <div class="row">
            <div class="form-group col-xs-6">
              <label>Date</label>
              <input type="date" id="holiday_date" class="form-control">
              <input type='hidden' id='edit_holiday_table_pk' />
            </div>

            <div class="form-group col-xs-6">
              <label>Holiday</label>
              <input type="text" id="holiday_name" class="form-control">
            </div>

            <div class="form-group col-xs-6">
              <label>Type</label>
              <div id='select_date_div'></div>
            </div>

            <div class="form-group col-xs-12">
              <div id='edit_holiday_form_info'></div>
            </div>

            
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" id="holiday_edit_close" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" id="holiday_edit_confirm" class="btn btn-primary" href="#edit-holiday-prompt" data-toggle="modal">Confirm</button>
      </div>
    </div>
  </div>
</div>


    <div class="modal modal-default" id="holiday-remove-prompt" tabindex="-1">
      <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><i class="fa fa-warning"></i></h4>
            </div>
            <div class="modal-body text-centered">
              <h4 class="text-red" id='removal_message'></h4>
            </div>
            <div class="modal-footer">
              <input type='hidden' id='holiday_removal_pk' />
              <button type="button" id='holiday_remove_close' class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
              <button type="button" id='holiday_remove_confirm' class="btn btn-primary" data-dismiss="modal">Confirm</button>
            </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->






<div class="holiday-management-wrapper">



<div class="content-wrapper">
  <!-- <div class="container"> -->
    <section class="content-header">
    </section>

    <section class="content">

      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-suitcase"></i></span>
            <div class="info-box-content">
              <h3>Holidays</h3>
            </div>
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">

              <table id="holiday_table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Holiday</th>
                    <th>Type</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>

<?php
  foreach ($holidays as $index => $holiday) {
    $type = ($holiday['type'] == 'regular') ? 'Regular Holiday' : 'Special Holiday';
?>

                  <tr>
                    <td> <?= nice_date($holiday['date'], 'F d'); ?> </td>
                    <td> <?= $holiday['name']; ?> </td>
                    <td> <?= $type; ?> </td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="#" onclick="show_holiday_edit_form(<?= $holiday['id']?>)" data-toggle="modal">Edit</a>
                        <a class="btn btn-danger btn-sm" href="#" onclick="show_holiday_remove_form(<?= $holiday['id']?>)" data-toggle="modal">Remove</a>
                    </td>
                  </tr>

<?php
  }
?>

                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>

    </section>
    
<!--   </div> -->
</div>

</div>