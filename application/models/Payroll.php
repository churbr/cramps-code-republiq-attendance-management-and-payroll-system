<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Payroll extends CI_Model {

		public function __construct() {
			parent::__construct();		
			$this->load->helper('array');	
		}
		public function getQuickPayroll($user,$date){
			list($start, $end) = explode(';', $date);
			$total_deductions = 0;
			$total_adjustments = 0;
			$inclusive_days = [];
			$weekends = [];
			$regular_holidays_query = [];
			$special_holidays_query = [];
			$regular_hours = [];
			$employee = '';
			$employee_leaves = [];
			$adjustments = [];
			$overtime_hours = [];
			$deductions = [];
			$final['deductions'] = [];
			$final['adjustments'] = [];
			$final['total_deductions'] = '';
			$final['total_adjustments'] = '';	
			$final['weeks'] = '';		
			$final['timelogs'] = [];
			for ($d = date($start); $d <= $end; $d=date('Y-m-d',strtotime($d."+1 day"))) {
				$inclusive_days[] = $d;
				if(date('N', strtotime($d)) >= 6){
					$weekends[] = $d;
				}
			}
			
			//holidays
			$this->db->select('id, name, date, type');
			$this->db->from('holiday');
			$this->db->where('status',1);
			$query1 = $this->db->get();
			if($query1->num_rows()) {
				$holidays = $query1->result_array();
				foreach($holidays as $holiday){
					if($holiday['type']=='regular'){
						$regular_holidays_query[] = $holiday['date'];
					}
					else{
						$special_holidays_query[] = $holiday['date'];
					}
				}
			}
			
			//employees list
			$this->db->select('employee.id, employee.firstname, employee.lastname, employee.type, salary.rate');
			$this->db->from('employee');
			$this->db->join('salary','salary.employee_id = employee.id','left outer');
			$this->db->where('employee.id',$user);
			$query2 = $this->db->get();

			//timelogs
			$this->db->select('id,employee_id, timein_date,timeout_date,hour,minute,pay');
			$this->db->from('timelog');
			$this->db->where('timein_date >=',$start);
			$this->db->where('timeout_date <=',$end);
			$this->db->where('employee_id',$user);
			$query3 = $this->db->get();
			
			//overtimes
			$this->db->select('id, employee_id, start_date,end_date,hour,minute,pay');
			$this->db->from('overtime');
			$this->db->where('start_date >=',$start);
			$this->db->where('end_date <=',$end);
			$this->db->where('status','approved');
			$this->db->where('employee_id',$user);
			$query4 = $this->db->get();
			
			//deductions
			$this->db->select('employee_deduction.employee_id,employee_deduction.value,deduction.name');
			$this->db->from('employee_deduction');
			$this->db->join('deduction','deduction.id = employee_deduction.deduction_id','left outer');
			$this->db->where('employee_deduction.employee_id',$user);
			$query5 = $this->db->get();
			
			//leaves
			$this->db->select('id, employee_id, start_date,end_date');
			$this->db->from('leave');
			$this->db->where('start_date >=',$start);
			$this->db->where('end_date <=',$end);
			$this->db->where('status','approved');
			$this->db->where('employee_id',$user);
			$query6 = $this->db->get();
			
			//adjustments
			$this->db->select('id, employee_id, name,value, type');
			$this->db->from('custom_adjustments');
			$this->db->where('employee_id',$user);
			$query7 = $this->db->get();
			
			if($query3->num_rows()) {
				$regular_hours = $query3->result_array();
			}	
			if($query6->num_rows()) {
				$employee_leaves = $query6->result_array();
			}	
			if($query7->num_rows()) {
				$adjustments = $query7->result_array();
			}
			if($query4->num_rows()) {
				$overtime_hours = $query4->result_array();
			}	
			if($query5->num_rows()) {
				$deductions = $query5->result_array();
			}	
			
			if($query2->num_rows()) {
				$employee = $query2->result_array(0);				
				
				//Fixed deductions
				foreach($deductions as $workarea){					
					$total_deductions += $workarea['value'];	
					$final['deductions'][] = array('name'=>$workarea['name'],
												   'val'=>$workarea['value']
												  );				
				}					
				//Dynamic deductions/adjustments
				foreach($adjustments as $workarea){		
					//Textual and computational Adjustments/Deductions			
					if($workarea['type']=='sub'){							
						$total_deductions += $workarea['value'];
						$final['deductions'][] = array('name'=>$workarea['name'],
												   'val'=>$workarea['value']
												  );		
					}
					else{
						$total_adjustments += $workarea['value'];
						$final['adjustments'][] = array('name'=>$workarea['name'],
												   'val'=>$workarea['value']
												  );	
					}																												
				}	
				$final['total_deductions'] = $total_deductions;
				$final['total_adjustments'] = $total_adjustments;
				$v_required_hours = 0;
				$v_overtime_hours = 0;
				$v_salary_hours = 0;
				$v_weekly_salary_hours = 0;
				$v_regular_holiday_hours = 0;
				$v_leave_hours = 0;
				$v_weekend_hours = 0;
				$v_weekly_reg_hours = 0;
				$v_weekly_ot_hours = 0;
				$v_weekly_rh_hours = 0;
				$v_weekly_sh_hours = 0;
				$v_regular_hours = 0;
				$v_weekly_overtime_holiday_balance = 0;
				$v_special_holiday_hours = 0;
				$weekcount = 0;
				$daycount =0;
				$leave_dates = [];
				$absence = 0;
				$v_leave_hours = 0;
				//print_r($inclusive_days);
				//echo '<br/><br/>';
				//print_r($regular_hours);
				//echo '<br/><br/>';
				foreach($inclusive_days as $x_day){
					$daycount++;
					//echo 'daycount is '.$daycount.'<br/>';
					//Required hours are computed per week
					if(!in_array($x_day,$regular_holidays_query)&&!in_array($x_day,$special_holidays_query)&&!in_array($x_day,$weekends)){
						$v_required_hours += 8;
						
					}
					
					$v_regular_hours = '0';
					$v_regular_holiday_hours = 0;
					$v_overtime_hours = 0;
					
					$leave = '';
					$v_weekend_hours = 0;
					$v_special_holiday_hours = 0;
					if(in_array($x_day,$weekends)||in_array($x_day,$regular_holidays_query)||in_array($x_day,$special_holidays_query)){
						$v_regular_hours = '--';
						if(in_array($x_day,$regular_holidays_query)){
							$v_regular_holiday_hours = 'X';
							$v_special_holiday_hours = '--';
						}else if(in_array($x_day,$special_holidays_query)){
							$v_special_holiday_hours = 'X';
							$v_regular_holiday_hours = '--';
						}
						else if(in_array($x_day,$weekends)){
							$v_regular_holiday_hours = '--';
							$v_special_holiday_hours = '--';
						}
					}
					else{
						$v_regular_hours = 0;
						$v_regular_holiday_hours = '--';
						$v_special_holiday_hours = '--';
					}
					
					//Regular hours		
					$passed = false;							
					foreach($regular_hours as $workarea){
						
						if($workarea['timein_date']==$x_day){														
							$passed = true;
							
							if(in_array($x_day,$weekends)){
								$v_overtime_hours += $workarea['hour'];
								$v_weekly_ot_hours += $workarea['hour'];
								$v_weekly_salary_hours += $workarea['hour'];
							}
							else if(in_array($x_day,$regular_holidays_query)){								
								$v_weekly_overtime_holiday_balance += $workarea['hour'];
								$v_weekly_rh_hours += $workarea['hour'];
								//$v_weekly_salary_hours += $workarea['hour']; //on top of free 8 hours
								$v_weekly_salary_hours += 8; //free 8 hours
							}
							else if(in_array($x_day,$special_holidays_query)){								
								$v_weekly_overtime_holiday_balance += 0.3 * $workarea['hour'];
								$v_weekly_sh_hours += $workarea['hour'];
								//$v_weekly_salary_hours += 0.3 * $workarea['hour']; //on top of free 8 hours is 30% of actual hours worked
								$v_weekly_salary_hours += 8; //free 8 hours
							}
							else{								
								$v_regular_hours += $workarea['hour'];
								$v_weekly_reg_hours += $workarea['hour'];
								$v_weekly_salary_hours += $workarea['hour'];
							}
							//echo 'Weekly salary hours '.$v_weekly_salary_hours.'<br/>';
						}
						
					}
					//If for this particular working day, no timelog matches for the employee
					//but at the same time it is holiday, then we give salary hours for the employee without multiplier
					//as well as record the holiday hours
					if($passed==false){
						if(in_array($x_day,$regular_holidays_query)){
							$v_weekly_salary_hours += 8;
						}
						else if(in_array($x_day,$special_holidays_query)){
							$v_weekly_salary_hours += 8;
						}
						else if(in_array($x_day,$weekends)){
							//nothing
						}
						else{
							$absence += 8;
						}
					}
					
					//Overtimes
					foreach($overtime_hours as $workarea2){						
						if($workarea2['start_date']==$x_day){														
							if(in_array($x_day,$regular_holidays_query)){								
								$v_weekly_salary_hours += $workarea2['hour']; //full amount of worked hours on top of free 8 hours
								$v_overtime_hours += $workarea2['hour'];
								$v_weekly_ot_hours += $workarea2['hour'];
							}
							else if(in_array($x_day,$special_holidays_query)){								
								$v_weekly_salary_hours += 0.3 * $workarea2['hour']; //30% only since there is already free 8 hours given
								$v_overtime_hours += $workarea2['hour'];
								$v_weekly_ot_hours += $workarea2['hour'];
							}	
							else{
								$v_overtime_hours += $workarea2['hour'];
								$v_weekly_ot_hours += $workarea2['hour'];
								$v_weekly_salary_hours += $workarea2['hour'];
							}																									
						}
						
					}
					
					//Leaves
					foreach($employee_leaves as $workarea3){
						
						if($workarea3['start_date']<=$x_day&&$workarea3['end_date']>=$x_day){														
							if(!in_array($x_day,$special_holidays_query)&&!in_array($x_day,$regular_holidays_query)&&!in_array($x_day,$weekends)){	
								if(!in_array($x_day,$leave_dates)){	
									$leave_dates[] = $x_day;	
									$v_leave_hours += 8;
									$leave = 'X';
									$v_weekly_salary_hours += 8;	
									//echo 'emp '.$employee['id'].' had taken leave at '.$x_day.'<br/>';																										
								}
							}							
						}
						
					}
					//if current date in the loop is Sunday, then consider it as cutoff for the week
					//if(date('N', strtotime($x_day)) >= 7){
					//echo 'Salary hours daily '.$v_weekly_salary_hours.'<br/>';
					if($daycount>=7){						
						//echo 'Required hours for the week is '.$v_required_hours.'<br/>';											
						$regAndLeave = $v_weekly_reg_hours + $v_leave_hours;
						$deficit = $v_required_hours - $regAndLeave;
						//Deficit simply means the hours missing to complete the required hours per week
						//echo 'Reg Hours '.$v_weekly_reg_hours.' and Leave '.$v_leave_hours.' and absence '.$absence.'<br/>';
						//echo 'Reg and Leave '.$regAndLeave.' versus req '.$v_required_hours.'<br/>';
						//echo 'Weekly salary hours '.$v_weekly_salary_hours.'<br/>';
						if($regAndLeave<$v_required_hours){
							//echo 'Reg and Leave is less than required<br/>';
							if($deficit<=$v_weekly_ot_hours){
								//if deficit can be covered by the OT hours, then no change in Salary/Payable hours
								//Only that OT should instead be recognized as Regular hours
								$regAndLeave = $v_required_hours;
								$v_weekly_reg_hours = $regAndLeave - $v_leave_hours;			
								$v_weekly_ot_hours -= $deficit;	
								$deficit = 0;
							}
							else{
								//echo 'Deficit1<br/>';
								//echo '	Reg '.$v_weekly_reg_hours.' + OT '.$v_weekly_ot_hours.'<br/>';
								//Else, if Deficit is greater than the available OT balance,
								//then we add OT to Regular hours, make OT zero
								$regAndLeave += $v_weekly_ot_hours;								
								$v_weekly_reg_hours += $v_weekly_ot_hours;																						
								$v_weekly_ot_hours = 0;
								$deficit = $v_required_hours - $regAndLeave;
								//$v_weekly_salary_hours -= $deficit;	
							}						
						}
						else if($regAndLeave > $v_required_hours){
							//If total worked hours for the week exceeds required hours, then we consider absences
							//and deduct 8 hours per daily absence from payable hours
							if($absence>0){
								if($regAndLeave>$absence){
									$v_weekly_salary_hours -= $absence;
									$regAndLeave -= $absence;
									if($v_weekly_salary_hours<0){
										$v_weekly_salary_hours = 0;										
									}
								}
							}
							//Only when even after substracting hours due to absences that
							//Regular hours + leave is still greater than Required hours, then we round it down
							//to make it equal to Required hours and not in excess
							//The different between the two is then subtracted also to Salary/Payable hours
							if($regAndLeave > $v_required_hours){
								$v_weekly_salary_hours -= $regAndLeave - $v_required_hours;
								$regAndLeave = $v_required_hours;								
							}
							//$v_weekly_reg_hours = $regAndLeave - $v_leave_hours;
						}
						$v_weekly_reg_hours += $v_weekly_rh_hours + $v_weekly_sh_hours;
						//echo 'Weekly salary hours '.$v_weekly_salary_hours.'<br/>';
						$final['weekpay'][$weekcount] = $v_weekly_salary_hours * $employee[0]['rate'];
						$final['weekreg'][$weekcount] = $v_weekly_reg_hours;
						$final['weekot'][$weekcount] = $v_weekly_ot_hours;
						//echo 'Weekly salary: '.$v_weekly_salary_hours.' A<br/>';
						$v_required_hours = 0;		
						$v_weekly_salary_hours = 0;
						$v_weekly_reg_hours = 0;
						$v_weekly_rh_hours = 0;
						$v_weekly_sh_hours = 0;
						$v_leave_hours = 0;
						$v_weekly_ot_hours = 0;
						$absence = 0;
					}	
					$absent = '';
					//Record absence on a regular day
					if(!in_array($x_day,$leave_dates)&&!in_array($x_day,$special_holidays_query)&&!in_array($x_day,$regular_holidays_query)&&!in_array($x_day,$weekends)){
						if($v_regular_hours==0){
							$absent = 'X';
						} 
					}
					if($v_regular_hours>0&&$v_regular_hours<8&&$v_overtime_hours>0){
						$def = 8 - $v_regular_hours;
						if($v_overtime_hours>$def){
							//$v_regular_hours = 8;
							//$v_overtime_hours -= $def;
						}
						else{
							//$v_regular_hours += $v_overtime_hours;
							//$v_overtime_hours = 0;
						}		
					}
					$final['timelogs'][$weekcount][] = array(
						'day'=>$x_day,
						'regular'=>$v_regular_hours,
						'overtime'=>$v_overtime_hours,
						'leave'=>$leave,
						'rholiday'=>$v_regular_holiday_hours,
						'sholiday'=>$v_special_holiday_hours,
						'absence'=>$absent
					);
					if($daycount>=7&&$weekcount<1){
						$daycount = 0;
						$weekcount++;
					}
					$week_sholiday_hours = 0;	
					$week_rholiday_hours = 0;			
					$v_employee_rate = 0;					
					$v_regular_hours = 0;
					$v_special_holiday_hours = 0;
					$v_regular_holiday_hours = 0;
					$v_overtime_hours = 0;
					$v_leave_hours = 0;
					$v_salary_hours = 0;
				}	
				//This will be trigged if last day of the period is in the middle of the week
				//as it will not trigger the weekend logic earlier
				/*if($v_required_hours>0){																		
					$regAndLeave = $v_regular_hours + $v_leave_hours;
					if($regAndLeave<$v_required_hours){
						$deficit = $v_required_hours - $regAndLeave;
						if($deficit<=$v_overtime_hours){								
							$regAndLeave = $v_required_hours;
							$v_regular_hours = $regAndLeave - $v_leave_hours;
							$v_overtime_hours -= $deficit;	
							$deficit = 0;
						}
						else{
							$regAndLeave += $v_overtime_hours;
							$v_regular_hours += $v_overtime_hours;								
							$v_overtime_hours = 0;
							$deficit = $v_required_hours - $regAndLeave;						
						}						
					}
					else if($regAndLeave > $v_required_hours){
						$regAndLeave = $v_required_hours;
						$v_regular_hours = $regAndLeave - $v_leave_hours;
					}
					$final['weekpay'][$weekcount] = $v_weekly_salary_hours * $employee[0]['rate'];							
					//echo 'Weekly salary: '.$v_weekly_salary_hours.' B<br/>';
					$week_sholiday_hours = 0;	
					$week_rholiday_hours = 0;			
					$v_employee_rate = 0;
					$v_required_hours = 0;
					$v_regular_hours = 0;
					$v_special_holiday_hours = 0;
					$v_regular_holiday_hours = 0;
					$v_overtime_hours = 0;
					$v_leave_hours = 0;
					$v_salary_hours = 0;	
				}
				else if($v_weekly_salary_hours>0){
					$final['weekpay'][$weekcount] = $v_weekly_salary_hours * $employee[0]['rate'];
				}
				else{
					$final['weekpay'][$weekcount] =0;
				}*/			
			}
			$final['weeks'] = $weekcount;
			//print_r($final['weekpay']);
			return $final;
		}
		public function getPayrollOverview($date){
			list($start, $end) = explode(';', $date);
			$final = [];
			$regular_hours = [];
			$regular_hours_required = 0;
			$overtime_hours = [];
			$deductions = [];
			$special_holidays_query = [];
			$regular_holidays_query = [];
			$holidays = [];
			$inclusive_days = [];
			$weekends = [];
			$weekly_required_hours = [];
			$weekly_regular_hours = [];
			$employee_hours = [];
			$adjustments = [];
			$final_payroll_report = [];
			$weekly_overtime_hours = [];
			$employee_leaves = [];
			$gross = 0;
					
			for ($d = date($start); $d <= $end; $d=date('Y-m-d',strtotime($d."+1 day"))) {
				$inclusive_days[] = $d;
				if(date('N', strtotime($d)) >= 6){
					$weekends[] = $d;
				}
			}
			//employees list
			$this->db->select('employee.id, employee.firstname, employee.lastname, employee.type, salary.rate');
			$this->db->from('employee');
			$this->db->join('salary','salary.employee_id = employee.id','left outer');
			$query = $this->db->get();
			
			//holidays
			$this->db->select('id, name, date, type');
			$this->db->from('holiday');
			$this->db->where('status',1);
			$query2 = $this->db->get();
			
			
			//timelogs
			$this->db->select('id,employee_id, timein_date,timeout_date,hour,minute,pay');
			$this->db->from('timelog');
			$this->db->where('timein_date >=',$start);
			$this->db->where('timeout_date <=',$end);
			$query3 = $this->db->get();
			
			//overtimes
			$this->db->select('id, employee_id, start_date,end_date,hour,minute,pay');
			$this->db->from('overtime');
			$this->db->where('start_date >=',$start);
			$this->db->where('end_date <=',$end);
			$this->db->where('status','approved');
			$query4 = $this->db->get();
			
			//deductions
			$this->db->select('employee_deduction.employee_id,employee_deduction.value,deduction.name');
			$this->db->from('employee_deduction');
			$this->db->join('deduction','deduction.id = employee_deduction.deduction_id','left outer');
			$query5 = $this->db->get();
			
			//leaves
			$this->db->select('id, employee_id, start_date,end_date');
			$this->db->from('leave');
			$this->db->where('start_date >=',$start);
			$this->db->where('end_date <=',$end);
			$this->db->where('status','approved');
			$query6 = $this->db->get();
			
			//adjustments
			$this->db->select('id, employee_id, name,value, type');
			$this->db->from('custom_adjustments');
			$query7 = $this->db->get();
			
			if($query2->num_rows()) {
				$holidays = $query2->result_array();
				foreach($holidays as $holiday){
					if($holiday['type']=='regular'){
						$regular_holidays_query[] = $holiday['date'];
					}
					else{
						$special_holidays_query[] = $holiday['date'];
					}
				}
			}
			
			if($query3->num_rows()) {
				$regular_hours = $query3->result_array();
			}	
			if($query6->num_rows()) {
				$employee_leaves = $query6->result_array();
			}	
			if($query7->num_rows()) {
				$adjustments = $query7->result_array();
			}
			if($query4->num_rows()) {
				$overtime_hours = $query4->result_array();
			}	
			if($query5->num_rows()) {
				$deductions = $query5->result_array();
			}	
			if($query->num_rows()) {
				$employees = $query->result_array();
				
				//record weekly required hours
				foreach($inclusive_days as $inclusive){					
					if(!in_array($inclusive,$weekends)&&!in_array($inclusive,$regular_holidays_query)&&!in_array($inclusive,$special_holidays_query)){
						$regular_hours_required += 8;
					}
					if(in_array($inclusive,$weekends)&&$regular_hours_required>0){
						$weekly_required_hours[] = $regular_hours_required;						
						$regular_hours_required = 0;
					}
				}		
				if($regular_hours_required>0){
					$weekly_required_hours[] = $regular_hours_required;					
					$regular_hours_required = 0;
				}
				
				foreach($employees as $employee){
					$overtime_days = [];
					$regular_days = [];
					$leave_dates = [];
					$total_regular_hours = 0;
					$total_regular_minutes = 0;
					$total_regular_pay = 0;
					$regular_rate = 0;
					$total_overtime_hours = 0;
					$total_overtime_minutes = 0;
					$total_overtime_pay = 0;
					$overtime_rate = 0;
					$gross = 0;
					$total_deductions = 0;
					$deduction_texts = '';
					$regular_texts='';	
					$ot_texts='';	
					$rholiday_texts='';
					$sholiday_texts='';
					$leave_texts='';
					$absence_texts='';
					$week_sholiday_hours = 0;	
					$week_rholiday_hours = 0;
					$netpay = 0;
					$weekcount = 0;
					//Fixed deductions
					foreach($deductions as $workarea){
						if($employee['id']==$workarea['employee_id']){
							$total_deductions -= $workarea['value'];	
							if($deduction_texts==''){
								$deduction_texts = $workarea['name'].' = '.'₱'.$workarea['value'];
							}
							else{
								$deduction_texts = $deduction_texts.'<br/>'.$workarea['name'].' = '.'₱'.$workarea['value'];
							}		
						}
					}					
					//Dynamic deductions
					$dyn_executed = false;
					$dyn_text='';
					foreach($adjustments as $workarea){
						if($employee['id']==$workarea['employee_id']){
							if($workarea['type']=='sub'){
								$dyn_executed = true;
								$total_deductions -= $workarea['value'];
								if($dyn_text==''){
									$dyn_text = $workarea['name'].' = '.'₱'.$workarea['value'];
								}
								else{
									$dyn_text = $dyn_text.'<br/>'.$workarea['name'].' = '.'₱'.$workarea['value'];
								}
							}																								
						}
					}
					if($dyn_executed){
						$deduction_texts = $deduction_texts.'<br/><br/>Other Deductions:<br/>'.$dyn_text;
					}					
					//Dynamic adjustments
					$dyn_executed = false;
					$dyn_text='';
					foreach($adjustments as $workarea){
						if($employee['id']==$workarea['employee_id']){
							if($workarea['type']=='add'){
								$dyn_executed = true;
								$total_deductions += $workarea['value'];
								if($dyn_text==''){
									$dyn_text = $workarea['name'].' = '.'₱'.$workarea['value'];
								}
								else{
									$dyn_text = $dyn_text.'<br/>'.$workarea['name'].' = '.'₱'.$workarea['value'];
								}
							}		
						}
					}
					if($dyn_executed){
						$deduction_texts = $deduction_texts.'<br/><br/>Other Adjustments:<br/>'.$dyn_text;
					}
					$v_employee_rate = 0;
					$v_required_hours = 0;
					$v_regular_hours = 0;
					$v_special_holiday_hours = 0;
					$v_regular_holiday_hours = 0;
					$v_overtime_hours = 0;
					$v_leave_hours = 0;
					$v_salary_hours = 0;	
					$week_sholiday_hours = 0;	
					$week_rholiday_hours = 0;
					$daycount =0;
					$absence = 0;
					foreach($inclusive_days as $x_day){
						$daycount++;
						$leave='';
						if(!in_array($x_day,$regular_holidays_query)&&!in_array($x_day,$special_holidays_query)&&!in_array($x_day,$weekends)){
							$v_required_hours += 8;
							
						}	
						//Regular hours		
						$passed = false;							
						foreach($regular_hours as $workarea){
							
							if($workarea['timein_date']==$x_day){
								
								if($employee['id']==$workarea['employee_id']){
									$passed = true;
									
									//echo $employee['id'].' = '.$workarea['employee_id'].'<br/>';
									if(in_array($x_day,$weekends)){
										$v_overtime_hours += $workarea['hour'];
										$v_salary_hours += $workarea['hour'];
									}
									else if(in_array($x_day,$regular_holidays_query)){
										$v_regular_holiday_hours += $workarea['hour'];
										//$v_regular_hours += $workarea['hour'];
										//$v_salary_hours += $workarea['hour']; //on top of the free 8 hours
										$v_salary_hours += 8;//free 8 hours
									}
									else if(in_array($x_day,$special_holidays_query)){
										$v_special_holiday_hours += $workarea['hour'];
										//$v_regular_hours += $workarea['hour'];
										//$v_salary_hours += 0.3 * $workarea['hour']; //30% as on top of free 8 hours
										$v_salary_hours += 8;//free 8 hours
									}
									else{
										$v_regular_hours += $workarea['hour'];
										$v_salary_hours += $workarea['hour'];
									}																		
								}
							}
							
						}
						//If for this particular working day, no timelog matches for the employee
						//but at the same time it is holiday, then we give salary hours for the employee without multiplier
						//as well as record the holiday hours
						if($passed==false){
							if(in_array($x_day,$regular_holidays_query)){
								$v_salary_hours += 8;
							}
							else if(in_array($x_day,$special_holidays_query)){
								$v_salary_hours += 8;
							}
							else if(in_array($x_day,$weekends)){
								//nothing
							}
							else{
								$absence += 8;
							}
						}
						
						//Overtimes
						foreach($overtime_hours as $workarea2){
							
							if($workarea2['start_date']==$x_day){
								
								if($employee['id']==$workarea2['employee_id']){
									if(in_array($x_day,$regular_holidays_query)){
										//$v_regular_holiday_hours += $workarea2['hour'];
										$v_overtime_hours += $workarea2['hour'];
										$v_salary_hours += $workarea2['hour']; 
									}
									else if(in_array($x_day,$special_holidays_query)){
										//$v_special_holiday_hours += $workarea2['hour'];
										$v_overtime_hours += $workarea2['hour'];
										$v_salary_hours += 0.3 * $workarea2['hour'];
									}
									else{
										$v_overtime_hours += $workarea2['hour'];
										$v_salary_hours += $workarea2['hour'];
									}																		
								}
							}
							
						}
						
						//Leaves
						foreach($employee_leaves as $workarea3){
							
							if($workarea3['start_date']<=$x_day&&$workarea3['end_date']>=$x_day){
								
								if($employee['id']==$workarea3['employee_id']){		
									if(!in_array($x_day,$special_holidays_query)&&!in_array($x_day,$regular_holidays_query)&&!in_array($x_day,$weekends)){	
										if(!in_array($x_day,$leave_dates)){	
											$leave_dates[] = $x_day;	
											$v_leave_hours += 8;
											$leave = 'X';
											$v_salary_hours += 8;	
											//echo 'emp '.$employee['id'].' had taken leave at '.$x_day.'<br/>';																										
										}
									}
								}
							}
							
						}
						//echo 'Emp '.$employee['firstname'].' has reg hours '.$v_regular_hours.'<br/>';
						//If current date is Sunday
						//if(date('N', strtotime($x_day)) >= 7){
						if($daycount>=7){
							//echo 'Required hours for the week is '.$v_required_hours.'<br/>';	
							//echo 'salary hours of '.$employee['id'].' is '.$v_salary_hours.' with rate of '.$employee['rate'].' = '.$employee['rate']*$v_salary_hours.' with leave hr '.$v_leave_hours.'<br/>';
							$regAndLeave = $v_regular_hours + $v_leave_hours;
							$deficit = $v_required_hours - $regAndLeave;
							if($regAndLeave<$v_required_hours){								
								if($deficit<=$v_overtime_hours){
									if($deficit>0){
										//$regular_texts = $regular_texts.'+'.$deficit.' from Overtime<br/>';
										//$ot_texts = $ot_texts.$deficit.' hours transferred to Regular<br/>';
									}
									$regAndLeave = $v_required_hours;
									$v_regular_hours = $regAndLeave - $v_leave_hours;
									$v_overtime_hours -= $deficit;	
									$deficit = 0;
								}
								else{
									$regAndLeave += $v_overtime_hours;
									$v_regular_hours += $v_overtime_hours;
									if($v_overtime_hours>0){
										//$regular_texts = $regular_texts.'+'.$v_overtime_hours.' from Overtime<br/>';
										//$ot_texts = $ot_texts.$v_overtime_hours.' hours transferred to Regular<br/>';
									}
									$v_overtime_hours = 0;
									$deficit = $v_required_hours - $regAndLeave;
								}
								
							}
							
							else if($regAndLeave > $v_required_hours){
								//echo 'Reg and Leave '.($v_required_hours).'<br/>';
								if($absence>0){
									if($regAndLeave>$absence){
										$v_salary_hours -= $absence;
										$regAndLeave -= $absence;
										if($v_salary_hours<0){
											$v_salary_hours = 0;
										}
									}
								}
								if($regAndLeave>$v_required_hours){
									$v_salary_hours -= $regAndLeave - $v_required_hours;
									$regAndLeave = $v_required_hours;									
								}
								//$v_regular_hours = $regAndLeave - $v_leave_hours;
					
							}
							$v_regular_hours += $v_special_holiday_hours + $v_regular_holiday_hours;
							if($regular_texts==''){
								$regular_texts = 'Week '.($weekcount+1).'<br/>Required Hours = '.$v_required_hours.'<br/>Regular hours = '.$v_regular_hours.'<br/>';
							}
							else{
								$regular_texts = $regular_texts.'---------------------<br/>Week '.($weekcount+1).'<br/>Required Hours = '.$v_required_hours.'<br/>Regular hours = '.$v_regular_hours.'<br/>';
							}
							if($sholiday_texts==''){
								$sholiday_texts = 'Week '.($weekcount+1).'<br/>Holiday hours = '.$v_special_holiday_hours.'<br/>';
							}
							else{
								$sholiday_texts = $sholiday_texts.'---------------------<br/>Week '.($weekcount+1).'<br/>Holiday hours = '.$v_special_holiday_hours.'<br/>';
							}
							if($rholiday_texts==''){
								$rholiday_texts = 'Week '.($weekcount+1).'<br/>Holiday hours = '.$v_regular_holiday_hours.'<br/>';
							}
							else{
								$rholiday_texts = $rholiday_texts.'---------------------<br/>Week '.($weekcount+1).'<br/>Holiday hours = '.$v_regular_holiday_hours.'<br/>';
							}
							if($ot_texts==''){
								$ot_texts = 'Week '.($weekcount+1).'<br/>Overtime hours = '.$v_overtime_hours.'<br/>';
							}
							else{
								$ot_texts = $ot_texts.'---------------------<br/>Week '.($weekcount+1).'<br/>Overtime hours = '.$v_overtime_hours.'<br/>';
							}
							if($leave_texts==''){
								$leave_texts = 'Week '.($weekcount+1).'<br/>Leave days = '.($v_leave_hours/8).'<br/>';
							}
							else{
								$leave_texts = $leave_texts.'---------------------<br/>Week '.($weekcount+1).'<br/>Leave days = '.($v_leave_hours/8).'<br/>';
							}
							//echo 'Week '.$weekcount.' text is:<br/>'.$regular_texts.'<br/>';
							
							//echo 'Pay for salary hours '.$v_salary_hours.'<br/>';	
							$employee_hours[] = array(
								'emp_id' => $employee['id'],
								'firstname' => $employee['firstname'],
								'lastname'  => $employee['lastname'],
								'type'	=> $employee['type'],
								'weekend'=>$x_day,
								'required' => $v_required_hours,
								'regular' => $v_regular_hours,
								'regular_texts'=>$regular_texts,
								'special_holiday' => $v_special_holiday_hours,
								'sholiday_texts'=>$sholiday_texts,
								'regular_holiday' => $v_regular_holiday_hours,
								'rholiday_texts'=>$rholiday_texts,
								'overtime' => $v_overtime_hours,
								'ot_texts'=>$ot_texts,
								'leave' => $v_leave_hours,
								'leave_texts'=>$leave_texts,
								'deduction'=>$total_deductions,
								'deduction_texts'=>$deduction_texts,
								'absence'=>floor($absence),
								'absence_texts'=>$absence_texts,
								'rate' => $employee['rate'],
								'pay' => $employee['rate'] * $v_salary_hours
							);
							$weekcount++;
							$daycount = 0;
							$week_sholiday_hours = 0;	
							$week_rholiday_hours = 0;			
							$v_employee_rate = 0;
							$v_required_hours = 0;
							$v_regular_hours = 0;
							$v_special_holiday_hours = 0;
							$v_regular_holiday_hours = 0;
							$v_overtime_hours = 0;
							$absence = 0;
							$v_leave_hours = 0;
							$v_salary_hours = 0;	
						}	
					}	
					//This is triggered if last day of the period is not Sunday
					if($v_required_hours>0){
						$weekcount++;					
						if($regular_texts==''){
							$regular_texts = 'Week '.$weekcount.'<br/>Required Hours = '.$v_required_hours.'<br/>Regular hours = '.$v_regular_hours.'<br/>';
						}
						else{
							$regular_texts = $regular_texts.'---------------------<br/>Week '.$weekcount.'<br/>Required Hours = '.$v_required_hours.'<br/>Regular hours = '.$v_regular_hours.'<br/>';
						}
						if($sholiday_texts==''){
							$sholiday_texts = 'Week '.$weekcount.'<br/>Holiday hours = '.$v_special_holiday_hours.'<br/>';
						}
						else{
							$sholiday_texts = $sholiday_texts.'---------------------<br/>Week '.$weekcount.'<br/>Holiday hours = '.$v_special_holiday_hours.'<br/>';
						}
						if($rholiday_texts==''){
							$rholiday_texts = 'Week '.$weekcount.'<br/>Holiday hours = '.$v_regular_holiday_hours.'<br/>';
						}
						else{
							$rholiday_texts = $rholiday_texts.'---------------------<br/>Week '.$weekcount.'<br/>Holiday hours = '.$v_regular_holiday_hours.'<br/>';
						}
						if($ot_texts==''){
							$ot_texts = 'Week '.$weekcount.'<br/>Overtime hours = '.$v_overtime_hours.'<br/>';
						}
						else{
							$ot_texts = $ot_texts.'---------------------<br/>Week '.$weekcount.'<br/>Overtime hours = '.$v_overtime_hours.'<br/>';
						}
						if($leave_texts==''){
							$leave_texts = 'Week '.$weekcount.'<br/>Leave days = '.($v_leave_hours/8).'<br/>';
						}
						else{
							$leave_texts = $leave_texts.'---------------------<br/>Week '.$weekcount.'<br/>Leave days = '.($v_leave_hours/8).'<br/>';
						}
						$absence = 0;
						$regAndLeave = $v_regular_hours + $v_leave_hours;
						if($regAndLeave<$v_required_hours){
							$deficit = $v_required_hours - $regAndLeave;
							if($deficit<=$v_overtime_hours){
								if($deficit>0){
									$regular_texts = $regular_texts.'+'.$deficit.' from Overtime<br/>';
									$ot_texts = $ot_texts.$deficit.' hours transferred to Regular<br/>';
								}
								$regAndLeave = $v_required_hours;
								$v_regular_hours = $regAndLeave - $v_leave_hours;
								$v_overtime_hours -= $deficit;	
								$deficit = 0;
							}
							else{
								$regAndLeave += $v_overtime_hours;
								$v_regular_hours += $v_overtime_hours;
								if($v_overtime_hours>0){
									$regular_texts = $regular_texts.'+'.$v_overtime_hours.' from Overtime<br/>';
									$ot_texts = $ot_texts.$v_overtime_hours.' hours transferred to Regular<br/>';
								}
								$v_overtime_hours = 0;
								$deficit = $v_required_hours - $regAndLeave;
							}
							$absence = $deficit/8;
						}
						else if($regAndLeave > $v_required_hours){
							$regAndLeave = $v_required_hours;
							$v_regular_hours = $regAndLeave - $v_leave_hours;
						}
													
						$employee_hours[] = array(
							'emp_id' => $employee['id'],
							'firstname' => $employee['firstname'],
							'lastname'  => $employee['lastname'],
							'type'	=> $employee['type'],
							'weekend'=>$x_day,
							'required' => $v_required_hours,
							'regular' => $v_regular_hours,
							'regular_texts'=>$regular_texts,
							'special_holiday' => $v_special_holiday_hours,
							'sholiday_texts'=>$sholiday_texts,
							'regular_holiday' => $v_regular_holiday_hours,
							'rholiday_texts'=>$rholiday_texts,
							'overtime' => $v_overtime_hours,
							'ot_texts'=>$ot_texts,
							'leave' => $v_leave_hours,
							'leave_texts'=>$leave_texts,
							'deduction'=>$total_deductions,
							'deduction_texts'=>$deduction_texts,
							'absence'=>floor($absence),
							'absence_texts'=>$absence_texts,
							'rate' => $employee['rate'],
							'pay' => $employee['rate'] * $v_salary_hours
						);
						$week_sholiday_hours = 0;	
						$week_rholiday_hours = 0;			
						$v_employee_rate = 0;
						$v_required_hours = 0;
						$v_regular_hours = 0;
						$v_special_holiday_hours = 0;
						$v_regular_holiday_hours = 0;
						$v_overtime_hours = 0;
						$v_leave_hours = 0;
						$v_salary_hours = 0;	
					}									
				}
			}
			$total_regular_pay = 0;		
			$prevempid = 0;	
			$sum_regular = 0;	
			$sum_r_holiday = 0;
			$sum_s_holiday = 0;	
			$sum_required = 0;
			$ind = 0;
			
			//Summarize all results into one row only per Employee since currently, the records are split into each week. We need to combine them to show in the report
			foreach($employee_hours as $emp_hour){				
				$total_regular_pay = 0;
				$total_regular_pay += $emp_hour['pay'];
				$gross = round($total_regular_pay,2);
				if($gross<0){
//						$gross = 0;
				}
				
				$sum_regular +=	$emp_hour['regular'];		
				$sum_r_holiday += $emp_hour['regular_holiday'];
				$sum_s_holiday += $emp_hour['special_holiday'];
				$sum_required += $emp_hour['required'];
				$search_key = false;
				$search_key = array_search($emp_hour['emp_id'],array_column($final,'id'));
				//echo 'Employee '.$emp_hour['emp_id'].' has regular '.$sum_regular.' and overtime of '.$emp_hour['overtime'].' and deduction of '.$total_deductions.' and gross of '.$gross.'<br/>';
				if($search_key!==false){				
					$final[$search_key]['regular'] = $final[$search_key]['regular'] + $sum_regular;
					$final[$search_key]['regular_texts'] = $emp_hour['regular_texts'];
					$final[$search_key]['rholiday_texts'] = $emp_hour['rholiday_texts'];
					$final[$search_key]['sholiday_texts'] = $emp_hour['sholiday_texts'];
					$final[$search_key]['ot_texts'] = $emp_hour['ot_texts'];
					$final[$search_key]['leave_texts'] = $emp_hour['leave_texts'];
					$final[$search_key]['absence_texts'] = $emp_hour['absence_texts'];
					$final[$search_key]['overtime'] = $final[$search_key]['overtime'] + $emp_hour['overtime'];
					$final[$search_key]['leave'] = $final[$search_key]['leave'] + $emp_hour['leave'];
					$final[$search_key]['absence'] = $final[$search_key]['absence'] + $emp_hour['absence'];
					$final[$search_key]['special_holiday'] = $final[$search_key]['special_holiday'] + $sum_s_holiday;
					$final[$search_key]['regular_holiday'] = $final[$search_key]['regular_holiday'] + $sum_r_holiday;
					$final[$search_key]['required'] = $final[$search_key]['required'] + $sum_required;
					$final[$search_key]['gross'] = $final[$search_key]['gross'] + $gross;
					$final[$search_key]['deductions'] = $emp_hour['deduction'];
					$final[$search_key]['netpay'] = $final[$search_key]['gross'] + $emp_hour['deduction'];
				}
				else{					
					$runningnetpay = $gross + $total_deductions;
					$final[] = array(
						'id' => $emp_hour['emp_id'],
						'firstname' => $emp_hour['firstname'],
						'lastname'  => $emp_hour['lastname'],
						'type'	=> $emp_hour['type'],
						'regular' => $sum_regular,
						'regular_texts'=>$emp_hour['regular_texts'],
						'overtime' => $emp_hour['overtime'],
						'ot_texts'=>$emp_hour['ot_texts'],
						'leave' => $emp_hour['leave'],
						'leave_texts'=>$emp_hour['leave_texts'],
						'absence'=>$emp_hour['absence'],
						'absence_texts'=>$emp_hour['absence_texts'],
						'special_holiday'=>$sum_s_holiday,
						'sholiday_texts'=>$emp_hour['sholiday_texts'],
						'regular_holiday'=>$sum_r_holiday,
						'rholiday_texts'=>$emp_hour['rholiday_texts'],
						'required'=>$sum_required,
						'gross' => $gross,
						'deduction_texts'=>$emp_hour['deduction_texts'],
						'deductions' => $emp_hour['deduction'],
						'netpay' => $runningnetpay
					);
				}
				
					//echo $emp_hour['emp_id']."<br/>";
				
				$sum_regular = 0;
				$sum_r_holiday = 0;
				$sum_s_holiday = 0;
				$sum_required = 0;
				
				$prevempid = $emp_hour['emp_id'];
				$ind++;
			}
			
			foreach($final as $emp_hour){
				//echo print_r($emp_hour);
				//echo '<br/>';
			}	
			
			return $final;
		}
	}