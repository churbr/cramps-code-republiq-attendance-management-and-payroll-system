<?php
	if(!empty($success)) {
?>

	<div class="modal modal-success in" id="success-prompt-modal" tabindex="-1" aria-hidden="false" style="display: block;">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body text-centered">
					<h1><i class="fa fa-fw fa-smile-o"></i></h1>
					<h5>Updated Successfully</h5>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		setTimeout(function() {
			window.location.replace(location.href);
		}, 2000);
	</script>

<?php
	}
?>

	<div class="content-wrapper login-page-background-cloud" style="background: #ecf0f1 !important;">
		<div class="container">
			<section class="content-header">
			</section>

			<section class="content">


				<div class="row">
					<div class="col-md-3">
						<div class="box box-primary">
							<div class="box-body box-profile">
								<img class="profile-user-img img-responsive img-circle" src="<?= base_url('resources/dist/img/users/' . $picture); ?>" alt="User profile picture" style="width: 128px; height: 128px;" />
								<h3 class="profile-username text-center"> <?= $this->session->fullname ?> </h3>
								<p class="text-muted text-center"> <?= $employee_position; ?> </p>

								<ul class="list-group list-group-unbordered">
									<li class="list-group-item">
										<b>Employment Date</b>
										<a class="pull-right">
											<?= nice_date($date_hired, 'M d, Y'); ?>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="col-md-9">
						<div class="nav-tabs-custom">

							<ul class="nav nav-tabs">
								<li <?php echo (strcmp($current_tab, 'about_me') === 0) ? "class='active'" : ''; ?> ><a href="#activity" data-toggle="tab">About Me</a></li>
								<li <?php echo (strcmp($current_tab, 'update_info') === 0) ? "class='active'" : ''; ?> > <a href="#update" data-toggle="tab">Update</a></li>
								<li <?php echo (strcmp($current_tab, 'change_picture') === 0) ? "class='active'" : ''; ?> ><a href="#update-profile-picture" data-toggle="tab">Profile Picture</a></li>
								<li <?php echo (strcmp($current_tab, 'change_password') === 0) ? "class='active'" : ''; ?> ><a href="#change-password" data-toggle="tab">Change Password</a></li>
							</ul>

							<div class="tab-content">
								<div <?php echo (strcmp($current_tab, 'about_me') === 0) ? "class='active tab-pane'" : "class='tab-pane'"; ?> id="activity">
									<div class="">
										<div class="box-body">
											<h4>
												<strong>
													<i class="fa fa-user margin-r-5"></i>Personal Information
												</strong>
											</h4>

											<h4> <?= $this->session->fullname; ?> </h4>
											<p class="text-muted">
												<?php
													$type = 'N/A';

													switch ($this->session->userdata('type')) {
														case 'super_admin':
															$type = 'Super Admin';
														break;

														case 'admin':
															$type = 'Admin';
														break;

														case 'regular_employee':
															$type = 'Employee';
														break;

														case 'project_manager':
															$type = 'Project Manager';
														break;

														case 'board_of_director':
															$type = 'Board Of Director';
														break;
													}

													echo $type;
												?>
											</p>

											<h4> <?= nice_date($basic_info['birthday'], 'M d, Y'); ?> </h4>
											<p class="text-muted">Birthdate</p>

											<h4> <?= ucfirst($basic_info['gender']); ?> </h4>
											<p class="text-muted">Gender</p>

											<hr />

											<strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>
											<p class="text-muted"><?= $address; ?></p>

											<hr />

											<strong><i class="fa  fa-phone margin-r-5"></i> Contact</strong>
											<p class="text-muted">Contact Number: <?= substr($contact, 0, 6) . '****'; ?> </p>
											<p class="text-muted">Email: <?= $email; ?> </p>

											<hr />

											<strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
											<p class="text-muted">
												<?php
													if($employee_skills !== false) { echo $employee_skills; }
													else { echo 'No skills set yet.'; }
												?>
											</p>
										</div>
									</div>
								</div>

								<div <?php echo (strcmp($current_tab, 'update_info') === 0) ? "class='active tab-pane'" : "class='tab-pane'"; ?> id="update">

									<?= form_open('user/update-info', array('class' => 'form-horizontal', 'id' => 'update_info_form')); ?>
										<div class="form-group">
											<label class="col-sm-2 control-label">Address</label>
											<div class="col-sm-6">
												<input type="text" id='update_info_address' name='address' class="form-control" value="<?= $address; ?>" />
											</div>
											<div class='error' id='update_info_address_msg'></div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Contact Number</label>
											<div class="col-sm-6">
												<input type="text" id='update_info_contact' name='contact' class="form-control" value="<?= $contact; ?>" />
											</div>
											<div class='error' id='update_info_contact_msg'></div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Email</label>
											<div class="col-sm-6">
												<input type="text" id='update_info_email' name='email' class="form-control" value="<?= $email; ?>" />
											</div>
											<div class='error' id='update_info_email_msg'></div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label"></label>
											<div class="col-sm-6" id='update_info_msg'>
											</div>
										</div>

										<hr />

										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" id='update_info_button' class="btn btn-primary">Save</button>
											</div>
										</div>
									</form>
								</div>


								<div <?php echo (strcmp($current_tab, 'change_password') === 0) ? "class='active tab-pane'" : "class='tab-pane'"; ?> id="change-password">

										<?= form_open('user/change-password', array('class' => 'form-horizontal', 'id' => 'change_password')); ?>

										<div class="form-group">
											<label class="col-sm-2 control-label">Old Password</label>
											<div class="col-sm-6">
												<input type="password" name='oldpass' id='oldpass_field' class="form-control" onkeyup="checkpass(oldpass_field)" />
											</div>
											<div id='oldpass_info'></div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label">New Password</label>
											<div class="col-sm-6">
												<input type="password" name='newpass' id='newpass_field' class="form-control" onkeyup="checkpass(newpass_field)" />
											</div>
											<div id='newpass_info'></div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Confirm New Password</label>
											<div class="col-sm-6">
												<input type="password" name='confirmpass' id='confirmpass_field' class="form-control" onkeyup="checkpass(confirmpass_field)" />
											</div>
											<div id='repass_info'></div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label"></label>
											<div class="col-sm-6">
												<div id='changepass_info'></div>
											</div>
										</div>

										<hr />

										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" id='changepass_button' class="btn btn-primary">Save</button>
												<input type='hidden' value='<?= base_url();?>' id='base_url' />
											</div>
										</div>
									</form>
								</div>



								<div <?php echo (strcmp($current_tab, 'change_picture') === 0) ? "class='active tab-pane'" : "class='tab-pane'"; ?> id="update-profile-picture">
									<?php echo form_open_multipart('upload/index', array('class' => 'form-horizontal')); ?>
										<div class="form-group">
											<label class="col-sm-2 control-label">Update Profile Picture</label>
											<div class="col-sm-6">
												<input type="file" name='picture' />
												<input type='hidden' name='upload_picture' value='true' />
											</div>
										</div>

										<?php
											if(!empty($upload_error)) {

										echo <<<UPLOAD_ERROR

											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
													<p class='error'>$upload_error</p>
												</div>
											</div>
UPLOAD_ERROR;
											}
										?>

										<hr />

										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" class="btn btn-primary">Save</button>
											</div>
										</div>

									</form>
								</div>

							</div>
						</div>
					</div>
				</div>

			</section>
		</div>
	</div>

<script src="<?= base_url('resources/dist/js/password_checker.js'); ?>"></script>