<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->library(array('e_user', 'e_authentication', 'e_security'));
			$this->load->model(array('holiday2','reports','payroll','adminm','admin_db', 'employee', 'salary', 'timelog', 'holiday'));
			$this->load->helper(array('e_time', 'cookie'));
		}

		// --------------------------------------------------------------------------

		public function home($param='') {			
			$this->e_security->only_allow('admin');
			$data = $this->employee->get_overall_info($this->session->id);
			$modal_functions = $this->input->post('modal_emp_function');
			$modal_emp_id = $this->input->post('modal_emp_id');
			if($modal_functions=='deactivate'){
				$modal_update['status'] = 0;
				$reason = $this->input->post('deactivate_reason');
				if($this->adminm->admin_update_info($modal_emp_id,$modal_update)){
					$param = 6;
					$this->adminm->deactivate_reason($modal_emp_id,$reason,0);
				}
				else{
					$param = 7;
				}
			}
			elseif($modal_functions=='activate'){
				$modal_update['status'] = 1;
				if($this->adminm->admin_update_info($modal_emp_id,$modal_update)){
					$param = 5;
					$this->adminm->deactivate_reason($modal_emp_id,'',1);
				}
				else{
					$param = 7;
				}
			}
			$data['employee_list'] = $this->admin_db->get_all_employees();
			$data['employee_position'] = $this->employee->get_position();
			$data['page_title'] = 'Home';
			$data['param'] = $param;
			$data['all_skills'] = $this->adminm->get_all_skills();			
			$data['all_pm'] = $this->adminm->get_all_pm();
			$this->load->view('admin/header', $data);
			$this->load->view('admin/modal',$data);
			$this->load->view('admin/nav');
			$this->load->view('admin/employee_management');
			$this->load->view('admin/footer');
		}

		// --------------------------------------------------------------------------

		public function change_password() {
			$this->e_security->only_allow('admin');
			$data = $this->employee->get_overall_info($this->session->id);
		    $data['param'] = '';
			$this->load->view('admin/header', $data);
			$this->load->view('admin/modal',$data);
			$this->load->view('admin/nav');
			$this->load->view('admin/change_password');
			$this->load->view('admin/footer');
		}

		// --------------------------------------------------------------------------

		public function employee_profile($employee_id = null) {
			
			$this->e_security->only_allow('admin');
			$data = $this->employee->get_overall_info( $this->session->id );
			$data['employee_position'] = $this->employee->get_position();
			$data['employee_skills'] = $this->employee->skills( $employee_id );
			$data['employee_skill_id'] = $this->admin_db->get_employee_skill_id( $employee_id );
			$data['skill_list'] = $this->admin_db->get_skill();
			$data['position_list'] = $this->admin_db->get_position();
			$data['project_manager_list'] = $this->admin_db->pm_list();
			$data['assigned_to_pm_id'] = $this->admin_db->assigned_to_pm_id($employee_id);
			$data['deactivation_detail'] = $this->admin_db->deactivation_detail( $employee_id );
			$data['employee_detail'] = $this->admin_db->get_employee_info( $employee_id );
			$data['get_position_id'] = $this->admin_db->get_position_id( $employee_id );
			$data['employee_leave_detail'] = $this->admin_db->get_leave_detail( $employee_id );
			$data['param'] = '';
			$this->load->view('admin/header', $data);
			$this->load->view('admin/modal',$data);
			$this->load->view('admin/nav');
			$this->load->view('admin/employee_profile');
			$this->load->view('admin/footer');
		}

		// --------------------------------------------------------------------------
//Start of Change BORJA 3_13_2016
		public function employee_timelog($user='',$param='') {
			$this->e_security->only_allow('admin');
			if($user<>''){
				$data = $this->employee->get_overall_info($this->session->id);
				$data['timelogs'] = [];
				$data['overtimes'] = [];
				if($this->input->post('emp_id')&&$this->input->post('timelog_from')&&$this->input->post('timelog_to'))
				{
					$data['timelogs'] = $this->reports->getTimelogs($this->input->post('emp_id'),$this->input->post('timelog_from'),$this->input->post('timelog_to'));
					$data['overtimes'] = $this->reports->getOvertimes($this->input->post('emp_id'),$this->input->post('timelog_from'),$this->input->post('timelog_to'));
				}
				$data['basic'] = '';
				$data['param'] = $param;
				$data['emp_id'] = $user;
				$data['basic'] = $this->employee->get_basic_info($user);
				$data['all_holidays'] = $this->holiday->view();
				$data['all_skills'] = $this->adminm->get_all_skills();
				$data['all_pm'] = $this->adminm->get_all_pm();
				$this->load->view('admin/header', $data);
				$this->load->view('admin/modal',$data);
				$this->load->view('admin/nav');
				$this->load->view('admin/employee_timelog',$data);
				$this->load->view('admin/footer');
			}else{
				self::home();
			}
			
		}

		// --------------------------------------------------------------------------

		public function edit_holiday(){		
			if($this->input->post('holiday_id_edit')&&$this->input->post('holiday_date_edit')&&$this->input->post('holiday_type_edit')&&$this->input->post('holiday_name_edit')){
				echo $this->holiday2->editHoliday($this->input->post('holiday_id_edit'),$this->input->post('holiday_name_edit'),$this->input->post('holiday_type_edit'),$this->input->post('holiday_date_edit'));				 
			}
			
		}	
		public function add_holiday(){
			if($this->input->post('holiday_date')&&$this->input->post('holiday_type')&&$this->input->post('holiday_name')){
				echo $this->holiday2->addHoliday($this->input->post('holiday_name'),$this->input->post('holiday_type'),$this->input->post('holiday_date'));
			}
		}
		public function add_employee(){
			$feedback = '';
			$firstname = $this->input->post('firstname');
			$lastname = $this->input->post('lastname');
			$middlename = ' ';
			$birthdate = $this->input->post('birthdate');
			$gender = $this->input->post('gender');
			$address = $this->input->post('address');
			$contact = $this->input->post('contact');
			$email = $this->input->post('email');
			$position = $this->input->post('position');
			$type = $this->input->post('type');
			$startdate = $this->input->post('startdate');
			$projectmanager = $this->input->post('projectmanager');
			$rate = $this->input->post('rate');
			if($firstname&&$lastname&&$gender){
				$no_problem = TRUE;

				if(!$this->e_security->is_validAddress($address)) {
					//$no_problem = FALSE;
				}
	
				if(!$this->e_security->valid_phoneNumber($contact)) {
					//$no_problem = FALSE;
				}
	
				if(!$this->e_security->is_validEmail($email)) {
					//$no_problem = FALSE;
				}
				if($no_problem){
					$data['firstname'] = $firstname;
					$data['lastname'] = $lastname;
					$data['middlename'] = $middlename;
					$data['birthdate'] = $birthdate;
					$data['gender'] = $gender;
					$data['address'] = $address;
					$data['contact'] = $contact;
					$data['email'] = $email;
					$data['position'] = $position;
					$data['type'] = $type;
					$data['startdate'] = $startdate;
					$data['projectmanager'] = $projectmanager;
					$data['rate'] = $rate;
					$feedback = $this->adminm->add_employee($data);
				}
				else{
					$feedback = 'Required fields must be filled';
				}
			}
			else{
			}
			self::home($feedback);
		}
		public function payroll_for_employee($user='',$param='') {
			$this->e_security->only_allow('admin');
			$data = $this->employee->get_overall_info($this->session->id);
			
			if($this->input->post('id')&&$this->input->post('rate')&&$this->input->post('updatepayrollflag')){				
				$this->adminm->updateRate($this->input->post('id'),$this->input->post('rate'));
				$this->adminm->updateDeductions($this->input->post('id'),$this->input->post('philhealth'),$this->input->post('sss'),$this->input->post('pagibig'),$this->input->post('tax'));
				 $deduct_array = $this->input->post('deduct_name');
				 $deduct_array_values = $this->input->post('deduct_value');
				 $adjust_array = $this->input->post('adjust_name');
				 $adjust_array_values = $this->input->post('adjust_value');
				 $this->adminm->resetAdjustments($user);				 			 
				 for($i = 0; $i < count($deduct_array); $i++)
				 {
				 	if($deduct_array[$i]&&$deduct_array_values[$i]){
						$this->adminm->addAdjustment($user,$deduct_array[$i],$deduct_array_values[$i],'sub');	
					}
				 }
				 
				 for($i = 0; $i < count($adjust_array); $i++)
				 { 
				 	if($adjust_array[$i]&&$adjust_array_values[$i]){
						$this->adminm->addAdjustment($user,$adjust_array[$i],$adjust_array_values[$i],'add');	
					}
				 }
			}
			$data['quickpayroll_result']='';
 			$data['final']['deductions'] = [];
			$data['final']['adjustments'] = [];
			$data['final']['total_deductions'] = '';
			$data['final']['total_adjustments'] = '';
			$data['final']['weeks'] = '';
			$data['final']['timelogs'] = [];
			$data['final']['weekpay'] = [];
			$data['user'] = $user;
			$param='';
			$param2='';
			if($this->input->post('id')&&$this->input->post('date_from')&&$this->input->post('date_to')&&$this->input->post('quickflag')){
				$payperiod = $this->input->post('date_from').';'.$this->input->post('date_to');	
				$data['quickpayroll_result'] = 'X';
				$param=$this->input->post('date_from');
				$param2=$this->input->post('date_to');
				$data['final'] = $this->payroll->getQuickPayroll($this->input->post('id'),$payperiod); //Needed to keep the drop down select the previously selected value
			}
			$arr = $this->adminm->get_basic_info_2($this->session->id);
			$data['picture'] = $arr['picture'];
			
			$data['custom_additions'] = '';
			$data['custom_deductions'] = '';
			$data['deductions'] =$this->adminm->getEmployeeDeductions($user);
			$data['employee_info'] = $this->employee->get_overall_info($user);
			$data['employee_basic_info'] = $this->adminm->get_basic_info_2($user);				
			$data['emp_skills'] = $this->adminm->get_employee_skill($user);
			$data['all_skills'] = $this->adminm->get_all_skills();
			$data['all_pm'] = $this->adminm->get_all_pm();
			$data['pm_details'] = $this->adminm->get_pm($user);
			$data['param1']=$param;
			$data['param2']=$param2;	
			$data['param'] = '';
			$data['custom_additions'] = $this->adminm->getAdjustments($user,'add');
			$data['custom_deductions'] = $this->adminm->getAdjustments($user,'sub');
			$this->load->view('admin\Header',$data);
			$this->load->view('admin\modal',$data);
			$this->load->view('admin\nav',$data);
			$this->load->view('admin\payroll_for_employee',$data);
			$this->load->view('admin\Footer');
			
		}
		// --------------------------------------------------------------------------

		public function payroll($param='') {
			$this->e_security->only_allow('admin');
			$data = $this->employee->get_overall_info($this->session->id);
			$arr = $this->adminm->get_basic_info_2($this->session->id);
			$data['picture'] = $arr['picture'];
			$data['param'] = $param;			
			$data['all_holidays'] = $this->holiday->view();
			$data['all_skills'] = $this->adminm->get_all_skills();
			$data['all_pm'] = $this->adminm->get_all_pm();
			$data['payroll_overview'] = [];
			$data['param1'] = '';
			$data['param2'] = '';
			if($this->input->post('date_from')&&$this->input->post('date_to')){
				$payperiod = $this->input->post('date_from').';'.$this->input->post('date_to');
				$data['payroll_overview'] = $this->payroll->getPayrollOverview($payperiod);
				$data['param1'] = $this->input->post('date_from'); //Needed to keep the drop down select the previously selected value
				$data['param2'] = $this->input->post('date_to');
			}
		
			$this->load->view('admin\Header',$data);
			$this->load->view('admin\modal',$data);
			$this->load->view('admin\nav',$data);
			$this->load->view('admin\Payroll',$data);
			$this->load->view('admin\Footer');
		}

		// --------------------------------------------------------------------------

		public function holiday($param='') {
			$this->e_security->only_allow('admin');
			if($this->input->post('holiday_id_remove')){
				if($this->holiday2->removeHoliday($this->input->post('holiday_id_remove'))){
					$param = 8;
				}
			}
			$data = $this->employee->get_overall_info($this->session->id);
			$data['holidays'] = $this->holiday->view();
			$data['param'] = $param;	
			$data['all_holidays'] = $this->holiday->view();
			$data['all_skills'] = $this->adminm->get_all_skills();
			$data['all_pm'] = $this->adminm->get_all_pm();
			$this->load->view('admin/header', $data);
			$this->load->view('admin/modal', $data);
			$this->load->view('admin/nav', $data);
			$this->load->view('admin/holiday', $data);
			$this->load->view('admin/footer');
		}
//End of Change BORJA 3_13_2016
		// --------------------------------------------------------------------------
//Start of Change BORJA 3_13_2016
		public function reports($param='') {
			$this->e_security->only_allow('admin');
			$data = $this->employee->get_overall_info($this->session->id);
			
			$arr = $this->adminm->get_basic_info_2($this->session->id);
			$data['picture'] = $arr['picture'];
			$data['param'] = $param;			
			$data['all_holidays'] = $this->holiday->view();
			$data['all_skills'] = $this->adminm->get_all_skills();
			$data['all_pm'] = $this->adminm->get_all_pm();
			$data['selected_month'] = $this->input->post('selectmonth') ? $this->input->post('selectmonth') : 'all';
			$data['selected_year'] = $this->input->post('selectyear') ? $this->input->post('selectyear') : 'all';
			$data['dropdown_months'] = $this->adminm->getDropdown('months');			
			$data['dropdown_years'] = $this->adminm->getDropdown('years');
			if($param==''){ //main report
				$month='all';
				$year='all';
				$month=$this->input->post('selectmonth');
				$year=$this->input->post('selectyear');
				$data['report_details'] = $this->reports->report_stats($month,$year);
			}
			$this->load->view('admin\Header',$data);
			$this->load->view('admin\modal',$data);
			$this->load->view('admin\nav');
			$this->load->view('admin\Reports',$data);
			$this->load->view('admin\Footer');
		}
//End of Change BORJA 3_13_2016
		// --------------------------------------------------------------------------

		public function logout() {
			$this->session->sess_destroy();
			delete_cookie('hash');
			redirect('/index');
		}

		// --------------------------------------------------------------------------
	}