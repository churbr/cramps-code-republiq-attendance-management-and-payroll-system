<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Attendance extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->model(array('timelog', 'leave', 'overtime', 'notification', 'timelog_whole', 'timelog_overtime'));
			$this->load->library('e_attendance');
		}

		// --------------------------------------------------------------------------

		public function log() {

			$visitor_ip = $this->input->ip_address();
			$static_ip = $this->setting->static_ip();

			if($_SERVER['REQUEST_METHOD'] === 'POST') {
				if($this->session->is_loggedin) {
					if($visitor_ip == $static_ip ) {
						if(!$this->timelog->on_leave($this->session->id)) {

							if($this->timelog->employee_timedin()) {
								if($this->timelog->time_out() === true) { echo 'timed_out'; }
							} else {
								if($this->timelog->time_in() === true) { echo 'timed_in'; }
							}

						} else { echo 'on_leave'; }
					} else { echo 'invalid_ip'; }
				} else { echo 'not_loggedin'; }
			}
		}

		// --------------------------------------------------------------------------

		public function request_overtime() {

			$timelog_pk = $this->input->post('table_pk');
			$timelog = $this->timelog->get($this->session->id, $timelog_pk );

			$ot_start = $this->e_attendance->to_regular( $this->input->post('ot_start_time') );
			$ot_end =  $this->e_attendance->to_regular( $this->input->post('ot_end_time') );
			$ot_reason = $this->input->post('ot_reason');
			$pm_id = $this->input->post('pm_id');

			$regdate_start = date('Y-m-d', strtotime( $timelog[0]['timein_date'] ));
			$regtime_start = $timelog[0]['time_in'];

			$regtime_end = $timelog[0]['time_out'];
			$regdate_end = date('Y-m-d', strtotime( $timelog[0]['timeout_date'] ));
			
			$overtime_info = $this->e_attendance->overtime_request( $timelog_pk, $pm_id, $regdate_start, $regdate_end, $regtime_start, $regtime_end, $ot_start, $ot_end, $ot_reason );

			if(is_array($overtime_info)) {
				echo json_encode($overtime_info);
			}else {
				echo $overtime_info;
			}
		}

		// --------------------------------------------------------------------------

		public function request_leave() {
			// Aduuuuu - kin !

			if($this->input->post('leave_type_id') && $this->input->post('from') && $this->input->post('to')) {

				$leave_type_id = intval($this->input->post('leave_type_id'));
				$from = $this->input->post('from');
				$to = $this->input->post('to');

				if($this->e_attendance->date_hasPassed($from, $to)) {
					if($this->e_attendance->valid_range($from, $to)) {
						if($this->leave->request_exist($leave_type_id)) {

							$remaining_days = $this->leave->remaining_days($this->session->id, $leave_type_id);
							$day_count = count($this->e_attendance->get_workingDays($from, $to));

							if($day_count !== 0) {
								if(($remaining_days - $day_count) >= 0) {

									$leave_request = $this->leave->request($leave_type_id, $from, $to);
									$notification = $this->notification->add(1, $leave_request['id'], 0, 'Leave Application');
									echo json_encode($notification);
								
								} else { echo '6'; } # No enough remaining days
							} else { echo '5'; } # Saturdays and Sundays are not included
						} else { echo '4'; } # Request type does not exist
					} else  { echo '3'; } # Date range is invalid
				} else { echo '2'; } # Date has already passed
			} else { echo '1'; } # No input
		}

		// --------------------------------------------------------------------------

		public function timelog_search() {
			$html = '';

			$from = $this->input->post('from');
			$to = $this->input->post('to');
			
			if(date_range($from, $to) !== FALSE) {
				$timelog = $this->timelog->view($this->session->id, $from, $to);
			
				if(count($timelog)) {

					$hours_gained = $this->timelog->ajax_count_hours($timelog);

					foreach ($timelog as $index => $log) {

						if($log['type'] == 'leave') {
							$html .= '<tr>';
							$html .= '<td style="display: none;">' . $log['timestamp'] . '</td>';
							$html .= '<td>' . $log['date'] . '</td>';
							$html .= '<td>--</td>';
							$html .= '<td>--</td>';
							$html .= '<td>' . $log['total_time'] . ' hrs <span class="label bg-navy">Leave</span></td>';
							$html .= '<td>';
							$html .= '<div class="btn-group">';
							$html .= '<a class="dropdown-toggle" data-toggle="dropdown">';
							$html .= '<span><i class="fa fa-fw fa-toggle-down"></i></span>';
							$html .= '<span class="sr-only">Toggle Dropdown</span>';
							$html .= '</a>';
							$html .= '<ul class="dropdown-menu" role="menu">';
							$html .= '<li><a onclick="show_leave_modal(' . $log['id'] . ')" href="#/">View</a></li>';
							$html .= '</ul>';
							$html .= '</div>';
							$html .= '</td>';
							$html .= '</tr>';
						}elseif($log['type'] == 'overtime') {

							$hr_word = ($log['hour'] > 1) ? ' hrs ' : ' hr ';
							$min_word = ($log['minute'] > 1) ? ' mins ' : ' min ';

							if($log['hour'] == 0 && $log['minute'] == 0) {
								$overtime_total_time = '<strong>NC</strong>';
							}elseif ($log['hour'] > 0 && $log['minute'] == 0) {
								$overtime_total_time = $log['hour'] . $hr_word;
							}elseif($log['hour'] == 0 && $log['minute'] > 0) {
								$overtime_total_time = $log['minute'] . ' ' . $min_word;
							}else {
								$overtime_total_time = $log['hour'] . $hr_word . $log['minute'] . $min_word;
							}

							// ------------------------------------------------------------------------------

							$tr_id = 'tr_overtime_request_' . $log['id'];

							// ------------------------------------------------------------------------------

							if($log['notification_status'] == 0 && $log['overtime_status'] != 'pending') {
								$seen_status = 'class="bg-gray"';
							}else { $seen_status = ''; }

							// ------------------------------------------------------------------------------

							$overtime_status = function($overtime_status) {
								$html_form = null;

								switch ($overtime_status) {
									case 'approved':
										$html_form = '<span class="label bg-teal">OT Approved</span>';
									break;

									case 'pending':
										$html_form = '<span class="label label-warning">OT Pending</span>';
									break;

									case 'declined':
										$html_form = '<span class="label label-danger">OT Declined</span>';
									break;
								}

								return $html_form;
							};

							// ------------------------------------------------------------------------------


							$html .= '<tr ' . $seen_status . ' id="' . $tr_id . '">';
							$html .= '<td style="display: none;">' . $log['timestamp'] . '</td>';
							$html .= '<td>' . $log['start_date'] . '</td>';
							$html .= '<td>' . $log['start_time'] . '</td>';
							$html .= '<td>' . $log['end_time'] . '</td>';
							$html .= '<td>' . $overtime_total_time . ' ' . $overtime_status($log['overtime_status']) . '</td>';
							$html .= '<td>';
							$html .= '<div class="btn-group">';
							$html .= '<a class="dropdown-toggle" data-toggle="dropdown">';
							$html .= '<span><i class="fa fa-fw fa-toggle-down"></i></span>';
							$html .= '<span class="sr-only">Toggle Dropdown</span>';
							$html .= '</a>';
							$html .= '<ul class="dropdown-menu" role="menu">';

								if($log['overtime_status'] == 'pending') {

									$html .= '<li><a onclick="show_overtime_modal(' . $log['id'] . ')" href="#/">View</a></li>';
									$html .= '<li class="divider"></li>';
									$html .= '<li><a onclick="cancel_overtime_request(' . $log['id'] . ')" href="#/">Cancel</a></li>';
								
								}elseif($log['overtime_status'] == 'declined') {

									$html .= '<li><a onclick="show_overtime_modal(' . $log['id'] . ')" href="#/">View</a></li>';
									$html .= '<li class="divider"></li>';
									$html .= '<li><a onclick="resend_overtime_request(' . $log['id'] . ')" href="#/">Apply for Overtime</a></li>';
								
								}else {

									$html .= '<li><a onclick="show_overtime_modal(' . $log['id'] . ')" href="#/">View</a></li>';
		
								}

							$html .= '</ul>';
							$html .= '</div>';
							$html .= '</td>';
							$html .= '</tr>';


						} elseif($log['type'] == 'timelog_overtime') {

							$hr_word = ($log['hour'] > 1) ? ' hrs ' : ' hr ';
							$min_word = ($log['minute'] > 1) ? ' mins ' : ' min ';

							if($log['hour'] == 0 && $log['minute'] == 0) {
								$timelog_total_time = '<strong>NC</strong>';
							}elseif ($log['hour'] > 0 && $log['minute'] == 0) {
								$timelog_total_time = $log['hour'] . $hr_word;
							}elseif($log['hour'] == 0 && $log['minute'] > 0) {
								$timelog_total_time = $log['minute'] . ' ' . $min_word;
							}else {
								$timelog_total_time = $log['hour'] . $hr_word . $log['minute'] . $min_word;
							}

							$html .= '<tr>';
							$html .= "<td style='display: none;'> {$log['timestamp']} </td>";
							$html .= "<td> {$log['timein_date']} </td>";
							$html .= "<td> {$log['time_in']} </td>";
							$html .= "<td> {$log['time_out']} </td>";
							$html .= "<td> $timelog_total_time </td>";
							$html .= '<td>';
							$html .= '<div class="btn-group" style="pointer-events: none; opacity: 0.4;">';
							$html .= '<a class="dropdown-toggle" data-toggle="dropdown">';
							$html .= '<span><i class="fa fa-fw fa-toggle-down"></i></span>';
							$html .= '<span class="sr-only">Toggle Dropdown</span>';
							$html .= '</a>';
							$html .= '</div>';
							$html .= '</td>';
							$html .= '</tr>';

					} else {

							$hr_word = ($log['hour'] > 1) ? ' hrs ' : ' hr ';
							$min_word = ($log['minute'] > 1) ? ' mins ' : ' min ';

							if($log['hour'] == 0 && $log['minute'] == 0) {
								$timelog_total_time = '<strong>NC</strong>';
							}elseif ($log['hour'] > 0 && $log['minute'] == 0) {
								$timelog_total_time = $log['hour'] . $hr_word;
							}elseif($log['hour'] == 0 && $log['minute'] > 0) {
								$timelog_total_time = $log['minute'] . $min_word;
							}else {
								$timelog_total_time = $log['hour'] . $hr_word . $log['minute'] . $min_word;
							}

							if(!is_null($log['time_out'])) {
								$html .= '<tr>';
								$html .= '<td style="display: none;">' . $log['timestamp'] . '</td>';
								$html .= '<td>' . $log['timein_date'] . '</td>';
								$html .= '<td>' . $log['time_in'] . '</td>';
								$html .= '<td>' . $log['time_out'] . '</td>';
								$html .= '<td>' . $timelog_total_time . '</td>';

								if($this->session->type == 'regular_employee') {
									$html .= '<td>';
									$html .= '<div class="btn-group">';
									$html .= '<a class="dropdown-toggle" data-toggle="dropdown">';
									$html .= '<span><i class="fa fa-fw fa-toggle-down"></i></span>';
									$html .= '<span class="sr-only">Toggle Dropdown</span>';
									$html .= '</a>';
									$html .= '<ul class="dropdown-menu" role="menu">';
									$html .= '<li><a onclick="show_overtime_form(' . $log['id'] . ')" href="#/">Apply for Overtime</a></li>';
									$html .= '</ul>';
									$html .= '</div>';
									$html .= '</td>';
								}

								$html .= '</tr>';

							} else {
								$html .= '<tr>';
								$html .= '<td style="display: none;">' . $log['timestamp'] . '</td>';
								$html .= '<td>' . $log['timein_date'] . '</td>';
								$html .= '<td>' . $log['time_in'] . '</td>';
								$html .= '<td> -- </td>';
								$html .= '<td>' . $timelog_total_time . '</td>';


									if($this->session->type == 'regular_employee') {
										$html .= '<td>';
										$html .= '<div class="btn-group" style="pointer-events: none; opacity: 0.4;">';
										$html .= '<a class="dropdown-toggle" data-toggle="dropdown">';
										$html .= '<span><i class="fa fa-fw fa-toggle-down"></i></span>';
										$html .= '<span class="sr-only">Toggle Dropdown</span>';
										$html .= '</a>';
										$html .= '</div>';
										$html .= '</td>';
									}

								$html .= '</tr>';
								
							}
						}
					}
				}else {
					$html .= '<tr>';
					$html .= '<td></td>';
					$html .= '<td><p class="error">Log is empty</p></td>';
					$html .= '<td></td>';
					$html .= '<td></td>';
					$html .= '<td></td>';
					$html .= '<td></td>';
					$html .= '</tr>';
				}
				
				$data = array (
					'html' => $html,
					'total_regular' => $hours_gained['regular'],
					'total_overtime' => $hours_gained['overtime']
				);

				echo json_encode($data);
			} else { echo json_encode('false'); }
			
		}

		// --------------------------------------------------------------------------

		public function leave_info() {
			$table_pk = $this->input->post('table_pk');
			$this->notification->seen($this->session->id, 1, $table_pk);
			$data = $this->leave->get_data($table_pk, $this->session->id);
			echo json_encode($data);
		}

		// --------------------------------------------------------------------------

		public function overtime_info() {
			$table_pk = $this->input->post('table_pk');
			$this->notification->seen($this->session->id, 2, $table_pk);
			$data = $this->overtime->get_data($table_pk, $this->session->id);

			$data['start_date'] = nice_date($data['start_date'], 'M d, Y');
			$data['end_date'] = nice_date($data['end_date'], 'M d, Y');
			$data['date_filed'] = nice_date($data['date_filed'], 'M d, Y');
			$data['date_updated'] = nice_date($data['date_updated'], 'M d, Y');
			echo json_encode($data);
		}

		// --------------------------------------------------------------------------

		public function hp_view_overtime_info() {
			$table_pk = $this->input->post('table_pk');
			$this->notification->ot_seen_by_hp($this->session->id, $table_pk);
			$data = $this->overtime->hp_get_data($table_pk);

			$data['start_date'] = nice_date($data['start_date'], 'M d, Y');
			$data['end_date'] = nice_date($data['end_date'], 'M d, Y');
			$data['date_filed'] = nice_date($data['date_filed'], 'M d, Y');
			$data['date_updated'] = nice_date($data['date_updated'], 'M d, Y');
			echo json_encode($data);
		}

		// --------------------------------------------------------------------------
		
		public function hp_view_leave_info() {

			$table_pk = $this->input->post('table_pk');
			$this->notification->leave_seen_by_hp($table_pk);
			$data = $this->leave->hp_get_data($table_pk);

			if(!is_null($data['bod_id'])) {
				$data['bod_name'] = $this->leave->bod_name($data['bod_id']);
			}

			$data['start_date'] = nice_date($data['start_date'], 'M d, Y');
			$data['end_date'] = nice_date($data['end_date'], 'M d, Y');
			$data['date_filed'] = nice_date($data['date_filed'], 'M d, Y');
			$data['date_updated'] = nice_date($data['date_updated'], 'M d, Y');
			echo json_encode($data);
		}

		// --------------------------------------------------------------------------

		public function overtime_info_resendreq() {
			$table_pk = $this->input->post('table_pk');
			$this->notification->seen($this->session->id, 2, $table_pk);
			$data = $this->overtime->get_data($table_pk, $this->session->id);

			$timein_extract = $this->e_attendance->time_extract($data['start_time'], true);
			$timeout_extract = $this->e_attendance->time_extract($data['end_time'], true);

			$timein_miltime = $this->e_attendance->to_military($timein_extract['hour'], $timein_extract['ampm']);
			$timeout_miltime = $this->e_attendance->to_military($timeout_extract['hour'], $timeout_extract['ampm']);

			$data['start_time'] = $timein_miltime . ':' . $timein_extract['minute'];
			$data['end_time'] = $timeout_miltime . ':' . $timeout_extract['minute'];
			echo json_encode($data);
		}

		// --------------------------------------------------------------------------

		public function timelog_info() {
			$table_pk = $this->input->post('table_pk');
			$data = $this->timelog->get_data($table_pk, $this->session->id);

			$timein_extract = $this->e_attendance->time_extract($data['time_in'], true);
			$timeout_extract = $this->e_attendance->time_extract($data['time_out'], true);

			$timein_miltime = $this->e_attendance->to_military($timein_extract['hour'], $timein_extract['ampm']);
			$timeout_miltime = $this->e_attendance->to_military($timeout_extract['hour'], $timeout_extract['ampm']);

			$data['main_start_time'] = $data['time_in'];
			$data['main_end_time'] = $data['time_out'];

			$data['table_pk'] = $this->input->post('table_pk');
			$data['time_in'] = $timein_miltime . ':' . $timein_extract['minute'];
			$data['time_out'] = $timeout_miltime . ':' . $timeout_extract['minute'];

			echo json_encode($data);
		}

		// --------------------------------------------------------------------------

		public function seen() {
			$notification_type_id = $this->input->post('notification_type_id');
			$table_pk = $this->input->post('table_pk');
			$this->notification->seen($this->session->id, $notification_type_id, $table_pk);
		}

		// --------------------------------------------------------------------------

		public function cancel_overtime_request() {
			$status = FALSE;

			$overtime_pk = $this->input->post('table_pk');

			$timelog_pk_whole = $this->timelog_whole->timelog_pk( $overtime_pk );
			$timelog_pk_overtime = $this->timelog_overtime->timelog_pk( $overtime_pk );

			if($timelog_pk_whole !== FALSE) {
				$timelog_pk = $timelog_pk_whole;
			}else {
				$timelog_pk = $timelog_pk_overtime;
			}

			if($this->overtime->hide($overtime_pk)) {
				if($this->timelog->show( $timelog_pk )) {
					if($timelog_pk_whole !== FALSE) {
						if( $this->timelog_whole->hide( $timelog_pk, $overtime_pk ) ) { $status = TRUE; }
					}else {
						if( $this->timelog_overtime->hide( $timelog_pk, $overtime_pk ) ) { $status = TRUE; }
					}
				}
			}

			if($status === TRUE) {
				echo 'true';
			} else { echo 'false'; }
		}

		// --------------------------------------------------------------------------

		public function cancel_leave_request() {
			$leave_pk = $this->input->post('table_pk');

			if( $this->leave->cancel_request($leave_pk) ) {
				echo 'true';
			}else { echo 'false'; }
		}

		// --------------------------------------------------------------------------

		public function approve_overtime() {
			$table_pk = $this->input->post('table_pk');
			$status = $this->input->post('status');
			$note = $this->input->post('note');

			if(trim($note) == '') { $note = NULL; }

			$application_status = $this->overtime->update($table_pk, $status, $note);

			if($application_status == TRUE) {
				echo 'success';
			} else {
				echo 'fail';
			}
		}

		// --------------------------------------------------------------------------

		public function approve_leave() {
			$table_pk = $this->input->post('table_pk');
			$status = $this->input->post('status');

			$application_status = $this->leave->update($table_pk, $status);

			if($application_status == TRUE) {
				echo 'success';
			} else {
				echo 'fail';
			}
		}

		// --------------------------------------------------------------------------

	}