function resend_overtime_request(table_pk) {

	$.post(
		'attendance/overtime-info-resendreq.html',
		{ table_pk: table_pk },
		function(response) {
			var parsed = jQuery.parseJSON(response);

			$('#overtime_request_form #ot_start_time').val(parsed.start_time);
			$('#overtime_request_form #ot_end_time').val(parsed.end_time);
			$('#overtime_request_form').modal('show');
		}
	);

	return false;
}

// ----------------------------------------------------------------------------------------------------------------

function show_overtime_form(table_pk) {

	$.post(
		'attendance/timelog-info.html',
		{ table_pk: table_pk },
		function(response) {
			var parsed = jQuery.parseJSON(response);

			$('#overtime_request_form #ot_reason').val('');
			$('#overtime_request_form #table_pk').val(parsed.table_pk);
			$('#overtime_request_form #ot_start_time').val(parsed.time_in);
			$('#overtime_request_form #ot_end_time').val(parsed.time_out);
			$('#overtime_request_form .main_start_time').html(parsed.main_start_time);
			$('#overtime_request_form .main_end_time').html(parsed.main_end_time);
			$('#overtime_request_form').modal('show');
		}
	);

	return false;
}

// ----------------------------------------------------------------------------------------------------------------

function show_leave_modal(table_pk) {

	$.post(
		'attendance/leave-info.html',
		{ table_pk: table_pk },
		function(response) {
			var parsed = jQuery.parseJSON(response);

			$('#leave_request_modal .modal-title').html(parsed.leave_type);
			$('#leave_request_modal #leave_request_from').html('From: ' + parsed.start_date);
			$('#leave_request_modal #leave_request_to').html('To: ' + parsed.end_date);
			$('#leave_request_modal #leave_request_approve_author').html('Approved by: ' + parsed.manager_name);
			$('#leave_request_modal #leave_request_date_filed').html('Date Filed: ' + parsed.date_filed);
			$('#leave_request_modal').modal('show');
		}
	);
	
	return false;
}

// ----------------------------------------------------------------------------------------------------------------

function show_overtime_modal(table_pk) {

	$.post(
		'attendance/overtime-info.html',
		{ table_pk: table_pk },
		function(response) {
			var parsed = jQuery.parseJSON(response);
			hour = parsed.hour;
			minute = parsed.minute;

			table_row = '#tr_overtime_request_' + table_pk;

			if(parsed.status == 'declined') {
				approved_by = 'Noted by: ' + parsed.firstname + ' ' + parsed.lastname + ' on ' + parsed.date_updated;
			}else if(parsed.status == 'pending') {
				approved_by = 'To be approved by: ' + parsed.firstname + ' ' + parsed.lastname;
			} else {
				approved_by = 'Approved by: ' + parsed.firstname + ' ' + parsed.lastname + ' on ' + parsed.date_updated;
			}

			if(hour == 0 || hour == 1) { hour = hour + ' hr.'; } else { hour = hour + ' hrs.'; }
			if(minute == 0) { minute = ''; } else if(minute == 1) { minute = ' 1 min.'; } else { minute = ' ' + minute + ' mins.'; }
			total_time = hour + minute;

			$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');
			$(table_row).attr('class', '');

			$('#overtime_request_modal #from').html('From: ' + parsed.start_date);
			$('#overtime_request_modal #to').html('To: ' + parsed.end_date);
			$('#overtime_request_modal #start').html('Start: ' + parsed.start_time);
			$('#overtime_request_modal #end').html('End: ' + parsed.end_time);
			$('#overtime_request_modal #total_hours').html('Total Hours: ' + total_time);
			$('#overtime_request_modal #reason').html(parsed.note);
			$('#overtime_request_modal #approved_by').html(approved_by);
			$('#overtime_request_modal #date_filed').html('Date Filed: ' + parsed.date_filed);
			$('#overtime_request_modal').modal('show');

		}
	);

	return false;
}

// ----------------------------------------------------------------------------------------------------------------

function cancel_overtime_request(table_pk) {
	var base_url = $('.base_url').attr('id');

	$.post(
		base_url + 'attendance/cancel-overtime-request.html',
		{ table_pk: table_pk },
		function(response) {
			if(response == 'true') {
				if($('#timelog_table').length) {
					$('#timelog_table').load(location.href + ' #timelog_table');
				}else {
					$('#tr_overtime_request_' + table_pk + ' .request_status').html('<span class="label label-danger">Cancelled</span>');
					$('#tr_overtime_request_' + table_pk + ' #cancel_button').fadeOut();
					$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');

				}
			}
		}
	);
	
	return false;
}

// ----------------------------------------------------------------------------------------------------------------

function cancel_leave_request( table_pk ) {
	var base_url = $('.base_url').attr('id');

	$.post(
		base_url + 'attendance/cancel-leave-request.html',
		{ table_pk: table_pk },
		function(response) {
			if(response == 'true') {
				$('#tr_leave_request_' + table_pk + ' .request_status').html('<span class="label label-danger">Cancelled</span>');
				$('#tr_leave_request_' + table_pk + ' #cancel_button').fadeOut();
				$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');
			}
		}
	);
	
	return false;
}

// ----------------------------------------------------------------------------------------------------------------

function seen_request(notification_type_id, table_pk) {
	var base_url = $('.base_url').attr('id');

	if(notification_type_id == 1) { notification_type = 'leave'; }
	if(notification_type_id == 2) { notification_type = 'overtime'; }

	var table_row = '#tr_' + notification_type + '_request_' + table_pk;

	$.post(
		base_url + 'attendance/seen.html',
		{ notification_type_id: notification_type_id, table_pk: table_pk },
		function(response) {
			$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');
			$(table_row).attr('class', '');
		}
	);
	
	return false;
}

// ----------------------------------------------------------------------------------------------------------------

// $('#timelog_table').load(location.href + ' #timelog_table');


