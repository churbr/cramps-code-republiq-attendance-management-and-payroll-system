<html>
<body>
	
<style type="text/css">
	.header-2 table,th,td {margin-left: 0; border: 1px solid black;}
	.header-1 td {padding: 5px; border: none !important;}
	.header-2 th {text-align: left;}
</style>


<div>
	<h2>Code Republiq</h2>
	<p>Unit 1, 3rd floor, E-Park Bldg. Kauswagan Rd.</p>
	<p>6000 Cebu City, Philippines</p>
	<strong>Pay Slip</strong>
</div>

<div class="header-1" style="margin-top: 20px;">
	<table>
		<tr>
			<td>Name: <?=$employee_basic_info['firstname']?> <?=$employee_basic_info['lastname']?></td>
			<td>Position: Web Developer</td>
		</tr>
		<tr>
			<td>Hourly Rate: <?=$employee_basic_info['rate']?></td>
			<td>Pay Period: <?=nice_date($from,'M d, Y')?> - <?=nice_date($to,'M d, Y')?></td>
		</tr>
	</table>
</div>


<div class="header-2" style="margin-top: 20px;">
	<table style="width: 400px;">
	  <tr>
	    <th><strong>Working Hours</strong></th>
	    <th> </th>
	  </tr>
	  <tr>
	    <td>Regular</td>
	    <td><?=$regular?></td>		
	  </tr>
	  <tr>
	    <td>Overtime</td>
	    <td><?=$overtime?></td>
	  </tr>
	  <tr>
	    <td style="height: 20px;"></td>
	    <td></td>
	  </tr>
	  <tr>
	    <th>Gross Pay</th>
	    <th>PHP <?=$gross?></th>
	  </tr>
	  <tr>
	    <td style="height: 20px;"></td>
	    <td></td>
	  </tr>
	  <tr>
	    <th><strong>Normal Deductions</strong></th>
	    <th> </th>
	  </tr>
	  <tr>
	    <td>PhilHealth</td>
	    <td><?=$deductions['ph']?></td>		
	  </tr>
	  <tr>
	    <td>SSS</td>
	    <td><?=$deductions['sss']?></td>		
	  </tr>
	  <tr>
	    <td>PAG-IBIG</td>
	    <td><?=$deductions['pagibig']?></td>		
	  </tr>
	  <tr>
	    <td>Income Tax</td>
	    <td><?=$deductions['tax']?></td>		
	  </tr>
	  <tr style="height: 20px;">
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	  	<th>Other Deductions</th>
	  	<th></th>
	  </tr>
	    <?php
		$totaldeductions = 0;
		$ind = 0;
		foreach($final['deductions'] as $deduction){
			if($ind>3){
				echo '<tr><td>'.$deduction['name'].'</td><td>'.$deduction['val'].'</td></tr>';				
			}
			$totaldeductions += $deduction['val'];
			$ind++;
		}
		?>
	  <tr style="height: 20px;">
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	  	<th><strong>Total Deductions</strong></th>
	  	<th>PHP <?=$totaldeductions?></th>
	  </tr>
	  <tr style="height: 20px;">
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	  	<th>Adjustments</th>
	  	<th></th>
	  </tr>
	    <?php
		$totaladjustments = 0;
		foreach($final['adjustments'] as $adjustment){
			echo '<tr><td>'.$adjustment['name'].'</td><td>'.$adjustment['val'].'</td></tr>';
			$totaladjustments += $adjustment['val'];
		}
		?>
	  <tr>
	    <td>--</td>
	    <td></td>
	  </tr>
	  <tr style="height: 20px;">
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	  	<th><strong>Total Adjustments</strong></th>
	  	<th>PHP <?=$totaladjustments?></th>
	  </tr>
	  <tr style="height: 20px;">
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	  	<th><strong>Net Pay</strong></th>
	  	<th>PHP <?=($gross-$totaldeductions+$totaladjustments)?></th>
	  </tr>
	</table>
</div>


</body>
</html>