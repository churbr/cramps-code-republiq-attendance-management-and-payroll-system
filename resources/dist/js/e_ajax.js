$(document).ready(function() {

	var base_url = $('.base_url').attr('id');

	// ----------------------------------- Modals -----------------------------------

	$('#overtime_request_form').modal({ show: false});
	$('#leave_request_modal').modal({ show: false});
	$('#overtime_request_modal').modal({ show: false});

	// --------------------------------------------------------------------------------
	
	$('#leave_request_clear').on('click', function() {
		$('#leave_req_type').val('');
		$('#leave_req_from').val('');
		$('#leave_req_to').val('');
		$('#leave_request_message').html('');
	});


	$('#overtime_request_clear').on('click', function() {
		$('#ot_start_time').val('');
		$('#ot_end_time').val('');
		$('#ot_reason').val('');
	});

	// --------------------------------------------------------------------------------

	$('#timein_button').on('click', function() {
		$.post(
			'attendance/log.html',
			{ time_logged: true },
			function(response) {

				/**
				 * 5 FLAGS:
				 *
				 *	timed_in		: Employee has timed in
				 * 	timed_out		: Employee has timed out
				 *	on_leave		: Employee is on leave
				 *	invalid_ip		: Invalid Ip Address
				 *	not_loggedin	: User is not logged in
				 *
				 **/

				 switch(response) {
				 	case 'timed_in':
				 		$('.round-button').attr('value', 'OUT');
				 		$('#timein_navmenu').html('Time out');
				 		$('#confirmation_message h3').html('Are you sure you want time out?');
				 		$('#view_recent_total_time').load(location.href + ' #view_recent_total_time');
				 		$('#recent_logs').load(location.href + ' #recent_logs');
				 	break;

				 	case 'timed_out':
				 		$('.round-button').attr('value', 'IN');
				 		$('#timein_navmenu').html('Time in');
				 		$('#confirmation_message h3').html('Are you sure you want time in?');
				 		$('#view_recent_total_time').load(location.href + ' #view_recent_total_time');
				 		$('#recent_logs').load(location.href + ' #recent_logs');
				 		
				 	break;

				 	case 'on_leave':
				 		$('#timelog_button_container').html("<img src='resources/dist/img/icons/locked.png' width='200px' data-toggle='tooltip' data-placement='right' data-original-title='You are currently on leave.' />");
				 		$('#time-prompt-modal').remove();
				 	break;

				 	case 'invalid_ip':
				 		$('#timelog_button_container').html("<img src='resources/dist/img/icons/locked.png' width='200px' data-toggle='tooltip' data-placement='right' data-original-title='You can only time in/out within the office.' />");
				 		$('#time-prompt-modal').remove();
				 	break;

				 	case 'not_loggedin':
				 		window.location.replace('/');
				 	break;

				 	default:
				 		console.log(response);
				 	break;
				 }
			}
		);
	});

	// --------------------------------------------------------------------------------

	$('form.timelog_search button').on('click', function() {

		var url		= $('form.timelog_search').attr('action');
		var method	= $('form.timelog_search').attr('method');
		var from 	= $('form.timelog_search #from').val();
		var to 		= $('form.timelog_search #to').val();

		if( from && to ) {
			$.ajax({
				url: url,
				type: method,
				data: { from: from, to: to},
				success:function(response) {
					var parsed = jQuery.parseJSON(response);

					var empty_pagination = "<div class='dataTables_paginate paging_simple_numbers' id='timelog_table_paginate'>";
						empty_pagination += "<ul class='pagination'>";
						empty_pagination += "<li class='paginate_button previous disabled' id='timelog_table_previous'>";
						empty_pagination += "<a href='#' aria-controls='timelog_table' data-dt-idx='0' tabindex='0'>Previous</a>";
						empty_pagination += '</li>';
						empty_pagination += "<li class='paginate_button active'><a href='#' aria-controls='timelog_table' data-dt-idx='1' tabindex='0'>1</a></li>";
						empty_pagination += "<li class='paginate_button next disabled' id='timelog_table_next'>";
						empty_pagination += "<a href='#' aria-controls='timelog_table' data-dt-idx='2' tabindex='0'>Next</a>";
						empty_pagination += '</li></ul></div>';
					
					$('#total_timelog_reghours').html(parsed.total_regular);
					$('#total_timelog_overtime').html(parsed.total_overtime);
					$('#timelog_table tbody').html(parsed.html);
					$('#timelog_table_paginate').html(empty_pagination);
				}
			});
		}

		return false;
	});

	// --------------------------------------------------------------------------------

	$('#change_password').on('submit', function() {
		var that = $(this),
					url		= that.attr('action'),
					method	= that.attr('method'),
					data	= {};

		that.find('[name]').each(function() {
			var that = $(this),
					name		= that.attr('name'),
					value		= that.val();
					data[name]	= value;
		});

		$.ajax({
			url: url,
			type: method,
			data: data,
			success:function(response) {

				if(response == 'success') {
					
					$('#confirmpass_field').css('border-color', '#D2D6DE');
					$('#changepass_button').attr('disabled', 'disabled');
					$('#oldpass_field').attr('disabled', 'disabled');
					$('#newpass_field').attr('disabled', 'disabled');
					$('#confirmpass_field').attr('disabled', 'disabled');

					$('#changepass_info').html("<p class='green'>Password is updated.</p>");
					setTimeout(function() { $('#changepass_info').html(''); }, 3000);
					// window.location.replace(location.href);
					
				}else {
					$('#changepass_info').html(response);
				}

			}
		});

		return false;
	});

	// -----------------------------------------------------------------------------------------

	$('#update_info_address').on('keyup', function() {
		var input = $(this).val();

		$.post(
			base_url + 'user/validate-address.html',
			{ address: input },
			function(response) {
				if(response == 'true') {
					$('#update_info_address_msg').html("<img src='" +  base_url + "resources/dist/img/icons/success.png' width='20px' height='20px'>");
				}else {
					$('#update_info_address_msg').html(response);
				}

		});

		return false;
	});

	// -----------------------------------------------------------------------------------------

	$('#update_info_contact').on('keyup', function() {
		var input = $(this).val();

		$.post(
			base_url + 'user/validate-contact.html',
			{ contact: input },
			function(response) {
				if(response == 'true') {
					$('#update_info_contact_msg').html("<img src='" +  base_url + "resources/dist/img/icons/success.png' width='20px' height='20px'>");
				}else {
					$('#update_info_contact_msg').html(response);
				}
		});

		return false;
	});

	// -----------------------------------------------------------------------------------------

	$('#update_info_email').on('keyup', function() {
		var input = $(this).val();

		$.post(
			base_url + 'user/validate-email.html',
			{ email: input },
			function(response) {
				if(response == 'true') {
					$('#update_info_email_msg').html("<img src='" +  base_url + "resources/dist/img/icons/success.png' width='20px' height='20px'>");
				}else {
					$('#update_info_email_msg').html(response);
				}
		});

		return false;
	});

	// -----------------------------------------------------------------------------------------

	$('#update_info_form').on('submit', function() {

		var that = $(this),
					url		= that.attr('action'),
					method	= that.attr('method'),
					data	= {};

		that.find('[name]').each(function() {
			var that = $(this),
					name		= that.attr('name'),
					value		= that.val();
					data[name]	= value;
		});

		$.ajax({
			url: url,
			type: method,
			data: data,
			success:function(response) {
				if(response == 'true') {
					$('#update_info_address, #update_info_contact, #update_info_email, #update_info_button').attr('disabled', 'disabled');
					$('#update_info_address_msg, #update_info_contact_msg, #update_info_email_msg').html('');
					$('#update_info_msg').html("<p class='green'>Your info is updated.</p>");
				}
			}
		});

		return false;
	});

	// -----------------------------------------------------------------------------------------

});