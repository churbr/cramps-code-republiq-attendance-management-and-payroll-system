<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Leave extends CI_Model {

		public function __construct() {
			$this->load->library(array('e_attendance', 'e_security'));
			$this->load->model(array('salary', 'employee'));
			parent::__construct();
		}

		// --------------------------------------------------------------------------

		public function get($employee_id = null, $status = '*') {

			$this->db->select("`leave`.`start_date`, `end_date`, `bod`.`firstname` AS 'bod_firstname', `bod`.`lastname` AS 'bod_lastname', `employee`.`firstname` AS 'emp_firstname', `employee`.`lastname` AS 'emp_lastname', `leave_type`.`name`, `span`");
			$this->db->from('leave');
			$this->db->join('`employee` AS `bod`', '`leave`.`bod_id` = `bod`.`id`');
			$this->db->join('`employee` AS `employee`', '`leave`.`employee_id` = `employee`.`id`');
			$this->db->join('`leave_type`', '`leave`.`leave_type_id` = `leave_type`.`id`');
			$this->db->where('`employee`.`id`', (int)$employee_id);
			$this->db->where('`leave`.`show`', 1);
			if($status !== '*') { $this->db->where('`leave`.`status`', $status); }
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_dates($employee_id = null) {

			$dates = array();

			if(!is_null($employee_id)) {
				$date_ranges = self::get($employee_id, 'approved');

				if($date_ranges !== false) {
					foreach ($date_ranges as $index => $date) {
						$start_date = $date['start_date'];
						$end_date = $date['end_date'];

						$dates = array_merge($dates, $this->e_attendance->get_days($start_date, $end_date));
					}

					return $dates;
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function leave_limits($employee_id = null) {
			if(!is_null($employee_id)) {

				$this->db->select("`leave_type`.`id`, `leave_type`.`name`, `leave_setting`.`remaining_days`, `leave_type`.`span` AS 'max_days'");
				$this->db->from('`leave_setting`');
				$this->db->join('`leave_type`', '`leave_setting`.`leave_type_id` = `leave_type`.`id`');
				$this->db->where('`leave_setting`.`employee_id`', (int)$employee_id);
				$query = $this->db->get();

				if($query->num_rows()) {
					return $query->result_array();
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function request($leave_type_id = null, $from = null, $to = null) {

			$data = array();

			if(!is_null($leave_type_id) && !is_null($from) && !is_null($to)) {
				if($this->e_security->is_validDate($from) && $this->e_security->is_validDate($to)) {
					
					$from = $this->e_attendance->date_extract($from);
					$to = $this->e_attendance->date_extract($to);
					$reg_hour = $this->setting->regular_hour();

					if(checkdate($from['month'], $from['day'], $from['year']) && checkdate($to['month'], $to['day'], $to['year'])) {
						
						$start_date = $from['year'] . '-' . $from['month'] . '-' . $from['day'];
						$end_date = $to['year'] . '-' . $to['month'] . '-' . $to['day'];
					 	$days_count = count($this->e_attendance->get_workingDays($start_date, $end_date));
					 	$employee_rate = $this->salary->get_rate();

					 	$sum = ($employee_rate * $reg_hour);
					 	$payment = $sum * $days_count;

					 	$this->db->trans_start();
						$this->db->insert('leave', array(
							'id' => NULL,
							'bod_id' => NULL,
							'employee_id' => $this->session->id,
							'leave_type_id' => $leave_type_id,
							'start_date' => $start_date,
							'end_date' => $end_date,
							'pay' => $payment,
							'status' => 'pending',
							'show' => 1,
							'note' => NULL
						));

						if($this->db->affected_rows()) {
							$id = $this->db->insert_id();

	 						$data = array(
								'id' => $id,
								'bod_id' => NULL,
								'employee_id' => $this->session->id,
								'leave_type_id' => $leave_type_id,
								'start_date' => $start_date,
								'end_date' => $end_date,
								'pay' => $payment,
								'status' => 'pending',
								'note' => NULL
							);

							self::deduct_remainingDays($this->session->id, $leave_type_id, $days_count);
						}

						$this->db->trans_complete();

						return $data;
					}
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function update($table_pk, $status) {
			$data = array (
				'bod_id' => $this->session->id,
				'status' => $status,
				'date_updated' => date('Y-m-d')
			);

			$this->db->where('`id`', $table_pk);
			$this->db->update('`leave`', $data);
			if($this->db->affected_rows()) { return true; }

			return false;
		}

		// --------------------------------------------------------------------------

		public function max_days($leave_type_id = null) { # This will return the maximum days of leave from global settings

			if(!is_null($leave_type_id)) {
				$this->db->select('span');
				$this->db->where('id', (int)$leave_type_id);
				$query = $this->db->get('leave_type');

				if($query->num_rows()) {
					$result = $query->result_array()[0];
					return $result['span'];
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function remaining_days($employee_id = null, $leave_type_id = null) { # This will return the remaining days on the type of leave requested by the employee

			if(!is_null($employee_id) && !is_null($leave_type_id)) {
				$this->db->select('remaining_days');
				$this->db->where('employee_id', (int)$employee_id);
				$this->db->where('leave_type_id', (int)$leave_type_id);
				$query = $this->db->get('leave_setting');

				if($query->num_rows()) {
					return $query->result_array()[0]['remaining_days'];
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function deduct_remainingDays($employee_id = null, $leave_type_id = null, $deduct_days = null) {

			$this->db->where('employee_id', (int)$employee_id);
			$this->db->where('leave_type_id', (int)$leave_type_id);
			$this->db->set('remaining_days', "remaining_days - $deduct_days", FALSE);
			$this->db->update('leave_setting');

			if($this->db->affected_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function return_remainingDays($employee_id = null, $leave_type_id = null, $return_days = null) {

			$this->db->where('employee_id', (int)$employee_id);
			$this->db->where('leave_type_id', (int)$leave_type_id);
			$this->db->set('remaining_days', "remaining_days + $return_days", FALSE);
			$this->db->update('leave_setting');

			if($this->db->affected_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function request_exist($leave_type_id = null) {
			if(!is_null($leave_type_id)) {
				$this->db->where('id', (int)$leave_type_id);
				$query = $this->db->get('leave_type');

				return ($query->num_rows()) ? true : false;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function bod_name($id) {
			$this->db->select('firstname, middlename, lastname');
			$this->db->where('id', $id);
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				$bod = $query->result_array()[0];
				$firstname = $bod['firstname'];
				$lastname = $bod['lastname'];
				// $middlename = 	substr($bod['middlename'], 0, 1) . '.';

				return $firstname . ' ' . $middlename . ' ' . $lastname;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function detailed_list($employee_id = null, $notification_id = null) {
			$leave_request = array();

			$this->db->select("
				`notification`.`notification_type_id`,
				`notification`.`id` AS 'notification_pk',
				`notification`.`notification_id` AS 'table_pk',
				`notification`.`status` AS 'seen_status',
				`notification`.`date` AS 'date_filed',
				`leave`.`bod_id`,
				`leave`.`start_date`,
				`leave`.`end_date`,
				`leave`.`status`,
				`leave`.`date_updated`,
				`leave_type`.`name` AS 'leave_type'
			");
			$this->db->from('`notification`');
			$this->db->join('`leave`', '`notification`.`notification_id` = `leave`.`id`');
			$this->db->join('`leave_type`', '`leave`.`leave_type_id` = `leave_type`.`id`');
			if(!is_null($notification_id)) { $this->db->where('`notification`.`id`', $notification_id); }
			$this->db->where('`notification`.`notification_type_id`', 1);
			// $this->db->where('`leave`.`show`', 1);
			$this->db->where('`leave`.`employee_id`', (int)$employee_id);
			$query = $this->db->get();

			if($query->num_rows()) {

				foreach ($query->result_array() as $index => $leave) {

					if($leave['bod_id'] !== NULL) {
						$manager_name = self::bod_name($leave['bod_id']);
					} else {
						$manager_name = ' -- ';
					}

					$leave_request[] = array (
			            'notification_type_id' => $leave['notification_type_id'],
			            'notification_pk' => $leave['notification_pk'],
			            'table_pk' => $leave['table_pk'],
			            'seen_status' => $leave['seen_status'],
			            'date_filed' => $leave['date_filed'],
			            'manager_name' => $manager_name,
			            'start_date' => $leave['start_date'],
			            'end_date' => $leave['end_date'],
			            'status' => $leave['status'],
			            'date_updated' => $leave['date_updated'],
			            'leave_type' => $leave['leave_type'],
			            'timestamp' => strtotime($leave['date_filed'])
					);
				}

				return $leave_request;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_data($table_pk = null, $employee_id = null) {

			$this->db->select('
				`leave`.`bod_id`, `start_date`, `end_date`, `date_updated` AS "date_approved", `leave`.`status`,
				`leave_type`.`name`,
				`notification`.`date` AS "date_filed"
			');
			$this->db->from('`leave`');
			$this->db->join('`leave_type`', '`leave`.`leave_type_id` = `leave_type`.`id`');
			$this->db->join('`notification`', '`leave`.`id` = `notification`.`id`');
			$this->db->where('`leave`.`id`', $table_pk);
			$this->db->where('`leave`.`show`', 1);
			$this->db->where('`leave`.`employee_id`', $employee_id);
			$query = $this->db->get();

			if($query->num_rows()) {
				$leave_data = $query->result_array()[0];

				if($leave_data['bod_id'] != NULL && $leave_data['status'] != 'pending') {
					$manager_name = self::bod_name($leave_data['bod_id']) . ' on ' . nice_date($leave_data['date_approved'], 'M d, Y');
				} else {
					$manager_name = ' -- ';
				}

				$leave_data = array (
					'manager_name' => $manager_name,
					'start_date' => nice_date($leave_data['start_date'], 'M d, Y'),
					'end_date' => nice_date($leave_data['end_date'], 'M d, Y'),
					'status' => $leave_data['status'],
					'leave_type' => $leave_data['name'],
					'date_filed' => nice_date($leave_data['date_filed'], 'M d, Y')
				);

			    return $leave_data;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function timelog($employee_id = null) {
			$leave_dates = array();

			$this->db->where('`employee_id`', $employee_id);
			$this->db->where('`show`', 1);
			$this->db->where('`status`', 'approved');
			$query = $this->db->get('`leave`');

			if($query->num_rows()) {
				$reg_hour = $this->setting->regular_hour();

				foreach ($query->result_array() as $index => $leave) {
					$leave_days[ $leave['id'] ] = $this->e_attendance->get_workingDays($leave['start_date'], $leave['end_date']);
				}

				foreach ($leave_days as $leave_pk => $leave_date) {
					foreach ($leave_date as $date) {
						$leave_dates[] = array(
							'type' => 'leave',
							'id' => $leave_pk,
							'date' => nice_date($date, 'M d, Y'),
							'total_time' => $reg_hour,
							'timestamp' => strtotime($date)
						);
					}
				}

				return $leave_dates;
			}

			return false;
		}

		// --------------------------------------------------------------------------


		public function timelog_specific($employee_id = null, $dates = null) {
			$specific_leave_dates = array();
			$leave_dates = array();

			$this->db->where('employee_id', $employee_id);
			$this->db->where('status', 'approved');
			$this->db->where('show', 1);
			$query = $this->db->get('leave');

			if($query->num_rows()) {
				$reg_hour = $this->setting->regular_hour();

				// ----------- Loop through the date ranges and extract its dates -----------

				foreach ($query->result_array() as $index => $leave) {
					$leave_days[ $leave['id'] ] = $this->e_attendance->get_workingDays($leave['start_date'], $leave['end_date']);
				}

				// ----------- All leave dates are extracted -----------

				foreach ($leave_days as $leave_pk => $leave_date) {
					foreach ($leave_date as $date) {
						$leave_dates[] = array(
							'type' => 'leave',
							'id' => $leave_pk,
							'date' => $date,
							'total_time' => $reg_hour,
							'timestamp' => strtotime($date)
						);
					}
				}

				// ----------- Filter the extracted dates with specified range  -----------

				foreach ($leave_dates as $leave) {
					if(in_array($leave['date'], $dates)) {
						$specific_leave_dates[] = array (
							'type' => 'leave',
							'id' => $leave['id'],
							'date' => nice_date($leave['date'], 'M d, Y'),
							'total_time' => $leave['total_time'],
							'timestamp' => $leave['timestamp']
						);
					}
				}

				return $specific_leave_dates;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function cancel_request( $table_pk ) {

			$this->db->where('`leave`.`status`', 'pending');
			$this->db->where('`leave`.`id`', $table_pk);
			$query = $this->db->get('`leave`');

			if($query->num_rows()) {

				$requested = $query->result_array()[0];
				$days_count = count($this->e_attendance->get_workingDays($requested['start_date'], $requested['end_date']));

				if(self::return_remainingDays($this->session->id, $requested['leave_type_id'], $days_count)) {

					$leave_data = array (
						'`status`' => 'cancelled',
						'`show`' => 0
					);

					$this->db->where('`id`', $table_pk);
					$this->db->update('`leave`', $leave_data);
					if($this->db->affected_rows()) { return true; }
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_application($notification_id = null) {
			$this->db->select("
				`leave`.`id` AS 'leave_id',
				`leave`.`start_date`,
				`end_date`,
				`leave`.`status` AS 'leave_status',
				`note`,
				`date_updated`,
				`notification`.`id` AS 'notification_id',
				`notification`.`date` AS 'date_filed',
				`notification`.`seen_by` AS 'seen_status',
				CONCAT(`employee`.`firstname`, ' ' , `employee`.`lastname`) AS 'employee_name'
			");
			
			$this->db->from('`leave`');
			$this->db->join('`notification`', '`leave`.`id` = `notification`.`notification_id`');
			$this->db->join('`employee`', '`leave`.`employee_id` = `employee`.`id`');
			if(!is_null($notification_id)) { $this->db->where('`notification`.`id`', $notification_id); }
			$this->db->where('`notification`.`notification_type_id`', 1);
			$this->db->where('`leave`.`show`', 1);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function is_pending( $table_pk ) {
			$this->db->where('`id`', $table_pk);
			$this->db->where('`status`', 'pending');
			$query = $this->db->get('`leave`');

			if($query->num_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------


		public function application_count( $status_type = null ) {
			$count = 0;

			if($status_type !== NULL) {
				$this->db->where('`status`', $status_type);
				$this->db->where('`show`', 1);
				$this->db->from('`leave`');
				$count = $this->db->count_all_results();
			}else {
				$this->db->where('`show`', 1);
				$this->db->from('`leave`');
				$count = $this->db->count_all_results();
			}

			return $count;
		}

		// --------------------------------------------------------------------------

		public function hp_get_data($table_pk = null) {
			$this->db->select("
				`leave`.`bod_id`,
				`leave`.`status` AS 'leave_status',
				CONCAT(`employee`.`firstname`, ' ' ,`employee`.`lastname`) AS 'employee_name',
				`start_date`,
				`end_date`,
				`date_updated`,
				`note`,
				`notification`.`date` AS 'date_filed',
				`leave_type`.`name` AS 'leave_type'
			");
			$this->db->from('`leave`');
			$this->db->join('`employee`', '`leave`.`employee_id` = `employee`.`id`');
			$this->db->join('`notification`', '`leave`.`id` = `notification`.`notification_id`');
			$this->db->join('`leave_type`', '`leave`.`leave_type_id` = `leave_type`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 1);
			$this->db->where('`leave`.`id`', $table_pk);
			$this->db->where('`leave`.`show`', 1);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}
		
		// --------------------------------------------------------------------------

	}