 <!-- Employee Management Wrapper -->
      <div class="employee-management-wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Payroll
              <small>
                <ol class="breadcrumb">
                  <li><a href="<?=base_url("admin/payroll")?>"><i class="fa fa-money"></i> Payroll</a></li>
                  <li class="active"><?=$employee_basic_info['firstname']?> <?=$employee_basic_info['middlename']?> <?=$employee_basic_info['lastname']?></li>
                </ol>
              </small>
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">

            <!-- Your Page Content Here -->

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" 
                  <?php
				  	if(strlen($employee_basic_info['picture'])>0){
						echo 'src="'.base_url("resources/dist/img/users").'/'.$employee_basic_info['picture'].'"';
					}else{
						echo 'src="'.base_url("resources/dist/img/users/default.png").'"';
					}
				  ?>
                  alt="User profile picture" style="width: 128px; height: 128px;">
                  <h3 class="profile-username text-center"><?=$employee_basic_info['firstname']?> <?=$employee_basic_info['middlename']?> <?=$employee_basic_info['lastname']?></h3>
                  <p class="text-muted text-center">Web Developer</p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Status</b> <a class="pull-right">
                      <?php
					  	if($employee_info['status']=='1'){
							echo 'Active';
						}
						else{
							echo 'Inactive';
						}
					  ?>
                      </a>
                    </li>
                    <li class="list-group-item">
                      <b>Employment Date</b> <a class="pull-right"><?=$employee_info['date_hired']?></a>
                    </li>
                  </ul>

                </div><!-- /.box-body -->
              </div><!-- /.box -->


            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a a href="#quick-payroll" data-toggle="tab">Quick Payroll</a></li>
                  <li><a href="#activity" data-toggle="tab">Update Payroll</a></li>
                </ul>

                <div class="tab-content">
                  <div class="tab-pane" id="activity">


                    <!-- Update Payroll Box -->
                    <div class="">
                      <h4>Salary</h4>

                      <?= form_open('admin/payroll-for-employee/'.$user, array('class' => 'form-horizontal', 'id' => 'payroll_mgt_form')); ?>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Hourly Rate</label>
                          <div class="col-sm-6">
                          	<input type="hidden" value="X" name="updatepayrollflag" id="updatepayrollflag" />
                            <input type="hidden" id="id" name="id" class="form-control" value="<?=$employee_basic_info['id']?>" />
                            <input type="number" id="rate" name="rate" class="form-control" value="<?=$employee_basic_info['rate']?>" />
                          </div>
                        </div>
                     

                      <hr>

                      <h4>Deductions</h4>

                      <hr>

                      
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Phil Health</label>
                          <div class="col-sm-6">
                            <input type="number" class="form-control" id="philhealth" name="philhealth" value="<?=$deductions['ph']?>" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">SSS</label>
                          <div class="col-sm-6">
                            <input type="number" class="form-control" id="sss" name="sss" value="<?=$deductions['sss']?>" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Pag-Ibig</label>
                          <div class="col-sm-6">
                            <input type="number" class="form-control" id="pagibig" name="pagibig" value="<?=$deductions['pagibig']?>" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Income Tax</label>
                          <div class="col-sm-6">
                            <input type="number" class="form-control" id="tax" name="tax" value="<?=$deductions['tax']?>" />
                          </div>
                        </div>

                        <hr>

                        <h4>Other Deductions <a href="#add-deduction-modal" data-toggle="modal"><i class="fa fa-plus-circle" onclick="addDeductionFields()"></i></a></h4>
                        <hr>
                        <div id="deduction_group">
						<?php							
							foreach($custom_deductions as $workarea){
						?>
                        	<div class="form-group">
                              <label class="col-sm-2 control-label">Name</label>
                              <div class="col-sm-3">
                                <input type="text" id="deduct_name[]" name="deduct_name[]" class="form-control" value="<?=$workarea['name']?>"/>                           
                              </div>
                              
                              <label class="col-sm-2 control-label">Amount</label>
                              <div class="col-sm-3">
                                <input type="number" id="deduct_value[]" name="deduct_value[]" class="form-control" value="<?=$workarea['value']?>"/>
                                <a href="#remove-od-prompt" data-toggle="modal" onclick="$(this).closest('.form-group').remove();">Remove</a>
                              </div>
                            </div>
                        <?php								
							}
						?>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Name</label>
                              <div class="col-sm-3">
                                <input type="text" id="deduct_name[]" name="deduct_name[]" class="form-control" />                           
                              </div>
                              
                              <label class="col-sm-2 control-label">Amount</label>
                              <div class="col-sm-3">
                                <input type="number" id="deduct_value[]" name="deduct_value[]" class="form-control" />
                                <a href="#remove-od-prompt" data-toggle="modal" onclick="$(this).closest('.form-group').remove();">Remove</a>
                              </div>
                            </div>
						</div>
                        
                        <h4>Other Adjustments <a href="#add-adjustment-modal" data-toggle="modal"><i class="fa fa-plus-circle" onclick="addAdjustmentFields()"></i></a></h4>
                        <hr>
                        <div id="adjustment_group">
						<?php					
							foreach($custom_additions as $workarea){
						?>
                        	<div class="form-group">
                              <label class="col-sm-2 control-label">Name</label>
                              <div class="col-sm-3">
                                <input type="text" id="adjust_name[]" name="adjust_name[]" class="form-control" value="<?=$workarea['name']?>"/>                           
                              </div>
                              
                              <label class="col-sm-2 control-label">Amount</label>
                              <div class="col-sm-3">
                                <input type="number" id="adjust_value[]" name="adjust_value[]" class="form-control" value="<?=$workarea['value']?>"/>
                                <a href="#remove-od-prompt" data-toggle="modal" onclick="$(this).closest('.form-group').remove();">Remove</a>
                              </div>
                            </div>
                        <?php								
							}
						?>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Name</label>
                              <div class="col-sm-3">
                                <input type="text" id="adjust_name[]" name="adjust_name[]" class="form-control" />                           
                              </div>
                              
                              <label class="col-sm-2 control-label">Amount</label>
                              <div class="col-sm-3">
                                <input type="number" id="adjust_value[]" name="adjust_value[]" class="form-control" />
                                <a href="#remove-od-prompt" data-toggle="modal" onclick="$(this).closest('.form-group').remove();">Remove</a>
                              </div>
                            </div>
						</div>
                        <hr>

                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <input type="submit" class="btn btn-primary" value="Save" />
                          </div>
                        </div>
                      <?= form_close(); ?>
                    </div><!-- /.box -->


                  </div><!-- /.tab-pane -->


                  <div class="active tab-pane" id="quick-payroll">

                    <h4>Pay Period</h4>

                    <?= form_open('admin/payroll-for-employee/'.$employee_basic_info['id'], array('class' => 'form-horizontal', 'id' => 'payroll_mgt_quick_form')); ?>
                      <div class="form-group">
                        <label class="col-sm-1 control-label">From</label>
                        <div class="col-sm-3">
                          <input type="hidden" value="X" name="quickflag" id="quickflag" />
                          <input type="hidden" id="id" name="id" class="form-control" value="<?=$employee_basic_info['id']?>" />
						  <!--
                          <select class="form-control pull-right" id="payperiod" name="payperiod">
                          	<?php /*
								$curDate = date("Y-m-d");
								$curDateMid = date("Y-m-15");
								for($i=0;$i<=12;$i++){
									if($i==0){
										if($curDate>=$curDateMid){
											$latestDateP2Start = date("Y-m-15", strtotime("-".$i." months"));
											$latestDateP2End = date("Y-m-t", strtotime("-".$i." months"));
											$latestDateP1Start = date("Y-m-01", strtotime("-".$i." months"));
											$latestDateP1End = date("Y-m-14", strtotime("-".$i." months"));
											if($param1==''){
												echo '<option selected value="'.$latestDateP2Start.';'.$latestDateP2End.'">'.$latestDateP2Start.' - '.$latestDateP2End.'</option>';
											}
											else{
												if($param1==$latestDateP2Start.';'.$latestDateP2End){
													echo '<option selected value="'.$latestDateP2Start.';'.$latestDateP2End.'">'.$latestDateP2Start.' - '.$latestDateP2End.'</option>';
												}
												else{																						
													echo '<option value="'.$latestDateP2Start.';'.$latestDateP2End.'">'.$latestDateP2Start.' - '.$latestDateP2End.'</option>';
												}
											}
											if($param1==$latestDateP1Start.';'.$latestDateP1End){
												echo '<option selected value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
											}
											else{
												echo '<option value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
											}
										}
										else{
											$latestDateP1Start = date("Y-m-01", strtotime("-".$i." months"));
											$latestDateP1End = date("Y-m-14", strtotime("-".$i." months"));
											if($param1==''){
												echo '<option selected value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
											}
											else{
												if($param1==$latestDateP1Start.';'.$latestDateP1End){
													echo '<option selected value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
												}
												else{
													echo '<option value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
												}
											}
											
										}
									}
									else{
										$tempDateP2Start = date("Y-m-15", strtotime("-".$i." months"));
										$tempDateP2End = date("Y-m-t", strtotime("-".$i." months"));										
										$tempDateP1Start = date("Y-m-01", strtotime("-".$i." months"));
										$tempDateP1End = date("Y-m-14", strtotime("-".$i." months"));
										if($param1==$tempDateP2Start.';'.$tempDateP2End){
											echo '<option selected value="'.$tempDateP2Start.';'.$tempDateP2End.'">'.$tempDateP2Start.' - '.$tempDateP2End.'</option>';
										}
										else{
											echo '<option value="'.$tempDateP2Start.';'.$tempDateP2End.'">'.$tempDateP2Start.' - '.$tempDateP2End.'</option>';
										}
										if($param1==$tempDateP1Start.';'.$tempDateP1End){
											echo '<option selected value="'.$tempDateP1Start.';'.$tempDateP1End.'">'.$tempDateP1Start.' - '.$tempDateP1End.'</option>';
										}
										else{										
											echo '<option value="'.$tempDateP1Start.';'.$tempDateP1End.'">'.$tempDateP1Start.' - '.$tempDateP1End.'</option>';
										}
									}																		
								} */
							?>
                          </select> -->
						  <input class="form_control pull_right" type="date" name="date_from" onchange="updateTo(this)" id="date_from" value="<?=$param1?>"/>	
                        </div>
                        <label class="col-sm-1 control-label">To</label>
                        <div class="col-sm-3">
						  <input class="form_control pull_right" type="date" name="date_to" id="date_to" value="<?=$param2?>" readonly/>						  
                        </div>
                      
                        <div class="col-sm-2 payroll-search-button">
                          <button type="submit" class="btn btn-default"><i class="fa fa-calculator"></i></button>
                        </div>
                      
                    <?= form_close(); ?>

                    <hr>
					<?php if($quickpayroll_result){ ?>
                    <div class="show-calculate-payroll">

                      <br>

                      <div class="">
                        <div class="col-md-12">
                          <!-- Custom Tabs -->
                          <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                            	<!--Display one tab per week, i.e. Week 1, Week 2, Week 3-->
                              <?php
							  	$gross = 0;
								$net = 0;
							  	if($final['weeks']>0){
									for($i=0;$i<=$final['weeks'];$i++){
										if($i==0){
											echo '<li class="active"><a href="#tab_1" data-toggle="tab">Week 1</a></li>';
										}
										else{
											echo '<li><a href="#tab_'.($i+1).'" data-toggle="tab">Week '.($i+1).'</a></li>';
										}
									}	
								}
							  ?>                          
                            </ul>
                            <div class="tab-content">
                            <!--Display one tab content per week where Week 1 is active by default-->
                              <?php							 
							  	if($final['weeks']>0){
									for($i=0;$i<=$final['weeks'];$i++){
									?>
                                    	<?php
											if($i==0){
										?>
                                        	<div class="tab-pane active" id="tab_<?=($i+1)?>">
                                        <?php
											}else{
										?>
                                        	<div class="tab-pane" id="tab_<?=($i+1)?>">
                                        <?php
											}
											$total_regulars = 0;
											$total_overtimes = 0;
											$total_leaves = 0;
											$total_absences = 0;
											$total_rholidays = 0;
											$total_sholidays = 0;
										?>
                                    	
                                            <div class="row">
                                              <div class="col-md-12">
                                                <div class="">
                                                  <div class="box-body">
                                                    <table class="table table-hover">
                                                      <tr>
                                                        <th>Date</th>
                                                        <th>Regular</th>
                                                        <th style="width: 40px">Overtime</th>
                                                        <th>Leave</th>
                                                        <th>Reg Holiday</th>
                                                        <th>Sp Holiday</th>
                                                        <th>Absent</th>
                                                      </tr>
                                                      <!--We loop through all the days in the current week in the current tab for the current Employee-->
                                                       <?php
													  	
													  	if($final['timelogs'][$i]>0){
															for($j=0;$j<count($final['timelogs'][$i]);$j++){
																$total_regulars += $final['timelogs'][$i][$j]['regular'];	
																$total_overtimes += $final['timelogs'][$i][$j]['overtime'];	
																if($final['timelogs'][$i][$j]['leave']=='X') {
																	$total_leaves++;
																}	
																if($final['timelogs'][$i][$j]['rholiday']=='X') {
																	$total_rholidays++;
																}	
																if($final['timelogs'][$i][$j]['sholiday']=='X') {
																	$total_sholidays++;
																}	
																if($final['timelogs'][$i][$j]['absence']=='X') {
																	$total_absences++;
																}							
															?>
                                                            	<tr>
                                                                  <td><?=$final['timelogs'][$i][$j]['day']?></td>
                                                                  <td><?=$final['timelogs'][$i][$j]['regular']?></td>
                                                                  <td><?=$final['timelogs'][$i][$j]['overtime']?></td>
                                                                  <td>
                                                                  <?php
                                                                  	if($final['timelogs'][$i][$j]['leave']=='X'){
																	 echo '<i style="color:green" class="fa fa-check-circle"></i>';
																	}else{
																	 echo '--';
																	}
																  ?>
                                                                  </td>
                                                                  <td>
                                                                  <?php
                                                                  	if($final['timelogs'][$i][$j]['rholiday']=='X'){
																	 echo '<i style="color:green" class="fa fa-check-circle"></i>';
																	}else{
																	 echo '--';
																	}
																  ?>
                                                                  </td>
                                                                  <td>
                                                                  <?php
                                                                  	if($final['timelogs'][$i][$j]['sholiday']=='X'){
																	 echo '<i style="color:green" class="fa fa-check-circle"></i>';
																	}else{
																	 echo '--';
																	}
																  ?>
                                                                  </td>
                                                                  <td>
																  <?php
                                                                  	if($final['timelogs'][$i][$j]['absence']=='X'){
																	 echo '<i style="color:green" class="fa fa-check-circle"></i>';
																	}else{
																	 echo '--';
																	}
																  ?></td>
                                                                </tr>
                                                            <?php
															}
														}
													  ?>                                                                                                           
                                                    </table>
                                                  </div><!-- /.box-body -->
                                                </div><!-- /.box -->
                                                <div class="row">
                                                  <div class="col-xs-6">
                                                    <h4>Total Regular Hours = <?=$final['weekreg'][$i]?></h4>
                                                  </div>                                      
                                                </div>
                                                <div class="row">                                      
                                                  <div class="col-xs-6">
                                                    <h4>Total Overtime = <?=$final['weekot'][$i]?></h4>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-xs-6">
                                                    <h4>Total Leave Days = <?=$total_leaves?></h4>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-xs-6">
                                                    <h4>Total Regular Holiday = <?=$total_rholidays?></h4>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-xs-6">
                                                    <h4>Total Special Holiday = <?=$total_sholidays?></h4>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-xs-6">
                                                    <h4>Total Absences = <?=$total_absences?></h4>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-xs-6">
                                                    <h3>Pay = ₱<?=$final['weekpay'][$i]?></h3>
                                                    <?php $gross += $final['weekpay'][$i];?>
                                                  </div>
                                                </div>
                                              </div><!-- /.col -->
                                            </div>
                                          </div><!-- /.tab-pane -->
									<?php
									}	
								}
							  ?>                                                            
                            </div><!-- /.tab-content -->
                          </div><!-- nav-tabs-custom -->
                        </div><!-- /.col -->

                      </div> <!-- /.row -->

                      <hr>

                      <div class="">
                        <div class="col-xs-12">
                          <div class="box box-solid">
                            <div class="box-body">
                              <div class="box-group" id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel box box-danger">
                                  <div class="box-header with-border">
                                    <h4 class="box-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        Deductions
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="box-body">
                                      <?php									  	
										foreach($final['deductions'] as $deduction){
											echo '<h4>'.$deduction['name'].' = '.'₱'.$deduction['val'].'</h4><br/>';
										}
										echo '<h3>Total Deductions = '.'₱'.$final['total_deductions'].'</h3><br/>';										
									  ?>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel box box-danger">
                                  <div class="box-header with-border">
                                    <h4 class="box-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        Adjustments
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="box-body">
                                      <?php
									  	foreach($final['adjustments'] as $adjustment){
											echo '<h4>'.$adjustment['name'].' = '.'₱'.$adjustment['val'].'</h4><br/>';
										}
										echo '<h3>Total Adjustments = '.'₱'.$final['total_adjustments'].'</h3><br/>';		
									  ?>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel box box-primary">
                                  <div class="box-header with-border">
                                    <h4 class="box-title">
                                      Salary
                                    </h4>
                                  </div>
                                  <div>
                                    <div class="box-body">
                                      <h3>Gross Pay: ₱<?=$gross?></h3> <br/> 
                                      <h3>Total Deductions: ₱<?=$final['total_deductions']?></h3> <br/> 
                                      <h3>Total Adjustments: ₱<?=$final['total_adjustments']?></h3> <br/> 
                                      <h3>Net Pay: ₱<?=($gross-$final['total_deductions']+$final['total_adjustments'])?></h3> <br/>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div><!-- /.box-body -->
                          </div><!-- /.box -->
                        </div><!-- /.col -->
                      </div><!-- /.row -->

                      <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-4">
                           <a class="btn btn-block btn-app bg-red" href="<?=base_url('export/payroll-for-employee/15')?>/<?=urlencode($param1)?>/<?=urlencode($param2)?>">
                            <i class="fa fa-file-pdf-o"></i> Export to PDF
                          </a>
                        </div>
                      </div>

                    </div>
					<?php } ?>
                  </div>


                </div><!-- /.tab-content -->

              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.employee-management-wrapper -->