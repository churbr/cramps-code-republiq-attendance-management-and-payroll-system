<!-- Employee Management Wrapper -->
      <div class="employee-management-wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Payroll
              <small></small>
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">

            <!-- Your Page Content Here -->

            <div class="row">
              <div class="col-md-10 col-xs-12">
                <div class="box box-danger">
                  <div class="box-body">
                    <div class="row">
                      <?= form_open('admin/payroll', array('id' => 'payroll_overview_form')); ?>
                      <div class="form-group col-md-5 col-xs-12">
                        <label>From</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>                            
                          <!--<select class="form-control pull-right" id="payperiod" name="payperiod">
                            <?php /*
                $curDate = date("Y-m-d");
                $curDateMid = date("Y-m-15");
                for($i=0;$i<=12;$i++){ //How many months ago the Drop down for payroll overview allows
                  if($i==0){
                    if($curDate>=$curDateMid){
                      $latestDateP2Start = date("Y-m-15", strtotime("-".$i." months"));
                      $latestDateP2End = date("Y-m-t", strtotime("-".$i." months"));
                      $latestDateP1Start = date("Y-m-01", strtotime("-".$i." months"));
                      $latestDateP1End = date("Y-m-14", strtotime("-".$i." months"));
                      if($param1==''){
                        echo '<option selected value="'.$latestDateP2Start.';'.$latestDateP2End.'">'.$latestDateP2Start.' - '.$latestDateP2End.'</option>';
                      }
                      else{
                        if($param1==$latestDateP2Start.';'.$latestDateP2End){
                          echo '<option selected value="'.$latestDateP2Start.';'.$latestDateP2End.'">'.$latestDateP2Start.' - '.$latestDateP2End.'</option>';
                        }
                        else{                                           
                          echo '<option value="'.$latestDateP2Start.';'.$latestDateP2End.'">'.$latestDateP2Start.' - '.$latestDateP2End.'</option>';
                        }
                      }
                      if($param1==$latestDateP1Start.';'.$latestDateP1End){
                        echo '<option selected value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
                      }
                      else{
                        echo '<option value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
                      }
                    }
                    else{
                      $latestDateP1Start = date("Y-m-01", strtotime("-".$i." months"));
                      $latestDateP1End = date("Y-m-14", strtotime("-".$i." months"));
                      if($param1==''){
                        echo '<option selected value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
                      }
                      else{
                        if($param1==$latestDateP1Start.';'.$latestDateP1End){
                          echo '<option selected value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
                        }
                        else{
                          echo '<option value="'.$latestDateP1Start.';'.$latestDateP1End.'">'.$latestDateP1Start.' - '.$latestDateP1End.'</option>';
                        }
                      }
                      
                    }
                  }
                  else{
                    $tempDateP2Start = date("Y-m-15", strtotime("-".$i." months"));
                    $tempDateP2End = date("Y-m-t", strtotime("-".$i." months"));                    
                    $tempDateP1Start = date("Y-m-01", strtotime("-".$i." months"));
                    $tempDateP1End = date("Y-m-14", strtotime("-".$i." months"));
                    if($param1==$tempDateP2Start.';'.$tempDateP2End){
                      echo '<option selected value="'.$tempDateP2Start.';'.$tempDateP2End.'">'.$tempDateP2Start.' - '.$tempDateP2End.'</option>';
                    }
                    else{
                      echo '<option value="'.$tempDateP2Start.';'.$tempDateP2End.'">'.$tempDateP2Start.' - '.$tempDateP2End.'</option>';
                    }
                    if($param1==$tempDateP1Start.';'.$tempDateP1End){
                      echo '<option selected value="'.$tempDateP1Start.';'.$tempDateP1End.'">'.$tempDateP1Start.' - '.$tempDateP1End.'</option>';
                    }
                    else{                   
                      echo '<option value="'.$tempDateP1Start.';'.$tempDateP1End.'">'.$tempDateP1Start.' - '.$tempDateP1End.'</option>';
                    }
                  }                                   
                }*/
              ?>
                          </select>  -->
              <input class="form-control pull-right" type="date" name="date_from" onchange="updateTo(this)" id="date_from" value="<?=$param1?>"/>             
                        </div>
                      </div>
                      <div class="form-group col-md-5 col-xs-12">
                        <label>To</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
              <input class="form-control pull-right" type="date" name="date_to" id="date_to" value="<?=$param2?>" readonly/>              
                        </div>
                      </div>
                      <div class="form-group col-md-2 col-xs-12">
                        <label>Calculate</label>
                        <div class="input-group">
                          <button type="submit" class="btn btn-default"><i class="fa fa-calculator"></i></button>                          
                        </div>
                      </div>
                      <?= form_close(); ?>
                    </div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
              <div class="col-xs-12">
                <div class="box box-danger">
                  <div class="box-body">
                    <table id="example1" class="table table-hover" style="font-size:12">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Type</th>                          
                          <th>Regular</th>
                          <th>O.T.</th>
                          <th>R.H. Work</th>
                          <th>S.H. Work</th>
                          <th>Leaves</th>
                          <th>Absences</th>
                          <th>Gross</th>
                          <th>Deductions/ Adjustments</th>
                          <th>Net Pay</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
              foreach($payroll_overview as $workarea){
            ?>
                          <tr>
                              <td><?=$workarea['firstname']?> <?=$workarea['lastname']?></td>
                              <?php
                  if($workarea['type']=='regular_employee'){
                  echo '<td>Emp</td>';
                }
                else if($workarea['type']=='board_of_director'){
                  echo '<td>BOD</td>';
                }
                else if($workarea['type']=='project_manager'){
                  echo '<td>PM</td>';
                }
                else if($workarea['type']=='admin'){
                  echo '<td>Admin</td>';
                }
                else if($workarea['type']=='super_admin'){
                  echo '<td>Super Admin</td>';
                }
                ?>                                                         
                              <td><?=$workarea['regular']?>
                              <?php
                  if($workarea['regular_texts']<>''){
                  echo '<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="'. $workarea['regular_texts'].'"></i>';
                }
                ?>
                              </td>
                              <td><?=$workarea['overtime']?>
                              <?php
                  if($workarea['ot_texts']<>''){
                  echo '<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="'.$workarea['ot_texts'].'"></i>';
                }
                ?>
                              </td>
                              <td><?=$workarea['regular_holiday']?>
                              <?php
                  if($workarea['rholiday_texts']<>''){
                  echo '<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="'.$workarea['rholiday_texts'].'"></i>';
                }
                ?>
                              </td>
                              <td><?=$workarea['special_holiday']?>
                              <?php
                  if($workarea['sholiday_texts']<>''){
                  echo '<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="'.$workarea['sholiday_texts'].'"></i>';
                }
                ?>
                              </td>
                              <td><?=$workarea['leave']/8?>
                              <?php
                  if($workarea['leave_texts']<>''){
                  echo '<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="'.$workarea['leave_texts'].'"></i>';
                }
                ?>
                              </td>
                              <td><?=$workarea['absence']/8?>
                              <?php
                  if($workarea['absence_texts']<>''){
                  echo '<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="'.$workarea['absence_texts'].'"></i>';
                }
                ?>
                              </td>
                              <td>₱<?=$workarea['gross']?></td>
                              <td>₱<?= abs($workarea['deductions']); ?>
                              <?php
                  if($workarea['deduction_texts']<>''){
                  echo '<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="'. 'Deductions: <br />';
                  echo $workarea['deduction_texts'];
                  echo '"></i>';
                  // CHUCHUCHU
                }
                ?>
                              </td>
                              <td>
                                <?php
                                  if($workarea['netpay'] < 0) {
                                    echo '₱ 0';
                                  }else {
                                    echo '₱' . $workarea['netpay'];
                                  }
                                ?>
                              </td>
                              <td>
                                <div class="btn-group">
                                  <a class="btn btn-info btn-sm" href="<?=base_url('admin/payroll-for-employee').'/'.$workarea['id']?>">Manage</a>
                                </div>
                              </td>
                            </tr>
                        <?php
              }
            ?>                        
                      </tbody>
                    </table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
              <div class="col-md-2 col-sm-8 col-xs-6">
                <div class="box box-danger">
                  <div class="box-body">
                    <div class="col-xs-12">
                      <button class="btn btn-block btn-default btn-sm print-payroll"><i class="fa fa-fw fa-print"></i>Print</button>
                    </div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- /.col -->
            </div><!-- /.row -->

          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.employee-management-wrapper -->


    <script type="text/javascript">
    $('.print-payroll').on('click',function(){
    printReports();
    })
    </script>
