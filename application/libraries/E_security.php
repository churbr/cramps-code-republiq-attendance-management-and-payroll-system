<?php if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class E_security {

		public function generate_cookie() {

			$characters = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5,6,7,8,9,0';
			$characters_array = explode(',', $characters);
			shuffle($characters_array);
			
			return implode('', $characters_array);
		}

		// --------------------------------------------------------------------------

		public function check_login() {
			$CI = & get_instance();
			$CI->load->library('e_user');

			if($CI->e_user->is_remembered(get_cookie('hash')) || $CI->session->is_loggedin) {
				self::redirect_as($CI->session->type);
			}
		}

		// --------------------------------------------------------------------------

		public function only_allow($allowed_employee_type = null) {
			$CI = & get_instance();

			if(strcmp($CI->session->type, $allowed_employee_type) !== 0) {
				self::redirect_as($CI->session->type);
			}
		}

		// --------------------------------------------------------------------------

		public function redirect_as($employee_type = null) {
			$CI = & get_instance();

			$url_suffix = $CI->config->item('url_suffix');
			$page = null;

			switch ($employee_type) {

				case 'super_admin':
					$page = 'superadmin/home' . $url_suffix;
				break;

				case 'admin':
					$page = 'admin/home' . $url_suffix;
				break;

				case 'project_manager':
					$page = 'pm/home' . $url_suffix;
				break;

				case 'board_of_director':
					$page = 'bod/home' . $url_suffix;
				break;

				case 'regular_employee':
					$page = 'home' . $url_suffix;
				break;

				default:
					$page = 'index' . $url_suffix;
				break;
			}

			redirect(base_url($page));
		}

		// --------------------------------------------------------------------------

		public function valid_phoneNumber($contact = null) {

			if(!is_null($contact)) {
				if(preg_match('#^(9){1}([0-9]){9}$#', $contact) == 1) { return true; }
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function is_validName($name = null) {

			if(!is_null($name)) {
				if(preg_match('#^([a-zA-Z-\s]){2,}$#', $name) == 1) { return true; }
			}
			
			return false;
		}

		// --------------------------------------------------------------------------

		public function check_password($password = null) {

			if(!is_null($password)) {
				if(strlen($password) >= 8 && strlen($password) <= 30) { return true; }
			}
			
			return false;
		}

		// --------------------------------------------------------------------------

		public function is_validEmail($email = null) {

			if(!is_null($email) && preg_match('#^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$#', $email)) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function is_validDate($date = null) {

			if(!is_null($date) && preg_match("#^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$#",$date)) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function is_validAddress($address = null) {
			
			if(!is_null($address)) {
				if(preg_match('#^([a-zA-Z0-9,\-\.\s]){10,}$#', $address)) { return true; }
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function external_ip() {
			$external_ip = file_get_contents("http://ipecho.net/plain");

			return $external_ip;
		}

		// --------------------------------------------------------------------------

		public function password_strength( $password = null ) {

		/**
		 *		--=	PREDEFINED CHARACTER CLASSES OR LITERALS ==-
		 *
		 *		.	Any character (may or may not match line terminators)
		 *		\d	A digit: [0-9]
		 *		\D	A non-digit: [^0-9]
		 *		\s	A whitespace character: [ \t\n\x0B\f\r]
		 *		\S	A non-whitespace character: [^\s]
		 *		\w	A word character: [a-zA-Z_0-9]
		 *		\W	A non-word character: [^\w]
		 **/

			$pass_strength = 'weak';


		/**
		 *	STRONG PASSWORD : ^   (?=.*[a-z])   (?=.*[A-Z])   (?=.*[0-9])   (?=.*[!@#$);_:^&,(*\.\s])   .   {9,}   $
		 *	A strong password must contain atleast one lowercase letter, one uppercase letter,
		 *	numbers and symbols or special characters which is specified within the brackets.
		 **/

		/**
		 * MEDIUM PASSWORD : ^   ([a-zA-Z])   (?=.*[0-9])   .   {9,}   $
		 * A medium security level password contains lowercase letters or uppercase letters and must contain atleast one number.
		 **/

		/**
		 *	WEAK PASSWORD : ^   [a-zA-Z]   {9,}   $
		 *	Weak passwords are those that only contain letters either uppercase or lowercase.
		**/

			if(!is_null($password)) {
				if(strlen($password) >= 9) {
					
					if(preg_match('/^[a-zA-Z]{9,}$/', $password)) { $pass_strength = 'weak'; }
					if(preg_match('/^([a-zA-Z])(?=.*[0-9]).{9,}$/', $password)) { $pass_strength = 'medium'; }
					if(preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$);_:^&,(*\.\s]).{9,}$/', $password)) { $pass_strength = 'strong'; }

				}else {
					$pass_strength = 'too_short';
				}
			}

			return $pass_strength;

		}

		// --------------------------------------------------------------------------

	}