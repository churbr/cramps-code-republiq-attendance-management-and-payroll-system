$(function () {
	$('#reqfeed_table').DataTable({
	    'columnDefs': [{
	        'targets': [0],
	        'visible': false,
	        'searchable': false
	    }],

	    'order': [[ 0, 'desc' ]]
	});

	$('#timelog_table').DataTable({
	    'columnDefs': [{
	        'targets': [0],
	        'visible': false,
	        'searchable': false
	    }],

	    'order': [[ 0, 'desc' ]]
	  });


	$('#holiday_table').DataTable({});

	$('#hp_timelog_table').DataTable({
	    'columnDefs': [{
	        'targets': [0],
	        'visible': false,
	        'searchable': false
	    }],

	    'order': [[ 0, 'desc' ]]
	  });

	$("#example1").DataTable();
	
	$('#example2').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": true,
		"info": true,
		"autoWidth": false
	});

});