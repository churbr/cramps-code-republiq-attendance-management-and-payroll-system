<!-- Modals -->

<div class="modal" id="employee_notification_overtime_modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button id='hp_view_modal_close' type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"><span id='hp_name'></span>
				<small>Overtime</small>
				</h3>
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-xs-4">
						<h4>From: <span id='ot_date_from'></span> </h4>
					</div>
					
					<div class="form-group col-xs-4">
						<h4>To: <span id='ot_date_to'></span> </h4>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-xs-4">
						<h4>Start: <span id='ot_start_time'></span> </h4>
					</div>
					
					<div class="form-group col-xs-4">
						<h4>End: <span id='ot_end_time'></span> </h4>
					</div>				
				</div>
				
				<div class="row">
					<div class="form-group col-xs-12">
						<h3>Total Time: <span id='ot_total_time'></span> </h3>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-xs-12">
						<p id='ot_reason'></p>
						<input type='hidden' id='overtime_id' />
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
				<h5 class="pull-right text-red" id='ot_status'></h5>
				<h5 class="pull-left text-red">Date Filed: <span id='date_filed'></span> </h5>
			</div>
			
			<div class="modal-body" id='hp_ot_comfirm_buttons'>
				<div class="row">
					<div class="col-sm-3 col-xs-4 pull-right">
						<button class="btn btn-block btn-danger" href="#request-decline-prompt" data-toggle="modal">
							<i class="fa fa-fw fa-close"></i> Decline
						</button>
					</div>
					
					<div class="col-sm-3 pull-right">
						<button class="btn btn-block btn-success" href="#leave-request-approve-prompt" data-toggle="modal">
							<i class="fa fa-fw fa-check"></i> Approve
						</button>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="modal" id="request-decline-prompt" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class="fa fa-warning"></i></h4>
			</div>
			
			<div class="modal-body text-centered">
				<div class="form-group">
					<h4 class="text-red">You are about to decline this overtime application, leave a note?</h4>
					<textarea id='ot_leave_note' class="form-control" rows="3" placeholder="Your note here..."></textarea>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn primary" id='hp_declined_ot' data-dismiss="modal">Confirm</button>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-default" id="leave-request-approve-prompt" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class="fa fa-warning"></i></h4>
			</div>
			
			<div class="modal-body text-centered">
				<h4 class="text-red">You are about to approve this application.</h4>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-default" id='hp_approved_ot' data-dismiss="modal">Confirm</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->

<div class="content-wrapper ">
	<div class="container">

		<section class="content-header">
		</section>

		<section class="content">


			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-aqua">
						<span class="info-box-icon"><i class="fa fa-feed"></i></span>
							<div class="info-box-content">
							<span class="info-box-text">Application Feed</span>
						
							<div class="progress">
								<div class="progress-bar" style="width: 100%"></div>
							</div>
							
							<span class="progress-description">
								<?php
									if($overtime_pending_count > 0) {
										$word = ($overtime_pending_count > 1) ? 'applications' : 'application';
										echo $overtime_pending_count . ' pending ' . $word;
									}else { echo 'No pending application'; }
								?>
							</span>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12">
					
					<div class="box box-primary">
						<div class="box-body">

							<table id="reqfeed_table" class="table table-hover">
								<thead>
									<tr>
										<th></th>
										<th>Date Filed</th>
										<th>Name</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

									<?php
										if($employee_overtime_request_list === FALSE) {
											echo "
												<tr>
													<td style='display: none;'></td>
													<td><p class='error'>No record.</p></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											";
										} else {

											foreach ($employee_overtime_request_list as $index => $application) {

												$id = 'id="overtime_tr_' . $application['overtime_id'] . '"';

												if($application['overtime_status'] == 'approved') {
													$overtime_status = "<span class='label label-success'>Approved</span>";
												} elseif($application['overtime_status'] == 'pending') {
													$overtime_status = "<span class='label label-warning'>Pending</span>";
												} else {
													$overtime_status = "<span class='label label-danger'>Declined</span>";
												}

												if($application['seen_status'] == 0) { $seen_status = 'class="bg-gray"'; }
												else { $seen_status = ''; }
?>
													<tr <?= $seen_status . ' ' . $id; ?>>
														<td></td>
														<td> <?= nice_date($application['date_filed'], 'M d, Y'); ?> </td>
														<td> <?= $application['firstname'] . ' ' . substr($application['middlename'], 0, 1) . '' . $application['lastname']; ?> </td>
														<td id='status'> <?= $overtime_status; ?> </td>
														<td>
															<div class="btn-group">
																<a class="btn btn-info btn-sm" onclick="employee_notification_overtime_modal(<?= $application['overtime_id'];?>)" data-toggle="modal">View</a>
															</div>
														</td>
													</tr>
<?php

											}

										}
?>

								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>


		</section>
	</div>
</div>