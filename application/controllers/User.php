<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class User extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->model(array('admin_db', 'employee', 'timelog'));
			$this->load->library(array('e_security', 'e_attendance'));
		}

		// ------------------------------------------------------------------------------

		public function check_oldpass() {

			if($this->input->post('oldpassword')) {

				$oldpass = $this->employee->get_password($this->session->username);
				$oldpass_input = $this->input->post('oldpassword');

				if(strcmp($oldpass_input, $oldpass) === 0) { echo 'success'; }
				else { echo 'fail'; }

			}else {
				echo 'fail';
			}
		}

		// ------------------------------------------------------------------------------

		public function check_newpass() {

			if($this->input->post('newpassword')) {

				$password = $this->input->post('newpassword');
				$password_strength = $this->e_security->password_strength($password);

				if(!is_null($password_strength)) {
					echo $password_strength;
				}
			}
		}

		// ------------------------------------------------------------------------------

		public function change_password() {

			if($this->session->is_loggedin) {

				$status = null;
				$oldpass_input = $this->input->post('oldpass');
				$newpass_input = $this->input->post('newpass');
				$confirmpass_input = $this->input->post('confirmpass');

				$db_oldpass = $this->employee->get_password($this->session->username);
				$strength_newpass = $this->e_security->password_strength($newpass_input);
				$strength_confirmpass = $this->e_security->password_strength($confirmpass_input);


				if($oldpass_input&&$newpass_input&&$confirmpass_input) {
					if(strcmp($oldpass_input, $db_oldpass) === 0) {
						if(strcmp($newpass_input, $confirmpass_input) === 0) {

							if($strength_newpass == 'weak' || $strength_newpass == 'too_short' && $strength_confirmpass == 'weak' || $strength_confirmpass == 'too_short') {
								$status = "<p class='error'>Password strength should be atleast medium.</p>";
							}else {
								$employee_id = $this->session->id;

								if($this->employee->change_password($employee_id, $newpass_input)) {
									$status = 'success';
								}
							}
						}
					}
				}else {
					$status = "<p class='error'>Complete the form.</p>";
				}
			}

			echo $status;
		}

		// ------------------------------------------------------------------------------

		public function update_info() {

			$this->form_validation->set_rules('address', 'Address', 'required|trim|min_length[10]');
			$this->form_validation->set_rules('contact', 'Contact Number', 'required|numeric|trim|xss_clean|min_length[10]');
			$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|min_length[8]');
			$no_problem = TRUE;

			if(!$this->e_security->is_validAddress($this->input->post('address'))) {
				$no_problem = FALSE;
			}

			if(!$this->e_security->valid_phoneNumber($this->input->post('contact'))) {
				$no_problem = FALSE;
			}

			if(!$this->e_security->is_validEmail($this->input->post('email'))) {
				$no_problem = FALSE;
			}

			$info = array(
				'address' => $this->input->post('address'),
				'contact' => $this->input->post('contact'),
				'email' => $this->input->post('email')
			);

			if($this->form_validation->run() == TRUE && $no_problem) {
				if($this->employee->update_info($info)) {
					echo 'true';
				}
			}else { echo 'false'; }
		}

		// ------------------------------------------------------------------------------

		public function info() { // Returns the employee_id and type of employee
			if($this->session->is_loggedin && $this->input->post('request')) {

				$info = array(
					'employee_id' => $this->session->id,
					'type' => $this->session->type
				);

				echo json_encode($info);
			}
		}

		// ------------------------------------------------------------------------------

		public function validate_address() {
			$this->form_validation->set_rules('address', 'Address', 'required|trim|min_length[10]');

		    if ($this->form_validation->run() == TRUE)  {
				if($this->e_security->is_validAddress($this->input->post('address'))) {
					echo 'true';
				}else { echo 'Address is invalid format'; }
		    } else { echo form_error('address'); }
		}

		// ------------------------------------------------------------------------------

		public function validate_contact() {
			$this->form_validation->set_rules('contact', 'Contact Number', 'required|numeric|trim|xss_clean|min_length[10]');

		    if ($this->form_validation->run() == TRUE) {
				if($this->e_security->valid_phoneNumber($this->input->post('contact'))) {
					echo 'true';
				}else { echo 'Contact number is invalid format.'; }
		    } else { echo form_error('contact'); }
		}

		// ------------------------------------------------------------------------------

		public function validate_email() {
			$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|min_length[8]');

		    if ($this->form_validation->run() == TRUE) {
				if($this->e_security->is_validEmail($this->input->post('email'))) {
					echo 'true';
				}else { echo 'Invalid email.'; }
		    } else { echo form_error('email'); }
		}

		// ------------------------------------------------------------------------------

		public function get_info() {

			$employee_id = $this->input->post('employee_id');
			$employee_data = $this->employee->get_basic_info( $employee_id );

			if($employee_data !== FALSE) {
				echo json_encode($employee_data);
			}else {
				echo 'false';
			}
		}

		// ------------------------------------------------------------------------------

		public function admin_update_info() {

			if($this->input->post('update_submitted')) {

				$employee_id = $this->input->post('employee_id');

				$info_updates = array (
					'username'		=> $this->input->post('username'),
					'firstname' 	=> $this->input->post('firstname'),
					'middlename'	=> $this->input->post('middlename'),
					'lastname'		=> $this->input->post('lastname'),
					'gender'		=> $this->input->post('gender'),
					'birthday'		=> $this->input->post('birthday'),
					'address'		=> $this->input->post('address'),
					'contact'		=> $this->input->post('contact'),
					'email'			=> $this->input->post('email'),
					'date_hired'	=> $this->input->post('date_started')
				);

				$this->admin_db->update_info($info_updates, $employee_id);
				$this->admin_db->update_position($employee_id, $this->input->post('position'));
				$this->admin_db->update_skill($employee_id, $this->input->post('skill'));
				
				$this->e_security->only_allow('admin');
				$data = $this->employee->get_overall_info( $this->session->id );
				$data['employee_position'] = $this->employee->get_position();
				$data['position_list'] = $this->admin_db->get_position();
				$data['employee_skills'] = $this->employee->skills( $employee_id );
				$data['employee_skill_id'] = $this->admin_db->get_employee_skill_id( $employee_id );
				$data['skill_list'] = $this->admin_db->get_skill();
				$data['deactivation_detail'] = $this->admin_db->deactivation_detail( $employee_id );
				$data['employee_detail'] = $this->admin_db->get_employee_info( $employee_id );
				$data['get_position_id'] = $this->admin_db->get_position_id( $employee_id );

				$this->load->view('admin/header', $data);
				$this->load->view('admin/modal');
				$this->load->view('admin/nav');
				$this->load->view('admin/employee_profile');
				$this->load->view('admin/footer');

			} else {

				$this->e_security->only_allow('admin');
				$data = $this->employee->get_overall_info($this->session->id);
				$data['employee_list'] = $this->admin_db->get_all_employees();
				$data['employee_position'] = $this->employee->get_position();
				$data['page_title'] = 'Home';

				$this->load->view('admin/header', $data);
				$this->load->view('admin/modal');
				$this->load->view('admin/nav');
				$this->load->view('admin/employee_management');
				$this->load->view('admin/footer');
			}

		}

		// ------------------------------------------------------------------------------

		public function admin_change_password() {

			$employee_id = $this->input->post('employee_id');
			$newpass_input = $this->input->post('newpass');
			$confirmpass_input = $this->input->post('confirmpass');

			$strength_newpass = $this->e_security->password_strength($newpass_input);
			$strength_confirmpass = $this->e_security->password_strength($confirmpass_input);
			$status = null;


			if($newpass_input&&$confirmpass_input) {
				if(strcmp($newpass_input, $confirmpass_input) === 0) {

					if($strength_newpass == 'weak' || $strength_newpass == 'too_short' && $strength_confirmpass == 'weak' || $strength_confirmpass == 'too_short') {
						$status = "<p class='error'>Password strength should be atleast medium.</p>";
					}else {

						if($this->employee->change_password($employee_id, $newpass_input)) {
							$status = 'success';
						}
					}
				}
			}else {
				$status = "<p class='error'>Complete the form.</p>";
			}

			echo $status;
		}

		// ------------------------------------------------------------------------------

		public function assign_pm() {

			if($this->input->post('admin_assign_pm')) {

				$employee_id = $this->input->post('employee_id');
				$this->e_security->only_allow('admin');
				$this->admin_db->admin_assign_pm($employee_id, $this->input->post('project_manager_list'));
				$data = $this->employee->get_overall_info( $this->session->id );
				$data['employee_position'] = $this->employee->get_position();
				$data['employee_skills'] = $this->employee->skills( $employee_id );
				$data['employee_skill_id'] = $this->admin_db->get_employee_skill_id( $employee_id );
				$data['assigned_to_pm_id'] = $this->admin_db->assigned_to_pm_id($employee_id);
				$data['skill_list'] = $this->admin_db->get_skill();
				$data['position_list'] = $this->admin_db->get_position();
				$data['project_manager_list'] = $this->admin_db->pm_list();
				$data['deactivation_detail'] = $this->admin_db->deactivation_detail( $employee_id );
				$data['employee_detail'] = $this->admin_db->get_employee_info( $employee_id );
				$data['get_position_id'] = $this->admin_db->get_position_id( $employee_id );
				$data['employee_leave_detail'] = $this->admin_db->get_leave_detail( $employee_id );

				

				$this->load->view('admin/header', $data);
				$this->load->view('admin/modal');
				$this->load->view('admin/nav');
				$this->load->view('admin/employee_profile');
				$this->load->view('admin/footer');

			}

		}

		// ------------------------------------------------------------------------------

		public function admin_update_leave() {
			if($this->input->post('gender')) {

				$employee_id = $this->input->post('employee_id');

				if($this->input->post('gender') == 'male') {
					$sl_days = $this->input->post('sick_leave'); // 1
					$vl_days = $this->input->post('vacation_leave'); // 2
					$pl_days = $this->input->post('paternity_leave'); // 3		

					$leave_data = array (
					   array (
					      'leave_type_id' => 1,
					      'employee_id' => $employee_id,
					      'remaining_days' => $sl_days
					   ),

					   array(
					      'leave_type_id' => 2,
					      'employee_id' => $employee_id,
					      'remaining_days' => $vl_days
					   ),

					   array(
					      'leave_type_id' => 3,
					      'employee_id' => $employee_id,
					      'remaining_days' => $pl_days
					   )
					);

					$this->admin_db->set_leave_days($employee_id = null, $leave_data);

				} else {
					$sl_days = $this->input->post('sick_leave');
					$vl_days = $this->input->post('vacation_leave');
					$ml_days = $this->input->post('maternity_leave'); // 4

					$leave_data = array (
					   array (
					      'leave_type_id' => 1,
					      'employee_id' => $employee_id,
					      'remaining_days' => $sl_days
					   ),

					   array(
					      'leave_type_id' => 2,
					      'employee_id' => $employee_id,
					      'remaining_days' => $vl_days
					   ),

					   array(
					      'leave_type_id' => 4,
					      'employee_id' => $employee_id,
					      'remaining_days' => $ml_days
					   )
					);

					$this->admin_db->set_leave_days($employee_id = null, $leave_data);
				}

			}

			echo 'true';

		}

		// ------------------------------------------------------------------------------


		// CHUCHUCHU

		public function add_holiday() {
			$holiday_date = $this->input->post('holiday_date');
			$holiday_type = $this->input->post('holiday_type');
			$holiday_name = $this->input->post('holiday_name');

			if($holiday_date&&$holiday_type&&$holiday_name) {

				if($this->admin_db->insert_holiday( $holiday_name, $holiday_type, $holiday_date)) {
					echo 'success';
				}else {
					echo 'failed';
				}

			} else { echo "Please complete the form."; }
		}


		public function view_holiday_detail() {

			$holiday_id = $this->input->post('table_pk');
			$holiday_detail = $this->admin_db->get_holiday_data( $holiday_id );

			echo json_encode($holiday_detail);
		}

		public function admin_update_holiday() {

			$holiday_date = $this->input->post('holiday_date');
			$holiday_name = $this->input->post('holiday_name');
			$holiday_pk = $this->input->post('holiday_pk');
			$holiday_type = $this->input->post('holiday_type');

			$this->admin_db->update_holiday($holiday_pk, $holiday_name, $holiday_type, $holiday_date);

			echo 'true';
		}


		public function admin_remove_holiday() {
			$holiday_pk = $this->input->post('holiday_pk');
			$this->admin_db->remove_holiday($holiday_pk);

			echo 'true';
		}

		// CHUCHUHCU


}