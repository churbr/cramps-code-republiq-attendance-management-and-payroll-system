<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Admin_db extends CI_Model {

		public function __construct() {
			parent::__construct();
		}

		// ------------------------------------------------------------------------

		public function get_all_employees() {
			$this->db->where('id !=', $this->session->id);
			$query = $this->db->get('employee');

			if($query->num_rows()) { return $query->result_array(); }
			return false;
		}

		// ------------------------------------------------------------------------

		public function get_employee_info( $employee_id = null ) {
			$this->db->where('id', $employee_id);
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}

		// ------------------------------------------------------------------------

		public function deactivation_detail( $employee_id = null ) {
			$this->db->where('employee_id', $employee_id);
			$query = $this->db->get('deactivate');

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}

		// ------------------------------------------------------------------------

		public function get_skill() {
			$this->db->select('id, title');
			$query = $this->db->get('skill');

			if($query->num_rows()) { return $query->result_array(); }
			return false;
		}

		// ------------------------------------------------------------------------

		public function get_employee_skill_id($employee_id = null) {
			$this->db->select('skill_id');
			$this->db->where('employee_id', $employee_id);
			$query = $this->db->get('employee_skill');

			if($query->num_rows()) { return $query->result_array(); }
			return false;
		}

		// ------------------------------------------------------------------------


		public function get_position() {
			$this->db->select('id, title');
			$query = $this->db->get('position');

			if($query->num_rows()) { return $query->result_array(); }
			return false;
		}

		// ------------------------------------------------------------------------

		public function get_position_id( $employee_id = null ) {
			$this->db->select('position_id');
			$this->db->where('employee_id', $employee_id);
			$query = $this->db->get('employee_position');

			if($query->num_rows()) { return $query->result_array()[0]; }
			return false;
		}		

		// ------------------------------------------------------------------------

		public function update_info($updates = null, $employee_id = null) {

			if(!is_null($updates)) {
				$this->db->where('id', $employee_id);
				$this->db->update('employee', $updates);
				
				if($this->db->affected_rows()) {
					return true;
				}
			}

			return false;
		}

		// ------------------------------------------------------------------------

		public function update_position($employee_id = null, $position_id = null) {

			$this->db->where('employee_id', $employee_id);
			$this->db->update('employee_position', array('position_id' => $position_id));
			
			if($this->db->affected_rows()) {
				return true;
			}

			return false;
		}

		// ------------------------------------------------------------------------

		public function update_skill($employee_id = null, $skills = array()) {

			$skill_info = array();

			if( self::get_employee_skill_id($employee_id) !== FALSE ) {
				$employee_skill_id = array_column(self::get_employee_skill_id($employee_id), 'skill_id');
			}else {
				$employee_skill_id = NULL;
			}

			if(count($skills)) {

				if(!is_null($employee_skill_id)) {
					foreach ($skills as $index => $skill) {
						if(!in_array($skill, $employee_skill_id)) {
							$skill_info[] = array (
								'employee_id' => $employee_id,
								'skill_id' => $skill
							);
						}
					}
				} else {
					foreach ($skills as $index => $skill) {
						$skill_info[] = array (
							'employee_id' => $employee_id,
							'skill_id' => $skill
						);
					}
				}

				if(count($skill_info)) { $this->db->insert_batch('employee_skill', $skill_info); }
			}
		}

		// ------------------------------------------------------------------------

		public function pm_list() {
			$this->db->select('id, firstname, lastname');
			$this->db->where('type', 'project_manager');
			$query = $this->db->get('employee');

			if($query->num_rows()) { return $query->result_array(); }
			return false;
		}

		// ------------------------------------------------------------------------

		public function assigned_to_pm_id($employee_id = null) {
			$this->db->select('pm_id');
			$this->db->where('employee_id', $employee_id);
			$this->db->where('status', 1);
			$query = $this->db->get('manage');

			if($query->num_rows()) { return array_column( $query->result_array(), 'pm_id' ); }
			return false;
		}

		// ------------------------------------------------------------------------

		public function admin_assign_pm($employee_id = null, $pm = array()) {

			if(count($pm)) {
				$this->db->where('employee_id', $employee_id);
				$this->db->update('manage', array('status' => 0));
				$pm_list = array();

				foreach ($pm as $index => $pm_id) {
					$pm_list[] = array (
						'employee_id' => $employee_id,
						'pm_id' => $pm_id,
						'date' => date('Y-m-d'),
						'status' => 1
					);
				}

				$this->db->insert_batch('manage', $pm_list);
			}else {

				$this->db->where('employee_id', $employee_id);
				$this->db->update('manage', array('status' => 0));
				$pm_list = array();


			}

		}

		// ------------------------------------------------------------------------

		public function get_leave_detail($employee_id = null) {
			$this->db->select('`leave_setting`.*, `leave_type`.*');
			$this->db->from('`leave_type`');
			$this->db->join('`leave_setting`', '`leave_type`.`id` = `leave_setting`.`leave_type_id`');
			$this->db->where('`leave_setting`.`employee_id`', $employee_id);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}

		// ------------------------------------------------------------------------

		public function set_leave_days($employee_id = null, $leave_days = array()) {
			//$this->db->update_batch('leave_setting', $leave_days, 'employee_id');
			foreach($leave_days as $leave){
				//echo $leave['leave_type_id'].' Emp: '.$leave['employee_id'].' Rem: '.$leave['remaining_days'].'<br/>';
				$this->db->where('leave_type_id', $leave['leave_type_id']);
				$this->db->where('employee_id', $leave['employee_id']);
				$this->db->update('leave_setting', array('remaining_days' => $leave['remaining_days']));
			}
		}

		// ------------------------------------------------------------------------


		// CHUCHUCHU

		public function insert_holiday($name = null, $type = null, $date = null) {

			$this->db->insert('holiday', array (
				'id' => NULL,
				'name' => $name,
				'type' => $type,
				'date' => $date,
				'status' => 1
			));

			if($this->db->affected_rows()) { return true; }
			return false;
		}

		public function get_holiday_data( $holiday_id ) {
			$return_data = array();

			$this->db->where('id', $holiday_id);
			$query = $this->db->get('holiday');

			if($query->num_rows()) {
				$data = $query->result_array()[0];

				$return_data = array(
					'id' => $data['id'],
					'name' => $data['name'],
					'type' => $data['type'],
					'date' => $data['date'],
					'status' => $data['status'],
				);

				return $return_data;
			}

			return false;
		}

		public function update_holiday($id = null, $name = null, $type = null, $date = null) {
			
			$data = array(
				'name' => $name,
				'type' => $type,
				'date' => $date
			);

			$this->db->where('id', $id);
			$this->db->update('holiday', $data);

			if($this->db->affected_rows()) { return true; }

			return false;
		}

		public function remove_holiday($holiday_id) {

			$this->db->where('id', $holiday_id);
			$query = $this->db->update('holiday', array('status' => 0));

			if($this->db->affected_rows()) { return true; }
			return false;
		}

		// CHUCHUCHU



	}