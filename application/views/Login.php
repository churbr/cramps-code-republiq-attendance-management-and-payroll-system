<!DOCTYPE HTML>
<html>
	<head>
		<title>CRAMPS</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="icon" href="<?= base_url('resources/landing/images/favicon/cr.ico'); ?>" type="image/x-icon" />
		<link rel="stylesheet" href="<?= base_url('resources/landing/assets/css/bootstrap.min.css'); ?>" />
		<link rel="stylesheet" href="<?= base_url('resources/landing/assets/css/AdminLTE.min.css'); ?>" />
		<link rel="stylesheet" href="<?= base_url('resources/landing/assets/css/main.css'); ?>" />
		<link rel="stylesheet" href="<?= base_url('resources/landing/assets/css/custom.css'); ?>" />
		<link rel="stylesheet" href="<?= base_url('resources/landing/assets/css/media.css'); ?>" />
		<link rel="stylesheet" href="<?= base_url('resources/landing/assets/plugins/iCheck/square/blue.css'); ?>" />
		<script src="<?= base_url('resources/landing/assets/js/jquery.min.js'); ?>"></script>
		<script src="<?= base_url('resources/landing/assets/plugins/iCheck/icheck.min.js'); ?>"></script>
		<script src="<?= base_url('resources/landing/assets/js/login.js'); ?>"></script>
		<link rel="stylesheet" href="<?= base_url('resources/dist/css/e_style.css'); ?>">
	</head>
	
	<body class='loading'>

		<div id="wrapper">
			<div id="main">
				<header id="header">
					<h1>Code Republiq</h1>
					<p class="landing-page-description">Attendance Management &nbsp;&bull;&nbsp; Payroll System</p>
					<nav>
						<div class="login-box">
							<div class="login-box-body login-page-background-cloud">
								<p class="login-box-msg text-danger">

						          <?php
										echo isset($login_error) ? $login_error : '';
										echo form_error('username');
										echo form_error('password');
						          ?>

								</p>
								
								<?php echo form_open('auth/user-login'); ?>
								
									<div class="form-group has-feedback">

								<?php
									echo form_input(array (
										'name' => 'username',
										'placeholder' => 'Username',
										'autocomplete' => 'off',
										'class' => 'form-control',
										'value' => set_value('username')
									));
								?>


										<span class="glyphicon glyphicon-user form-control-feedback"></span>
									</div>
									<div class="form-group has-feedback">


								<?php
									echo form_password(array(
										'name' => 'password',
										'placeholder' => 'Password',
										'class' => 'form-control'
									));
								?>


										<span class="glyphicon glyphicon-lock form-control-feedback"></span>
									</div>
									
									<div class="row">
										<div class="col-xs-8">
											<div class="checkbox icheck pull-left">
												<label>
													<input type="checkbox" name="remember" value="on"> Remember Me
												</label>
											</div>
										</div>
										<div class="col-sm-4 col-xs-12 signin-button">
											<button type="submit" class="btn btn-danger btn-block btn-flat">Sign In</button>
										</div>
									</div>
								<?= form_close(); ?>
							</div>
						</div>
					</nav>
				</header>

				<footer id="footer">
					<a href="<?= base_url('aboutus/crampsdev'); ?>"><span class="copyright">&copy; CRAMPS Development Team</span></a>
				</footer>
			</div>
		</div>
		
		<script>
			window.onload = function() { document.body.className = ''; }
			window.ontouchmove = function() { return false; }
			window.onorientationchange = function() { document.body.scrollTop = 0; }
		</script>
	</body>
</html>