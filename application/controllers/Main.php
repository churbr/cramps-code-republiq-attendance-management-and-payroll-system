<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Main extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->library(array('e_user', 'e_authentication', 'e_security'));
			$this->load->model(array('employee', 'salary', 'timelog', 'holiday'));
			$this->load->helper(array('e_time', 'cookie'));
		}

		// --------------------------------------------------------------------------

		public function index() {
			//echo $this->encryption->decrypt('625c533ad36de7136d22467d780d393545639042386441ac7ae1051a7be5e7085726f562f2b8c1c71aa8892c7fb090e56c3c6d5dad2b04d1e3b43cbd5bf5c9864ujYF6MMxg5kKDeRQP1CbK4Bnnc/V9VqZ9N8JpAONYc=');
			$this->e_security->check_login();
			$this->load->view('login');
		}

		// --------------------------------------------------------------------------

		public function about_us() {
			echo '<a href="' . base_url() . '">Go back</a>';
		}

		// --------------------------------------------------------------------------

		public function home() {
			$this->e_security->only_allow('regular_employee');	
			$data = $this->employee->get_overall_info($this->session->id);
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['visitor_ip'] = $this->input->ip_address();
			$data['static_ip'] = $this->setting->static_ip();
			$data['employee_position'] = $this->employee->get_position();
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['notification_count'] = $this->notification->count($this->session->id);
			$data['notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['page_title'] = 'Home';

			$this->load->view('regemp/header', $data);
			$this->load->view('regemp/modal');
			$this->load->view('regemp/nav');
			$this->load->view('regemp/time');
			$this->load->view('regemp/footer');
		}

		// --------------------------------------------------------------------------

		public function request_feed() {

			$this->e_security->only_allow('regular_employee');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['notification_count'] = $this->notification->count($this->session->id);
			$data['notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['request_feed'] = $this->employee->request_feed();
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['employee_position'] = $this->employee->get_position();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['page_title'] = 'Request Feed';

			$this->load->view('regemp/header', $data); 
			$this->load->view('regemp/modal');
			$this->load->view('regemp/nav');
			$this->load->view('regemp/reqfeed');
			$this->load->view('regemp/footer');
		}

		// --------------------------------------------------------------------------

		public function time_logs() {
			$this->e_security->only_allow('regular_employee');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['employee_position'] = $this->employee->get_position();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['project_manager'] = $this->employee->project_manager($this->session->id);
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['notification_count'] = $this->notification->count($this->session->id);
			$data['timelog'] = $this->timelog->view($this->session->id);
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['page_title'] = 'Time Logs';

			$this->load->view('regemp/header', $data);
			$this->load->view('regemp/modal');
			$this->load->view('regemp/nav');
			$this->load->view('regemp/timelog');
			$this->load->view('regemp/footer');
		}

		// --------------------------------------------------------------------------

		public function view_notification($notification_type = null, $notification_id = null) {

			$this->e_security->only_allow('regular_employee');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['employee_position'] = $this->employee->get_position();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['project_manager'] = $this->employee->project_manager($this->session->id);
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['notification_list'] = $this->notification->get($this->session->id);
			$data['notification_count'] = $this->notification->count($this->session->id);
			$data['timelog'] = $this->timelog->view($this->session->id);
			$data['request_feed'] = $this->notification->feed($notification_type, $notification_id);
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['page_title'] = 'Notification';

			$this->load->view('regemp/header', $data);
			$this->load->view('regemp/modal');
			$this->load->view('regemp/nav');

			if($notification_type == 'leave' || $notification_type == 'overtime') {
				$this->load->view('regemp/reqfeed');
			}else {
				$this->load->view('regemp/timelog');
			}
			
			$this->load->view('regemp/footer');
		}

		// --------------------------------------------------------------------------

		public function edit_profile() {
			$this->e_security->only_allow('regular_employee');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['employee_position'] = $this->employee->get_position();
			$data['employee_skills'] = $this->employee->skills();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['notification_count'] = $this->notification->count($this->session->id);
			$data['notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['basic_info'] = $this->employee->get_overall_info($this->session->id);
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['current_tab'] = 'about_me';
			$data['page_title'] = 'About me';

			$this->load->view('regemp/header', $data);
			$this->load->view('regemp/modal');
			$this->load->view('regemp/nav');
			$this->load->view('regemp/editprofile');
			$this->load->view('regemp/footer');
		}

		// --------------------------------------------------------------------------

		public function view_holiday() {
			$this->e_security->only_allow('regular_employee');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['employee_position'] = $this->employee->get_position();
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['notification_count'] = $this->notification->count($this->session->id);
			$data['notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['holidays'] = $this->holiday->view();
			$data['page_title'] = 'Holidays';

			$this->load->view('regemp/header', $data);
			$this->load->view('regemp/modal');
			$this->load->view('regemp/nav');
			$this->load->view('regemp/holiday');
			$this->load->view('regemp/footer');
		}

		// --------------------------------------------------------------------------

		public function logout() {
			$this->session->sess_destroy();
			delete_cookie('hash');
			redirect('/index');
		}

		// --------------------------------------------------------------------------

		public function test() {
			echo $this->encryption->encrypt('welcome1');
		}
	}