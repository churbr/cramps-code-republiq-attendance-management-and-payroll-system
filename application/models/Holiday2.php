<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Holiday2 extends CI_Model {

		public function __construct() {
			parent::__construct();
			$this->load->library(array('e_security', 'e_attendance'));
		}

		public function addHoliday($name,$type,$date){
			$this->db->select('date');
			$this->db->where('date', $date);
			$query_overtime = $this->db->get('holiday');
			if($query_overtime->num_rows()) {
				return 1;
			}
			else{
				$this->db->insert('holiday', array(
						'name' => $name,
						'date' => $date,
						'type' => $type,
						'status' => '1'
						));
				if($this->db->affected_rows()) 
				{ 
					return 0;
				}
				else{
					return 2;
				}
			}
		}
		public function removeHoliday($id){
			$this->db->select('date');
			$this->db->where('id', $id);
			$query_overtime = $this->db->get('holiday');
			if($query_overtime->num_rows()) {
				$this->db->delete('holiday', array('id' => $id)); 
				return true;
			}
			else{
				return false;
			}
		}
		public function editHoliday($id,$name,$type,$date){
			$this->db->select('date');
			$this->db->where('id', $id);
			$query_overtime = $this->db->get('holiday');
			if($query_overtime->num_rows()) {
				$this->db->select('date');
				$this->db->where('date', $date);
				$this->db->where('id !=', $id);
				$query_overtime2 = $this->db->get('holiday');
				if($query_overtime2->num_rows()) {
					return 1;
				}
				else{
					$this->db->where('id', $id);
					$this->db->update('holiday', array(
							'name' => $name,
							'date' => $date,
							'type' => $type,
							));
					if($this->db->affected_rows()) 
					{ 
						return 0;
					}
					else{
						return 2;
					}
				}
			}
			else{
				return 3;
			}
		}	
	}