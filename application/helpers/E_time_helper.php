<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('extract_timelog')) {

	function get_days() {
		$days = array(
			1 =>	'monday',
					'tuesday',
					'wednesday',
					'thursday',
					'friday',
					'saturday',
					'sunday'
		);

		return $days;
	}

}