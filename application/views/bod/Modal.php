<?php
  if(isset($timed_in) && $timed_in === true) {
    $word = 'out';
  }else {
    $word = 'in';
  }
?>

	<div class="modal" id="employee-view-recent-modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Recent Logs</h4>
			</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tr>
											<th>Date</th>
											<th>Day</th>
											<th>Time In</th>
											<th>Time Out</th>
											<th>Total Hours</th>
										</tr>
										
										<tr>
											<td>10/14/2015</td>
											<td>Wednesday</td>
											<td>10:00 AM</td>
											<td>07:00 PM</td>
											<td>9</td>
										</tr>
										
										<tr>
											<td>10/14/2015</td>
											<td>Wednesday</td>
											<td>10:00 AM</td>
											<td>07:00 PM</td>
											<td>9 <span class="label bg-teal">OT</span></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="modal-footer">
					<h4 class="pull-right">Total Regular Hours: 9</h4>
					<h4 class="pull-left">Total Overtime: 9</h4>
				</div>
			</div>
		</div>
	</div>
	
	<?php
	  if(isset($visitor_ip, $static_ip) && strcmp($visitor_ip, $static_ip) === 0 && $this->session->is_loggedin && !$this->timelog->on_leave($this->session->id)) {
	?>
	  <div class="modal modal-warning" id="time-prompt-modal" tabindex="-1">
	    <div class="modal-dialog">
	        <div class="modal-content">

	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <h4 class="modal-title"> <i class="fa fa-warning"></i> </h4>
	          </div>
	          
	          <div id='confirmation_message' class="modal-body text-centered">
	            <h3>Are you sure you want to time <?= $word; ?>?</h3>
	          </div>

	          <div class="modal-footer">
	            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
	            <button id='timein_button' type="button" class="btn btn-outline" data-dismiss="modal">Continue</button>
	          </div>

	        </div>
	    </div>
	  </div>
	<?php } ?>

<?= form_open('attendance/request-leave', array('class' => 'request_leave')); ?>

  <div class="modal" id="employee-leave-request-modal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">File a request.</h4>
        </div>

        <div class="modal-body">

          <div class="row">
            <div class="form-group col-md-6 col-xs-12">
              <div class="form-group">

                  <select id='leave_req_type' name='leave_type_id' class="form-control leave-type-select">
                    <option value="0" disabled selected>Select Request</option>
                    <option value="1">Sick leave</option>
                    <option value="2">Vacation leave</option>

                    <?php
                      if(strcmp($gender, 'male') === 0) {
                        echo '<option value="3">Paternity leave</option>';
                      }else {
                        echo '<option value="4">Maternity leave</option>';
                      }
                    ?>
                  </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-6 col-xs-12">
              <label>From</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="date" id='leave_req_from' name="from" class="form-control pull-right">
              </div>
            </div>

            <div class="form-group col-md-6 col-xs-12">
              <label>To</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="date" id='leave_req_to' name="to" class="form-control pull-right">
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" id='leave_request_close' class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="button" id='leave_request_clear' class="btn btn-danger">Clear</button>
          <input type='submit' class='btn btn-primary' value='Ok' />
        </div>
        <div id='leave_request_message'></div>
      </div>
    </div>
  </div>
  
<?= form_close(); ?>
	
	<div class="wrapper">