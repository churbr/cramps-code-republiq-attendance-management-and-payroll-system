<?php
	if(!empty($success)) {
?>

	<div class="modal modal-success in" id="success-prompt-modal" tabindex="-1" aria-hidden="false" style="display: block;">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body text-centered">
					<h1><i class="fa fa-fw fa-smile-o"></i></h1>
					<h5>Updated Successfully</h5>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		setTimeout(function() {
			window.location.replace("<?= base_url('admin/employee-profile/' . $employee_detail['id']); ?>");
		}, 2000);
	</script>

<?php
	}
?>

<div class="employee-management-wrapper">
<div class="content-wrapper">
	<section class="content-header">

		<h1>
			Employee Management
			<small>
				<ol class="breadcrumb">
					<li><a href="<?= base_url('admin/home.html'); ?>"><i class="fa fa-user"></i> Manage Employees</a></li>
					<li class="active"> <?= $employee_detail['firstname'] . '\'s Profile ';?> </li>
				</ol>
			</small>
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-3">
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle" src="<?= base_url('resources/dist/img/users/' . $employee_detail['picture']); ?>" alt="User profile picture" style="width: 128px; height: 128px;">
						<h3 class="profile-username text-center"> <?= $employee_detail['firstname'] . ' ' . substr($employee_detail['middlename'], 0, 1)  . $employee_detail['lastname']; ?> </h3>
						<p class="text-muted text-center"> <?= $employee_position; ?> </p>

						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Status</b>
								<a class="pull-right">
									<?php
										if($employee_detail['status'] == 1) {
											echo 'Active';
										} else {
											echo 'Inactive';
										}
									?>
								</a>
							</li>

							<li class="list-group-item">
								<b>Employment Date</b>
								<a class="pull-right"> <?= nice_date($employee_detail['date_hired'], 'M d, Y'); ?> </a>
							</li>
							
<?php
							if($deactivation_detail !== FALSE && $employee_detail['status'] == 0) {
?>
								<li class="list-group-item">
									<h3><strong class="text-red"> Deactivated </strong></h3>
									<p class="text-red"> <?= $deactivation_detail['reason']; ?> </p>
								</li>
<?php
							}
?>							
							
						</ul>
					</div>
				</div>
			</div>
			
			<div class="col-md-9">
			
			<div class="nav-tabs-custom">
			
			<ul class="nav nav-tabs">
				<li class="active"><a href="#activity" data-toggle="tab">About Me</a></li>
				<li><a href="#update" data-toggle="tab">Update</a></li>
				<li><a href="#profile-picture" data-toggle="tab">Profile Picture</a></li>
				<li><a href="#change-password" data-toggle="tab">Change Password</a></li>
				<li><a href="#project-manager" data-toggle="tab">Project Manager</a></li>
				<li><a href="#update-leave" data-toggle="tab">Leave</a></li>
			</ul>
				
			<div class="tab-content">
				<div class="active tab-pane" id="activity">


				<div class="">
				<div class="box-body">
				<h4><strong><i class="fa fa-user margin-r-5"></i>  Personal Information</strong></h4>
				

				<h4> <?= $employee_detail['firstname'] . ' ' . $employee_detail['lastname']; ?> </h4>

				<p class="text-muted">
					<?php
						$type = 'N/A';

						switch ($this->session->userdata('type')) {
							case 'super_admin':
								$type = 'Super Admin';
							break;

							case 'admin':
								$type = 'Admin';
							break;

							case 'regular_employee':
								$type = 'Employee';
							break;

							case 'project_manager':
								$type = 'Project Manager';
							break;

							case 'board_of_director':
								$type = 'Board Of Director';
							break;
						}

						echo $type;
					?>
				</p>
				
				<h4> <?= nice_date($employee_detail['birthday'], 'M d, Y'); ?> </h4>
				<p class="text-muted">Birthdate</p>
				
				<h4> <?= ucfirst($employee_detail['gender']); ?> </h4>
				<p class="text-muted">Gender</p>

				<hr>

				<strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>
				<h4> <?= $employee_detail['address']; ?> </h4>

				<hr>

				<strong><i class="fa  fa-phone margin-r-5"></i> Contact</strong>
				<h4>Contact Number: <?= substr($employee_detail['contact'], 0, 6) . '****'; ?></h4>
				<h4>Email: <?= $employee_detail['email']; ?></h4>

				<hr>

				<strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
				<h4>
					<?php
						if($employee_skills !== false) { echo $employee_skills; }
						else { echo 'No skills set yet.'; }
					?>
				</h4>

				</div>
				</div>


				</div>



				<div class="tab-pane" id="update">
					<?= form_open('user/admin-update-info', array('class' => 'form-horizontal')); ?>
						<div class="form-group">
							<label class="col-sm-2 control-label">Username</label>
							<div class="col-sm-6">
								<input type="text" name='username' class="form-control" value="<?= $employee_detail['username']; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">First Name</label>
							<div class="col-sm-6">
								<input type="text" name='firstname' class="form-control" value="<?= $employee_detail['firstname']; ?>">
							</div>
						</div>

						<!-- <div class="form-group">
							<label class="col-sm-2 control-label">Middle Name</label>
							<div class="col-sm-6">
								<input type="text" name='middlename' class="form-control" value="<?= $employee_detail['middlename']; ?>">
							</div>
						</div> -->

						
						<div class="form-group">
							<label class="col-sm-2 control-label">Last Name</label>
							<div class="col-sm-6">
								<input type="text" name='lastname' class="form-control" value="<?= $employee_detail['lastname']; ?>">
							</div>
						</div>
						
						<div class="form-group">
						<label class="col-sm-2 control-label">Gender</label>
						
						<div class="col-sm-6">
							<select name='gender' class="form-control">
								<option value='male' <?= ($employee_detail['gender'] == 'male') ? 'selected' : ''; ?> >Male</option>
								<option value='female' <?= ($employee_detail['gender'] == 'female') ? 'selected' : ''; ?> >Female</option>
							</select>
						</div>
						
						</div>
					
						<div class="form-group">
							<label class="col-sm-2 control-label">Birthdate</label>
							<div class="col-sm-6">
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="date" name='birthday' class="form-control pull-right" value="<?= $employee_detail['birthday']; ?>">
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Address</label>
							<div class="col-sm-6">
								<input type="text" name='address' class="form-control" value="<?= $employee_detail['address']; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Contact Number</label>
							<div class="col-sm-6">
								<input type="text" name='contact' class="form-control" value="<?= $employee_detail['contact']; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Email</label>
							<div class="col-sm-6">
								<input type="text" name='email' class="form-control" value="<?= $employee_detail['email']; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Position</label>
							<div class="col-sm-6">
								<select name='position' class="form-control select2" data-placeholder="Select..." style="width: 100%;">
									<?php


										foreach ($position_list as $index => $position) {

											if($position['id'] == $get_position_id['position_id']) {
												echo '<option value="' . $position['id'] . '" selected>' . $position['title'] . '</option>';
											} else {
												echo '<option value="' . $position['id'] . '">' . $position['title'] . '</option>';
											}
										}
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Skills</label>
							<div class="col-sm-6">
								<select name='skill[]' class="form-control select2" multiple="multiple" data-placeholder="Select..." style="width: 100%;">
									<?php
										foreach ($skill_list as $index => $skill) {
											echo '<option value="' . $skill['id'] . '">' . $skill['title'] . '</option>';
										}
									?>
								</select>
							</div>
						</div>


						<div class="form-group">
							<label class="col-sm-2 control-label">Date Started</label>
							
							<div class="col-sm-6">
								<div class="input-group">
									<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</div>
									<input type="date" name='date_started' class="form-control pull-right" value="<?= $employee_detail['date_hired']; ?>">
									<input type='hidden' name='employee_id' value='<?= $employee_detail['id']; ?>' />
									<input type='hidden' name='update_submitted' value='true' />
								</div>
							</div>
						</div>

						<hr />

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-primary">Save</button>
							</div>
						</div>
					</form>
				</div>


				<div class="tab-pane" id="profile-picture">
					<?php echo form_open_multipart('upload/admin_upload', array('class' => 'form-horizontal')); ?>
						<div class="form-group">
							<label class="col-sm-2 control-label">Update Profile Picture</label>
							<div class="col-sm-6">
								<input type="file" name='picture' />
								<input type='hidden' name='employee_id' value="<?= $employee_detail['id']; ?>" />
								<input type='hidden' name='upload_picture' value='true' />
							</div>
						</div>

						<hr />

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-primary">Save</button>
							</div>
						</div>
					</form>
				</div>

				<div class="tab-pane" id="change-password">
					<?= form_open('user/admin-change-password', array('class' => 'form-horizontal', 'id' => 'admin_change_password')); ?>
						<div class="form-group">
							<label class="col-sm-2 control-label">New Password</label>
							<div class="col-sm-6">
								<input type="password" id="newpass" class="form-control">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Confirm New Password</label>
							<div class="col-sm-6">
								<input type="password" id="confirmpass" class="form-control">
							</div>
						</div>


						<div class="form-group">
							<div class="col-sm-12">
								<div class='changepass_info'></div>
							</div>
						</div>

						<hr />

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type='hidden' id='employee_id' value="<?= $employee_detail['id']; ?>" />
								<button id='submit_button' type="submit" class="btn btn-primary">Save</button>
							</div>
						</div>
					</form>
				</div>


				<div class="tab-pane" id="project-manager">
					<?php echo form_open_multipart('user/assign_pm', array('class' => 'form-horizontal', 'id' => 'assign_pm_form')); ?>
						<div class="form-group">
							<label class="col-sm-2 control-label">Project Manager</label>
							<div class="col-sm-6">

								<select id='project_manager_list' name='project_manager_list[]' class="form-control select2" multiple="multiple" data-placeholder="Select..." style="width: 100%;">
								
								<?php
									if($project_manager_list === FALSE) {
										echo '<option disabled selected>No Project Manager Available</option>';
									} else {
										foreach ($project_manager_list as $index => $project_manager) {
											if( in_array($project_manager['id'], $assigned_to_pm_id) ) {
												echo "<option selected value='{$project_manager['id']}'> {$project_manager['firstname']} {$project_manager['lastname']} </option>";
											} else {
												echo "<option value='{$project_manager['id']}'> {$project_manager['firstname']} {$project_manager['lastname']} </option>";
											}
										}
									}
								?>

								</select>
							</div>
						</div>
						
						<hr />

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type='hidden' name='employee_id' value='<?= $employee_detail['id']; ?>' />
								<input type='hidden' name='admin_assign_pm' value='true' />
								<button type="submit" class="btn btn-primary">Save</button>
							</div>
						</div>
					</form>
				</div>


				<div class="tab-pane" id="update-leave">
					<?php echo form_open('user/admin-update-leave', array('class' => 'form-horizontal', 'id' => 'update_leave_form')); ?>
						
						<?php
							
							foreach ($employee_leave_detail as $index => $leave_detail) { 
						?>						
								<div class="form-group">
									<label class="col-sm-2 control-label"><?= $leave_detail['name']; ?></label>
									<div class="col-sm-6">

						<?php
										if($leave_detail['code'] == 'sick_leave') {
											echo '<input id="sl_days" name="sl_days" type="number" value="' . $leave_detail['remaining_days'] . '" class="form-control">';
										} elseif($leave_detail['code'] == 'vacation_leave') {
											echo '<input id="vl_days" name="vl_days" type="number" value="' . $leave_detail['remaining_days'] . '" class="form-control">';
										} elseif($leave_detail['code'] == 'paternity_leave') {
											echo '<input id="pl_days" name="pl_days" type="number" value="' . $leave_detail['remaining_days'] . '" class="form-control">';
										} elseif($leave_detail['code'] == 'maternity_leave') {
											echo '<input id="ml_days" name="ml_days" type="number" value="' . $leave_detail['remaining_days'] . '" class="form-control">';
										}
						?>
									</div>
								</div>
						<?php
							}
						?>

						<div class="form-group">
							<div class="col-sm-6">
								<div id='leave_form_info'></div>
							</div>
						</div>

						<hr />

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type='hidden' id='employee_id' name='employee_id' value='<?= $employee_detail['id']; ?>' />
								<input type='hidden' id='gender' name='gender' value='<?= $employee_detail['gender']; ?>' />
								<button type="submit" class="btn btn-primary">Save</button>
							</div>
						</div>
					<?= form_close(); ?>
				</div>


			</div>
			</div>
			</div>
		</div>
	</section>
</div>
</div>