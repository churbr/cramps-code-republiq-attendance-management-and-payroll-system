<?php

	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Overtime extends CI_Model {

		public function __construct() {
			$this->load->library('e_attendance');
			$this->load->model(array('salary', 'timelog_overtime'));
			parent::__construct();
		}

		// --------------------------------------------------------------------------

		public function request($pm_id = NULL, $start_time = NULL, $end_time = NULL, $start_date = NULL, $end_date = NULL, $hour = 0, $minute = 0, $note = NULL) {

		 	$payment = $this->e_attendance->generate_pay($hour);

		 	if(!empty(trim($note))) { $note = $note; }
		 	else { $note = NULL; }

		 	$this->db->trans_start();

			$this->db->insert('overtime', array (
				'id' => NULL,
				'pm_id' => $pm_id,
				'employee_id' => $this->session->id,
				'start_time' => $start_time,
				'end_time' => $end_time,
				'start_date' => $start_date,
				'end_date' => $end_date,
				'hour' => $hour,
				'minute' => $minute,
				'pay' => $payment,
				'status' => 'pending',
				'show' => 1,
				'date_updated' => NULL,
				'note' => $note
			));

			if($this->db->affected_rows()) {
				$id = $this->db->insert_id();

				$data = array (
					'id' => $id,
					'receiver_id' => $pm_id,
					'sender_id' => $this->session->id
				);

				$this->db->trans_complete();
				return $data;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function detailed_list($employee_id = null, $notification_id = null) {

			$overtime_request = array();

			$this->db->select("
				`notification`.`notification_type_id`,
				`notification`.`id` AS 'notification_pk',
				`notification`.`notification_id` AS 'table_pk',
				`notification`.`status` AS 'seen_status',
				`notification`.`date` AS 'date_filed',
				`employee`.`firstname`,
				`employee`.`middlename`,
				`employee`.`lastname`,
				`overtime`.`start_time`,
				`overtime`.`end_time`,
				`overtime`.`start_date`,
				`overtime`.`end_date`,
				`overtime`.`hour`,
				`overtime`.`minute`,
				`overtime`.`note`,
				`overtime`.`date_updated`,
				`overtime`.`status`
			");

			$this->db->from('`notification`');
			$this->db->join('`overtime`', '`notification`.`notification_id` = `overtime`.`id`');
			$this->db->join('`employee`', '`overtime`.`pm_id` = `employee`.`id`');
			if(!is_null($notification_id)) { $this->db->where('`notification`.`id`', $notification_id); }
			$this->db->where('`notification`.`notification_type_id`', 2);
			// $this->db->where('`overtime`.`show`', 1);
			$this->db->where('`overtime`.`employee_id`', (int)$employee_id);
			$query = $this->db->get();

			if($query->num_rows()) {

				foreach ($query->result_array() as $index => $overtime) {
					$mi = substr($overtime['middlename'], 0, 1) . '.';

					$overtime_request[] = array (
			            'notification_type_id' => $overtime['notification_type_id'],
			            'notification_pk' => $overtime['notification_pk'],
			            'table_pk' => $overtime['table_pk'],
			            'seen_status' => $overtime['seen_status'],
			            'date_filed' => $overtime['date_filed'],
			            'manager_name' => $overtime['firstname'] . '  ' . $overtime['lastname'],
			            'start_time' => $overtime['start_time'],
			            'end_time' => $overtime['end_time'],
			            'start_date' => $overtime['start_date'],
			            'end_date' => $overtime['end_date'],
			            'hour' => $overtime['hour'],
			            'minute' => $overtime['minute'],
			            'date_updated' => $overtime['date_updated'],
			            'status' => $overtime['status'],
			            'note' => $overtime['note'],
			            'timestamp' => strtotime($overtime['date_filed'])
					);
				}

				return $overtime_request;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function update($table_pk, $status, $note) {
			$data = array (
				'status' => $status,
				'date_updated' => date('Y-m-d'),
				'note' => $note  = ($note == NULL) ? NULL : $note
			);

			$this->db->where('`pm_id`', $this->session->id);
			$this->db->where('`id`', $table_pk);
			$this->db->update('`overtime`', $data);
			if($this->db->affected_rows()) { return true; }

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_data($table_pk = null, $employee_id = null) {
			$this->db->select("
				`overtime`.`start_time`, `end_time`, `start_date`, `end_date`, `hour`, `minute`, `overtime`.`status`, `note`, `date_updated`,
				`notification`.`date` AS 'date_filed',
				`employee`.`firstname`, `middlename`, `lastname`
			");
			$this->db->from('`overtime`');
			$this->db->join('`notification`', '`overtime`.`id` = `notification`.`notification_id`');
			$this->db->join('`employee`', '`overtime`.`pm_id` = `employee`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 2);
			$this->db->where('`overtime`.`employee_id`', $employee_id);
			$this->db->where('`overtime`.`id`', $table_pk);
			$this->db->where('`overtime`.`show`', 1);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function hp_get_data($table_pk = null) {
			$this->db->select("
				`overtime`.`start_time`, `end_time`, `start_date`, `end_date`, `hour`, `minute`, `overtime`.`status` AS 'overtime_status', `note`, `date_updated`,
				`notification`.`date` AS 'date_filed',
				`employee`.`firstname`, `middlename`, `lastname`
			");
			$this->db->from('`overtime`');
			$this->db->join('`notification`', '`overtime`.`id` = `notification`.`notification_id`');
			$this->db->join('`employee`', '`overtime`.`employee_id` = `employee`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 2);
			$this->db->where('`overtime`.`id`', $table_pk);
			$this->db->where('`overtime`.`show`', 1);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function timelog($employee_id = null) {
			$overtime_log = array();

			$query = $this->db->select("
						`overtime`.`id`, `start_date`, `start_time`, `end_time`, `hour`, `minute`, `overtime`.`status` AS 'overtime_status',
						`notification`.`status` AS 'notification_status'
					")
					->from('`overtime`')->join('`notification`', '`overtime`.`id` = `notification`.`notification_id`')
					->where('`notification`.`notification_type_id`', 2)
					->where('`overtime`.`employee_id`', $employee_id)
					->where('`overtime`.`show`', 1)
					->get();

			if($query->num_rows()) {
				foreach ($query->result_array() as $index => $overtime) {
					$overtime_log[] = array(
						'type' => 'overtime',
						'id' => $overtime['id'],
						'start_date' => nice_date($overtime['start_date'], 'M d, Y'),
						'start_time' => $overtime['start_time'],
						'end_time' => $overtime['end_time'],
						'hour' => $overtime['hour'],
						'minute' => $overtime['minute'],
						'overtime_status' => $overtime['overtime_status'],
						'notification_status' => $overtime['notification_status'],
						'timestamp' => strtotime($overtime['start_date'])
					);
				}

				return $overtime_log;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function timelog_specific($employee_id = null, $dates = null) {
			$overtime_log = array();

			$query = $this->db->select("
						`overtime`.`id`, `start_date`, `start_time`, `end_time`, `hour`, `minute`, `overtime`.`status` AS 'overtime_status',
						`notification`.`status` AS 'notification_status'
					")
					->from('`overtime`')->join('`notification`', '`overtime`.`id` = `notification`.`notification_id`')
					->where('`notification`.`notification_type_id`', 2)
					->where('`overtime`.`employee_id`', $employee_id)
					->where('`overtime`.`show`', 1)
					->where_in('`overtime`.`start_date`', $dates)
					->get();

			if($query->num_rows()) {
				foreach ($query->result_array() as $index => $overtime) {
					$overtime_log[] = array (
						'type' => 'overtime',
						'id' => $overtime['id'],
						'start_date' => nice_date($overtime['start_date'], 'M d, Y'),
						'start_time' => $overtime['start_time'],
						'end_time' => $overtime['end_time'],
						'hour' => $overtime['hour'],
						'minute' => $overtime['minute'],
						'overtime_status' => $overtime['overtime_status'],
						'notification_status' => $overtime['notification_status'],
						'timestamp' => strtotime($overtime['start_date'])
					);
				}

				return $overtime_log;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function hide( $table_pk ) {
			
			$overtime_data = array (
				'`status`' => 'cancelled',
				'`show`' => 0,
				'`date_updated`' => date('Y-m-d')
			);

			$this->db->update('`overtime`', $overtime_data, array('`id`' => $table_pk ));
			if($this->db->affected_rows()) { return true; }

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_application($notification_id = null) {
			$this->db->select("
				`overtime`.`id` AS 'overtime_id', `overtime`.`start_time`, `end_time`, `start_date`, `end_date`, `hour`, `minute`, `overtime`.`status` AS `overtime_status`, `note`, `date_updated`,
				`notification`.`id` AS 'notification_id', `notification`.`date` AS 'date_filed', `notification`.`seen_by` AS 'seen_status',
				`employee`.`firstname`, `middlename`, `lastname`
			");

			$this->db->from('`overtime`');
			$this->db->join('`notification`', '`overtime`.`id` = `notification`.`notification_id`');
			$this->db->join('`employee`', '`overtime`.`employee_id` = `employee`.`id`');
			if(!is_null($notification_id)) { $this->db->where('`notification`.`id`', $notification_id); }
			$this->db->where('`notification`.`notification_type_id`', 2);
			$this->db->where('`overtime`.`pm_id`', $this->session->id);
			$this->db->where('`overtime`.`show`', 1);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_specific_application( $notification_id = null ) {
			$this->db->select("
				`overtime`.`id` AS 'overtime_id', `overtime`.`start_time`, `end_time`, `start_date`, `end_date`, `hour`, `minute`, `overtime`.`status` AS `overtime_status`, `note`, `date_updated`,
				`notification`.`id` AS 'notification_id', `notification`.`date` AS 'date_filed', `notification`.`seen_by` AS 'seen_status',
				`employee`.`firstname`, `middlename`, `lastname`
			");

			$this->db->from('`overtime`');
			$this->db->join('`notification`', '`overtime`.`id` = `notification`.`notification_id`');
			$this->db->join('`employee`', '`overtime`.`employee_id` = `employee`.`id`');
			$this->db->where('`notification`.`notification_type_id`', 2);
			$this->db->where('`notification`.`id`', $notification_id);
			$this->db->where('`overtime`.`pm_id`', $this->session->id);
			$this->db->where('`overtime`.`show`', 1);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}		

		// --------------------------------------------------------------------------

		public function application_count( $status_type = null ) {
			$count = 0;

			if($status_type !== NULL) {
				$this->db->where('`status`', $status_type);
				$this->db->where('`show`', 1);
				$this->db->from('`overtime`');
				$count = $this->db->count_all_results();
			}else {
				$this->db->where('`show`', 1);
				$this->db->from('`overtime`');
				$count = $this->db->count_all_results();
			}

			return $count;
		}

		// --------------------------------------------------------------------------

		public function is_pending( $table_pk ) {
			$this->db->where('`id`', $table_pk);
			$this->db->where('`status`', 'pending');
			$query = $this->db->get('`overtime`');

			if($query->num_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

	}