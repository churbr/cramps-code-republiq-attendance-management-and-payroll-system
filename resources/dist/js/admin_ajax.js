$(document).ready(function() {

	var base_url = $('.base_url').attr('id');

	$('#admin_change_password').on('submit', function() {

		newpass = $('#admin_change_password #newpass').val();
		confirm_newpass = $('#admin_change_password #confirmpass').val();
		employee_id = $('#admin_change_password #employee_id').val();

		$.post(
			base_url + 'user/admin-change-password.html',
			{ employee_id: employee_id, newpass: newpass, confirmpass: confirm_newpass },
			function(response) {
				if(response == 'success') {
					$('#admin_change_password #newpass').attr('disabled', 'disabled');
					$('#admin_change_password #confirmpass').attr('disabled', 'disabled');
					$('#admin_change_password #submit_button').attr('disabled', 'disabled');
					$('#admin_change_password .changepass_info').html("<h3 class='green'>Password successfully changed.</h3>");
				}else {
					$('#admin_change_password .changepass_info').html(response);
				}
			}
		);

		return false;
	});

$('#update_leave_form').on('submit', function() {

		gender = $('#update_leave_form #gender').val();
		employee_id = $('#update_leave_form #employee_id').val();

		if(gender == 'male') {
			sick_leave_days = $('#update_leave_form #sl_days').val();
			vacation_leave_days = $('#update_leave_form #vl_days').val();
			paternity_leave_days = $('#update_leave_form #pl_days').val();
		} else {
			sick_leave_days = $('#update_leave_form #sl_days').val();
			vacation_leave_days = $('#update_leave_form #vl_days').val();
			maternity_leave_days = $('#update_leave_form #ml_days').val();
		}
		if(gender == 'male') {

			$.post(
				base_url + 'user/admin-update-leave',
				{ employee_id: employee_id, gender: gender, sick_leave: sick_leave_days, vacation_leave: vacation_leave_days, paternity_leave: paternity_leave_days },
				function(response) {
					if(response == 'true') {
						$('#leave_form_info').html('<h4 class="green">Leave has been set.</h4>');
					}
					
				}
			);

		} else {

			$.post(
				base_url + 'user/admin-update-leave',
				{ employee_id: employee_id, gender: gender, sick_leave: sick_leave_days, vacation_leave: vacation_leave_days, maternity_leave: maternity_leave_days },
				function(response) {
					console.log(response);
					if(response == 'true') {
						$('#leave_form_info').html('<h4 class="green">Leave has been set.</h4>');
					}
					
				}
			);
		}

		return false;
	});

	// -----------------------------------------------------------------------------------------------------------------

	// CHUCHUCHU
	
	$('#add_holiday_button').on('click', function() {

		holiday_date = $('#holiday_date').val();
		holiday_name = $('#holiday_name').val();
		holiday_type = $('#holiday_type').val();

		$.post(
			base_url + 'user/add-holiday.html',
			{ holiday_date: holiday_date, holiday_name: holiday_name, holiday_type: holiday_type },
			function(response) {
				if(response == 'success') {

					$('#holiday_date').attr('disabled', 'disabled');
					$('#holiday_name').attr('disabled', 'disabled');
					$('#holiday_type').attr('disabled', 'disabled');

					$('#add_holiday_info').html('<h2 class="green">Holiday added.</h2>');
					
					setTimeout(function() {
						$('#add_holiday_info').html('');
						$('#holiday_close_button').click();
					}, 2000);

				}else {
					$('#add_holiday_info').html('<h3 class="error">' + response + '</h3>');
				}
			}
		);

		return false;
	});


	$('#holiday_edit_confirm').on('click', function() {

		holiday_date = $('#edit-holiday-modal #holiday_date').val()
		holiday_name = $('#edit-holiday-modal #holiday_name').val()
		holiday_pk = $('#edit-holiday-modal #edit_holiday_table_pk').val()
		holiday_edit_type = $('#edit-holiday-modal #holiday_edit_type').val()

		$.post(
			base_url + 'user/admin-update-holiday',
			{ holiday_date: holiday_date, holiday_name: holiday_name, holiday_pk: holiday_pk, holiday_type: holiday_edit_type },
			function(response) {
				if(response == 'true') {
					$('#edit_holiday_form_info').html('<h4 class="green">Holiday Updated</h4>');
					setTimeout(function() {
						$('#add_holiday_info').html('');
						$('#holiday_edit_close').click();
						$('#holiday_table').load(location.href + ' #holiday_table');
					}, 2000);
				}
			}
		);


		return false;
	});


	$('#holiday_remove_confirm').on('click', function() {

		holiday_pk = $('#holiday-remove-prompt #holiday_removal_pk').val();


		$.post(
			base_url + 'user/admin-remove-holiday',
			{ holiday_pk: holiday_pk },
			function(response) {
				if(response == 'true') {
					$('#holiday_table').load(location.href + ' #holiday_table');
					$('#holiday_remove_close').click();
				}
			}
		);

		return false;
	});

	// CHUCHUCHU
	

});