<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Code Republiq | Admin</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="icon" href="<?= base_url('resources/landing/images/favicon/cr.ico'); ?>" type="image/x-icon" />
    <link rel="stylesheet" href="<?= base_url('resources/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('resources/dist/css/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('resources/dist/css/ionicons/css/ionicons.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('resources/plugins/select2/select2.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('resources/plugins/datatables/dataTables.bootstrap.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('resources/dist/css/AdminLTE.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('resources/dist/css/skins/_all-skins.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('resources/plugins/iCheck/flat/blue.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('resources/dist/css/admin_custom.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('resources/dist/css/e_style.css'); ?>">
    <script src="<?= base_url('resources/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>
    <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/dist/js/app.min.js'); ?>"></script>
	<script src="<?= base_url('resources/dist/js/password_checker.js'); ?>"></script>
    <script src="<?= base_url('resources/plugins/fastclick/fastclick.min.js'); ?>"></script>
    <script src="<?= base_url('resources/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?= base_url('resources/dist/js/admin_custom.js'); ?>"></script>

    <script src="<?= base_url('resources/dist/js/print.js'); ?>"></script>

    <script src="<?= base_url('resources/dist/js/admin_ajax.js'); ?>"></script>
    <script src="<?= base_url('resources/dist/js/sorting-table.js'); ?>"></script>
    <script src="<?= base_url('resources/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?= base_url('resources/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('resources/plugins/select2/select2.full.min.js'); ?>"></script>
	<script src="<?= base_url('resources/dist/js/multiple.js'); ?>"></script>
    <script src="<?= base_url('resources/dist/js/hp_e_ajax.js'); ?>" async="async" ></script>
    <script src="<?= base_url('resources/dist/js/hp_e_functions.js'); ?>"></script>
  </head>

  <body class="hold-transition skin-blue sidebar-mini layout-boxed sidebar-collapse">

    <div class='base_url' id='<?= base_url(); ?>'></div>