<?php

	if(!defined('BASEPATH')) exit('No direct script access allowed.');
	
	class Timelog_overtime extends CI_Model {
			
		public function __construct() {
			$this->load->library('e_attendance');
			$this->load->model(array('holiday', 'overtime', 'leave'));
			parent::__construct();
		}
		
		// --------------------------------------------------------------------------

		public function timelog($employee_id = null, $table_pk = null) {

			$timelogs = array();

			$this->db->where('employee_id', $employee_id);
			if(!is_null($table_pk)) { $this->db->where('`id`', $table_pk); }
			$this->db->where('`show`', 1);
			$query = $this->db->get('`timelog_overtime`');

			if($query->num_rows()) {
				foreach ($query->result_array() as $index => $log) {
					$timelogs[] = array(
						'type' => 'timelog_overtime',
						'id' => $log['id'],
						'time_in' => $log['time_in'],
						'time_out' => $log['time_out'],
						'timein_date' => nice_date($log['timein_date'], 'M d, Y'),
						'timeout_date' => nice_date($log['timeout_date'], 'M d, Y'),
						'hour' => $log['hour'],
						'minute' => $log['minute'],
						'timestamp' => strtotime($log['timein_date'])
					);
				}

				return $timelogs;
			}

			return false;
		}

		// --------------------------------------------------------------------------


		public function timelog_specific( $employee_id = null, $dates = null ) {

			$timelogs = array();

			$this->db->where('employee_id', $employee_id);
			$this->db->where('show', 1);
			$this->db->where_in('timein_date', $dates);
			// $this->db->or_where_in('timeout_date', $dates);
			// $this->db->where_in('timeout_date', $dates);
			$query = $this->db->get('timelog_overtime');

			if($query->num_rows()) {
				foreach ($query->result_array() as $index => $log) {
					$timelogs[] = array(
						'type' => 'timelog_overtime',
						'id' => $log['id'],
						'time_in' => $log['time_in'],
						'time_out' => $log['time_out'],
						'timein_date' => nice_date($log['timein_date'], 'M d, Y'),
						'hour' => $log['hour'],
						'minute' => $log['minute'],
						'timestamp' => strtotime($log['timein_date'])
					);
				}

				return $timelogs;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function dual_insert( $lside_timelog, $rside_timelog ) {
			$timelogs = array();

			if(count($lside_timelog)) {
				$lside_timelog['pay'] = $this->e_attendance->generate_pay( $lside_timelog['hour'] );
				$timelogs[] = $lside_timelog;
			}

			if(count($rside_timelog)) {
				$rside_timelog['pay'] = $this->e_attendance->generate_pay( $rside_timelog['hour'] );
				$timelogs[] = $rside_timelog;
			}

			$this->db->insert_batch('timelog_overtime', $timelogs);

			if($this->db->affected_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function single_insert( $excess_log ) {

			$timelogs = array();

			if(count($excess_log)) {
				$excess_log['pay'] = $this->e_attendance->generate_pay( $excess_log['hour'] );
				$timelogs[] = $excess_log;
			}

			$this->db->insert('timelog_overtime', $excess_log);

			if($this->db->affected_rows()) { return true; }

			return false;
		}

		// --------------------------------------------------------------------------

		public function timelog_pk( $overtime_pk ) {
			$this->db->select('timelog_id');
			$this->db->where('overtime_id', $overtime_pk);
			$this->db->where('employee_id', $this->session->id);
			$query = $this->db->get('timelog_overtime');

			if($query->num_rows()) {
				return $query->result_array()[0]['timelog_id'];
			}
			
			return false;
		}

		// --------------------------------------------------------------------------

		public function hide( $timelog_id, $overtime_id ) {

			$this->db->where('timelog_id', $timelog_id);
			$this->db->where('overtime_id', $overtime_id);
			$this->db->where('employee_id', $this->session->id);
			$this->db->update('timelog_overtime', array('show' => 0));
			
			if($this->db->affected_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

	}