<div class="wrapper">
	<header class="main-header">

		<a href="<?= base_url('admin/home.html'); ?>" class="logo">
			<span class="logo-mini">
				<b>C</b>R
			</span>
			
			<span class="logo-lg">
				<b>Code </b>
				Republiq
			</span>
		</a>
		
		<nav class="navbar navbar-static-top" role="navigation">
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>
			
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?= base_url('resources/dist/img/users/' . $picture); ?>" class="user-image" alt="User Image">
							<span class="hidden-xs">Administrator</span>
						</a>
						
						<ul class="dropdown-menu mobile-responsive-dropdown">
							<li class="user-header">
								<img src="<?= base_url('resources/dist/img/users/' . $picture); ?>" class="img-circle" alt="User Image">
								<p>
									Administrator
									<small></small>
								</p>
							</li>
							<li class="user-footer">
								
								<div class="pull-left">
									<a href="<?= base_url('admin/change-password.html'); ?>" class="btn btn-default btn-flat">Change Password</a>
								</div>
								
								<div class="pull-right">
									<a href="<?= base_url('admin/logout.html'); ?>" class="btn btn-default btn-flat">Sign out</a>
								</div>
								
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	
	<aside class="main-sidebar">
		<section class="sidebar">

			<div class="user-panel">
				<div class="pull-left image">
					<img src="<?= base_url('resources/dist/img/users/' . $picture); ?>" class="img-circle" alt="User Image" />
				</div>
				
				<div class="pull-left info">
					<p>Administrator</p>
				</div>
			</div>


			<ul class="sidebar-menu">
			
				<li class="header">Welcome Admin!</li>
				
				<li class="treeview">
					
					<a href="#">
						<i class="fa fa-users"></i>
						<span>Employee</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					
					<ul class="treeview-menu">
						<li><a href="<?= base_url('admin/home.html'); ?>">Manage Employees</a></li>
						<li><a href="#add-employee-modal" data-toggle="modal">Add Employee</a></li>
					</ul>
				</li>
				
				<li>
					<a href="<?= base_url('admin/payroll.html'); ?>">
						<i class="fa fa-money"></i> <span>Payroll</span>
					</a>
				</li>
				
				<li class="treeview">
					
					<a href="#">
						<i class="fa fa-suitcase"></i>
						<span>Holiday</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					
					<ul class="treeview-menu">
						<li><a href="<?= base_url('admin/holiday.html'); ?>">Manage Holidays</a></li>
						<li><a href="#add-holiday-modal" data-toggle="modal">Add Holiday</a></li>
					</ul>
				</li>
				
				<li>
					<a href="<?= base_url('admin/reports.html'); ?>"><i class="fa fa-pencil-square-o"></i>
					<span>Reports</span></a>
				</li>
			</ul>
		</section>
	</aside>