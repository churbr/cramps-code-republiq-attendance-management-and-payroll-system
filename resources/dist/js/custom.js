//overtime time select
$(document).ready(function () {
    toggleFields();
    $(".leave-type-select").change(function () {
    toggleFields();
    });

});

function toggleFields() {
    if ($(".leave-type-select").val() == 5)
        $(".overtime-time-select").show();
    else
        $(".overtime-time-select").hide();
}

//Start of change Borja 3/2/2016
function hideToast(){
	$('#admin-notification').hide();
}

function updateEmployee(form){
	var that = $(form),
				url		= that.attr('action'),
				method	= that.attr('method'),
				data	= {};

	that.find('[name]').each(function() {
		var that = $(this),
				name		= that.attr('name'),
				value		= that.val();
				data[name]	= value;
	});
	
	$.ajax({
		url: url,
		type: method,
		data: data,
		success:function(response) {			
			if(response==0){
				$('#emp_upd_message').html('Update Successful');
			}
			else if(response==1){
				$('#emp_upd_message').html('Update Failed');
			}
			else{
				$('#emp_upd_message').html(response);
			}
		

		}
		
	});

	return false;
}
function updatePW(form){
	var that = $(form),
				url		= that.attr('action'),
				method	= that.attr('method'),
				data	= {};

	that.find('[name]').each(function() {
		var that = $(this),
				name		= that.attr('name'),
				value		= that.val();
				data[name]	= value;
	});
	if(data['pw1']!=data['pw2']){
		$('#emp_upd_pw_message').html('Passwords did not match');
	}
	else if(data['pw1'].length==0){
		$('#emp_upd_pw_message').html('Enter password');
	}
	else{
		$.ajax({
			url: url,
			type: method,
			data: data,
			success:function(response) {
				if(response==0){
					$('#emp_upd_pw_message').html('Password change successful');
					$('#pw1').val('');
					$('#pw2').val('');
				}
				else if(response==1){
					$('#emp_upd_pw_message').html('Password change failed');
				}
				else if(response==2){
					$('#emp_upd_pw_message').html('Passwords did not match');
				}
	
			}
		});
	}
	return false;
}

function updatePM(form){
	var that = $(form),
				url		= that.attr('action'),
				method	= that.attr('method'),
				data	= {};

	that.find('[name]').each(function() {
		var that = $(this),
				name		= that.attr('name'),
				value		= that.val();
				data[name]	= value;
	});

	$.ajax({
		url: url,
		type: method,
		data: data,
		success:function(response) {
			if(response==0){
				$('#emp_upd_pm_message').html('Project Manager added Successfully');
			}
			else if(response==1){
				$('#emp_upd_pm_message').html('Project Manager assignment failed');
			}

		}
	});
	
	return false;
}
function addHoliday(form){
	var that = $(form),
				url		= that.attr('action'),
				method	= that.attr('method'),
				data	= {};

	that.find('[name]').each(function() {
		var that = $(this),
				name		= that.attr('name'),
				value		= that.val();
				data[name]	= value;
	});
			
	if(data['holiday_name'].length==0||data['holiday_date'].length==0){
		$('#add_holiday_message').html('Fill in all fields');
	}
	else{
		$.ajax({
			url: url,
			type: method,
			data: data,
			success:function(response) {
				if(response==0){
					$('#add_holiday_message').html('Holiday added Successfully');
				}
				else if(response==1){
					$('#add_holiday_message').html('Date already used by another holiday');
				}
				else if(response==2){
					$('#add_holiday_message').html('Holiday addition failed');
				}
				else{
					$('#add_holiday_message').html('An error occured');
				}
			}
		});
	}
	return false;
}

function editHoliday(form){
	var that = $(form),
				url		= that.attr('action'),
				method	= that.attr('method'),
				data	= {};

	that.find('[name]').each(function() {
		var that = $(this),
				name		= that.attr('name'),
				value		= that.val();
				data[name]	= value;
	});

	if(data['holiday_name_edit'].length==0||data['holiday_date_edit'].length==0){
		$('#edit_holiday_message').html('Fill in all fields');		
	}
	else{		
		$.ajax({
			url: url,
			type: method,
			data: data,
			success:function(response) {			
				if(response==0){
					$('#edit_holiday_message').html('Holiday changed Successfully');
				}
				else if(response==1){
					$('#edit_holiday_message').html('Date already used by another holiday');
				}
				else if(response==2){
					$('#edit_holiday_message').html('Holiday modification failed');
				}
				else if(response==3){
					$('#edit_holiday_message').html('Holiday not found');
				}
				else{
					$('#edit_holiday_message').html('An error occured');
				}
			}
		});		
	}
	return false;
}

function setActivateModalText(action, id,name){
	$('#activate_modal_text').html('You are about to '+action+': '+name);	
	$('#modal_emp_id').val(id);	
	$('#modal_emp_function').val(action);	
}
function setAdminNotifText(text){
	$('#admin_notification_text').html(text);	
}
function updateEditHolidayTexts(id,name,type,date){
	$('#edit_holiday_name').html('You are about to edit '+name);
	$('#edit_holiday_title').html('Edit '+name);
	$('#holiday_name_edit').val(name);
	$('#holiday_date_edit').val(date);
	$('#holiday_id_edit').val(id);
	$('#holiday_type_edit option[value="'+type+'"]').attr("selected",true);
}
function updateRemoveHolidayTexts(id,name){
	$('#holiday_id_remove').val(id);
	$('#remove_holiday_name').html('You are about to remove '+name);
}

function refreshPage(){
	location.reload();
}

function addAdjustmentFields(){
	//Dynamically add Other Adjustments fields on button click
	$('#adjustment_group').append('<div class="form-group"><label class="col-sm-2 control-label">Name</label><div class="col-sm-3"><input type="text" id="adjust_name[]" name="adjust_name[]" class="form-control" /></div><label class="col-sm-2 control-label">Amount</label><div class="col-sm-3"><input type="number" id="adjust_value[]" name="adjust_value[]" class="form-control" /><a href="#remove-od-prompt" data-toggle="modal" onclick="$(this).closest(\'.form-group\').remove();">Remove</a></div></div>');	
}

function addDeductionFields(){
	//Dynamically add Other Deductions fields on button click
	$('#deduction_group').append('<div class="form-group"><label class="col-sm-2 control-label">Name</label><div class="col-sm-3"><input type="text" id="deduct_name[]" name="deduct_name[]" class="form-control" /></div><label class="col-sm-2 control-label">Amount</label><div class="col-sm-3"><input type="number" id="deduct_value[]" name="deduct_value[]" class="form-control" /><a href="#remove-od-prompt" data-toggle="modal" onclick="$(this).closest(\'.form-group\').remove();">Remove</a></div></div>');	
}
//End of change Borja 3/2/2016