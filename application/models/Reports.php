<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Reports extends CI_Model {

		public function __construct() {
			parent::__construct();		
			$this->load->helper('array');	
		}
		public function getTimelogs($user,$from,$to){
			$this->db->select('id,employee_id, timein_date,time_in,time_out,hour');
			$this->db->from('timelog');
			$this->db->where('employee_id',$user);
			$this->db->where('timein_date >=',$from);
			$this->db->where('timeout_date <=',$to);
			$query = $this->db->get();
			if($query->num_rows()) {
				return $query->result_array();
			}else{
				return [];
			}
		}
		public function getOvertimes($user,$from,$to){
			$this->db->select('id,employee_id, start_date,start_time,end_time,hour');
			$this->db->from('overtime');
			$this->db->where('employee_id',$user);
			$this->db->where('status','approved');
			$this->db->where('start_date >=',$from);
			$this->db->where('start_date <=',$to);
			$query = $this->db->get();
			if($query->num_rows()) {				
				return $query->result_array();
			}else{
				return [];
			}
		}
		public function report_stats($month='all',$year='all'){
			$final = [];
			$approved_ot = [];
			$total_ot = [];
			$sickleave = [];
			$vacation = [];
			$paternity = [];
			$maternity = [];
			//employees list
			$this->db->select('employee.id, employee.firstname, employee.lastname, employee.gender');
			$this->db->from('employee');
			$query = $this->db->get();
			
			if($year=='all'){ //Get statistics for all time
				//approved ot
				$this->db->select('employee.id, sum(overtime.hour) as approved_ot');
				$this->db->from('employee');
				$this->db->join('overtime','employee.id=overtime.employee_id');
				$this->db->where('overtime.status','approved');
				$this->db->group_by('overtime.employee_id');
				$query2 = $this->db->get();
				
				//total ot
				$this->db->select('employee.id, sum(overtime.hour) as total_ot');
				$this->db->from('employee');
				$this->db->join('overtime','employee.id=overtime.employee_id');
				$this->db->where('overtime.status !=','declined');
				$this->db->group_by('overtime.employee_id');
				$query3 = $this->db->get();
				
				//Sick Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','sick_leave');
				$query4 = $this->db->get();
							
				//Vacation Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','vacation_leave');
				$query5 = $this->db->get();
				
				//Paternity Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','paternity_leave');
				$query6 = $this->db->get();
				
				//Maternity Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','maternity_leave');
				$query7 = $this->db->get();
			}
			else if($month=='all'){ //get values for Specific year for all months
				//approved ot
				$this->db->select('employee.id, sum(overtime.hour) as approved_ot');
				$this->db->from('employee');
				$this->db->join('overtime','employee.id=overtime.employee_id');
				$this->db->where('overtime.status','approved');
				$this->db->where('overtime.start_date <=',$year.'-12-31');
				$this->db->where('overtime.start_date >=',$year.'-01-01');
				$this->db->group_by('overtime.employee_id');
				$query2 = $this->db->get();
				
				//total ot
				$this->db->select('employee.id, sum(overtime.hour) as total_ot');
				$this->db->from('employee');
				$this->db->join('overtime','employee.id=overtime.employee_id');
				$this->db->where('overtime.status !=','declined');
				$this->db->where('overtime.start_date <=',$year.'-12-31');
				$this->db->where('overtime.start_date >=',$year.'-01-01');
				$this->db->group_by('overtime.employee_id');
				$query3 = $this->db->get();
				
				//Sick Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','sick_leave');
				$this->db->where('leave.start_date <=',$year.'-12-31');
				$this->db->where('leave.start_date >=',$year.'-01-01');
				$query4 = $this->db->get();
							
				//Vacation Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','vacation_leave');
				$this->db->where('leave.start_date <=',$year.'-12-31');
				$this->db->where('leave.start_date >=',$year.'-01-01');
				$query5 = $this->db->get();
				
				//Paternity Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','paternity_leave');
				$this->db->where('leave.start_date <=',$year.'-12-31');
				$this->db->where('leave.start_date >=',$year.'-01-01');
				$query6 = $this->db->get();
				
				//Maternity Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','maternity_leave');
				$this->db->where('leave.start_date <=',$year.'-12-31');
				$this->db->where('leave.start_date >=',$year.'-01-01');
				$query7 = $this->db->get();
			}
			else{//Specific year and month
				//approved ot
				$start = $year.'-'.$month."-01";
				$end = date($year.'-'.$month."-t");
				$this->db->select('employee.id, sum(overtime.hour) as approved_ot');
				$this->db->from('employee');
				$this->db->join('overtime','employee.id=overtime.employee_id');
				$this->db->where('overtime.status','approved');
				$this->db->where('overtime.start_date <=',$end);
				$this->db->where('overtime.start_date >=',$start);
				$this->db->group_by('overtime.employee_id');
				$query2 = $this->db->get();
				
				//total ot
				$this->db->select('employee.id, sum(overtime.hour) as total_ot');
				$this->db->from('employee');
				$this->db->join('overtime','employee.id=overtime.employee_id');
				$this->db->where('overtime.status !=','declined');
				$this->db->where('overtime.start_date <=',$end);
				$this->db->where('overtime.start_date >=',$start);
				$this->db->group_by('overtime.employee_id');
				$query3 = $this->db->get();
				
				//Sick Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','sick_leave');
				$this->db->where('leave.start_date <=',$end);
				$this->db->where('leave.start_date >=',$start);
				$query4 = $this->db->get();
							
				//Vacation Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','vacation_leave');
				$this->db->where('leave.start_date <=',$end);
				$this->db->where('leave.start_date >=',$start);
				$query5 = $this->db->get();
				
				//Paternity Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','paternity_leave');
				$this->db->where('leave.start_date <=',$end);
				$this->db->where('leave.start_date >=',$start);
				$query6 = $this->db->get();
				
				//Maternity Leave
				$this->db->select('leave.employee_id, leave.start_date, leave.end_date');
				$this->db->from('leave');
				$this->db->join('leave_type','leave_type.id = leave.leave_type_id');
				$this->db->where('leave_type.code','maternity_leave');
				$this->db->where('leave.start_date <=',$end);
				$this->db->where('leave.start_date >=',$start);
				$query7 = $this->db->get();
			}
			
			
			if($query2->num_rows()) {
				$approved_ot = $query2->result_array();
			}
			if($query3->num_rows()) {
				$total_ot = $query3->result_array();
			}
			if($query4->num_rows()) {
				$sickleave = $query4->result_array();
			}
			if($query5->num_rows()) {
				$vacation = $query5->result_array();
			}
			if($query6->num_rows()) {
				$paternity = $query6->result_array();
			}
			if($query7->num_rows()) {
				$maternity = $query7->result_array();
			}
			if($query->num_rows()) {
				$employees = $query->result_array();
				
				foreach($employees as $res){
				
					$approved_ot_value = 0;
					$total_ot_value = 0;
					$total_sick_leaves = 0;
					$total_vacation_leaves = 0;
					$total_paternity_leaves = 0;
					$total_maternity_leaves = 0;
					foreach($approved_ot as $workarea){
						if($res['id']==$workarea['id']){
							$approved_ot_value = $workarea['approved_ot'];
							break;
						}
					}	
					
					foreach($total_ot as $workarea){
						if($res['id']==$workarea['id']){
							$total_ot_value = $workarea['total_ot'];
							break;
						}
					}	
					
					foreach($sickleave as $workarea){
						if($res['id']==$workarea['employee_id']){		
							$date1 = date_create($workarea['end_date']);	
							$date2 = date_create($workarea['start_date']);				
							$date_diff = date_diff($date1,$date2,true);					
							$total_sick_leaves = $total_sick_leaves + $date_diff->format('%d');
						}
					}
					
					foreach($vacation as $workarea){
						if($res['id']==$workarea['employee_id']){
							
							$date1 = date_create($workarea['end_date']);	
							$date2 = date_create($workarea['start_date']);				
							$date_diff = date_diff($date1,$date2,true);					
							$total_vacation_leaves = $total_vacation_leaves + $date_diff->format('%d');
						}
					}
					foreach($paternity as $workarea){
						if($res['id']==$workarea['employee_id']){		
							$date1 = date_create($workarea['end_date']);	
							$date2 = date_create($workarea['start_date']);				
							$date_diff = date_diff($date1,$date2,true);					
							$total_paternity_leaves = $total_paternity_leaves + $date_diff->format('%d');
						}
					}
					foreach($maternity as $workarea){
						if($res['id']==$workarea['employee_id']){		
							$date1 = date_create($workarea['end_date']);	
							$date2 = date_create($workarea['start_date']);				
							$date_diff = date_diff($date1,$date2,true);					
							$total_maternity_leaves = $total_maternity_leaves + $date_diff->format('%d');
						}
					}	
					$final[] = array(
						'id' => $res['id'],
						'firstname' => $res['firstname'],
						'lastname'  => $res['lastname'],
						'gender'	=> $res['gender'],
						'approved_ot' => $approved_ot_value,
						'total_ot' => $total_ot_value,
						'sickleave' => $total_sick_leaves,
						'vacation' => $total_vacation_leaves,
						'paternity' => $total_paternity_leaves,
						'maternity' => $total_maternity_leaves,
						'absences'=>''
					);
					
				}		
				return $final;
			}
		}
	}