<div class="employee-management-wrapper">
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Employee Management
				<small></small>
			</h1>
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-danger">
						<div class="box-body">
							<table id="example1" class="table table-hover">
								<thead>
									<tr>
										<th>Employee Name</th>
										<th>Type</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								
								<tbody>

<?php
								if($employee_list !== FALSE) {

									foreach ($employee_list as $index => $employee) {										
										if(strlen($employee['middlename']) > 0) {
											$employee_name = $employee['firstname'] . ' ' . substr($employee['middlename'], 0, 1) . ' ' . $employee['lastname'];
										}
										else{
											$employee_name = $employee['firstname'] . ' ' . $employee['lastname'];
										}
										

										$type = function($code) {

											$position_type = null;

											switch ($code) {
												case 'super_admin':
													$position_type = 'Super Admin';
												break;

												case 'admin':
													$position_type = 'Admin';
												break;

												case 'regular_employee':
													$position_type = 'Employee';
												break;

												case 'project_manager':
													$position_type = 'Project Manager';
												break;

												case 'board_of_director':
													$position_type = 'Board Of Director';
												break;

												default:
													$position_type = 'N/A';
												break;
											}

											return $position_type;
										};

										if($employee['status'] == 1) {
											$status = '<span class="label label-success">Active</span>';
										}else { $status = '<span class="label label-danger">Inactive</span>'; }

?>

									<tr>
										<td> <?= $employee_name; ?> </td>
										<td> <?= $type( $employee['type'] )?> </td>
										<td> <?= $status; ?> </td>
										
										<td>
											<div class="btn-group">
												<a class="btn btn-info btn-sm" href="<?= base_url('admin/employee-profile/' . $employee['id']); ?>">View</a>
												<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
													<span class="caret"></span>
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												
												<ul class="dropdown-menu" role="menu">
													<li><a href="<?= base_url('admin/employee-timelog/').'/'.$employee['id']?>">View Time Logs</a></li>
													<li class="divider"></li>
													<li><a href="#employee-deactivate-prompt"
													<?php
														if($employee['status']){
															echo 'onclick="setActivateModalText(\'deactivate\','.$employee["id"].',\''.$employee["firstname"].' '.$employee["middlename"].' '.$employee["lastname"].'\')"';
														}
														else{
															echo 'onclick="setActivateModalText(\'activate\','.$employee["id"].',\''.$employee["firstname"].' '.$employee["middlename"].' '.$employee["lastname"].'\')"';				}
													 ?>
													data-toggle="modal">
													<?php
														if($employee['status']){
															echo 'Deactivate';
														}
														else{
															echo 'Activate';
														}
													  ?>
													</a></li>
												</ul>
											</div>
										</td>
									</tr>

<?php										

									}

								}else {
									echo "
										<tr>
											<td> <p class='error'>No record</p> </td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									";
								}

?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>