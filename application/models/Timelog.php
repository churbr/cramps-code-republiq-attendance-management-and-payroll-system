<?php

	if(!defined('BASEPATH')) exit('No direct script access allowed.');
	
	class Timelog extends CI_Model {

		/* "Any task can be accomplished as long as it's broken down into manageable pieces" */
			
		public function __construct() {
			$this->load->library('e_attendance');
			$this->load->model(array('holiday', 'overtime', 'leave'));
			parent::__construct();
		}

		// --------------------------------------------------------------------------

		public function view($employee_id = null, $from = null, $to = null) {

			$overall_timelog = array();

			if(!is_null($from) && !is_null($to)) {

				$date_range = date_range($from, $to);

				if($date_range !== false) {
					$timelogs = self::get_specific($employee_id, $date_range);
					$leave_dates = $this->leave->timelog_specific($employee_id, $date_range);
					$overtime_logs = $this->overtime->timelog_specific($employee_id, $date_range);
					$timelog_overtime = $this->timelog_overtime->timelog_specific($employee_id, $date_range);

					if($timelogs !== false) {  $overall_timelog = array_merge($overall_timelog, $timelogs); }
					if($leave_dates !== false) {  $overall_timelog = array_merge($overall_timelog, $leave_dates); }
					if($overtime_logs !== false) {  $overall_timelog = array_merge($overall_timelog, $overtime_logs); }
					if($timelog_overtime !== false) {  $overall_timelog = array_merge($overall_timelog, $timelog_overtime); }
				}

			}else {

				$leave_dates = $this->leave->timelog($employee_id);
				$overtime_logs = $this->overtime->timelog($employee_id);
				$timelog_overtime = $this->timelog_overtime->timelog($employee_id);
				$timelogs = self::get($employee_id);

				if($leave_dates !== false) { $overall_timelog = array_merge($overall_timelog, $leave_dates); }
				if($overtime_logs !== false) {  $overall_timelog = array_merge($overall_timelog, $overtime_logs); }
				if($timelog_overtime !== false) { $overall_timelog = array_merge($overall_timelog, $timelog_overtime); }
				if($timelogs !== false) { $overall_timelog = array_merge($overall_timelog, $timelogs); }

			}

			usort($overall_timelog, function($date_a, $date_b) {
		 		return $date_a['timestamp'] < $date_b['timestamp'];
			});

			return $overall_timelog;
		}

		// --------------------------------------------------------------------------

		public function get($employee_id = null, $table_pk = null) {
			$timelogs = array();

			$this->db->where('employee_id', $employee_id);
			if(!is_null($table_pk)) { $this->db->where('`id`', $table_pk); }
			$this->db->where('`show`', 1);
			$query = $this->db->get('`timelog`');

			if($query->num_rows()) {
				foreach ($query->result_array() as $index => $log) {
					$timelogs[] = array(
						'type' => 'timelog',
						'id' => $log['id'],
						'time_in' => $log['time_in'],
						'time_out' => $log['time_out'],
						'timein_date' => nice_date($log['timein_date'], 'M d, Y'),
						'timeout_date' => nice_date($log['timeout_date'], 'M d, Y'),
						'hour' => $log['hour'],
						'minute' => $log['minute'],
						'timestamp' => strtotime($log['timein_date'])
					);
				}

				return $timelogs;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_specific($employee_id = null, $dates = null) {
			$timelogs = array();

			$this->db->where('employee_id', $employee_id);
			$this->db->where_in('timein_date', $dates);
			// $this->db->or_where_in('timeout_date', $dates);
			// $this->db->where_in('timeout_date', $dates);
			$this->db->where('`show`', 1);
			$query = $this->db->get('timelog');

			if($query->num_rows()) {
				foreach ($query->result_array() as $index => $log) {
					$timelogs[] = array(
						'type' => 'timelog',
						'id' => $log['id'],
						'time_in' => $log['time_in'],
						'time_out' => $log['time_out'],
						'timein_date' => nice_date($log['timein_date'], 'M d, Y'),
						'hour' => $log['hour'],
						'minute' => $log['minute'],
						'timestamp' => strtotime($log['timein_date'])
					);
				}

				return $timelogs;
			}

			return false;

			//return $query;
		}

		// --------------------------------------------------------------------------

		public function time_in() {

			$date		= date('Y-m-d');
			$time_in 	= date('h:i a');

			$this->db->insert('timelog', array(
				'id' => NULL,
				'employee_id' => $this->session->id,
				'time_in' => $time_in,
				'time_out' => NULL,
				'timein_date' => $date,
				'timeout_date' => NULL,
				'hour' => NULL,
				'minute' => NULL,
				'pay' => NULL
			));

			if($this->db->affected_rows()) {
				self::log_timein();
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function time_out() {

			$timed_in = self::employee_timedin();

			if($timed_in) {

				$time_in  = self::get_timein();
				$time_out = date('h:i a');
				$timein_date = self::get_timein_date();
				$timeout_date = date('Y-m-d');
				$holiday = $this->holiday->type($timein_date);

				$time = $this->e_attendance->calculate_hours($time_in, $time_out, $timein_date, $timeout_date);
				$payment = $this->e_attendance->generate_pay($time['hour']);

				// if($holiday) {
				// 	$type = $holiday;

				// 	if(strcmp($type, 'regular') === 0) { $payment = $payment * 2; }
				// 	if(strcmp($type, 'special') === 0) { $payment = $payment * 1.30; }
				// }

				$this->db->where('time_out', NULL);
				$this->db->where('employee_id', $this->session->id);
				$this->db->update('timelog', array(
					'time_out' => $time_out,
					'timeout_date' => $timeout_date,
					'hour' => $time['hour'],
					'minute' => $time['minute'],
					'pay' => $payment
				));

				if($this->db->affected_rows()) {
					self::log_timeout();
					return true;
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function employee_timedin() {
			$this->db->where('time_in !=', NULL);
			$this->db->where('time_out', NULL);
			$this->db->where('employee_id', $this->session->id);
			$query = $this->db->get('timelog');

			$status = ($query->num_rows() > 0) ? true : false;

			return $status;
		}

		// --------------------------------------------------------------------------

		public function get_timein() {
			$this->db->select('time_in');
			$this->db->where('employee_id', $this->session->id);
			$this->db->where('time_out', NULL);
			$query = $this->db->get('timelog');

			if($query->num_rows()) {
				return $query->result_array()[0]['time_in'];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_timein_date() {
			$this->db->select('timein_date');
			$this->db->where('employee_id', $this->session->id);
			$this->db->where('time_out', NULL);
			$query = $this->db->get('timelog');

			if($query->num_rows()) {
				return $query->result_array()[0]['timein_date'];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function log_timein() {

			if($this->session->is_loggedin) {

				$this->db->select('*');
				$this->db->where('employee_id', $this->session->id);
				$this->db->where('timedin_date', date('Y-m-d'));
				$query = $this->db->get('loglimit');

				if($query->num_rows()) {
					// UPDATE
					$this->db->where('employee_id', $this->session->id);
					$this->db->where('timedin_date', date('Y-m-d'));
					$this->db->set('logged_timeins', 'logged_timeins+1', FALSE);
					$this->db->update('loglimit');

					if($this->db->affected_rows()) {
						return true;
					}

				}else {
					// INSERT
					$this->db->insert('loglimit', array(
						'id' => NULL,
						'employee_id' => $this->session->id,
						'logged_timeins' => 1,
						'logged_timeouts' => 0,
						'timedin_date' => date('Y-m-d'),
						'timedout_date' => NULL
					));

					if($this->db->affected_rows()) {
						return true;
					}
				}

			}

			return false;
		}

		// --------------------------------------------------------------------------
		
		public function log_timeout() {

			if($this->session->is_loggedin) {

				$this->db->select('*');
				$this->db->where('employee_id', $this->session->id);
				$this->db->where('timedin_date', date('Y-m-d'));
				$query = $this->db->get('loglimit');

				if($query->num_rows()) {
					$this->db->where('employee_id', $this->session->id);
					$this->db->where('timedin_date', date('Y-m-d'));
					$this->db->set('logged_timeouts', 'logged_timeouts+1', FALSE);
					$this->db->set('timedout_date', date('Y-m-d'));
					$this->db->update('loglimit');

					if($this->db->affected_rows()) {
						return true;
					}
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function on_leave($employee_id = null) {

			if(!is_null($employee_id)) {
				$leave_dates = $this->leave->get_dates($employee_id);
				$date_today = date('Y-m-d');

				if($leave_dates !== false) {
					if(in_array($date_today, $leave_dates)) { return true; }
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_data($table_pk = null, $employee_id = null) {
			$this->db->where('id', $table_pk);
			$this->db->where('employee_id', $employee_id);
			$query = $this->db->get('timelog');

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function count_hours($timelog = null) { // Total hours count only for weekly attendance

			$total_reghour = 0;
			$total_regmin = 0;
			$total_ot_hour = 0;
			$total_ot_min = 0;

			if($timelog) {

				foreach ($timelog as $index => $log) {
					foreach ($log as $day => $day_index) {
						foreach ($day_index as $timelog_info) {

							if($timelog_info['type'] == 'leave') {

								$total_reghour += $timelog_info['total_time'];

							}elseif($timelog_info['type'] == 'overtime') {

								$total_ot_hour += $timelog_info['hour'];
								$total_ot_min += $timelog_info['minute'];

							}elseif($timelog_info['type'] == 'overtime_pending') {

								$total_reghour += $timelog_info['hour'];
								$total_regmin += $timelog_info['minute'];

							}elseif ($timelog_info['type'] == 'overtime_declined') {

								$total_reghour += $timelog_info['hour'];
								$total_regmin += $timelog_info['minute'];

							} else { // `timelog` && `timelog_overtime` table included

								$total_reghour += $timelog_info['hour'];
								$total_regmin += $timelog_info['minute'];

							}
						}
					}
				}


				$regular = $this->e_attendance->convert_hours($total_reghour, $total_regmin);
				$overtime = $this->e_attendance->convert_hours($total_ot_hour, $total_ot_min);


				return array (
					'regular' => $regular,
					'overtime' => $overtime
				);
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function ajax_count_hours($timelog = null) {

			$total_reghour = 0;
			$total_regmin = 0;
			$total_ot_hour = 0;
			$total_ot_min = 0;

			if($timelog) {

				foreach ($timelog as $index => $log) {

					if($log['type'] == 'leave') {

						$total_reghour += $log['total_time'];
					
					}elseif($log['type'] == 'overtime') {

						if( $log['overtime_status'] == 'pending' ) {

							$total_reghour += $log['hour'];
							$total_regmin += $log['minute'];

						} elseif( $log['overtime_status'] == 'declined' ) {

							$total_reghour += $log['hour'];
							$total_regmin += $log['minute'];

						} elseif( $log['overtime_status'] == 'approved' ) {

							$total_ot_hour += $log['hour'];
							$total_ot_min += $log['minute'];

						}
					
					} elseif($log['type'] == 'timelog_overtime') {

						$total_reghour += $log['hour'];
						$total_regmin += $log['minute'];

					} else {
					
						$total_reghour += $log['hour'];
						$total_regmin += $log['minute'];
					
					}
				}

				$regular = $this->e_attendance->convert_hours($total_reghour, $total_regmin);
				$overtime = $this->e_attendance->convert_hours($total_ot_hour, $total_ot_min);

				return array (
					'regular' => $regular,
					'overtime' => $overtime
				);
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function show($table_pk = null) {

			$this->db->where('id', $table_pk);
			$this->db->update('timelog', array('`show`' => 1));

			if($this->db->affected_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function hide($table_pk = null) {

			$this->db->where('id', $table_pk);
			$this->db->update('timelog', array('`show`' => 0));

			if($this->db->affected_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------
	}