<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Setting extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}

		public function static_ip() {
			$this->db->select('value');
			$this->db->where('name', 'static_ip');
			$query = $this->db->get('setting');

			if($query->num_rows()) {
				$result = $query->result_array()[0];
				return $result['value'];
			}

			return false;
		}

		public function regular_hour() {
			$this->db->select('value');
			$this->db->where('name', 'regular_hours');
			$query = $this->db->get('setting');

			if($query->num_rows()) {
				$result = $query->result_array()[0];
				return $result['value'];
			}

			return false;
		}
	}