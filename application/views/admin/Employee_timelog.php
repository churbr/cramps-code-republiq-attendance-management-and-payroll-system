
      <!-- Employee Time Logs Wrapper -->
      <div class="employee-time-logs-wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?=$basic['firstname'].' '.$basic['lastname']?>
              <small>
                <ol class="breadcrumb">
                  <li><a href="<?=base_url('admin/home')?>"><i class="fa fa-user"></i> Manage Employee</a></li>
                  <li class="active">Time Logs</li>
                </ol>
              </small>
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">

            <!-- Your Page Content Here -->

            <div class="row">
              <div class="col-md-4 col-sm-8 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-red"><i class="fa fa-clock-o"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Regular Hours</span>
                    <span class="info-box-number">
                      <?php
                       $total_reg = 0;
                       foreach($timelogs as $timelog){
                          $total_reg += $timelog['hour'];
                       }
                       echo $total_reg;
                      ?>
                    </span>
                  </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
              </div><!-- /.col -->
              <div class="col-md-4 col-sm-8 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-red"><i class="fa fa-hourglass"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Overtime</span>
                    <span class="info-box-number">
                      <?php
                       $total_ot = 0;
                       foreach($overtimes as $ot){
                          $total_ot += $ot['hour'];
                       }
                       echo $total_ot;
                      ?>
                    </span>
                  </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
              </div><!-- /.col -->
            </div>
            
            <?= form_open('admin/employee-timelog/'.$emp_id, array('id' => 'emp_timelog')); ?>
            <div class="row">
              <div class="form-group col-md-4 col-sm-4 col-xs-12">
                <label>From</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                  <input type="hidden" value="<?=$emp_id?>" id="emp_id" name="emp_id" />
                  <input type="date" value="<?=date('Y-m-d')?>" class="form-control pull-right" id="timelog_from" name="timelog_from" required>
                </div>
              </div>
              <div class="form-group col-md-4 col-sm-4 col-xs-12">
                <label>To</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                    </div>
                  <input type="date" class="form-control pull-right" id="timelog_to" name="timelog_to" required>
                </div>
              </div>
              <div class="form-group col-md-4 col-sm-4 col-xs-12">
                <label>Search</label>
                <div class="input-group">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
            <?= form_close(); ?>

            <div class="row">
              <div class="col-xs-12">
                <div class="box box-danger">
                  <div class="box-body">
                    <table id="example1" class="table table-hover">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Time In</th>
                          <th>Time Out</th>
                          <th>Total Hours</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?php                          
                          foreach($timelogs as $timelog){
                          ?>
                          <tr>
                          <td><?=$timelog['timein_date']?></td>
                          <td><?=$timelog['time_in']?></td>
                          <td><?=$timelog['time_out']?></td>
                          <td><?=$timelog['hour']?>
                          <?php
                            if(in_array($timelog['timein_date'],array_column($overtimes,'start_date'))){
                              echo '<span class="label bg-teal">OT</span>';
                            }
                          ?>
                          </td>
                          <td>
                            <!--<div class="btn-group">
                              <a type="button" class="btn btn-primary btn-sm" href="#edit-regular-time-log-modal" onclick="$('#timelog_modal_title').html('<?=nice_date($timelog['timein_date'],'F d, Y')?>');$('#timelog_edit_from').val('<?=nice_date($timelog['time_in'],'HH:MM SS')?>')" data-toggle="modal">Edit</button>
                              </a>
                            </div>
                          -->
                          </td>
                        </tr>
                          <?php
                          }
                        ?>                        
                      </tbody>
                    </table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- /.col -->
            </div><!-- /.row -->

          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.employee-management-wrapper -->
