<div class="content-wrapper">
  <div class="container">
    <section class="content-header">
    
      <div class="display-time">
        <div id="time">
        <span>

          <script type="text/javascript">
            (function () {
              function checkTime(i) {
                return (i < 10) ? "0" + i : i;
              }

              function startTime() {
                var today = new Date(),
                h = checkTime(today.getHours()),
                m = checkTime(today.getMinutes()),
                s = checkTime(today.getSeconds());
                
                ampm = h >= 12 ? 'PM' : 'AM';
                h = ((h + 11) % 12 + 1);
                
                document.getElementById('time').innerHTML = h + ":" + m + " " +ampm;
                
                time.style.fontSize = "11em";
                time.style.fontFamily = "dinpro";
                
                t = setTimeout(function () {
                  startTime()
                }, 500);
              }

              startTime();

            })(); 
          </script>

        </span>
        </div>

        <div class="box-body">
          <h2> <?= date('l M d, Y'); ?> </h2>
        </div>

        <div id='timelog_button_container'>

<?php
  if(isset($visitor_ip, $static_ip) && strcmp($visitor_ip, $static_ip) === 0 && $this->session->is_loggedin && !$this->timelog->on_leave($this->session->id)) {
      $timein_text = ($timed_in == true) ? 'OUT' : 'IN';
?>
      <a href='#time-prompt-modal' data-toggle='modal'>
        <input class='round-button' type='submit' value="<?= $timein_text; ?>"/>
      </a>
<?php

  }else {

    $hover_msg = null;

    if(strcmp($visitor_ip, $static_ip) !== 0) { $hover_msg = 'You can only time in/out within the office.'; }
    if($this->timelog->on_leave($this->session->id)) { $hover_msg = 'You are currently on leave.'; }

    $smiley_icon = base_url('resources/dist/img/icons/locked.png');
    echo "<img src='$smiley_icon' width='200px' data-toggle='tooltip' data-placement='right' data-original-title='$hover_msg' />";
  }
?>

        </div>
      </div>


    </section>
  </div>
</div>