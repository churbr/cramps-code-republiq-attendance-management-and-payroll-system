	var dataref = new Firebase("https://e-notification.firebaseio.com/");

	$(document).ready(function() {

		var base_url = $('.base_url').attr('id');

		$.post(
			base_url + 'user/info.html',
			{ request: true },
			function(response) {
				var parsed = jQuery.parseJSON(response);
				session_employee_id = parsed.employee_id;
				session_employee_type = parsed.type;
			}
		);




		// -------------------------------------------------------------------------------------------------------------------




		/* Only use this in child changed for employee */
		(function($) {
			$.extend({
				play_sound: function() {
					return $(
						'<audio autoplay="autoplay" style="display:none;">'
						+ '<source src="' + base_url + 'resources/ding.mp3" />'
						+ '<embed src="' + base_url + 'resources/ding.mp3" hidden="true" autostart="true" loop="false" class="playSound" />'
						+ '</audio>'
					).appendTo('body');
				}
			});
		})(jQuery);



		// -------------------------------------------------------------------------------------------------------------------



		$('form.request_leave').on('submit', function() {

			var that = $(this),
						url		= that.attr('action'),
						method	= that.attr('method'),
						data	= {};

			that.find('[name]').each(function() {
				var that = $(this),
						name		= that.attr('name'),
						value		= that.val();
						data[name]	= value;
			});

			$.ajax({
				url: url,
				type: method,
				data: data,
				success: function(response) {
					var parsed = jQuery.parseJSON(response);

					if(!jQuery.isEmptyObject(parsed)) {

						dataref.push({
							id: parsed.id,
							notification_type_id: parsed.notification_type_id,
							notification_id: parsed.notification_id,
							sender_id: session_employee_id,
							receiver_id: parsed.employee_id,
							message: parsed.message,
							status: parsed.status,
							view: 0,
							date: parsed.date
						});

						if($('#reqfeed_table').length) {
							window.location.replace(base_url + 'request-feed.html');
						}

						$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');

						$('#leave_req_type').val('');
						$('#leave_req_from').val('');
						$('#leave_req_to').val('');
						$('#leave_request_message').html("<p class='green' style='font-size: 16px; padding: 5px 0px 10px; text-align: center;'>Leave Request Submitted!</p>");

						setTimeout(function() {
							$('#leave_request_message').html('');
							$('#leave_request_close').click();
						}, 2000);

					} else {

						switch(response) {
							case '1':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>Please fill in the form.</p>");
							break;

							case '2':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>The date has already passed.</p>");
							break;

							case '3':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>Invalid range of date.</p>");
							break;

							case '4':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>Type of request doesn't exist.</p>");
							break;

							case '5':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>Non-working days is not included.</p>");
							break;

							case '6':
								leave_desc = '';
								leave_val = $('#leave_req_type').val();

								if(leave_val == 1) {
									leave_desc = 'sick leave';
								}else if(leave_val == 2) {
									leave_desc = 'vacation leave';
								}else if(leave_val == 3) {
									leave_desc = 'paternity leave';
								}else if(leave_val == 4) {
									leave_desc = 'maternity leave';
								}

								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>You don't have enough " + leave_desc + " remaining days.</p>");
							break;
						}

					}
				}
			});

			return false;
		});



		// -------------------------------------------------------------------------------------------------------------------



		$('form.request_overtime').on('submit', function() {

			var that = $(this),
						url		= that.attr('action'),
						method	= that.attr('method'),
						data	= {};

			that.find('[name]').each(function() {
				var that = $(this),
						name		= that.attr('name'),
						value		= that.val();
						data[name]	= value;
			});

			$.ajax({
				url: url,
				type: method,
				data: data,
				success: function(response) {
					var parsed = jQuery.parseJSON(response);

					if(!jQuery.isEmptyObject(parsed)) {

						dataref.push({
							id: parsed.id,
							notification_type_id: parsed.notification_type_id,
							notification_id: parsed.notification_id,
							sender_id: session_employee_id,
							receiver_id: parsed.employee_id,
							message: parsed.message,
							status: parsed.status,
							view: 0,
							date: parsed.date
						});

						$('#overtime_application_msg').html('<div class="box_success">Overtime Application Sent</div>');

						setTimeout(function() {
							$('#timelog_table').load(location.href + ' #timelog_table');
							$('#overtime_application_msg').html('');
							$('#overtime_request_close').click();
						}, 2000);

					} else {

						switch(response) {
							case '1':
								$('#overtime_application_msg').html('<div class="box_error">Request must be atleast 1 hour</div>');
							break;

							case '2':
								$('#overtime_application_msg').html('<div class="box_error">Time must be within range of your time log</div>');
							break;

							case '3':
								$('#overtime_application_msg').html('<div class="box_error">Select Project Manager</div>');
							break;
						}
						
					}
				}
			});

			return false;
		});



		// ---------------------------------------------------------------------------------------------------------------------------



		dataref.on('child_added', function(snapshot) { // Detect Inserts
			var server_data = snapshot.val();
			var child_key = snapshot.key();

			if(session_employee_type == 'super_admin') {
			} else if(session_employee_type == 'admin') {
			} else if(session_employee_type == 'board_of_director') {
			} else if(session_employee_type == 'project_manager') {
			} else { }
		});



		// ---------------------------------------------------------------------------------------------------------------------------


		dataref.on('child_changed', function(snapshot) { // Detect Updates

			var server_data = snapshot.val();
			var child_key = snapshot.key();

			if (session_employee_type == 'super_admin') {
			} else if(session_employee_type == 'admin') {
			} else if(session_employee_type == 'board_of_director') {
			} else if(session_employee_type == 'project_manager') {
			}else { // regular_employee
				notify_regemp(server_data.notification_type_id, server_data.notification_id, server_data.status, server_data.sender_id, server_data.receiver_id, server_data.view);
			}

		});

	});



	// ---------------------------------------------------------------------------------------------------------------------------



	function notify_regemp(notification_type_id, notification_id, status, sender_id, receiver_id, view) {
		
		if(notification_type_id == 1 || notification_type_id == 2) {
			if(sender_id == session_employee_id && status != 0) {
				$.play_sound();
				construct_notification(notification_type_id, notification_id, status, sender_id, receiver_id);
				$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');
			}
		}
	}



	// ---------------------------------------------------------------------------------------------------------------------------

	function construct_notification(notification_type_id, notification_id, status, sender_id, receiver_id) {

		/** FLAGS:
		 *
		 *  0 : Pending
		 *  1 : Approved
		 *	-1 : Declined
		 **/

		var base_url = $('.base_url').attr('id');
		var action = '';
		var application = '';
		var html_format = '';
		
		$.post(
			base_url + 'user/get-info.html',
			{ employee_id: receiver_id },
			function(response) {

				var parsed = jQuery.parseJSON(response);

				if(!jQuery.isEmptyObject(parsed)) {

					if(notification_type_id == 1) { application = 'leave'; } else { application = 'overtime'; }
					if(status == 1) { action = 'approved' } else if(status == -1) { action = 'declined'; }

					$('body').append("<div id='notifications-bottom-right'></div>");
					$('#notifications-bottom-right').fadeIn(400);

					html_format +='<a href="#">';
					html_format +='<div id="notifications-bottom-right-tab">';
					html_format +='<div id="notifications-bottom-right-tab-close" class="close">';
					html_format +='<span class="iconb" data-icon="&#xe20e;">';
					html_format +='</span>';
					html_format +='</div>';
					html_format +='<div id="notifications-bottom-right-tab-avatar">';
					html_format +='<img src="resources/dist/img/users/' + parsed.picture + '" width="70" height="70" />';
					html_format +='</div>';
					html_format +='<div id="notifications-bottom-right-tab-right">';
					html_format +='<div id="notifications-bottom-right-tab-right-title">';
					html_format +='<span>' + parsed.firstname + ' ' + parsed.lastname + '</span>';
					html_format += ' ' + action + ' your ' + application + ' application.';
					html_format +='</div>';
					html_format +='</div>';
					html_format +='</div>';
					html_format +='</a>';

					$("#notifications-bottom-right").html();
					$("#notifications-bottom-right").html(html_format);

					setTimeout(function() {
						$("#notifications-bottom-right").fadeOut(400);
					}, 5000);
				}
			}
		);

		return false;
	}

	// ---------------------------------------------------------------------------------------------------------------------------



	function update_data() {
		var samplekey = '-KBhdDyqhtEjyGWIU029';
		dataref.child(samplekey).update({'status': 1 });
	}



	// ---------------------------------------------------------------------------------------------------------------------------
