<!DOCTYPE html>
<html>
	<head>
          <title>Code Repuliq | <?= $page_title; ?> </title>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">

          <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
          
          <link rel="icon" href="<?= base_url('resources/landing/images/favicon/cr.ico'); ?>" type="image/x-icon" />
          <link rel="stylesheet" href="<?= base_url('resources/bootstrap/css/bootstrap.min.css'); ?>">
          <link rel="stylesheet" href="<?= base_url('resources/dist/css/font-awesome/css/font-awesome.min.css'); ?>">
          <link rel="stylesheet" href="<?= base_url('resources/dist/css/ionicons/css/ionicons.min.css'); ?>">
          <link rel="stylesheet" type="text/css" href="<?= base_url('resources/plugins/select2/select2.min.css'); ?>">
          <link rel="stylesheet" href="<?= base_url('resources/dist/css/AdminLTE.min.css'); ?>">
          <link rel="stylesheet" href="<?= base_url('resources/dist/css/skins/_all-skins.min.css'); ?>">
          <link rel="stylesheet" href="<?= base_url('resources/dist/css/custom.css'); ?>">
          <link rel="stylesheet" href="<?= base_url('resources/dist/css/media.css'); ?>">
          <link rel="stylesheet" href="<?= base_url('resources/dist/css/notification.css'); ?>">
          <script src="<?= base_url('resources/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>
          <script src="<?= base_url('resources/bootstrap/js/bootstrap.min.js'); ?>"></script>
          <script src="<?= base_url('resources/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
          <script src="<?= base_url('resources/plugins/fastclick/fastclick.min.js'); ?>"></script>
          <script src="<?= base_url('resources/plugins/select2/select2.full.min.js'); ?>"></script>
          <script src="<?= base_url('resources/dist/js/app.min.js'); ?>"></script>
          <script src="<?= base_url('resources/dist/js/custom.js'); ?>"></script>
          <script src="<?= base_url('resources/dist/js/multiple.js'); ?>"></script>
          <script src="<?= base_url('resources/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
          <script src="<?= base_url('resources/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>
          <script src="<?= base_url('resources/dist/js/sorting-table.js'); ?>"></script>
          <script src="<?= base_url('resources/dist/js/websocket.js'); ?>"></script>


          <script src="<?= base_url('resources/dist/js/hp_e_ajax.js'); ?>" async="async" ></script>
          <script src="<?= base_url('resources/dist/js/hp_e_notification.js'); ?>" async="async" ></script>
          <script src="<?= base_url('resources/dist/js/hp_e_functions.js'); ?>"></script>


          <link rel="stylesheet" href="<?= base_url('resources/plugins/datatables/dataTables.bootstrap.css'); ?>">
          <link rel="stylesheet" href="<?= base_url('resources/dist/css/e_style.css'); ?>">
	</head>
	<body class="hold-transition skin-purple layout-top-nav layout-boxed">
		<div class='base_url' id='<?= base_url(); ?>'></div>