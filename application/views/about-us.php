<!DOCTYPE HTML>

<html>
	<head>
		<title>CRAMPS Development Team</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="icon" href="<?= base_url('resources/landing/images/favicon/cr.ico'); ?>" type="image/x-icon" />
		<link rel="stylesheet" href="<?= base_url('resources/landing/assets/css/bootstrap.min.css'); ?>" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>resources/dist/css/main-about-us.css" />		
		<link rel="stylesheet" href="<?php echo base_url(); ?>resources/dist/css/media.css" />	
	</head>
	<body id="top">

		<!-- Header -->
			<header id="header">
				<h1><strong>Hi there! We are the<br /> CRAMPS Development Team</strong></h1><br>
				<h1><a class="about-us-login" href="<?php echo base_url(); ?>">Login</a></h1>
			</header>

		<!-- Main -->
			<div id="main">

				<!-- One -->
					<section id="one">
						<header class="major">
							<center><img src="<?= base_url('resources/dist/img/code-republiq-about-us-logo.png');?>"></center>
						</header>
						<p>We help you derive your ideas and plans for web technologies and facilitate its growth and development into a realistic outcome. The collective efforts of our scrum team, and the efficiency of new technology and constant communication enable us to deliver quality and sustainable results that ultimately create value with you, our partners.<br><br>

						With the bold, daring and revolutionary spirit of creativity, coupled with the dedicated and systematic mindset of a focused workforce, we aim to ensure that your project goes through a smooth transformation from an idea to your personal masterpiece ready for the world to see.</p>
					</section>

				<!-- Two -->
					<section id="two">
						<h2>The Team</h2>
						<p>We are BS in Information and Communications Technology students of the University of San Carlos Technological Center. We specialize in web projects and this is our Capstone Project of 2015-2016 with our Capstone Adviser, Christine Peña.</p>
						<div class="row">
							<article class="6u 12u$(xsmall) work-item">
								<a href="<?php echo base_url();?>resources/dist/img/fulls/01.jpg" class="image fit thumb"><img src="<?php echo base_url();?>resources/dist/img/thumbs/01.jpg" alt="" /></a>
								<h3>Jason Roxas</h3>
								<p>Web Design / Front-End</p>
							</article>
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="<?php echo base_url();?>resources/dist/img/fulls/02.jpg" class="image fit thumb"><img src="<?php echo base_url();?>resources/dist/img/thumbs/02.jpg" alt="" /></a>
								<h3>Reynald Chu</h3>
								<p>Back-End</p>
							</article>
							<article class="6u 12u$(xsmall) work-item">
								<a href="<?php echo base_url();?>resources/dist/img/fulls/03.jpg" class="image fit thumb"><img src="<?php echo base_url();?>resources/dist/img/thumbs/03.jpg" alt="" /></a>
								<h3>Karla Bonotan</h3>
								<p>Quality Assurance Analyst</p>
							</article>
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="<?php echo base_url();?>resources/dist/img/fulls/04.jpg" class="image fit thumb"><img src="<?php echo base_url();?>resources/dist/img/thumbs/04.jpg" alt="" /></a>
								<h3>Faye Borja</h3>
								<p>Head Documentation</p>
							</article>
						</div>
					</section>

			</div>

		<!-- Footer -->
			<footer id="footer">
				<ul class="copyright">
					<li>&copy; CRAMPS Development Team</li>
				</ul>
			</footer>

			<!-- Scripts -->
			<script src="<?php echo base_url();?>resources/dist/js/jquery.min.js"></script>
			<script src="<?php echo base_url();?>resources/dist/js/skel.min.js"></script>
			<script src="<?php echo base_url();?>resources/dist/js/util.js"></script>
			<script src="<?php echo base_url();?>resources/dist/js/main-about-us.js"></script>
			<script src="<?php echo base_url();?>resources/dist/js/jquery.poptrox.min.js"></script>

	</body>
</html>