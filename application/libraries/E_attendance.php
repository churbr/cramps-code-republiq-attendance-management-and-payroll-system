<?php if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class E_attendance {

		public function calculate_hours($time_in, $time_out, $timein_date, $timeout_date) {

			$calculated_time = array();

			// Extract timed in && out
			$time_in = self::time_extract($time_in);
			$time_out = self::time_extract($time_out);

			// Extract date timed in && out
			$timedin_date = self::date_extract($timein_date);
			$timedout_date = self::date_extract($timeout_date);

			// Convert to military time
			$timedin_hour = self::to_military($time_in['hour'], $time_in['ampm']);
			$timedout_hour = self::to_military($time_out['hour'], $time_out['ampm']);

			// Calculate span of time ( Excluding minutes yet )
			$start = mktime($timedin_hour, $time_in['minute'], 0, $timedin_date['month'], $timedin_date['day'], $timedin_date['year']); 
			$end = mktime($timedout_hour, $time_out['minute'], 0, $timedout_date['month'], $timedout_date['day'], $timedout_date['year']);

			// Now get the time rendered
			$timespan = timespan($start, $end, 5);

			$timespan = explode(',', $timespan);
			$hours_rendered = 0;
			
			foreach ($timespan as $time) {
				$time = trim($time);

				/** NOTE:
				 *	There's no need to calculate the years and months precisely.
				 *	Because it's already an invalid length of time for an employee to work.
				 *	But we'll calculate it anyway just for the sake of logic.
				 **/

				if(strpos($time, 'Year') !== false) {
					$years = explode(' ', $time);
					$hours_rendered = $hours_rendered + (intval($years[0]) * 12 * 30 * 24);
				}

				if(strpos($time, 'Month') !== false) {
					$months = explode(' ', $time);
					$hours_rendered = $hours_rendered + (intval($months[0]) * 30 * 24);
				}

				if(strpos($time, 'Week') !== false) {
					$weeks = explode(' ', $time);
					$hours_rendered = $hours_rendered + (intval($weeks[0]) * 7 * 24);
				}

				if(strpos($time, 'Day') !== false) {
					$days = explode(' ', $time);
					$hours_rendered = $hours_rendered + (intval($days[0]) * 24);
				}

				if(strpos($time, 'Hour') !== false) {
					$hours = explode(' ', $time);
					$hours_rendered = $hours_rendered + intval($hours[0]);
				}

				if(strpos($time, 'Minute') !== false) {
					$minutes = explode(' ', $time);
					$calculated_time['minute'] = $minutes[0];
				}else {
					$calculated_time['minute'] = 0;
				}
			}

			$calculated_time['hour'] = $hours_rendered;

			return $calculated_time;
		}

		// --------------------------------------------------------------------------

		public function get_timespan($timespan = null) {

			$timespan = explode(',', $timespan);
			$hour = 0;
			$minute = 0;

			foreach ($timespan as $time) {
				$time = trim($time);

				if(strpos($time, 'Hour') !== false) {
					$get_hour = explode(' ', $time);
					$hour = $get_hour[0];
				}

				if(strpos($time, 'Minute') !== false) {
					$get_minute = explode(' ', $time);
					$minute = $get_minute[0];
				}
			}

			return array(
				'hour' => $hour,
				'minute' => $minute
			);
		}

		// --------------------------------------------------------------------------
		
		public function overtime_request( $timelog_pk = null, $pm_id = null, $regdate_start = null, $regdate_end = null, $regtime_start = null, $regtime_end = null, $overtime_start = null, $overtime_end = null, $ot_reason = null ) {

			$CI = & get_instance();
			$CI->load->model(array('employee', 'timelog', 'overtime', 'timelog_overtime', 'timelog_whole'));

			$overall_status = FALSE;
			$notification = NULL;
			$error_message = NULL;

			$regtime_start_extract = self::time_extract($regtime_start);
			$regtime_end_extract = self::time_extract($regtime_end);
			$regdate_start_extract = self::date_extract($regdate_start);
			$regdate_end_extract = self::date_extract($regdate_end);

			$regtime_start_hour_military = self::to_military($regtime_start_extract['hour'], $regtime_start_extract['ampm']);
			$regtime_start_minute_military = $regtime_start_extract['minute'];
			$regtime_start_timestamp = mktime($regtime_start_hour_military, $regtime_start_minute_military, 0, $regdate_start_extract['month'], $regdate_start_extract['day'], $regdate_start_extract['year']);

			// -------------------------------------------------------------------------------------------------------------------------------

			$overtime_start_extract = self::time_extract($overtime_start);
			$overtime_start_hour_military = self::to_military($overtime_start_extract['hour'], $overtime_start_extract['ampm']);
			$overtime_start_minute_military = $overtime_start_extract['minute'];

			if(($overtime_start_hour_military - $regtime_start_hour_military) < 0) {
				$overtime_start_timestamp = mktime($overtime_start_hour_military, $overtime_start_minute_military, 0, $regdate_end_extract['month'], $regdate_end_extract['day'], $regdate_end_extract['year']);
			} else {
				$overtime_start_timestamp = mktime($overtime_start_hour_military, $overtime_start_minute_military, 0, $regdate_start_extract['month'], $regdate_start_extract['day'], $regdate_start_extract['year']);
			}

			// -------------------------------------------------------------------------------------------------------------------------------

			$overtime_end_extract = self::time_extract($overtime_end);
			$overtime_end_hour_military = self::to_military($overtime_end_extract['hour'], $overtime_end_extract['ampm']);
			$overtime_end_minute_military = $overtime_end_extract['minute'];

			if($overtime_start_hour_military < $overtime_end_hour_military) {

				if(($overtime_start_hour_military - $regtime_start_hour_military) < 0) {
					$overtime_end_timestamp = mktime($overtime_end_hour_military, $overtime_end_minute_military, 0, $regdate_end_extract['month'], $regdate_end_extract['day'], $regdate_end_extract['year']);
				} else {
					$overtime_end_timestamp = mktime($overtime_end_hour_military, $overtime_end_minute_military, 0, $regdate_start_extract['month'], $regdate_start_extract['day'], $regdate_start_extract['year']);
				}

			} else {

				if(($overtime_end_hour_military - $overtime_start_hour_military) < 0) {
					$overtime_end_timestamp = mktime($overtime_end_hour_military, $overtime_end_minute_military, 0, $regdate_end_extract['month'], $regdate_end_extract['day'], $regdate_end_extract['year']);
				} else {
					$overtime_end_timestamp = mktime($overtime_end_hour_military, $overtime_end_minute_military, 0, $regdate_start_extract['month'], $regdate_start_extract['day'], $regdate_start_extract['year']);
				}

			}

			// -------------------------------------------------------------------------------------------------------------------------------

			$regtime_end_hour_military = self::to_military($regtime_end_extract['hour'], $regtime_end_extract['ampm']);
			$regtime_end_minute_military = $regtime_end_extract['minute'];
			$regtime_end_timestamp = mktime($regtime_end_hour_military, $regtime_end_minute_military, 0, $regdate_end_extract['month'], $regdate_end_extract['day'], $regdate_end_extract['year']);

			// -------------------------------------------------------------------------------------------------------------------------------

			$lside_timespan = timespan($regtime_start_timestamp, $overtime_start_timestamp, 5);
			$requested_ot_timespan = timespan($overtime_start_timestamp, $overtime_end_timestamp, 5);
			$rside_timespan = timespan($overtime_end_timestamp, $regtime_end_timestamp, 5);

			$excess_hrs_lside = self::get_timespan($lside_timespan);
			$requested_ot_hrs = self::get_timespan($requested_ot_timespan);
			$excess_hrs_rside = self::get_timespan($rside_timespan);

			if($CI->employee->is_managed_by($pm_id)) {
				if( ($overtime_start_timestamp >= $regtime_start_timestamp) && ($overtime_end_timestamp <= $regtime_end_timestamp) ) {
					if($requested_ot_hrs['hour'] >= 1) {

						$start_regdate = date('Y-m-d', $regtime_start_timestamp); // 2016-02-25
						$start_regtime = date('h:i a', $regtime_start_timestamp); // 04:54 AM

						$start_ot_date = date('Y-m-d', $overtime_start_timestamp); // 2016-02-25
						$start_overtime = date('h:i a', $overtime_start_timestamp); // 05:00 AM

						$end_ot_date = date('Y-m-d', $overtime_end_timestamp); // 2016-02-25
						$end_overtime = date('h:i a', $overtime_end_timestamp); // 07:00 AM

						$end_regdate = date('Y-m-d', $regtime_end_timestamp); // 2016-02-25
						$end_regtime = date('h:i a', $regtime_end_timestamp); // 08:51 AM

						// Storage for trimmed time
						$lside_timelog = array();
						$rside_timelog = array();










						if( // Middle selected
							( $excess_hrs_lside['hour'] > 0 || $excess_hrs_lside['minute'] > 0 )
							&& ( $requested_ot_hrs['hour'] > 0 || $requested_ot_hrs['minute'] > 0 )
							&& ( $excess_hrs_rside['hour'] > 0 || $excess_hrs_rside['minute'] > 0 )
						) {

							// Insert to `overtime` table, this will return an array if insertion is successful; false if failed
							$overtime = $CI->overtime->request($pm_id, $start_overtime, $end_overtime, $start_ot_date, $end_ot_date, $requested_ot_hrs['hour'], $requested_ot_hrs['minute'], $ot_reason);

							// These are 2 arrays containing 2 rows of records to be inserted in `timelog_overtime`
							$lside_timelog = array (
								'id' => NULL,
								'timelog_id' => $timelog_pk,
								'overtime_id' => $overtime['id'], 
								'employee_id' => $CI->session->id,
								'time_in' => $start_regtime,
								'time_out' => $start_overtime,
								'timein_date' => $start_regdate,
								'timeout_date' => $start_ot_date,
								'hour' => $excess_hrs_lside['hour'],
								'minute' => $excess_hrs_lside['minute'],
								'pay' => 0
							);
							
							$rside_timelog = array (
								'id' => NULL,
								'timelog_id' => $timelog_pk,
								'overtime_id' => $overtime['id'],
								'employee_id' => $CI->session->id,
								'time_in' => $end_overtime,
								'time_out' => $end_regtime,
								'timein_date' => $end_ot_date,
								'timeout_date' => $end_regdate,
								'hour' => $excess_hrs_rside['hour'],
								'minute' => $excess_hrs_rside['minute'],
								'pay' => 0
							);

							// This will return true if insertion is successful, false otherwise
							$trim_status = $CI->timelog_overtime->dual_insert($lside_timelog, $rside_timelog);
							
							if($trim_status === TRUE && $overtime !== FALSE) {
								if($CI->timelog->hide($timelog_pk)) {
									$overall_status = TRUE;
									$notification = $CI->notification->add(2, $overtime['id'], $pm_id, 'Overtime Application');
								}
							}
						}










						elseif( // Right selected
							( $excess_hrs_lside['hour'] > 0 || $excess_hrs_lside['minute'] > 0 )
							&& ( $requested_ot_hrs['hour'] > 0 || $requested_ot_hrs['minute'] > 0 )
							&& ( $excess_hrs_rside['hour'] == 0 && $excess_hrs_rside['minute'] == 0 )
						) {

							$overtime = $CI->overtime->request($pm_id, $start_overtime, $end_overtime, $start_ot_date, $end_ot_date, $requested_ot_hrs['hour'], $requested_ot_hrs['minute'], $ot_reason);
							
							$lside_timelog = array (
								'id' => NULL,
								'timelog_id' => $timelog_pk,
								'overtime_id' => $overtime['id'], 
								'employee_id' => $CI->session->id,
								'time_in' => $start_regtime,
								'time_out' => $start_overtime,
								'timein_date' => $start_regdate,
								'timeout_date' => $start_ot_date,
								'hour' => $excess_hrs_lside['hour'],
								'minute' => $excess_hrs_lside['minute'],
								'pay' => 0
							);

							$trim_status = $CI->timelog_overtime->single_insert($lside_timelog);
							
							if($trim_status === TRUE && $overtime !== FALSE) {
								if($CI->timelog->hide($timelog_pk)) {
									$overall_status = TRUE;
									$notification = $CI->notification->add(2, $overtime['id'], $pm_id, 'Overtime Application');
								}
							}
						}










						elseif( // Left selected
							( $excess_hrs_lside['hour'] == 0 && $excess_hrs_lside['minute'] == 0 )
							&& ( $requested_ot_hrs['hour'] > 0 || $requested_ot_hrs['minute'] > 0 )
							&& ( $excess_hrs_rside['hour'] > 0 || $excess_hrs_rside['minute'] > 0 )
						) {

							$overtime = $CI->overtime->request($pm_id, $start_overtime, $end_overtime, $start_ot_date, $end_ot_date, $requested_ot_hrs['hour'], $requested_ot_hrs['minute'], $ot_reason);
							
							$rside_timelog = array (
								'id' => NULL,
								'timelog_id' => $timelog_pk,
								'overtime_id' => $overtime['id'],
								'employee_id' => $CI->session->id,
								'time_in' => $end_overtime,
								'time_out' => $end_regtime,
								'timein_date' => $end_ot_date,
								'timeout_date' => $end_regdate,
								'hour' => $excess_hrs_rside['hour'],
								'minute' => $excess_hrs_rside['minute'],
								'pay' => 0
							);

							$trim_status = $CI->timelog_overtime->single_insert($rside_timelog);
							
							if($trim_status === TRUE && $overtime !== FALSE) {
								if($CI->timelog->hide($timelog_pk)) {
									$overall_status = TRUE;
									$notification = $CI->notification->add(2, $overtime['id'], $pm_id, 'Overtime Application');
								}
							}
						}










						elseif(
							( $excess_hrs_lside['hour'] == 0 && $excess_hrs_lside['minute'] == 0 )
							&& ( $requested_ot_hrs['hour'] > 0 || $requested_ot_hrs['minute'] > 0 )
							&& ( $excess_hrs_rside['hour'] == 0 && $excess_hrs_rside['minute'] == 0 )
						) { // Whole log selected

							$overtime = $CI->overtime->request($pm_id, $start_overtime, $end_overtime, $start_ot_date, $end_ot_date, $requested_ot_hrs['hour'], $requested_ot_hrs['minute'], $ot_reason);

							$whole_timelog = array (
								'id' => NULL,
								'timelog_id' => $timelog_pk,
								'overtime_id' => $overtime['id'],
								'employee_id' => $CI->session->id,
								'show' => 1,
								'status' => 1,
								'date' => date('Y-m-d')
							);

							$whole_status = $CI->timelog_whole->insert($whole_timelog);

							if( $whole_status === TRUE && $overtime !== FALSE ) {
								if($CI->timelog->hide($timelog_pk)) {
									$overall_status = TRUE;
									$notification = $CI->notification->add(2, $overtime['id'], $pm_id, 'Overtime Application');
								}
							}
						}










					} else { $error_message = '1'; } // Request must be atleast 1 hour
				} else { $error_message = '2'; } // Time must be within range of your time log
			} else { $error_message = '3'; } // Select Project Manager

			// ***************************** //

			if($overall_status === TRUE) {
				return $notification;
			}else {
				return $error_message;
			}

			// ***************************** //

		}

		// --------------------------------------------------------------------------

		public function military_extract($military_time = null) {

			if(!is_null($military_time)) {
				$time = explode(':', $military_time);

				return array(
					'hour' => $time[0],
					'minute' => $time[1]
				);
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function time_extract($time, $to_string = false) {

			$time = explode(':', $time); 		# Separate hour from minute and ampm
			$smnampm = explode(' ', $time[1]);	# Separate minute from ampm

			if($to_string) {
				$hour = $time[0];
				$minute = $smnampm[0];
				$ampm = $smnampm[1];
			}else {
				$hour = (int)$time[0];
				$minute = (int)$smnampm[0];
				$ampm = $smnampm[1];
			}

			return array(
				'hour' => $hour,
				'minute' => $minute,
				'ampm' => $ampm
			);
		}

		// --------------------------------------------------------------------------

		public function date_extract($date = NULL) {

			if($date !== NULL) {

				$date = explode('-', $date);

				$year	= $date['0'];
				$month	= $date['1'];
				$day	= $date['2'];

				return array(
					'month' => $month,
					'day' => $day,
					'year' => $year,
				);
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function to_military($hour, $ampm, $minute = false, $separate = false) {
			$military_time = false;

			if($minute === true && !$separate) {
				$military_time = date('Hi', strtotime($hour . $ampm));
			}elseif ($minute === true && $separate === true) {
				$military_time = date('H:i', strtotime($hour . $ampm));
			}else { $military_time = date('H', strtotime($hour . $ampm)); }

			return $military_time;
		}

		// --------------------------------------------------------------------------

		public function to_regular($military_time) {
			return date('h:i a', strtotime($military_time));
		}

		// --------------------------------------------------------------------------

		public function convert_hours($total_hours = 0, $total_minutes = 0) {
			$to_hours = intval($total_minutes / 60);
			$minutes_remain = $total_minutes % 60;
			$total_hours = $total_hours + $to_hours;

			$hr_word = ($total_hours > 1) ? ' hrs ' : ' hr ';
			$min_word = ($minutes_remain > 1) ? ' mins ' : ' min ';

			return $total_hours . $hr_word . $minutes_remain . $min_word;
		}

		// --------------------------------------------------------------------------

		public function generate_pay($total_time) {
			$CI = & get_instance();
			$CI->load->model('salary');

			$employee_rate = $CI->salary->get_rate();
			$payment = ($employee_rate * $total_time);

			return $payment;
		}

		// --------------------------------------------------------------------------

		public function weekly_range() { // This function returns the start and end range of the week

			$CI = & get_instance();
			$CI->load->library('calendar');

			$month 		= date('m');
			$day_int 	= date('d');
			$year 		= date('Y');
			$day_str 	= strtolower(date('l'));

			$start_month = $month;
			$start_day = null;
			$start_year = $year;

			$end_month = $month;
			$end_day = null;
			$end_year = $year;


			$prev_month_dates = $CI->calendar->adjust_date($month-1, $year);
			$next_month_dates = $CI->calendar->adjust_date($month+1, $year);

			$prev_month_total_days = $CI->calendar->get_total_days($prev_month_dates['month'], $year);
			$current_month_total_days = $CI->calendar->get_total_days($month, $year);
			$next_month_total_days = $CI->calendar->get_total_days($next_month_dates['month'], $year);

			switch ($day_str) {

				case 'monday':

					$start_day = $day_int;
					$end_day = $day_int + 6;

					if($end_day > $current_month_total_days) {
						$end_month = $next_month_dates['month'];
						$end_day = $end_day - $current_month_total_days;
						$end_year = $next_month_dates['year'];
					}

				break;

				case 'tuesday':

					$start_day = $day_int - 1;
					$end_day = $day_int + 5;

					if($start_day < 1) {
						$start_month = $prev_month_dates['month'];
						$start_day = $prev_month_total_days - abs($start_day);
						$start_year = $prev_month_dates['year'];
					}

					if($end_day > $current_month_total_days) {
						$end_month = $next_month_dates['month'];
						$end_day = $end_day - $current_month_total_days;
						$end_year = $next_month_dates['year'];
					}


				break;

				case 'wednesday':

					$start_day = $day_int - 2;
					$end_day = $day_int + 4;

					if($start_day < 1) {
						$start_month = $prev_month_dates['month'];
						$start_day = $prev_month_total_days - abs($start_day);
						$start_year = $prev_month_dates['year'];
					}

					if($end_day > $current_month_total_days) {
						$end_month = $next_month_dates['month'];
						$end_day = $end_day - $current_month_total_days;
						$end_year = $next_month_dates['year'];
					}

				break;

				case 'thursday':

					$start_day = $day_int - 3;
					$end_day = $day_int + 3;

					if($start_day < 1) {
						$start_month = $prev_month_dates['month'];
						$start_day = $prev_month_total_days - abs($start_day);
						$start_year = $prev_month_dates['year'];
					}

					if($end_day > $current_month_total_days) {
						$end_month = $next_month_dates['month'];
						$end_day = $end_day - $current_month_total_days;
						$end_year = $next_month_dates['year'];
					}

				break;

				case 'friday':

					$start_day = $day_int - 4;
					$end_day = $day_int + 2;

					if($start_day < 1) {
						$start_month = $prev_month_dates['month'];
						$start_day = $prev_month_total_days - abs($start_day);
						$start_year = $prev_month_dates['year'];
					}

					if($end_day > $current_month_total_days) {
						$end_month = $next_month_dates['month'];
						$end_day = $end_day - $current_month_total_days;
						$end_year = $next_month_dates['year'];
					}

				break;

				case 'saturday':

					$start_day = $day_int - 5;
					$end_day = $day_int + 1;

					if($start_day < 1) {
						$start_month = $prev_month_dates['month'];
						$start_day = $prev_month_total_days - abs($start_day);
						$start_year = $prev_month_dates['year'];
					}

					if($end_day > $current_month_total_days) {
						$end_month = $next_month_dates['month'];
						$end_day = $end_day - $current_month_total_days;
						$end_year = $next_month_dates['year'];
					}

				break;

				case 'sunday':
					$start_day = $day_int - 6;
					$end_day = $day_int + 0;

					if($start_day < 1) {
						$start_month = $prev_month_dates['month'];
						$start_day = $prev_month_total_days - abs($start_day);
						$start_year = $prev_month_dates['year'];
					}

					if($end_day > $current_month_total_days) {
						$end_month = $next_month_dates['month'];
						$end_day = $end_day - $current_month_total_days;
						$end_year = $next_month_dates['year'];
					}

				break;
			}

			$start_range = $start_year . '-' . $start_month . '-' . $start_day;
			$end_range = $end_year . '-' . $end_month . '-' . $end_day;

			$date_range = date_range($start_range, $end_range);

			return $date_range;
		}

		// --------------------------------------------------------------------------

		public function date_hasPassed($from = null, $to = null) {
			$from = strtotime($from);
			$to = strtotime($to);
			$today = strtotime(date('Y-m-d'));

			if($today <= $from && $today <= $to) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function valid_range($from = null, $to = null) {
			$CI = & get_instance();
			$CI->load->library('e_security');

			if($CI->e_security->is_validDate($from) && $CI->e_security->is_validDate($to)) {
				if(date_range($from, $to) !== false) {
					return true;
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_days($from = null, $to = null) {
			$CI = & get_instance();
			$CI->load->library('e_security');


			if(!is_null($from) && !is_null($to)) {
				if($CI->e_security->is_validDate($from) && $CI->e_security->is_validDate($to)) {

					$from = self::date_extract($from);
					$to = self::date_extract($to);

					if(checkdate($from['month'], $from['day'], $from['year']) && checkdate($to['month'], $to['day'], $to['year'])) {
						$start_range = $from['year'] . '-' . $from['month'] . '-' . $from['day'];
						$end_range = $to['year'] . '-' . $to['month'] . '-' . $to['day'];
						$date_range = date_range($start_range, $end_range);

						return $date_range;
					}
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------
		
		public function day_existed($from = null, $to = null, $date = null) { // Checks if the date is within the range of from and to

			$CI = & get_instance();
			$CI->load->library('e_security');

			if(!is_null($from) && !is_null($to) && !is_null($date)) {
				if($CI->e_security->is_validDate($from) && $CI->e_security->is_validDate($to) && $CI->e_security->is_validDate($date)) {
					$range_dates = self::get_days($from, $to);

					if($range_dates !== false) {
						if(in_array($date, $range_dates)) { return true; }
					}
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_workingDays($from = null, $to = null) {

			$CI = & get_instance();
			$CI->load->library('e_security');

			if(!is_null($from) && !is_null($to)) {
				if($CI->e_security->is_validDate($from) && $CI->e_security->is_validDate($to)) {

					$working_days = date_range($from, $to);

					foreach ($working_days as $index => $date) {
						$date = self::date_extract($date);
						$day = strtolower(date('l', mktime(0, 0, 0, $date['month'], $date['day'], $date['year'])));

						if(strcmp($day, 'saturday') === 0 || strcmp($day, 'sunday') === 0) {
							unset($working_days[$index]);
						}
					}

					return $working_days;
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------
		
	}