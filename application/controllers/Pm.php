<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pm extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->model(array('employee', 'salary', 'timelog', 'holiday'));
			$this->load->library(array('e_user', 'e_authentication', 'e_security'));
			$this->load->helper('cookie');
		}

		// --------------------------------------------------------------------------

		public function home() {

			$this->e_security->only_allow('project_manager');	
			$data = $this->employee->get_overall_info($this->session->id);
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['visitor_ip'] = $this->input->ip_address();
			$data['static_ip'] = $this->setting->static_ip();
			$data['employee_position'] = $this->employee->get_position();
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);
			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['page_title'] = 'Home';

			$this->load->view('pm/header', $data);
			$this->load->view('pm/modal');
			$this->load->view('pm/nav');
			$this->load->view('pm/time');
			$this->load->view('pm/footer');
		}

		// --------------------------------------------------------------------------

		public function request_feed() { // overtime request sa under niya na mga employees

			$this->e_security->only_allow('project_manager');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);
			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);
			$data['employee_overtime_request_list'] = $this->overtime->get_application();
			$data['overtime_pending_count'] = $this->overtime->application_count('pending');
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['employee_position'] = $this->employee->get_position();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['page_title'] = 'Request Feed';

			$this->load->view('pm/header', $data);
			$this->load->view('pm/modal');
			$this->load->view('pm/nav');
			$this->load->view('pm/reqfeed');
			$this->load->view('pm/footer');
		}

		// --------------------------------------------------------------------------
		
		public function my_request_feed() { // Iyang mga leave requests

			$this->e_security->only_allow('project_manager');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);
			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);
			$data['my_request_feed'] = $this->employee->request_feed();
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['employee_position'] = $this->employee->get_position();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['page_title'] = 'My Request Feed';

			$this->load->view('pm/header', $data); 
			$this->load->view('pm/modal');
			$this->load->view('pm/nav');
			$this->load->view('pm/myreqfeed');
			$this->load->view('pm/footer');
		}


		// --------------------------------------------------------------------------

		public function time_logs() {

			$this->e_security->only_allow('project_manager');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['employee_position'] = $this->employee->get_position();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['project_manager'] = $this->employee->project_manager($this->session->id);
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);
			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['timelog'] = $this->timelog->view($this->session->id);
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['page_title'] = 'Time Logs';

			$this->load->view('pm/header', $data);
			$this->load->view('pm/modal');
			$this->load->view('pm/nav');
			$this->load->view('pm/timelog');
			$this->load->view('pm/footer');
		}

		// --------------------------------------------------------------------------

		public function edit_profile() {
			$this->e_security->only_allow('project_manager');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['employee_position'] = $this->employee->get_position();
			$data['employee_skills'] = $this->employee->skills();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);
			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['basic_info'] = $this->employee->get_overall_info($this->session->id);
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['current_tab'] = 'about_me';
			$data['page_title'] = 'About me';

			$this->load->view('pm/header', $data);
			$this->load->view('pm/modal');
			$this->load->view('pm/nav');
			$this->load->view('pm/editprofile');
			$this->load->view('pm/footer');
		}

		// --------------------------------------------------------------------------

		public function view_notification($notification_type = null, $notification_id = null) {

			$this->e_security->only_allow('project_manager');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);			
			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);
			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);
			$data['employee_overtime_request_list'] = $this->overtime->get_application($notification_id);
			$data['overtime_pending_count'] = $this->overtime->application_count('pending');
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['employee_application'] = $this->overtime->get_application();
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['employee_position'] = $this->employee->get_position();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['page_title'] = 'Request Feed';

			$this->load->view('pm/header', $data); 
			$this->load->view('pm/modal');
			$this->load->view('pm/nav');
			$this->load->view('pm/reqfeed');
			$this->load->view('pm/footer');
		}

		// --------------------------------------------------------------------------

		public function view_my_notification($notification_type = null, $notification_id = null) {

			$this->e_security->only_allow('project_manager');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);			
			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);
			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);

			$data['my_request_feed'] = $this->leave->detailed_list( $this->session->id, $notification_id );
			
			$data['employee_overtime_request_list'] = $this->overtime->get_application($notification_id);
			$data['overtime_pending_count'] = $this->overtime->application_count('pending');
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['employee_application'] = $this->overtime->get_application();
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['employee_position'] = $this->employee->get_position();
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['page_title'] = 'Request Feed';

			$this->load->view('pm/header', $data); 
			$this->load->view('pm/modal');
			$this->load->view('pm/nav');
			$this->load->view('pm/myreqfeed');
			$this->load->view('pm/footer');
		}

		// --------------------------------------------------------------------------

		public function view_holiday() {

			$this->e_security->only_allow('project_manager');
			$data = $this->employee->get_overall_info($this->session->id);
			$data['employee_position'] = $this->employee->get_position();
			$data['leave_limits'] = $this->leave->leave_limits($this->session->id);
			$data['employee_notification_count'] = $this->notification->count_employee_overtime_applications($this->session->id);
			$data['employee_notification_list'] = $this->notification->get_employee_overtime_notification($this->session->id);
			$data['my_notification_count'] = $this->notification->count($this->session->id);
			$data['my_notification_list'] = $this->notification->get($this->session->id);
			$data['holiday_dates'] = $this->holiday->get_dates( date('Y') );
			$data['timed_in'] = $this->timelog->employee_timedin();
			$data['weekly_attendance'] = $this->employee->weekly_attendance($this->session->id, $this->e_attendance->weekly_range());
			$data['attendance_total_hours'] = $this->timelog->count_hours($data['weekly_attendance']);
			$data['holidays'] = $this->holiday->view();
			$data['page_title'] = 'Holidays';

			$this->load->view('pm/header', $data);
			$this->load->view('pm/modal');
			$this->load->view('pm/nav');
			$this->load->view('pm/holiday');
			$this->load->view('pm/footer');
		}

		// --------------------------------------------------------------------------

		public function logout() {
			$this->session->sess_destroy();
			delete_cookie('hash');
			redirect('/index');
		}

		// --------------------------------------------------------------------------

	}