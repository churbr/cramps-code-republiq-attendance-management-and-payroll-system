<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	$config['upload_path'] = './resources/dist/img/users/';
	$config['allowed_types'] = 'jpg|jpeg|png|gif';
	$config['max_size'] = 500;
	$config['file_ext_tolower'] = TRUE;
	$config['encrypt_name'] = TRUE;
	$config['image_library'] = 'gd2';
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = TRUE;