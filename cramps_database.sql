-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2016 at 03:45 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cramps_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_adjustments`
--

CREATE TABLE `custom_adjustments` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` enum('add','sub') NOT NULL,
  `value` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_adjustments`
--

INSERT INTO `custom_adjustments` (`id`, `employee_id`, `name`, `type`, `value`) VALUES
(17, 15, 'Travel', 'sub', 100),
(18, 15, 'Valentines Date', 'add', 555),
(19, 15, 'Test1', 'add', 500);

-- --------------------------------------------------------

--
-- Table structure for table `deactivate`
--

CREATE TABLE `deactivate` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `reason` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deactivate`
--

INSERT INTO `deactivate` (`id`, `employee_id`, `reason`, `status`, `date`) VALUES
(2, 16, '', 1, '2016-03-17'),
(4, 12, '', 1, '2016-03-17');

-- --------------------------------------------------------

--
-- Table structure for table `deduction`
--

CREATE TABLE `deduction` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL COMMENT 'The name of the company, i.e. SSS, PAG-IBIG, PhilHealth, etc.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deduction`
--

INSERT INTO `deduction` (`id`, `name`) VALUES
(1, 'Phil Health'),
(2, 'SSS'),
(3, 'Pag Ibig'),
(4, 'Income Tax');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `birthday` date NOT NULL,
  `picture` varchar(50) DEFAULT 'default.png',
  `type` enum('super_admin','admin','regular_employee','project_manager','board_of_director') DEFAULT 'regular_employee' COMMENT 'Type of employee',
  `contact` char(10) NOT NULL COMMENT 'Contact Phone No.',
  `email` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_hired` date NOT NULL,
  `date_resignation` date DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `username`, `password`, `firstname`, `middlename`, `lastname`, `gender`, `birthday`, `picture`, `type`, `contact`, `email`, `address`, `code`, `status`, `date_hired`, `date_resignation`, `date`) VALUES
(1, 'admin', '625c533ad36de7136d22467d780d393545639042386441ac7ae1051a7be5e7085726f562f2b8c1c71aa8892c7fb090e56c3c6d5dad2b04d1e3b43cbd5bf5c9864ujYF6MMxg5kKDeRQP1CbK4Bnnc/V9VqZ9N8JpAONYc=', 'Adrian', '', 'Borja', 'male', '1991-04-03', 'default.png', 'admin', '0932856795', 'adriantheelite@yahoo.com', 'Cebu City', '21c7e4032a5a4f38c219f16c6891f3aaa0f43b179', 1, '2015-03-10', NULL, '2015-09-27'),
(12, 'FayBorja', '625c533ad36de7136d22467d780d393545639042386441ac7ae1051a7be5e7085726f562f2b8c1c71aa8892c7fb090e56c3c6d5dad2b04d1e3b43cbd5bf5c9864ujYF6MMxg5kKDeRQP1CbK4Bnnc/V9VqZ9N8JpAONYc=', 'Faye', '', 'Borja', 'female', '1994-09-14', 'default.png', 'regular_employee', '0932856785', 'faye.borja14@gmail.com', 'Cebu City', '', 1, '2015-04-01', NULL, '2015-04-01'),
(13, 'JasRoxas', '625c533ad36de7136d22467d780d393545639042386441ac7ae1051a7be5e7085726f562f2b8c1c71aa8892c7fb090e56c3c6d5dad2b04d1e3b43cbd5bf5c9864ujYF6MMxg5kKDeRQP1CbK4Bnnc/V9VqZ9N8JpAONYc=', 'Jason', ' ', 'Roxas', 'male', '1994-07-04', 'default.png', 'project_manager', '0932756895', 'jasroxas091@gmail.com', 'Cebu City', '', 1, '2015-01-04', NULL, '2015-01-04'),
(14, 'KarBonotan', '625c533ad36de7136d22467d780d393545639042386441ac7ae1051a7be5e7085726f562f2b8c1c71aa8892c7fb090e56c3c6d5dad2b04d1e3b43cbd5bf5c9864ujYF6MMxg5kKDeRQP1CbK4Bnnc/V9VqZ9N8JpAONYc=', 'Karla', '', 'Bonotan', 'female', '1993-09-01', 'default.png', 'board_of_director', '0932856856', 'karla.bonotan@gmail.com', 'Cebu City', '', 1, '2015-04-09', NULL, '2015-04-09'),
(15, 'AleBonotan', '625c533ad36de7136d22467d780d393545639042386441ac7ae1051a7be5e7085726f562f2b8c1c71aa8892c7fb090e56c3c6d5dad2b04d1e3b43cbd5bf5c9864ujYF6MMxg5kKDeRQP1CbK4Bnnc/V9VqZ9N8JpAONYc=', 'Alex', '', 'Bonotan', 'male', '2016-03-04', 'default.png', 'regular_employee', '0932325199', 'jasroxas091@gmail.com', 'Talamban City', '', 1, '2016-03-17', NULL, '2016-03-17'),
(16, 'RexLobo', '625c533ad36de7136d22467d780d393545639042386441ac7ae1051a7be5e7085726f562f2b8c1c71aa8892c7fb090e56c3c6d5dad2b04d1e3b43cbd5bf5c9864ujYF6MMxg5kKDeRQP1CbK4Bnnc/V9VqZ9N8JpAONYc=', 'Rex Bernie', '', 'Lobo', 'male', '1989-02-02', 'default.png', 'regular_employee', '0932325199', 'rex@gmail.com', 'Talamban Cebu City', '', 1, '2015-02-03', NULL, '2015-02-03');

-- --------------------------------------------------------

--
-- Table structure for table `employee_deduction`
--

CREATE TABLE `employee_deduction` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `deduction_id` int(11) NOT NULL,
  `value` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_deduction`
--

INSERT INTO `employee_deduction` (`id`, `employee_id`, `deduction_id`, `value`) VALUES
(14, 12, 1, 100),
(15, 12, 2, 100),
(16, 12, 3, 100),
(17, 12, 4, 100),
(18, 13, 1, 100),
(19, 13, 2, 100),
(20, 13, 3, 100),
(21, 13, 4, 100),
(22, 14, 1, 100),
(23, 14, 2, 100),
(24, 14, 3, 100),
(25, 14, 4, 100),
(26, 15, 1, 200),
(27, 15, 2, 250),
(28, 15, 3, 275),
(29, 15, 4, 300);

-- --------------------------------------------------------

--
-- Table structure for table `employee_position`
--

CREATE TABLE `employee_position` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_position`
--

INSERT INTO `employee_position` (`id`, `employee_id`, `position_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_skill`
--

CREATE TABLE `employee_skill` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_skill`
--

INSERT INTO `employee_skill` (`id`, `employee_id`, `skill_id`) VALUES
(1, 15, 2);

-- --------------------------------------------------------

--
-- Table structure for table `holiday`
--

CREATE TABLE `holiday` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `type` enum('regular','special') NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `holiday`
--

INSERT INTO `holiday` (`id`, `name`, `type`, `date`, `status`) VALUES
(3, 'Holy Thursday', 'regular', '2016-04-02', 1),
(4, 'Better Friday', 'regular', '2016-04-03', 1),
(5, 'Araw ng Kagitingan', 'regular', '2016-04-09', 1),
(6, 'Labor Day', 'regular', '2016-05-01', 1),
(7, 'Independence Day', 'regular', '2016-06-12', 1),
(8, 'National Heroes Day', 'regular', '2016-08-31', 1),
(9, 'Bonifacio Day', 'regular', '2016-11-30', 1),
(10, 'Christmas Day', 'regular', '2016-12-25', 1),
(11, 'Rizal Day', 'regular', '2016-12-30', 1),
(12, 'GG Day', 'regular', '2016-03-14', 0),
(13, 'Best friends Day', 'regular', '2016-02-03', 0),
(14, 'Ginabot Festival', 'special', '2016-02-02', 0),
(15, 'Holiday1', 'regular', '2016-03-25', 1),
(16, 'Holiday2', 'regular', '2016-02-04', 1),
(17, 'Holiday3', 'regular', '2016-02-10', 1),
(18, 'Holiday4', 'regular', '2016-02-18', 1),
(19, 'Holiday5', 'regular', '2016-02-22', 1),
(20, 'Holiday6', 'regular', '2016-02-23', 1),
(21, 'Holiday7', 'regular', '2016-02-24', 1),
(22, 'Holiday8', 'regular', '2016-02-25', 1),
(23, 'Holiday9', 'regular', '2016-02-26', 1),
(24, 'Holiday10', 'regular', '2016-01-27', 1),
(25, 'TESTTEST', 'special', '2016-01-20', 1),
(26, 'Spec2', 'special', '2016-01-21', 1),
(27, 'Spec3a', 'special', '2016-01-28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `leave`
--

CREATE TABLE `leave` (
  `id` int(11) NOT NULL,
  `bod_id` int(11) DEFAULT NULL COMMENT 'Only BOD, Admin, Super Admins are allowed to confirm a leave request',
  `employee_id` int(11) NOT NULL COMMENT 'Employee who sent the leave request',
  `leave_type_id` int(11) NOT NULL COMMENT 'Primary key of the type of leave',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `pay` float NOT NULL COMMENT 'Paid amount of leave if approved',
  `status` enum('approved','pending','declined','cancelled') NOT NULL DEFAULT 'pending',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `date_updated` date DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Please note that when adding a leave, only working days are counted, i.e Monday - Friday';

--
-- Dumping data for table `leave`
--

INSERT INTO `leave` (`id`, `bod_id`, `employee_id`, `leave_type_id`, `start_date`, `end_date`, `pay`, `status`, `show`, `date_updated`, `note`) VALUES
(1, 14, 12, 2, '2016-03-17', '2016-03-18', 1600, 'declined', 1, '2016-03-16', NULL),
(2, 14, 13, 1, '2016-03-17', '2016-03-17', 800, 'approved', 1, '2016-03-16', NULL),
(3, 14, 12, 1, '2016-03-21', '2016-03-22', 1600, 'declined', 1, '2016-03-16', NULL),
(4, 14, 12, 1, '2016-03-23', '2016-03-25', 2400, 'declined', 1, '2016-03-16', NULL),
(5, NULL, 15, 1, '2016-03-21', '2016-03-23', 2400, 'cancelled', 0, NULL, NULL),
(6, NULL, 12, 1, '2016-03-22', '2016-03-24', 2400, 'cancelled', 0, NULL, NULL),
(7, NULL, 12, 2, '2016-03-21', '2016-03-23', 2400, 'cancelled', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leave_setting`
--

CREATE TABLE `leave_setting` (
  `id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `remaining_days` int(11) NOT NULL COMMENT 'Remaining days left for an employee can have for a leave'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This is where we store the remaining days of each employee to their leave requests';

--
-- Dumping data for table `leave_setting`
--

INSERT INTO `leave_setting` (`id`, `leave_type_id`, `employee_id`, `remaining_days`) VALUES
(1, 1, 1, 14),
(2, 2, 1, 14),
(3, 3, 1, 7),
(29, 1, 12, 5),
(30, 2, 12, 5),
(31, 4, 12, 5),
(32, 1, 13, 13),
(33, 2, 13, 14),
(34, 3, 13, 7),
(35, 1, 14, 14),
(36, 2, 14, 14),
(37, 4, 14, 60),
(38, 1, 15, 14),
(39, 2, 15, 14),
(40, 3, 15, 7),
(41, 1, 16, 14),
(42, 2, 16, 14),
(43, 3, 16, 7);

-- --------------------------------------------------------

--
-- Table structure for table `leave_type`
--

CREATE TABLE `leave_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `span` int(11) NOT NULL COMMENT 'Maximum number of days for a leave (Working days, meaning Monday - Friday)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Since only working days are counted, i.e Monday - Friday. Set the right value for span (Max number of days) without counting saturday and sunday';

--
-- Dumping data for table `leave_type`
--

INSERT INTO `leave_type` (`id`, `name`, `code`, `span`) VALUES
(1, 'Sick Leave', 'sick_leave', 14),
(2, 'Vacation Leave', 'vacation_leave', 14),
(3, 'Paternity Leave', 'paternity_leave', 7),
(4, 'Maternity Leave', 'maternity_leave', 60);

-- --------------------------------------------------------

--
-- Table structure for table `loglimit`
--

CREATE TABLE `loglimit` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `logged_timeins` int(11) NOT NULL COMMENT 'Records how many time-ins an employee has made for the day',
  `logged_timeouts` int(11) DEFAULT '0' COMMENT 'Records how many time-outs an employee has made for the day',
  `timedin_date` date NOT NULL,
  `timedout_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loglimit`
--

INSERT INTO `loglimit` (`id`, `employee_id`, `logged_timeins`, `logged_timeouts`, `timedin_date`, `timedout_date`) VALUES
(1, 13, 1, 0, '2016-03-16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `manage`
--

CREATE TABLE `manage` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL COMMENT 'Employee PK',
  `pm_id` int(11) NOT NULL COMMENT 'Project Manager PK',
  `date` date NOT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage`
--

INSERT INTO `manage` (`id`, `employee_id`, `pm_id`, `date`, `status`) VALUES
(24, 12, 13, '2016-03-14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `notification_type_id` int(11) NOT NULL COMMENT 'The type of notification received, e.g. Leave/Overtime',
  `notification_id` int(11) NOT NULL COMMENT 'This is the primary key of the instance of what kind of notification type it receives.',
  `employee_id` int(11) DEFAULT NULL COMMENT 'The receiver of the notification',
  `sender_id` int(11) NOT NULL COMMENT 'The sender of request',
  `message` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Flag if notification is seen or not',
  `seen_by` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Flag if notification is seen by manager or not',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `notification_type_id`, `notification_id`, `employee_id`, `sender_id`, `message`, `status`, `seen_by`, `date`) VALUES
(1, 2, 1, 13, 12, 'Overtime Application', 1, 1, '2016-03-14'),
(2, 2, 2, 13, 12, 'Overtime Application', 1, 1, '2016-03-14'),
(3, 2, 3, 13, 12, 'Overtime Application', 1, 1, '2016-03-14'),
(4, 2, 8, 13, 12, 'Overtime Application', 0, 1, '2016-03-16'),
(5, 1, 1, NULL, 12, 'Leave Application', 1, 1, '2016-03-16'),
(6, 1, 2, NULL, 13, 'Leave Application', 1, 1, '2016-03-16'),
(7, 1, 3, NULL, 12, 'Leave Application', 1, 1, '2016-03-16'),
(8, 1, 4, NULL, 12, 'Leave Application', 1, 1, '2016-03-16'),
(9, 1, 5, NULL, 15, 'Leave Application', 0, 0, '2016-03-17'),
(10, 1, 6, NULL, 12, 'Leave Application', 0, 0, '2016-03-17'),
(11, 1, 7, NULL, 12, 'Leave Application', 0, 0, '2016-03-17');

-- --------------------------------------------------------

--
-- Table structure for table `notification_type`
--

CREATE TABLE `notification_type` (
  `id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_type`
--

INSERT INTO `notification_type` (`id`, `title`) VALUES
(1, 'leave'),
(2, 'overtime');

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `id` int(11) NOT NULL,
  `pm_id` int(11) NOT NULL COMMENT 'Project Manager who can approve the overtime request',
  `employee_id` int(11) NOT NULL COMMENT 'Employee who requested the overtime',
  `start_time` varchar(10) NOT NULL,
  `end_time` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `hour` int(11) NOT NULL COMMENT 'Hours rendered',
  `minute` int(11) NOT NULL COMMENT 'Excess minutes rendered',
  `pay` int(11) NOT NULL COMMENT 'Paid amount',
  `status` enum('approved','pending','declined','cancelled') NOT NULL DEFAULT 'pending',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `date_updated` date DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `overtime`
--

INSERT INTO `overtime` (`id`, `pm_id`, `employee_id`, `start_time`, `end_time`, `start_date`, `end_date`, `hour`, `minute`, `pay`, `status`, `show`, `date_updated`, `note`) VALUES
(1, 13, 12, '04:00 pm', '06:00 pm', '2016-02-01', '2016-02-01', 2, 0, 200, 'approved', 1, '2016-03-14', NULL),
(2, 13, 12, '04:00 pm', '06:00 pm', '2016-02-02', '2016-02-02', 2, 0, 200, 'approved', 1, '2016-03-14', NULL),
(3, 13, 12, '04:00 pm', '06:00 pm', '2016-02-03', '2016-02-03', 2, 0, 200, 'approved', 1, '2016-03-14', NULL),
(4, 13, 15, '04:00 pm', '06:00 pm', '2016-03-07', '2016-03-07', 3, 0, 0, 'approved', 1, '2016-03-15', ''),
(5, 13, 15, '04:00 pm', '06:00 pm', '2016-03-08', '2016-03-08', 4, 0, 0, 'approved', 1, '2016-03-15', ''),
(6, 13, 15, '04:00 pm', '06:00 pm', '2016-03-16', '2016-03-16', 4, 0, 0, 'approved', 1, '2016-03-15', ''),
(7, 13, 15, '04:00 pm', '06:00 pm', '2016-02-10', '2016-02-10', 4, 0, 0, 'approved', 1, '2016-03-15', '');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `date_completed` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `id` int(11) NOT NULL,
  `title` varchar(35) NOT NULL,
  `code` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `title`, `code`) VALUES
(1, 'Web Developer', 'webdev'),
(2, 'Web Designer', 'ux'),
(3, 'Human Resource', 'hr'),
(4, 'Quality Assurance', 'qa'),
(5, 'System Administrator', 'sysad'),
(6, 'Research', 'rsch'),
(7, 'System Analyst', 'sysanal');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `overtime_count` int(11) NOT NULL DEFAULT '0',
  `overtime_hours` int(11) NOT NULL DEFAULT '0',
  `sl_count` int(11) NOT NULL DEFAULT '0',
  `vl_count` int(11) NOT NULL DEFAULT '0',
  `pl_count` int(11) NOT NULL DEFAULT '0',
  `ml_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `rate` float NOT NULL COMMENT 'Hourly rate'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `employee_id`, `rate`) VALUES
(1, 1, 100),
(12, 12, 100),
(13, 13, 100),
(14, 14, 100),
(15, 15, 100),
(16, 16, 100);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `value` varchar(20) NOT NULL,
  `description` text,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `name`, `value`, `description`, `date`) VALUES
(1, 'static_ip', '::1', 'In order for employees to only time in or out within the office.', '2016-03-05'),
(2, 'regular_hours', '8', 'Regular hours', '2016-03-05'),
(3, 'weekly_hours', '40', 'Employees should atleast render 40 hours every week.', '2016-03-05'),
(4, 'email', 'eminence43rd@gmail.c', 'Email used as a sender for emails', '2016-03-05');

-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skill`
--

INSERT INTO `skill` (`id`, `title`, `description`) VALUES
(1, 'PHP', 'PHP'),
(2, 'Ruby', 'Ruby'),
(3, 'CSS', 'CSS'),
(4, 'Javascript', 'Javascript');

-- --------------------------------------------------------

--
-- Table structure for table `timelog`
--

CREATE TABLE `timelog` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `time_in` varchar(10) NOT NULL,
  `time_out` varchar(10) DEFAULT NULL,
  `timein_date` date NOT NULL,
  `timeout_date` date DEFAULT NULL,
  `hour` int(11) DEFAULT NULL COMMENT 'Hours rendered of a log',
  `minute` int(11) DEFAULT NULL COMMENT 'Excess minutes, very useful when calculating payroll because it adds all the rendered minutes of the employee',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `pay` float DEFAULT NULL COMMENT 'Initial payment base on hours rendered because pay is calculated by the number of hours'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timelog`
--

INSERT INTO `timelog` (`id`, `employee_id`, `time_in`, `time_out`, `timein_date`, `timeout_date`, `hour`, `minute`, `show`, `pay`) VALUES
(1, 12, '08:00 am', '06:00 pm', '2016-02-01', '2016-02-01', 10, 0, 0, 0),
(2, 12, '08:00 am', '06:00 pm', '2016-02-02', '2016-02-02', 10, 0, 0, 0),
(3, 12, '08:00 am', '06:00 pm', '2016-02-03', '2016-02-03', 10, 0, 0, 0),
(5, 12, '08:00 am', '06:00 pm', '2016-02-05', '2016-02-05', 10, 0, 1, 0),
(6, 12, '08:00 am', '06:00 pm', '2016-02-08', '2016-02-08', 10, 0, 1, 0),
(7, 12, '08:00 am', '06:00 pm', '2016-02-09', '2016-02-09', 10, 0, 1, 0),
(8, 12, '08:00 am', '06:00 pm', '2016-02-10', '2016-02-10', 10, 0, 1, 0),
(9, 12, '08:00 am', '06:00 pm', '2016-02-11', '2016-02-11', 10, 0, 1, 0),
(10, 12, '08:00 am', '06:00 pm', '2016-02-12', '2016-02-12', 10, 0, 1, 0),
(11, 15, '08:00 am', '06:00 pm', '2016-02-29', '2016-02-29', 8, 0, 1, 0),
(12, 15, '08:00 am', '06:00 pm', '2016-03-01', '2016-03-01', 8, 0, 1, 0),
(13, 15, '08:00 am', '06:00 pm', '2016-03-02', '2016-03-02', 8, 0, 1, 0),
(14, 15, '08:00 am', '06:00 pm', '2016-03-03', '2016-03-03', 9, 0, 1, 0),
(15, 15, '08:00 am', '06:00 pm', '2016-03-04', '2016-03-04', 10, 0, 1, 0),
(16, 15, '08:00 am', '06:00 pm', '2016-03-07', '2016-03-07', 4, 0, 1, 0),
(17, 15, '08:00 am', '06:00 pm', '2016-03-08', '2016-03-08', 6, 0, 1, 0),
(18, 15, '08:00 am', '06:00 pm', '2016-03-09', '2016-03-09', 7, 0, 1, 0),
(19, 15, '08:00 am', '06:00 pm', '2016-03-10', '2016-03-10', 8, 0, 1, 0),
(20, 15, '08:00 am', '06:00 pm', '2016-03-11', '2016-03-11', 9, 0, 1, 0),
(21, 15, '08:00 am', '06:00 pm', '2016-03-14', '2016-03-14', 1, 0, 1, 0),
(22, 15, '08:00 am', '06:00 pm', '2016-03-15', '2016-03-15', 2, 0, 1, 0),
(23, 15, '08:00 am', '06:00 pm', '2016-03-16', '2016-03-16', 3, 0, 1, 0),
(24, 15, '08:00 am', '06:00 pm', '2016-03-17', '2016-03-17', 4, 0, 1, 0),
(25, 15, '08:00 am', '06:00 pm', '2016-03-22', '2016-03-22', 8, 0, 1, 0),
(26, 15, '08:00 am', '06:00 pm', '2016-03-21', '2016-03-21', 8, 0, 1, 0),
(27, 15, '08:00 am', '06:00 pm', '2016-03-23', '2016-03-23', 8, 0, 1, 0),
(28, 15, '08:00 am', '06:00 pm', '2016-03-24', '2016-03-24', 8, 0, 1, 0),
(29, 15, '08:00 am', '06:00 pm', '2016-02-01', '2016-02-01', 8, 0, 1, 0),
(30, 15, '08:00 am', '06:00 pm', '2016-02-02', '2016-02-02', 8, 0, 1, 0),
(31, 15, '08:00 am', '06:00 pm', '2016-02-03', '2016-02-03', 8, 0, 1, 0),
(32, 15, '08:00 am', '06:00 pm', '2016-02-04', '2016-02-04', 8, 0, 1, 0),
(33, 15, '08:00 am', '06:00 pm', '2016-02-05', '2016-02-05', 8, 0, 1, 0),
(34, 15, '08:00 am', '06:00 pm', '2016-02-08', '2016-02-08', 8, 0, 1, 0),
(35, 15, '08:00 am', '06:00 pm', '2016-02-09', '2016-02-09', 8, 0, 1, 0),
(36, 15, '08:00 am', '06:00 pm', '2016-02-10', '2016-02-10', 8, 0, 1, 0),
(37, 15, '08:00 am', '06:00 pm', '2016-02-11', '2016-02-11', 8, 0, 1, 0),
(38, 15, '08:00 am', '06:00 pm', '2016-02-12', '2016-02-12', 8, 0, 1, 0),
(39, 15, '08:00 am', '06:00 pm', '2016-02-15', '2016-02-15', 8, 0, 1, 0),
(40, 15, '08:00 am', '06:00 pm', '2016-02-16', '2016-02-16', 8, 0, 1, 0),
(41, 15, '08:00 am', '06:00 pm', '2016-02-17', '2016-02-17', 8, 0, 1, 0),
(42, 15, '08:00 am', '06:00 pm', '2016-02-18', '2016-02-18', 2, 0, 1, 0),
(43, 15, '08:00 am', '06:00 pm', '2016-02-19', '2016-02-19', 2, 0, 1, 0),
(44, 15, '08:00 am', '06:00 pm', '2016-01-04', '2016-01-04', 15, 0, 1, 0),
(45, 15, '08:00 am', '06:00 pm', '2016-01-05', '2016-01-05', 15, 0, 1, 0),
(46, 15, '08:00 am', '06:00 pm', '2016-01-06', '2016-01-06', 15, 0, 1, 0),
(47, 15, '08:00 am', '06:00 pm', '2016-01-07', '2016-01-07', 15, 0, 1, 0),
(48, 15, '08:00 am', '06:00 pm', '2016-01-11', '2016-01-11', 10, 0, 1, 0),
(49, 15, '08:00 am', '06:00 pm', '2016-01-12', '2016-01-12', 10, 0, 1, 0),
(50, 15, '08:00 am', '06:00 pm', '2016-01-13', '2016-01-13', 10, 0, 1, 0),
(51, 15, '08:00 am', '06:00 pm', '2016-01-14', '2016-01-14', 11, 0, 1, 0),
(52, 15, '08:00 am', '06:00 pm', '2016-01-18', '2016-01-18', 8, 0, 1, 0),
(53, 15, '08:00 am', '06:00 pm', '2016-01-19', '2016-01-19', 8, 0, 1, 0),
(54, 15, '08:00 am', '06:00 pm', '2016-01-20', '2016-01-20', 8, 0, 1, 0),
(55, 15, '08:00 am', '06:00 pm', '2016-01-21', '2016-01-21', 8, 0, 1, 0),
(56, 15, '08:00 am', '06:00 pm', '2016-01-22', '2016-01-22', 8, 0, 1, 0),
(57, 15, '08:00 am', '06:00 pm', '2016-01-25', '2016-01-25', 8, 0, 1, 0),
(58, 15, '08:00 am', '06:00 pm', '2016-01-27', '2016-01-27', 8, 0, 1, 0),
(59, 15, '08:00 am', '06:00 pm', '2016-01-28', '2016-01-28', 8, 0, 1, 0),
(60, 15, '08:00 am', '06:00 pm', '2016-01-29', '2016-01-29', 8, 0, 1, 0),
(61, 15, '08:00 am', '06:00 pm', '2016-01-26', '2016-01-26', 7, 0, 1, 0),
(62, 13, '03:12 pm', NULL, '2016-03-16', NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `timelog_overtime`
--

CREATE TABLE `timelog_overtime` (
  `id` int(11) NOT NULL,
  `timelog_id` int(11) NOT NULL,
  `overtime_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `time_in` varchar(10) NOT NULL,
  `time_out` varchar(10) DEFAULT NULL,
  `timein_date` date NOT NULL,
  `timeout_date` date DEFAULT NULL,
  `hour` int(11) DEFAULT NULL COMMENT 'Hours rendered',
  `minute` int(11) DEFAULT NULL COMMENT 'Excess minutes, very useful when calculating payroll because it adds all the rendered minutes of the employee',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `pay` float DEFAULT NULL COMMENT 'Initial payment base on hours rendered because pay is calculated by the number of hours'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table is where the excess hours of overtime will be stored';

--
-- Dumping data for table `timelog_overtime`
--

INSERT INTO `timelog_overtime` (`id`, `timelog_id`, `overtime_id`, `employee_id`, `time_in`, `time_out`, `timein_date`, `timeout_date`, `hour`, `minute`, `show`, `pay`) VALUES
(1, 1, 1, 12, '08:00 am', '04:00 pm', '2016-02-01', '2016-02-01', 8, 0, 1, 800),
(2, 2, 2, 12, '08:00 am', '04:00 pm', '2016-02-02', '2016-02-02', 8, 0, 1, 800),
(3, 3, 3, 12, '08:00 am', '04:00 pm', '2016-02-03', '2016-02-03', 8, 0, 1, 800);

-- --------------------------------------------------------

--
-- Table structure for table `timelog_whole`
--

CREATE TABLE `timelog_whole` (
  `id` int(11) NOT NULL,
  `timelog_id` int(11) NOT NULL,
  `overtime_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='If the employee requested the whole timelog, we cannot store anything in TIMELOG_OVERTIME table, so we use this table to store the information to maintain data consistency';

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `hash` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `custom_adjustments`
--
ALTER TABLE `custom_adjustments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_adjustments_fk` (`employee_id`);

--
-- Indexes for table `deactivate`
--
ALTER TABLE `deactivate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deactivate_employee_fk` (`employee_id`);

--
-- Indexes for table `deduction`
--
ALTER TABLE `deduction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_idnumber` (`username`);

--
-- Indexes for table `employee_deduction`
--
ALTER TABLE `employee_deduction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_deduction_bridge_fk` (`employee_id`),
  ADD KEY `deduction_employee_bridge_fk` (`deduction_id`);

--
-- Indexes for table `employee_position`
--
ALTER TABLE `employee_position`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_pos_bridge_fk` (`employee_id`),
  ADD KEY `pos_emp_bridge_fk` (`position_id`);

--
-- Indexes for table `employee_skill`
--
ALTER TABLE `employee_skill`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_skill_bridge_fk` (`employee_id`),
  ADD KEY `skill_employee_bridge_fk` (`skill_id`);

--
-- Indexes for table `holiday`
--
ALTER TABLE `holiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave`
--
ALTER TABLE `leave`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leave_bod_fk` (`bod_id`),
  ADD KEY `leave_employee_fk` (`employee_id`),
  ADD KEY `leave_type_fk` (`leave_type_id`);

--
-- Indexes for table `leave_setting`
--
ALTER TABLE `leave_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `general_leave_setting_fk` (`leave_type_id`),
  ADD KEY `leave_setting_employee_fk` (`employee_id`);

--
-- Indexes for table `leave_type`
--
ALTER TABLE `leave_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loglimit`
--
ALTER TABLE `loglimit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loglimit_employee_fk` (`employee_id`);

--
-- Indexes for table `manage`
--
ALTER TABLE `manage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `regemp_id_fk` (`employee_id`),
  ADD KEY `pm_id_fk` (`pm_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_type_fk` (`notification_type_id`),
  ADD KEY `notification_sender_fk` (`sender_id`);

--
-- Indexes for table `notification_type`
--
ALTER TABLE `notification_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `overtime_pm_fk` (`pm_id`),
  ADD KEY `overtime_employee_fk` (`employee_id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_employee_fk` (`employee_id`),
  ADD KEY `portfolio_project_fk` (`project_id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `report_employee_fk` (`employee_id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_salary_fk` (`employee_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timelog`
--
ALTER TABLE `timelog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timelog_employee_fk` (`employee_id`);

--
-- Indexes for table `timelog_overtime`
--
ALTER TABLE `timelog_overtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timelog_overtime_employee_fk` (`employee_id`),
  ADD KEY `timelog_overtime_timelog_fk` (`timelog_id`),
  ADD KEY `timelog_overtime_overtime_fk` (`overtime_id`);

--
-- Indexes for table `timelog_whole`
--
ALTER TABLE `timelog_whole`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timelog_whole_employee_fk` (`employee_id`),
  ADD KEY `timelog_whole_timelog_fk` (`timelog_id`),
  ADD KEY `timelog_whole_overtime_fk` (`overtime_id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_token_fk` (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `custom_adjustments`
--
ALTER TABLE `custom_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `deactivate`
--
ALTER TABLE `deactivate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `deduction`
--
ALTER TABLE `deduction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `employee_deduction`
--
ALTER TABLE `employee_deduction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `employee_position`
--
ALTER TABLE `employee_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_skill`
--
ALTER TABLE `employee_skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `holiday`
--
ALTER TABLE `holiday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `leave`
--
ALTER TABLE `leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `leave_setting`
--
ALTER TABLE `leave_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `leave_type`
--
ALTER TABLE `leave_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `loglimit`
--
ALTER TABLE `loglimit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `manage`
--
ALTER TABLE `manage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `notification_type`
--
ALTER TABLE `notification_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `skill`
--
ALTER TABLE `skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `timelog`
--
ALTER TABLE `timelog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `timelog_overtime`
--
ALTER TABLE `timelog_overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `timelog_whole`
--
ALTER TABLE `timelog_whole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `custom_adjustments`
--
ALTER TABLE `custom_adjustments`
  ADD CONSTRAINT `custom_adjustments_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `deactivate`
--
ALTER TABLE `deactivate`
  ADD CONSTRAINT `deactivate_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_deduction`
--
ALTER TABLE `employee_deduction`
  ADD CONSTRAINT `deduction_employee_bridge_fk` FOREIGN KEY (`deduction_id`) REFERENCES `deduction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_deduction_bridge_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_position`
--
ALTER TABLE `employee_position`
  ADD CONSTRAINT `emp_pos_bridge_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pos_emp_bridge_fk` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_skill`
--
ALTER TABLE `employee_skill`
  ADD CONSTRAINT `employee_skill_bridge_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `skill_employee_bridge_fk` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `leave`
--
ALTER TABLE `leave`
  ADD CONSTRAINT `leave_bod_fk` FOREIGN KEY (`bod_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `leave_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `leave_type_fk` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `leave_setting`
--
ALTER TABLE `leave_setting`
  ADD CONSTRAINT `general_leave_setting_fk` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `leave_setting_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `loglimit`
--
ALTER TABLE `loglimit`
  ADD CONSTRAINT `loglimit_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manage`
--
ALTER TABLE `manage`
  ADD CONSTRAINT `pm_id_fk` FOREIGN KEY (`pm_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `regemp_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_sender_fk` FOREIGN KEY (`sender_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notification_type_fk` FOREIGN KEY (`notification_type_id`) REFERENCES `notification_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `overtime`
--
ALTER TABLE `overtime`
  ADD CONSTRAINT `overtime_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `overtime_pm_fk` FOREIGN KEY (`pm_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD CONSTRAINT `portfolio_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `portfolio_project_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `report_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `salary`
--
ALTER TABLE `salary`
  ADD CONSTRAINT `employee_salary_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `timelog`
--
ALTER TABLE `timelog`
  ADD CONSTRAINT `timelog_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `timelog_overtime`
--
ALTER TABLE `timelog_overtime`
  ADD CONSTRAINT `timelog_overtime_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `timelog_overtime_overtime_fk` FOREIGN KEY (`overtime_id`) REFERENCES `overtime` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `timelog_overtime_timelog_fk` FOREIGN KEY (`timelog_id`) REFERENCES `timelog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `timelog_whole`
--
ALTER TABLE `timelog_whole`
  ADD CONSTRAINT `timelog_whole_employee_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `timelog_whole_overtime_fk` FOREIGN KEY (`overtime_id`) REFERENCES `overtime` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `timelog_whole_timelog_fk` FOREIGN KEY (`timelog_id`) REFERENCES `timelog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `employee_token_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
