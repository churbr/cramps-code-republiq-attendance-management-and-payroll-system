<?php

	if(!defined('BASEPATH')) exit('No direct script access allowed.');
	
	class Timelog_whole extends CI_Model {
			
		public function __construct() {
			parent::__construct();
		}

		// --------------------------------------------------------------------------

		public function insert( $whole_log_info ) {

			if(count($whole_log_info)) {
				$this->db->insert('timelog_whole', $whole_log_info);
			}

			if($this->db->affected_rows()) { return true; }

			return false;
		}

		// --------------------------------------------------------------------------

		public function timelog_pk( $overtime_pk ) {

			$this->db->select('timelog_id');
			$this->db->where('overtime_id', $overtime_pk);
			$this->db->where('employee_id', $this->session->id);
			$query = $this->db->get('timelog_whole');

			if($query->num_rows()) {
				return $query->result_array()[0]['timelog_id'];
			}
			
			return false;
		}

		// --------------------------------------------------------------------------

		public function hide( $timelog_id, $overtime_id) {
			
			$this->db->where('timelog_id', $timelog_id);
			$this->db->where('overtime_id', $overtime_id);
			$this->db->where('employee_id', $this->session->id);
			$this->db->update('timelog_whole', array('show' => 0));
			
			if($this->db->affected_rows()) {
				return true;
			}

			return false;
		}

		// --------------------------------------------------------------------------

	}