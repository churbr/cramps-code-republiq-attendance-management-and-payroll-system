<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Adminm extends CI_Model {

		public function __construct() {
			parent::__construct();
			$this->load->model(array('leave', 'overtime'));
			
		}
		public function getDropdown($type){
			$final = [];
			if($type=='months'){
				$final[] = array(
					'val'=>'all',
					'name'=>'All'
				);
				$final[] = array(
					'val'=>'01',
					'name'=>'January'
				);
				$final[] = array(
					'val'=>'02',
					'name'=>'February'
				);
				$final[] = array(
					'val'=>'03',
					'name'=>'March'
				);
				$final[] = array(
					'val'=>'04',
					'name'=>'April'
				);
				$final[] = array(
					'val'=>'05',
					'name'=>'May'
				);
				$final[] = array(
					'val'=>'06',
					'name'=>'June'
				);
				$final[] = array(
					'val'=>'07',
					'name'=>'July'
				);
				$final[] = array(
					'val'=>'08',
					'name'=>'August'
				);
				$final[] = array(
					'val'=>'09',
					'name'=>'September'
				);
				$final[] = array(
					'val'=>'10',
					'name'=>'October'
				);
				$final[] = array(
					'val'=>'11',
					'name'=>'November'
				);
				$final[] = array(
					'val'=>'12',
					'name'=>'December'
				);
				
			}
			else if($type=='years'){
				$final[] = array(
					'val'=>'all',
					'name'=>'All'
				);
				$curdate = date('Y');	
				for($i=0;$i<50;$i++){
					$final[] = array(
						'val'=>($curdate-$i),
						'name'=>($curdate-$i)
					);
				}
			}			
			return $final;
		}
		public function resetAdjustments($user){
			//This is called manually every time custom adjustments are modified/added into since
			//ID cannot be tracked due to dynamic form fields
			$this->db->where('employee_id',$user);
  			$this->db->delete('custom_adjustments');
		}
		public function addAdjustment($user,$name,$value,$type){
			$this->db->insert('custom_adjustments', array(
				'name' => $name,
				'employee_id'=>$user,
				'value' => $value,
				'type' => $type,
			));
		}
		public function getAdjustments($id,$type=''){
			$this->db->select('id,employee_id,name,type,value');
			$this->db->from('custom_adjustments');
			$this->db->where('employee_id', $id);
			if($type<>''){
				$this->db->where('type', $type);
			}
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}
			else{
				return [];
			}			
		}	
	    public function get_basic_info_2( $id ) {
		
			$this->db->select('employee.id, employee.firstname, employee.middlename, employee.lastname, employee.gender, employee.picture, employee.username, salary.rate');
			$this->db->from('employee');
			$this->db->join('salary','employee.id = salary.employee_id','left outer');
			$this->db->where('employee.id', $id);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}
		// --------------------------------------------------------------------------
		public function get_employees(){
			$this->db->select('id, firstname, middlename, lastname, type, status');
			$this->db->order_by('id', 'ASC');
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				return $query->result_array();	
			}
			else{
				return false;
			}
		}
		public function update_employee($id, $updates = null) {
			$ret = 0;
			if(!is_null($updates)) {
				$this->db->select('employee_id');
				$this->db->from('salary');
				$this->db->where('employee_id',$id);
				$query = $this->db->get();
				if($query->num_rows()&&$updates['rate']) {
					$this->db->where('employee_id', $id);
					$this->db->update('salary', array(
									'rate'=>$updates['rate']
									));					
				}
				else{
					$this->db->insert('salary', array(
						'employee_id' => $id,
						'rate' => $updates['rate']
					));					
				}
				if($this->db->affected_rows()) {					
					$ret = 0;
				}
				$this->db->where('id', $id);
				$this->db->update('employee', array(
												'lastname'=>$updates['lastname'],
												'firstname'=>$updates['firstname'],
												'username'=>$updates['username'],
												'birthday'=>$updates['birthdate'],
												'gender'=>$updates['gender'],
												'address'=>$updates['address'],
												'email'=>$updates['email'],
												'contact'=>$updates['contact'],
												'date_hired'=>$updates['startdate']
												));				
				if($this->db->affected_rows()) {					
					$ret = 0;
				}
				return $ret;
			}
			else{
				return 1;
			}
		}
		public function add_employee($employee_details){
			$newlycreated = 0;
			$username = substr($employee_details['firstname'],0,3).$employee_details['lastname'];
			$originalusername = $username;
			$runningnumber = 1;
			$username_existed = self::username_exist($username);
			while($username_existed){
				$username = $originalusername.$runningnumber;
				$username_existed = self::username_exist($username);
				$runningnumber++;
			}
			if($username_existed) {
				return 3;

			}else {
				$this->db->insert('employee', array(
					'username' => $username,
					'password' => $this->encryption->encrypt('welcome100'),
					'firstname' => $employee_details['firstname'],
					'lastname' => $employee_details['lastname'],
					'middlename' => $employee_details['middlename'],
					'gender' => $employee_details['gender'],
					'birthday' => $employee_details['birthdate'],
					'type' => $employee_details['type'],
					'code' => '',
					'picture' => 'default.png',
					'contact' => $employee_details['contact'],
					'email' => $employee_details['email'],
					'address' => $employee_details['address'],
					'date_hired' => $employee_details['startdate'],
					'date' => $employee_details['startdate'],
					'status' => '1',
				));
				$newlycreated = $this->db->insert_id();
				
				$this->db->select('id, span');
				$this->db->from('leave_type');
				if($employee_details['gender']=='male'){
					$this->db->where('id !=','4');
				}
				else{
					$this->db->where('id !=','3');
				}
				$query2 = $this->db->get();
				if($query2->num_rows()){
					foreach($query2->result_array() as $leave){
						$this->db->insert('leave_setting', array(
							'employee_id' => $newlycreated,
							'leave_type_id' => $leave['id'],
							'remaining_days'=> $leave['span']
						));
					}					
				}
				$this->db->select('employee_id');
				$this->db->from('salary');
				$this->db->where('employee_id',$newlycreated);
				$query = $this->db->get();
				if($query->num_rows()&&$updates['rate'].length>0) {
					$this->db->where('employee_id', $newlycreated);
					$this->db->update('salary', array(
									'rate'=>$employee_details['rate']
									));
				}
				else{
					$this->db->insert('salary', array(
						'employee_id' => $newlycreated,
						'rate' => $employee_details['rate']
					));
				}
			}

			if($this->db->affected_rows()) 
			{ 
				return 1;
			}
			else{
				return 2;
			}
		}
		
		public function username_exist($username) { // Checks if username Exists
			$this->db->select('username');
			$this->db->where('username', $username);
			$query = $this->db->get('employee');

			$status = ($query->num_rows() > 0) ? true : false;

			return $status;
		}
		public function deactivate_reason($user,$reason,$status){
			if($status==0){
				$this->db->delete('deactivate', array('employee_id' => $user)); 
				$this->db->insert('deactivate', array(
							'employee_id' => $user,
							'reason' => $reason,
							'status'=>$status,
							'date'=>date('Y-m-d')
							));
				if($this->db->affected_rows()) 
				{ 
					return true;
				}
				else{
					return false;
				}
			}
			else{
				$this->db->delete('deactivate', array('employee_id' => $user)); 
				$this->db->insert('deactivate', array(
							'employee_id' => $user,						
							'status'=>$status,
							'date'=>date('Y-m-d')
							));
				if($this->db->affected_rows()) 
				{ 
					return true;
				}
				else{
					return false;
				}
			}
			
		}
		public function admin_update_info($id, $updates = null) {

			if(!is_null($updates)) {
				$this->db->where('id', $id);
				$this->db->update('employee', $updates);
				
				if($this->db->affected_rows()) {
					return true;
				}
			}

			return false;
		}
		public function get_employee_skill($id){
			$this->db->select('employee_skill.employee_id, employee_skill.skill_id, skill.description');
			$this->db->from('employee_skill');
			$this->db->join('skill', 'employee_skill.skill_id = skill.id','left outer');
			$this->db->where('employee_skill.employee_id', $id);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}
		public function get_all_skills(){
			$this->db->select('*');
			$this->db->from('skill');
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}
		public function get_all_pm(){
			$this->db->select('id, firstname, middlename, lastname');
			$this->db->from('employee');
			$this->db->where('type', 'project_manager');
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}
		public function get_pm($emp_id) {

			$this->db->select('manage.pm_id, employee.firstname, employee.lastname');
			$this->db->from('manage');
			$this->db->join('employee', 'employee.id = manage.pm_id', 'left outer');
			$this->db->where('manage.employee_id', $emp_id);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}
		public function reset_skills($id){
			$this->db->delete('employee_skill', array('employee_id' => $id)); 
		}
		public function reset_managers($id){
		$this->db->delete('manage', array('employee_id' => $id)); 
		}
		public function add_skill($id, $skill){
			$this->db->select('skill_id');
			$this->db->from('employee_skill');
			$this->db->where('skill_id', $skill);
			$this->db->where('employee_id', $id);
			$query = $this->db->get();
			if($query->num_rows()) {
				return true;
			}
			else{
				$this->db->insert('employee_skill', array(
						'employee_id' => $id,
						'skill_id' => $skill,
						));
				if($this->db->affected_rows()) 
				{ 
					return true;
				}
				else{
					return false;
				}
			}
		}
		public function updateDeductions($id,$ph,$sss,$pagibig,$tax){
			if($ph==''){
				$ph='0';
			}
			if($pagibig==''){
				$pagibig='0';
			}
			if($sss==''){
				$sss='0';
			}
			if($tax==''){
				$tax='0';
			}
			$this->db->select('id,employee_id');
			$this->db->from('employee_deduction');
			$this->db->where('employee_id',$id);
			$this->db->where('deduction_id','1');
			$query = $this->db->get();			
			if($query->num_rows()) {
				$this->db->where('employee_id', $id);
				$this->db->where('id',$query->row()->id);
				$this->db->update('employee_deduction', array(
								'deduction_id' => '1',
								'value'=>$ph								
								));
			}
			else{
				$this->db->insert('employee_deduction', array(
					'employee_id' => $id,
					'deduction_id' => '1',
					'value' => $ph
				));
			}
			if($this->db->affected_rows()) 
			{ 
				
			}
			$this->db->select('id,employee_id');
			$this->db->from('employee_deduction');
			$this->db->where('employee_id',$id);
			$this->db->where('deduction_id','2');
			$query2 = $this->db->get();			
			if($query2->num_rows()) {
				$this->db->where('employee_id', $id);
				$this->db->where('id',$query2->row()->id);
				$this->db->update('employee_deduction', array(
								'deduction_id' => '2',
								'value'=>$sss
								));
			}
			else{
				$this->db->insert('employee_deduction', array(
					'employee_id' => $id,
					'deduction_id' => '2',
					'value' => $sss
				));
			}
			if($this->db->affected_rows()) 
			{ 
				
			}
			$this->db->select('id,employee_id');
			$this->db->from('employee_deduction');
			$this->db->where('employee_id',$id);
			$this->db->where('deduction_id','3');
			$query3 = $this->db->get();			
			if($query3->num_rows()) {
				$this->db->where('employee_id', $id);
				$this->db->where('id',$query3->row()->id);
				$this->db->update('employee_deduction', array(
								'deduction_id' => '3',
								'value'=>$pagibig
								));
			}
			else{
				$this->db->insert('employee_deduction', array(
					'employee_id' => $id,
					'deduction_id' => '3',
					'value' => $pagibig
				));
			}
			if($this->db->affected_rows()) 
			{ 
				
			}			
			$this->db->select('id,employee_id');
			$this->db->from('employee_deduction');
			$this->db->where('employee_id',$id);
			$this->db->where('deduction_id','4');
			$query4 = $this->db->get();			
			if($query4->num_rows()) {
				$this->db->where('employee_id', $id);
				$this->db->where('id',$query4->row()->id);
				$this->db->update('employee_deduction', array(
								'deduction_id' => '4',
								'value'=>$tax
								));
			}
			else{
				$this->db->insert('employee_deduction', array(
					'employee_id' => $id,
					'deduction_id' => '4',
					'value' => $tax
				));
			}
			if($this->db->affected_rows()) 
			{ 
				
			}	
			return true;
		}
		public function updateRate($id,$rate){
			$this->db->select('employee_id');
			$this->db->from('salary');
			$this->db->where('employee_id',$id);
			$query = $this->db->get();
			if($query->num_rows()&&$rate) {
				$this->db->where('employee_id', $id);
				$this->db->update('salary', array(
								'rate'=>$rate
								));
			}
			else{
				$this->db->insert('salary', array(
					'employee_id' => $id,
					'rate' => $rate
				));
			}
			return true;
		}
		public function getEmployeeDeductions($id){
			$this->db->select('deduction_id,value');
			$this->db->from('employee_deduction');
			$this->db->where('employee_id',$id);
			$final = array(
				'ph'	=> '',
				'pagibig' => '',
				'sss' => '',
				'tax' => ''
			);
			$query = $this->db->get();
			if($query->num_rows()) {
				foreach($query->result_array() as $res){
					if($res['deduction_id']==1){
						$final['ph'] = $res['value'];
					}
					else if($res['deduction_id']==2){
						$final['sss'] = $res['value'];
					}
					else if($res['deduction_id']==3){
						$final['pagibig'] = $res['value'];
					}
					else if($res['deduction_id']==4){
						$final['tax'] = $res['value'];
					}
				}
			}
			return $final;
		}
		public function getLeaveDetails($id){
			$final = array(
				'SL' => '',
				'VL' => '',
				'PT' => '',
				'MT' => ''
			);
			$this->db->select('leave_setting.employee_id,leave_setting.leave_type_id,leave_type.code,leave_setting.remaining_days');
			$this->db->from('leave_setting');
			$this->db->join('leave_type','leave_type.id=leave_setting.leave_type_id');
			$this->db->where('leave_setting.employee_id',$id);
			$query = $this->db->get();
			if($query->num_rows()) {
				foreach($query->result_array() as $arr){
					if($arr['code']=='SL'){
						$final['SL'] = $arr['remaining_days'];
					}
					else if($arr['code']=='VL'){
						$final['VL'] = $arr['remaining_days'];
					}
					else if($arr['code']=='PT'){
						$final['PT'] = $arr['remaining_days'];
					}
					else if($arr['code']=='MT'){
						$final['MT'] = $arr['remaining_days'];
					}
				}
				return $final;
			}
		}	
		public function updateLeave($id,$type,$amt){
			$this->db->select('id');
			$this->db->from('leave_type');
			$this->db->where('code',$type);
			$query = $this->db->get();
			if($query->num_rows()) {
				$this->db->select('employee_id');
				$this->db->from('leave_setting');
				$this->db->where('employee_id',$id);
				$this->db->where('leave_type_id', $query->row()->id);
				$query2 = $this->db->get();
				if($query2->num_rows()) {
					$this->db->where('employee_id', $id);
					$this->db->where('leave_type_id', $query->row()->id);
					$this->db->update('leave_setting', array(
									'remaining_days'=>$amt
									));
				}
				else{
					$this->db->insert('leave_setting', array(
					'employee_id' => $id,
					'leave_type_id' => $query->row()->id,
					'remaining_days' => $amt
					));
				}
			}
			return true;
		}
		public function add_manager($emp,$manager){
			$this->db->select('pm_id');
			$this->db->from('manage');
			$this->db->where('pm_id', $manager);
			$this->db->where('employee_id', $emp);
			$query = $this->db->get();
			if($query->num_rows()) {
				return true;
			}
			else{
				$this->db->insert('manage', array(
						'employee_id' => $emp,
						'pm_id' => $manager,
						'date' => date('Y-m-d'),
						'status' => '1'
						));
				if($this->db->affected_rows()) 
				{ 
					return true;
				}
				else{
					return false;
				}
			}
			
		}
	}