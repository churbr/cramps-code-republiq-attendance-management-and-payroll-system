<div class="modal" id="employee_notification_leave_modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" id='hp_view_modal_close' class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"> <span id='hp_name'>Bryan Chu</span>
          <small> <span id='leave_type'>Sick Leave</span> </small>
        </h3>
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-xs-4">
						<h4>From: <span id='leave_date_from'></span> </h4>
					</div>
					
					<div class="form-group col-xs-4">
						<h4>To: <span id='leave_date_to'></span> </h4>
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
				<h5 class="pull-right text-red">Noted by: <span id='leave_approved_by'>Jason Roxas on 10/18/2015</span> </h5>
				<h5 class="pull-left text-red">Date Filed: <span id='leave_date_filed'>10/18/2015</span> </h5>
				<input type='hidden' id='leave_id' name='leave_id' />
			</div>
			
			<div class="modal-body" id='hp_leave_comfirm_buttons'>
				<div class="row">
					<div class="col-sm-3 col-xs-4 pull-right">
						<button class="btn btn-block btn-danger" href="#leave-request-decline-prompt" data-toggle="modal">
							<i class="fa fa-fw fa-close"></i> Decline
						</button>
					</div>
				
					<div class="col-sm-3 pull-right">
						<button class="btn btn-block btn-success" href="#leave-request-approve-prompt" data-toggle="modal">
							<i class="fa fa-fw fa-check"></i> Approve
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<div class="modal modal-default" id="leave-request-approve-prompt" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class="fa fa-warning"></i></h4>
			</div>
			
			<div class="modal-body text-centered">
				<h4 class="text-red">You are about to approve this request.</h4>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" id='hp_approved_leave' data-dismiss="modal" class="btn btn-default">Confirm</button>
			</div>
			
		</div>
	</div>
</div>


<div class="modal modal-default" id="leave-request-decline-prompt" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class="fa fa-warning"></i></h4>
			</div>
			
			<div class="modal-body text-centered">
				<h4 class="text-red">You are about to decline this request.</h4>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" id='hp_declined_leave' data-dismiss="modal" class="btn btn-default">Confirm</button>
			</div>
		</div>
	</div>
</div>

<!-- ******************************************************************************************************************************** -->


<div class="content-wrapper ">
	<div class="container">
		<section class="content-header">
		</section>
		<section class="content">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-aqua">
						<span class="info-box-icon"><i class="fa fa-feed"></i></span>
						<div class="info-box-content">
							<span class="info-box-text">Request Feed</span>
							<div class="progress">
								<div class="progress-bar" style="width: 100%"></div>
							</div>
							
							<span class="progress-description">
                <?php
                  if($leave_pending_count > 0) {
                    $word = ($leave_pending_count > 1) ? 'applications' : 'application';
                    echo $leave_pending_count . ' pending ' . $word;
                  }else { echo 'No pending application'; }
                ?>
							</span>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-body">
 							<table id="reqfeed_table" class="table table-hover">
								<thead>
									<tr>
										<th style="display: none;"></th>
										<th>Date Filed</th>
										<th>Name</th>
										<th>Type</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

								<?php
									if($employee_notification_list_all === FALSE) {

										echo "
											<tr>
												<th style='display: none;'></th>
												<th><p class='error'>No record.</p></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										";



									}else {

										foreach ($employee_notification_list_all as $index => $notification_list) {

											$id = 'id="leave_tr_' . $notification_list['leave_id'] . '"';

											if($notification_list['seen_by'] == 0 && $notification_list['status'] != 'cancelled') {
												$seen_status = 'class="bg-gray"';
											}else { $seen_status = ''; }

											// --------------------------------------------------------------------------------

											if($notification_list['status'] == 'pending') {
												$html_status = '<span class="label label-warning">Pending</span>';
											}elseif($notification_list['status'] == 'declined') {
												$html_status = '<span class="label label-danger">Declined</span>';
											}else {
												$html_status = '<span class="label label-success">Approved</span>';
											}

											// --------------------------------------------------------------------------------

?>
											<tr <?= $seen_status . ' ' . $id; ?>>
												<td style='display: none;'></td>
												<td> <?= nice_date($notification_list['date'], 'M d, Y'); ?> </td>
												<td> <?= $notification_list['employee_name']; ?> </td>
												<td> <?= $notification_list['leave_type']; ?> </td>
												<td id='status'> <?= $html_status; ?> </td>
												<td>
													<div class="btn-group">
														<a class="btn btn-info btn-sm" onclick="employee_notification_leave_modal(<?= $notification_list['leave_id'] ?>)" data-toggle="modal">View</a>
													</div>
												</td>
											</tr>
<?php

										}
									}
								?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>