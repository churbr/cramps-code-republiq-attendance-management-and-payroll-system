	// -----------------------------------------------------------------------------------------------------------------------------

	function checkpass(field_name) {

		var field = $(field_name).attr('id');

		switch(field) {
			case 'oldpass_field':
				check_oldpass_field();
			break;

			case 'newpass_field':
				check_newpass_field();
			break;

			case 'confirmpass_field':
				setTimeout(check_confirmpass_field, 2000);
			break;

			default:
				console.log('Something went wrong!');
			break;
		}
	}

	// -----------------------------------------------------------------------------------------------------------------------------

	function check_oldpass_field() {

		var oldpassword = $('#oldpass_field').val();
		var base_url = $('#base_url').val();

		if(!oldpassword) {
			$('#oldpass_field').css('border-color', '#FF0000');
			$('#oldpass_info').html('<p class="error">Please input your old password.</p>');
		}

		if(oldpassword != '') {
			$('#oldpass_field').css('border-color', '#3C8DBC');

			$.post(
				base_url + 'user/check_oldpass',
				{ oldpassword: oldpassword },
				function(response) {
					
					if(response == 'success') {
						$('#oldpass_field').css('border-color', '#D2D6DE');
						$('#oldpass_info').html("<img src='" +  base_url + "resources/dist/img/icons/success.png' width='20px' height='20px'>");
					}else {
						$('#oldpass_field').css('border-color', '#FF0000');
						$('#oldpass_info').html("<p class='error'>Old password doesn't match.</p>");
					}

				}
			);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------------------

	function check_newpass_field() {

		var newpassword = $('#newpass_field').val();
		var base_url = $('#base_url').val();

		if(!newpassword) {
			$('#newpass_field').css('border-color', '#FF0000');
			$('#newpass_info').html('<p class="error">Please input a password.</p>');
		}

		if(newpassword != '') {
			$('#newpass_field').css('border-color', '#3C8DBC');

			$.post(
				base_url + 'user/check_newpass',
				{ newpassword: newpassword },
				function(response) { // 3 FLAGS : too_short; weak; medium; strong
					
					if(response != 'fail') {

						switch(response) {
							case 'too_short':
								$('#newpass_field').css('border-color', '#FF0000');
								$('#newpass_info').html("<p class='error'>Too short</p>");
							break;

							case 'weak':
								$('#newpass_field').css('border-color', '#FF0000');
								$('#newpass_info').html("<p class='gray'>Weak</p>");
							break;

							case 'medium':
								$('#newpass_field').css('border-color', '#D2D6DE');
								$('#newpass_info').html("<p class='blue'>Medium</p>");
							break;

							case 'strong':
								$('#newpass_field').css('border-color', '#D2D6DE');
								$('#newpass_info').html("<p class='green'>Strong</p>");
							break;

							default:
								console.log('Something went wrong!');
							break;
						}
					}

				}
			);
		}

	}

	// -----------------------------------------------------------------------------------------------------------------------------

	function check_confirmpass_field() {

		var newpassword = $('#newpass_field').val();
		var confirmpass = $('#confirmpass_field').val();
		var base_url = $('#base_url').val();

		if(!confirmpass) {
			$('#confirmpass_field').css('border-color', '#FF0000');
			$('#repass_info').html('<p class="error">Please confirm your password.</p>');
		}

		if(confirmpass != '') {
			$('#confirmpass_field').css('border-color', '#3C8DBC');

			$.post(
				base_url + 'user/check_newpass',
				{ newpassword: newpassword },
				function(newpassword_strength) {

					if(newpassword != '') {
						
						// ----------------------------------------------------------------------

							if(confirmpass == newpassword) {

								if(newpassword_strength == 'medium' || newpassword_strength == 'strong') {
									$('#repass_info').html("<img src='" +  base_url + "resources/dist/img/icons/success.png' width='20px' height='20px'>");
								}else {
									if(newpassword_strength == 'too_short') { newpassword_strength = 'too short'; }
									$('#confirmpass_field').css('border-color', '#FF0000');
									$('#repass_info').html("<p class='error'>New password is " + newpassword_strength + "</p>");
								}

							}else {
								$('#confirmpass_field').css('border-color', '#FF0000');
								$('#repass_info').html("<p class='error'>Password doesn't match.</p>");
							}

						// ----------------------------------------------------------------------

					}else {
						$('#confirmpass_field').css('border-color', '#FF0000');
						$('#repass_info').html("<p class='error'>New password is empty.</p>");
					}

				}
			);

		}
	}

	// -----------------------------------------------------------------------------------------------------------------------------