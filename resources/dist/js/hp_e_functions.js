	
	// ----------------------------------------------------------------------------------------------------------------
	
	function employee_notification_overtime_modal( overtime_id ) {
		var base_url = $('.base_url').attr('id');

		$.post(
			base_url + 'attendance/hp-view-overtime-info.html',
			{ table_pk: overtime_id },
			function(response) {
				var parsed = jQuery.parseJSON(response);

				if(parsed.hour > 1) { word_hour = ' hrs '; } else { word_hour = ' hr '; }
				if(parsed.minute > 1) { word_min = ' mins '; } else { word_min = ' mins'; }
				if(parsed.note == null) { note = 'Not noted.' } else { note = parsed.note; }
				if(parsed.date_updated == 'Unknown') { date_updated = '--'; } else { date_updated = parsed.date_updated; }

				if(parsed.overtime_status == 'pending') {
					overtime_status = 'Pending';
				}else if(parsed.overtime_status == 'declined') {
					overtime_status = 'Declined';
				}else {
					overtime_status = 'Approved';
				}

				ot_total_time = parsed.hour + word_hour + parsed.minute + word_min;

				$('#overtime_tr_' + overtime_id).attr('class', '');
				$('#employee_notification_overtime_modal #hp_name').html(parsed.firstname + ' ' + parsed.lastname);
				$('#employee_notification_overtime_modal #ot_date_from').html(parsed.start_date);
				$('#employee_notification_overtime_modal #ot_date_to').html(parsed.end_date);
				$('#employee_notification_overtime_modal #ot_start_time').html(parsed.start_time);
				$('#employee_notification_overtime_modal #ot_end_time').html(parsed.end_time);
				$('#employee_notification_overtime_modal #ot_total_time').html(ot_total_time);
				$('#employee_notification_overtime_modal #ot_reason').html(note);
				$('#employee_notification_overtime_modal #ot_status').html(overtime_status);
				$('#employee_notification_overtime_modal #date_filed').html(parsed.date_filed);
				$('#employee_notification_overtime_modal #overtime_id').val(overtime_id);

				$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');

				if(parsed.overtime_status == 'approved' || parsed.overtime_status == 'declined') {
					$('#hp_ot_comfirm_buttons').hide();
				}else {
					$('#hp_ot_comfirm_buttons').show();
				}

				$('#employee_notification_overtime_modal').modal('show');
			}
		);
	}

	// ----------------------------------------------------------------------------------------------------------------

	function seen_request(notification_type_id, table_pk) {
		var base_url = $('.base_url').attr('id');

		if(notification_type_id == 1) { notification_type = 'leave'; }
		if(notification_type_id == 2) { notification_type = 'overtime'; }

		var table_row = '#tr_' + notification_type + '_request_' + table_pk;

		$.post(
			base_url + 'attendance/seen.html',
			{ notification_type_id: notification_type_id, table_pk: table_pk },
			function(response) {
				$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');
				$(table_row).attr('class', '');
			}
		);
		
		return false;
	}

	// ----------------------------------------------------------------------------------------------------------------

	function cancel_leave_request( table_pk ) {
		var base_url = $('.base_url').attr('id');

		$.post(
			base_url + 'attendance/cancel-leave-request.html',
			{ table_pk: table_pk },
			function(response) {
				if(response == 'true') {
					$('#tr_leave_request_' + table_pk + ' .request_status').html('<span class="label label-danger">Cancelled</span>');
					$('#tr_leave_request_' + table_pk + ' #cancel_button').fadeOut();
					$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');
				}
			}
		);
		
		return false;
	}

	// ----------------------------------------------------------------------------------------------------------------
	
	function employee_notification_leave_modal( leave_id ) {
		var base_url = $('.base_url').attr('id');

		$.post(
			base_url + 'attendance/hp-view-leave-info.html',
			{ table_pk: leave_id },
			function(response) {
				var parsed = jQuery.parseJSON(response);

				if(parsed.bod_id == null) {
					approved_by = '--';
				} else { approved_by = parsed.bod_name + ' on ' + parsed.date_updated; }

				if(parsed.leave_status == 'pending') {
					leave_status = 'Pending';
				}else if(parsed.leave_status == 'declined') {
					leave_status = 'Declined';
				}else {
					leave_status = 'Approved';
				}

				$('#leave_tr_' + leave_id).attr('class', '');
				$('#employee_notification_leave_modal #hp_name').html(parsed.employee_name);
				$('#employee_notification_leave_modal #leave_type').html(parsed.leave_type);
				$('#employee_notification_leave_modal #leave_date_from').html(parsed.start_date);
				$('#employee_notification_leave_modal #leave_date_to').html(parsed.end_date);
				$('#employee_notification_leave_modal #leave_approved_by').html(approved_by);
				$('#employee_notification_leave_modal #leave_date_filed').html(parsed.date_filed);
				$('#employee_notification_leave_modal #leave_id').val(leave_id);

				$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');

				if(parsed.leave_status == 'approved' || parsed.leave_status == 'declined') {
					$('#hp_leave_comfirm_buttons').hide();
				}else {
					$('#hp_leave_comfirm_buttons').show();
				}

				$('#employee_notification_leave_modal').modal('show');
			}
		);
	}

	// ----------------------------------------------------------------------------------------------------------------


	// CHUCHUCHU

	function show_holiday_edit_form( holiday_id ) {

		var base_url = $('.base_url').attr('id');
		html_form_select = '';


		$.post(
			base_url + 'user/view-holiday-detail.html',
			{ table_pk: holiday_id },
			function(response) {
				var parsed = jQuery.parseJSON(response);

				$('#holiday_edit_title').html('Edit ' + parsed.name);
				$('#edit-holiday-modal #holiday_name').val(parsed.name);
				$('#edit-holiday-modal #holiday_date').val(parsed.date);
				$('#edit-holiday-modal #edit_holiday_table_pk').val(parsed.id);

				 if(parsed.type == 'regular') {
				 	html_form_select += '<select id="holiday_edit_type" class="form-control">';
				 	html_form_select += '<option value="regular" selected>Regular Holiday</option>';
				 	html_form_select += '<option value="special">Special Holiday</option>';
				 	html_form_select += '</select>';

				 	$('#edit-holiday-modal #select_date_div').html(html_form_select);
				 }else {
				 	html_form_select += '<select id="holiday_edit_type" class="form-control">';
				 	html_form_select += '<option value="regular">Regular Holiday</option>';
				 	html_form_select += '<option value="special" selected>Special Holiday</option>';
				 	html_form_select += '</select>';

				 	$('#edit-holiday-modal #select_date_div').html(html_form_select);
				 }

				$('#edit-holiday-modal').modal('show');
			}
		);

		return false;
	}

	function show_holiday_remove_form( holiday_id ) {

		var base_url = $('.base_url').attr('id');
		html_form_select = '';


		$.post(
			base_url + 'user/view-holiday-detail.html',
			{ table_pk: holiday_id },
			function(response) {
				var parsed = jQuery.parseJSON(response);
				$('#holiday-remove-prompt #removal_message').html('Are you sure to remove ' + parsed.name + '?');
				$('#holiday-remove-prompt #holiday_removal_pk').val(parsed.id);
			
				$('#holiday-remove-prompt').modal('show');

			}
		);

		return false;
	}

	// CHUCHUCHU