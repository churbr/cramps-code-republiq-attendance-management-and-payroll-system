	var dataref = new Firebase("https://e-notification.firebaseio.com/");

	$(document).ready(function() {

		var base_url = $('.base_url').attr('id');

		$.post(
			base_url + 'user/info.html',
			{ request: true },
			function(response) {
				var parsed = jQuery.parseJSON(response);
				session_employee_id = parsed.employee_id;
				session_employee_type = parsed.type;
			}
		);



		// -------------------------------------------------------------------------------------------------------------------------------



		/* Only use this in child changed for employee */
		(function($) {
			$.extend({
				play_sound: function() {
					return $(
						'<audio autoplay="autoplay" style="display:none;">'
						+ '<source src="' + base_url + 'resources/ding.mp3" />'
						+ '<embed src="' + base_url + 'resources/ding.mp3" hidden="true" autostart="true" loop="false" class="playSound" />'
						+ '</audio>'
					).appendTo('body');
				}
			});
		})(jQuery);



		// -------------------------------------------------------------------------------------------------------------------------------



		$('#hp_approved_ot').on('click', function() {
			var overtime_id = $('#overtime_id').val();
		 	comfirm_employee_application(overtime_id, 'approved');
		});



		// -------------------------------------------------------------------------------------------------------------------------------



		$('#hp_declined_ot').on('click', function() {
		 	var overtime_id = $('#overtime_id').val();
		 	comfirm_employee_application(overtime_id, 'declined');
		});



		// -------------------------------------------------------------------------------------------------------------------------------



		$('#hp_approved_leave').on('click', function() {
			var leave_id = $('#leave_id').val();
			comfirm_employee_application_leave(leave_id, 'approved');
		});



		// -------------------------------------------------------------------------------------------------------------------------------



		$('#hp_declined_leave').on('click', function() {
		 	var leave_id = $('#leave_id').val();
		 	comfirm_employee_application_leave(leave_id, 'declined');
		});



		// -------------------------------------------------------------------------------------------------------------------------------



		$('form.request_leave').on('submit', function() {

			var that = $(this),
						url		= that.attr('action'),
						method	= that.attr('method'),
						data	= {};

			that.find('[name]').each(function() {
				var that = $(this),
						name		= that.attr('name'),
						value		= that.val();
						data[name]	= value;
			});

			$.ajax({
				url: url,
				type: method,
				data: data,
				success: function(response) {
					var parsed = jQuery.parseJSON(response);

					if(!jQuery.isEmptyObject(parsed)) {

						dataref.push({
							id: parsed.id,
							notification_type_id: parsed.notification_type_id,
							notification_id: parsed.notification_id,
							sender_id: session_employee_id,
							receiver_id: parsed.employee_id,
							message: parsed.message,
							status: parsed.status,
							view: 0,
							date: parsed.date
						});

						if($('#reqfeed_table').length) {
							$('#leave_modal').load(location.href + ' #leave_modal');
							$('#reqfeed_table').load(location.href + ' #reqfeed_table');
						}

						$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');

						$('#leave_req_type').val('');
						$('#leave_req_from').val('');
						$('#leave_req_to').val('');
						$('#leave_request_message').html("<p class='green' style='font-size: 16px; padding: 5px 0px 10px; text-align: center;'>Leave Request Submitted!</p>");

						setTimeout(function() {
							$('#leave_request_message').html('');
							$('#leave_request_close').click();
						}, 2000);

					} else {

						switch(response) {
							case '1':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>Please fill in the form.</p>");
							break;

							case '2':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>The date has already passed.</p>");
							break;

							case '3':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>Invalid range of date.</p>");
							break;

							case '4':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>Type of request doesn't exist.</p>");
							break;

							case '5':
								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>Non-working days is not included.</p>");
							break;

							case '6':
								leave_desc = '';
								leave_val = $('#leave_req_type').val();

								if(leave_val == 1) {
									leave_desc = 'sick leave';
								}else if(leave_val == 2) {
									leave_desc = 'vacation leave';
								}else if(leave_val == 3) {
									leave_desc = 'paternity leave';
								}else if(leave_val == 4) {
									leave_desc = 'maternity leave';
								}

								$('#leave_request_message').html("<p class='error' style='padding: 5px 0px 10px; text-align: center;'>You don't have enough " + leave_desc + " remaining days.</p>");
							break;
						}

					}
				}
			});

			return false;
		});



		// -------------------------------------------------------------------------------------------------------------------------------

		

		dataref.on('child_changed', function(snapshot) { // Detect Updates

			var server_data = snapshot.val();
			var child_key = snapshot.key();

			if (session_employee_type == 'super_admin') {
			} else if(session_employee_type == 'admin') {
			} else if(session_employee_type == 'board_of_director') {
			} else if(session_employee_type == 'project_manager') {
			} else {
				notify_regemp(server_data.notification_type_id, server_data.notification_id, server_data.status, server_data.sender_id, server_data.receiver_id);
			}

		});



		// -------------------------------------------------------------------------------------------------------------------------------



		dataref.on('child_added', function(snapshot) {
			var server_data = snapshot.val();
			var child_key = snapshot.key();

			if (session_employee_type == 'super_admin') {
			} else if(session_employee_type == 'admin') {
			} else if(session_employee_type == 'board_of_director') {
			} else if(session_employee_type == 'project_manager') {

				if ( server_data.receiver_id == session_employee_id && server_data.notification_type_id == 2 && server_data.status == 0 && server_data.view == 0 ) {
					notification_employee_to_pm( server_data.notification_id, server_data.sender_id );
					$('#nav_dropdown_menu').load(location.href + ' #nav_dropdown_menu');
				}

			} else {  }

			dataref.child(child_key).update({'view': 1 });
		});

		
		// -------------------------------------------------------------------------------------------------------------------------------


		function comfirm_employee_application( overtime_id, application_status ) {

			var server_response = false;

			$.post (
				base_url + 'attendance/approve-overtime.html',
				{ table_pk: overtime_id, status: application_status, note: $('#ot_leave_note').val() },
				function(response) { }
			);

			if(application_status == 'approved') {
				firebase_status = 1;
			} else if(application_status == 'declined') {
				firebase_status = -1;
			}

			// -------------------------------------------------------------------------

			dataref.once('value', function(snapshot) {
				snapshot.forEach(function(server_data) {
					var key = server_data.key();
					var data = server_data.val();
					
					if(data.notification_type_id == 2 && data.notification_id == overtime_id && data.receiver_id == session_employee_id) {
						dataref.child(key).update({'status': firebase_status });
						return true;
					}
					
				});
			});



			// ----------------------------------------------------------------------------------------------------------------------------

			 if(application_status == 'approved') {
			 	status_html_form = '<span class="label label-success">Approved</span>';
			 }else if(application_status == 'declined') {
			 	status_html_form = '<span class="label label-danger">Declined</span>';
			 }

			$('#hp_view_modal_close').click();
			$('#overtime_tr_' + overtime_id + ' #status').html(status_html_form);

		}

		// -------------------------------------------------------------------------------------------------------------------------------

		function comfirm_employee_application_leave( leave_id, application_status ) {

			var server_response = false;

			$.post (
				base_url + 'attendance/approve-leave.html',
				{ table_pk: leave_id, status: application_status },
				function(response) { }
			);

			if(application_status == 'approved') {
				firebase_status = 1;
			} else if(application_status == 'declined') {
				firebase_status = -1;
			}

			// -------------------------------------------------------------------------

			dataref.once('value', function(snapshot) {
				snapshot.forEach(function(server_data) {
					var key = server_data.key();
					var data = server_data.val();
					
					if(data.notification_type_id == 1 && data.notification_id == leave_id) {
						dataref.child(key).update({'status': firebase_status });
						return true;
					}
					
				});
			});

			// ----------------------------------------------------------------------------------------------------------------------------

			 if(application_status == 'approved') {
			 	status_html_form = '<span class="label label-success">Approved</span>';
			 }else if(application_status == 'declined') {
			 	status_html_form = '<span class="label label-danger">Declined</span>';
			 }

			$('#hp_view_modal_close').click();
			$('#leave_tr_' + leave_id + ' #status').html(status_html_form);

		}

		// -------------------------------------------------------------------------------------------------------------------------------

		function notification_employee_to_pm( notification_id, sender_id, receiver_id ) {

			var base_url = $('.base_url').attr('id');
			var html_format = '';
			
			$.post(
				base_url + 'user/get-info.html',
				{ employee_id: sender_id },
				function(response) {

					var parsed = jQuery.parseJSON(response);

					if(!jQuery.isEmptyObject(parsed)) {

						$('body').append("<div id='notifications-bottom-right'></div>");
						$('#notifications-bottom-right').fadeIn(400);

						html_format +='<a href="#">';
						html_format +='<div id="notifications-bottom-right-tab">';
						html_format +='<div id="notifications-bottom-right-tab-close" class="close">';
						html_format +='<span class="iconb" data-icon="&#xe20e;">';
						html_format +='</span>';
						html_format +='</div>';
						html_format +='<div id="notifications-bottom-right-tab-avatar">';
						html_format +='<img src="' + base_url + 'resources/dist/img/users/' + parsed.picture + '" width="70" height="70" />';
						html_format +='</div>';
						html_format +='<div id="notifications-bottom-right-tab-right">';
						html_format +='<div id="notifications-bottom-right-tab-right-title">';
						html_format +='<span>' + parsed.firstname + ' ' + parsed.lastname + '</span> applied for overtime.';
						html_format +='</div>';
						html_format +='</div>';
						html_format +='</div>';
						html_format +='</a>';

						$("#notifications-bottom-right").html();
						$("#notifications-bottom-right").html(html_format);

						setTimeout(function() {
							$("#notifications-bottom-right").fadeOut(400);
						}, 5000);
					}
				}
			);

			return false;
		}

		// -------------------------------------------------------------------------------------------------------------------------------

});