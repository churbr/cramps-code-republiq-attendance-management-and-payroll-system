<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Export extends CI_Controller {



	public function __construct() {
				parent::__construct();
				$this->load->library(array('e_user', 'e_authentication', 'e_security'));
				$this->load->model(array('holiday2','reports','payroll','adminm','admin_db', 'employee', 'salary', 'timelog', 'holiday'));
				$this->load->helper(array('e_time', 'cookie'));
			}
		
		
	public function test(){
		$this->load->view('export-pdf/payslip');
		$html = $this->output->get_output();
	
		$this->load->library('dompdf_gen');
		$filename = "sample.pdf";
	
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream($filename);
	}
	
	public function payroll_for_employee($user='',$param='',$param2='') {
		$this->e_security->only_allow('admin');
		$data['from'] = urldecode($param);
		$data['to'] = urldecode($param2);
		if($user<>''&&$data['from']&&$data['to']){
			$basicinfo = $this->employee->get_basic_info($user);
			$data['fullname'] = $basicinfo['firstname'].' '.$basicinfo['lastname'];
			$data['quickpayroll_result']='';
			$data['final']['deductions'] = [];
			$data['final']['adjustments'] = [];
			$data['final']['total_deductions'] = '';
			$data['final']['total_adjustments'] = '';
			$data['final']['weeks'] = '';
			$data['final']['timelogs'] = [];
			$data['final']['weekpay'] = [];
			$data['user'] = $user;

			$payperiod = $data['from'].';'.$data['to'];	
			$data['final'] = $this->payroll->getQuickPayroll($user,$payperiod); //Needed to keep the drop down select the previously selected value
		
			$arr = $this->adminm->get_basic_info_2($this->session->id);
			$data['picture'] = $arr['picture'];
			
			$data['custom_additions'] = '';
			$data['custom_deductions'] = '';
			$data['deductions'] =$this->adminm->getEmployeeDeductions($user);
			$data['employee_info'] = $this->employee->get_overall_info($user);
			$data['employee_basic_info'] = $this->adminm->get_basic_info_2($user);				
			$data['emp_skills'] = $this->adminm->get_employee_skill($user);
			$data['all_skills'] = $this->adminm->get_all_skills();
			$data['all_pm'] = $this->adminm->get_all_pm();
			$data['pm_details'] = $this->adminm->get_pm($user);
	
			$data['param'] = '';
			$data['custom_additions'] = $this->adminm->getAdjustments($user,'add');
			$data['custom_deductions'] = $this->adminm->getAdjustments($user,'sub');
			
			$data['regular']='';
			$data['overtime']='';
			$data['gross']='';

			for($i=0;$i<count($data['final']['weekreg']);$i++){
				$data['regular'] += $data['final']['weekreg'][$i];
			}
			
			for($i=0;$i<count($data['final']['weekot']);$i++){
				$data['overtime'] += $data['final']['weekot'][$i];
			}
			
			for($i=0;$i<count($data['final']['weekpay']);$i++){
				$data['gross'] += $data['final']['weekpay'][$i];
			}
					
			$this->load->view('export-pdf/payslip', $data);
			$html = $this->output->get_output();
			
			$this->load->library('dompdf_gen');
			$filename = $data['fullname'].".pdf";
			
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream($filename);
			
		}
	}
			


}