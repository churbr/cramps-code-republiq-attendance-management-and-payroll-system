<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed.');

	class Employee extends CI_Model {

		public function __construct() {
			$this->load->model(array('leave', 'overtime'));
			parent::__construct();
		}

		// --------------------------------------------------------------------------

		public function account_existed($username) { // Check if account exists
			$this->db->select('username');
			$this->db->where('username', $username);
			$query = $this->db->get('employee');

			$status = ($query->num_rows() > 0) ? true : false;

			return $status;
		}

		// --------------------------------------------------------------------------

		public function get_info($id) { // Gets the user information to set session
			$this->db->select('id, username, firstname, middlename, lastname, type');
			$this->db->where('id', $id);
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_position() {

			$this->db->select('position.title');
			$this->db->from('position');
			$this->db->join('employee_position', 'employee_position.position_id = position.id');
			$this->db->where('employee_position.employee_id', $this->session->id);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array()[0]['title'];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_overall_info($id) { // Gets the user's necessary information
			$this->db->select('gender, birthday, picture, contact, email, address, code, status, date_hired, date_resignation, date');
			$this->db->where('id', $id);
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_basic_info( $id ) {
			$this->db->select('id, firstname, middlename, lastname, gender, picture');
			$this->db->where('id', $id);
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				return $query->result_array()[0];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_id($username) { // Gets the id (Primary Key) of the employee by username
			$this->db->select('id');
			$this->db->where('username', $username);
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				$result = $query->result_array()[0];
				return (int)$result['id'];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function get_password($username) { // Gets the password of a specific employee
			$this->db->select('password');
			$this->db->where('username', $username);
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				$result = $query->result_array();
				return $this->encryption->decrypt($result[0]['password']);
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function is_active($id = 0) { // Checks if employee's account is active
			$this->db->select('status');
			$this->db->where('id', $id);
			$query = $this->db->get('employee');

			if($query->num_rows()) {
				$get_status = (int)$query->result_array()[0]['status'];
				$account_status  = ($get_status === 1) ? true : false;
				
				return $account_status;
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function project_manager($employee_id = null) {

			$this->db->select('`employee`.`id`, `firstname`, `middlename`, `lastname`');
			$this->db->from('`manage`');
			$this->db->join('employee', '`manage`.`pm_id` = `employee`.`id`');
			$this->db->where('`manage`.`status`', 1);
			$this->db->where('`manage`.`employee_id`', $employee_id);
			$query = $this->db->get();

			if($query->num_rows()) {
				return $query->result_array();
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function is_managed_by($pm_id = null) {
			$manage = FALSE;
			$project_manager = self::project_manager( $this->session->id );

			foreach ($project_manager as $index => $pm_info) {
				if($pm_info['id'] == $pm_id) {
					$manage = TRUE;
				}
			}

			return $manage;
		}

		// --------------------------------------------------------------------------

		public function token_exist($employee_id) { // Checks if there's and existing cookie (Remember me function)
			$this->db->select('hash');
			$this->db->where('employee_id', $employee_id);
			$query = $this->db->get('token');

			$status = ($query->num_rows() > 0) ? true : false;

			return $status;
		}

		// --------------------------------------------------------------------------

		public function set_token($employee_id, $cookie) { // Sets the cookie for the employee to be remembered (Remember me function)

			$token_existed = self::token_exist($employee_id);

			if($token_existed) {
				$this->db->where('employee_id', $employee_id);
				$this->db->update('token', array('hash' => $cookie));

			}else {
				$this->db->insert('token', array(
					'employee_id' => $employee_id,
					'hash' => $cookie
				));

			}

			if($this->db->affected_rows()) { return true; }

			return false;
		}

		// --------------------------------------------------------------------------

		public function extract_token_info($hash) { // Gets the employee_id (foreign key) using the cookie value
			$this->db->select('employee_id');
			$this->db->where('hash', $hash);
			$query = $this->db->get('token');

			if($query->num_rows()) {
				$result = $query->result_array()[0];
				return (int)$result['employee_id'];
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function skills( $employee_id = null ) {

			$skill_list = '';

			if($this->session->is_loggedin) {

				$this->db->select('skill.title');
				$this->db->from('skill');
				$this->db->join('employee_skill', 'employee_skill.skill_id = skill.id');
				
				if(!is_null($employee_id)) {
					$this->db->where('employee_skill.employee_id', $employee_id);
				} else {
					$this->db->where('employee_skill.employee_id', $this->session->id);
				}

				$query = $this->db->get();

				if($query->num_rows()) {

					foreach ($query->result_array() as $skill) {
						$skill_list .= $skill['title'] . ', ';
					}

					$skill_list  = substr($skill_list, 0, -2);

					return $skill_list;
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function update_info($updates = null, $employee_id = null) {

			if(!is_null($updates)) {
				if(!is_null($employee_id)) {
					$this->db->where('id', $employee_id);
				}else { $this->db->where('id', $this->session->id); }

				$this->db->update('employee', $updates);
				
				if($this->db->affected_rows()) {
					return true;
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function change_password($employee_id = null, $password = null) {

			if(!is_null($employee_id) && !is_null($password)) {

				$password = $this->encryption->encrypt($password);

				$this->db->where('id', $employee_id);
				$this->db->update('employee', array('password' => $password));
				
				if($this->db->affected_rows()) {
					return true;
				}
			}

			return false;
		}

		// --------------------------------------------------------------------------

		public function weekly_attendance($employee_id, $weekly_range) {
			$weekly_attendance = array();

			$days = array(
				1 => array('monday' => array()),
				2 => array('tuesday' => array()),
				3 => array('wednesday' => array()),
				4 => array('thursday' => array()),
				5 => array('friday' => array()),
				6 => array('saturday' => array()),
				7 => array('sunday' => array())
			);

			// ------------------------------------

			$this->db->select('time_in, time_out, timein_date, hour, minute');
			$this->db->where('employee_id', $employee_id);
			$this->db->where('`show`', 1);
			$this->db->where_in('timein_date', $weekly_range);
			$this->db->order_by('timein_date', 'ASC');
			$query_timelog_overtime = $this->db->get('timelog_overtime');

			if($query_timelog_overtime->num_rows()) {

				foreach ($query_timelog_overtime->result_array() as $index => $reghours) {

					$time_in = $this->e_attendance->time_extract($reghours['time_in']);
					$military_hour = $this->e_attendance->to_military($time_in['hour'], $time_in['ampm']);
					$this_minute = ($time_in['minute'] === 0) ? '00' : $time_in['minute'];
					$timestamp = $reghours['timein_date'] . ' ' . $military_hour . ':' . $this_minute . ':00';

					$weekly_attendance[] = array (
						'time_in' => $reghours['time_in'],
						'time_out' => $reghours['time_out'],
						'timein_date' => $reghours['timein_date'],
						'hour' => $reghours['hour'],
						'minute' => $reghours['minute'],
						'timestamp' => strtotime($timestamp),
						'type' => 'regular_hour'
					);
				}
			}

			// -------------------------------------------------------------------------------------------------------

			$this->db->select('time_in, time_out, timein_date, hour, minute');
			$this->db->where('employee_id', $employee_id);
			$this->db->where('`show`', 1);
			$this->db->where_in('timein_date', $weekly_range);
			$this->db->order_by('timein_date', 'ASC');
			$query_timelog = $this->db->get('timelog');

			if($query_timelog->num_rows()) {

				foreach ($query_timelog->result_array() as $index => $reghours) {

					$time_in = $this->e_attendance->time_extract($reghours['time_in']);
					$military_hour = $this->e_attendance->to_military($time_in['hour'], $time_in['ampm']);
					$this_minute = ($time_in['minute'] === 0) ? '00' : $time_in['minute'];
					$timestamp = $reghours['timein_date'] . ' ' . $military_hour . ':' . $this_minute . ':00';

					$weekly_attendance[] = array (
						'time_in' => $reghours['time_in'],
						'time_out' => $reghours['time_out'],
						'timein_date' => $reghours['timein_date'],
						'hour' => $reghours['hour'],
						'minute' => $reghours['minute'],
						'timestamp' => strtotime($timestamp),
						'type' => 'regular_hour'
					);
				}
			}

			// -------------------------------------------------------------------------------------------------------

			$this->db->select('start_time, end_time, start_date, hour, minute');
			$this->db->where('employee_id', $employee_id);
			$this->db->where('status', 'pending');
			$this->db->where('`show`', 1);
			$this->db->where_in('start_date', $weekly_range);
			$this->db->order_by('start_date', 'ASC');
			$query_pending_overtime = $this->db->get('overtime');

			if($query_pending_overtime->num_rows()) {

				foreach ($query_pending_overtime->result_array() as $index => $overtime) {

					$time_in = $this->e_attendance->time_extract($overtime['start_time']);
					$military_hour = $this->e_attendance->to_military($time_in['hour'], $time_in['ampm']);
					$this_minute = ($time_in['minute'] === 0) ? '00' : $time_in['minute'];
					$timestamp = $overtime['start_date'] . ' ' . $military_hour . ':' . $this_minute . ':00';

					$weekly_attendance[] = array (
						'time_in' => $overtime['start_time'],
						'time_out' => $overtime['end_time'],
						'timein_date' => $overtime['start_date'],
						'hour' => $overtime['hour'],
						'minute' => $overtime['minute'],
						'timestamp' => strtotime($timestamp),
						'type' => 'overtime_pending'
					);
				}
			}

			// -------------------------------------------------------------------------------------------------------

			$this->db->select('start_time, end_time, start_date, hour, minute');
			$this->db->where('employee_id', $employee_id);
			$this->db->where('status', 'declined');
			$this->db->where('`show`', 1);
			$this->db->where_in('start_date', $weekly_range);
			$this->db->order_by('start_date', 'ASC');
			$query_overtime = $this->db->get('overtime');

			if($query_overtime->num_rows()) {

				foreach ($query_overtime->result_array() as $index => $overtime) {

					$time_in = $this->e_attendance->time_extract($overtime['start_time']);
					$military_hour = $this->e_attendance->to_military($time_in['hour'], $time_in['ampm']);
					$this_minute = ($time_in['minute'] === 0) ? '00' : $time_in['minute'];
					$timestamp = $overtime['start_date'] . ' ' . $military_hour . ':' . $this_minute . ':00';

					$weekly_attendance[] = array (
						'time_in' => $overtime['start_time'],
						'time_out' => $overtime['end_time'],
						'timein_date' => $overtime['start_date'],
						'hour' => $overtime['hour'],
						'minute' => $overtime['minute'],
						'timestamp' => strtotime($timestamp),
						'type' => 'overtime_declined'
					);
				}
			}

			// -------------------------------------------------------------------------------------------------------

			$this->db->select('start_time, end_time, start_date, hour, minute');
			$this->db->where('employee_id', $employee_id);
			$this->db->where('status', 'approved');
			$this->db->where('`show`', 1);
			$this->db->where_in('start_date', $weekly_range);
			$this->db->order_by('start_date', 'ASC');
			$query_overtime = $this->db->get('overtime');

			if($query_overtime->num_rows()) {

				foreach ($query_overtime->result_array() as $index => $overtime) {

					$time_in = $this->e_attendance->time_extract($overtime['start_time']);
					$military_hour = $this->e_attendance->to_military($time_in['hour'], $time_in['ampm']);
					$this_minute = ($time_in['minute'] === 0) ? '00' : $time_in['minute'];
					$timestamp = $overtime['start_date'] . ' ' . $military_hour . ':' . $this_minute . ':00';

					$weekly_attendance[] = array (
						'time_in' => $overtime['start_time'],
						'time_out' => $overtime['end_time'],
						'timein_date' => $overtime['start_date'],
						'hour' => $overtime['hour'],
						'minute' => $overtime['minute'],
						'timestamp' => strtotime($timestamp),
						'type' => 'overtime'
					);
				}
			}

			// -------------------------------------------------------------------------------------------------------

			foreach ($weekly_attendance as $assoc => $attendance) {
				$attendance_day = strtolower( date('l', $attendance['timestamp']) );

				switch ($attendance_day) {
					case 'monday':
						$days[1]['monday'][] = array (
							'time_in' => $attendance['time_in'],
							'time_out' => $attendance['time_out'],
							'timein_date' => $attendance['timein_date'],
							'hour' => $attendance['hour'],
							'minute' => $attendance['minute'],
							'timestamp' => $attendance['timestamp'],
							'type' => $attendance['type']
						);
					break;

					case 'tuesday':
						$days[2]['tuesday'][] = array (
							'time_in' => $attendance['time_in'],
							'time_out' => $attendance['time_out'],
							'timein_date' => $attendance['timein_date'],
							'hour' => $attendance['hour'],
							'minute' => $attendance['minute'],
							'timestamp' => $attendance['timestamp'],
							'type' => $attendance['type']
						);
					break;

					case 'wednesday':
						$days[3]['wednesday'][] = array (
							'time_in' => $attendance['time_in'],
							'time_out' => $attendance['time_out'],
							'timein_date' => $attendance['timein_date'],
							'hour' => $attendance['hour'],
							'minute' => $attendance['minute'],
							'timestamp' => $attendance['timestamp'],
							'type' => $attendance['type']
						);
					break;

					case 'thursday':
						$days[4]['thursday'][] = array (
							'time_in' => $attendance['time_in'],
							'time_out' => $attendance['time_out'],
							'timein_date' => $attendance['timein_date'],
							'hour' => $attendance['hour'],
							'minute' => $attendance['minute'],
							'timestamp' => $attendance['timestamp'],
							'type' => $attendance['type']
						);
					break;

					case 'friday':
						$days[5]['friday'][] = array (
							'time_in' => $attendance['time_in'],
							'time_out' => $attendance['time_out'],
							'timein_date' => $attendance['timein_date'],
							'hour' => $attendance['hour'],
							'minute' => $attendance['minute'],
							'timestamp' => $attendance['timestamp'],
							'type' => $attendance['type']
						);
					break;

					case 'saturday':
						$days[6]['saturday'][] = array (
							'time_in' => $attendance['time_in'],
							'time_out' => $attendance['time_out'],
							'timein_date' => $attendance['timein_date'],
							'hour' => $attendance['hour'],
							'minute' => $attendance['minute'],
							'timestamp' => $attendance['timestamp'],
							'type' => $attendance['type']
						);
					break;

					case 'sunday':
						$days[7]['sunday'][] = array (
							'time_in' => $attendance['time_in'],
							'time_out' => $attendance['time_out'],
							'timein_date' => $attendance['timein_date'],
							'hour' => $attendance['hour'],
							'minute' => $attendance['minute'],
							'timestamp' => $attendance['timestamp'],
							'type' => $attendance['type']
						);
					break;

				}

			}

			if(count($days) > 0) {
				return $days;
			}

			return false;
		}
		
		// --------------------------------------------------------------------------

		public function request_feed() {

			$request_feed = array();
			$leave_requests = $this->leave->detailed_list($this->session->id);
			$overtime_requests = $this->overtime->detailed_list($this->session->id);

			if($leave_requests !== false) {
				$request_feed = array_merge($request_feed, $leave_requests);
			}

			if($overtime_requests !== false) {
				$request_feed = array_merge($request_feed, $overtime_requests);
			}

			if(count($request_feed) > 0) { return $request_feed; }

			return false;
		}

		// --------------------------------------------------------------------------

	}