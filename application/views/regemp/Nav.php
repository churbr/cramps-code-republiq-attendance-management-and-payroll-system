<header class="main-header">
  <nav class="navbar navbar-static-top">
    <div class="container">

    <div class="navbar-header">
      <a href="<?= base_url('home.html');?>" class="navbar-brand"><img src="<?= base_url('resources/dist/img/code-republiq-logo.png');?>" style="width:16px;"></a>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
        <i class="fa fa-bars"></i>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="<?= base_url('home.html');?>" id="timein_navmenu"> <?php echo (!$timed_in) ? 'Time in' : 'Time out'; ?> </a></li>
        <!-- <li><a href="#employee_view_recent" data-toggle="modal">View Logs</a></li> -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?= base_url('request-feed.html'); ?>">Request Feed</a></li>
            <li><a href="<?= base_url('time-logs.html'); ?>">Time Logs</a></li>
            <li class="divider"></li>
            <li><a href="<?= base_url('view-holiday.html'); ?>">View Holidays</a></li>
          </ul>
        </li>
      </ul>
    </div>

    <!-- Navbar Right Menu -->
    <div id='nav_dropdown_menu' class="navbar-custom-menu">
      <ul class="nav navbar-nav">

        <li><a href="#employee-leave-request-modal" data-toggle="modal"><i class="fa fa-paper-plane-o"></i></a></li>

        <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
            </a>



<?php
    if($leave_limits !== false) {

      echo <<<DM
        <ul class='dropdown-menu mobile-responsive-dropdown'>
          <li class='header'>You have...</li>
          <li>
            <ul class='menu'>
              <li>
DM;

        foreach ($leave_limits as $index => $leave) {
?>
          <a href='#'>
            <i class="fa fa-home text-maroon"></i> <?= $leave['name']; ?>
            <small class="text-aqua pull-right">

<?php                
                $leave_remaning_days = $leave['remaining_days'];

                if($leave_remaning_days == 1) {
                  echo '1 day left';
                }elseif($leave_remaning_days == 0) {
                  echo 'No more days left';
                }else { echo $leave_remaning_days . ' days left'; }
?>
            </small>
          </a>
<?php
        }

      echo <<<DME
            </li>
          </ul>
        </li>
      </ul>
DME;

    }else {
      echo "<ul class='dropdown-menu'>
              <li class='header'>Leave limits not set.</li>
            </ul>";
    }
?>
        </li>

        <li class="dropdown notifications-menu">

          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <?= ($notification_count > 0) ? '<span class="label label-warning" id="notification_number">' . $notification_count . '</span>' : ''; ?>
            <i class="fa fa-bell-o"></i>
          </a>

          <!-- Dropdown list of notifications -->
          <ul class="dropdown-menu mobile-responsive-dropdown">
            <li class="header">
              <?= ($notification_count > 0) ? 'You have ' . $notification_count . ' unread notification(s)' : 'No notifications.'; ?>
            </li>


            <?php if($notification_list !== false) { ?>
              <li>
                <ul class="menu">
                  <li>

                  <?php
                      foreach ($notification_list as $index => $notification) {

                        $notification_id = $notification['id'];

                        if($notification['notification_type_id'] == 1) {
                  ?>
                        <a <?php echo ($notification['status'] == 0) ? 'class="bg-gray odd"' : ''; ?> href="<?= base_url('view-notification/leave/' . $notification_id);?>" data-toggle="modal">
                          <i class="fa fa-bullhorn text-aqua"></i> Leave Request
                        </a>
                  <?php
                      } else {
                  ?>
                        <a <?php echo ($notification['status'] == 0) ? 'class="bg-gray odd"' : ''; ?> href="<?= base_url('view-notification/overtime/' . $notification_id);?>" data-toggle="modal">
                          <i class="fa fa-bullhorn text-aqua"></i> Overtime Request
                        </a>
                  <?php
                        }
                      }
                  ?>

                  </li>
                </ul>
              </li>
            <?php } ?>

            <li class="footer"><a href="<?= base_url(); ?>request-feed.html">View all</a></li>
          </ul>
        </li>
        
        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= base_url('resources/dist/img/users/' . $picture); ?>" class="user-image" alt="User Image">
            <span class="hidden-xs"> <?= $this->session->fullname; ?> </span>
          </a>
          
          <ul class="dropdown-menu mobile-responsive-dropdown">
            <li class="user-header">
              <img src="<?= base_url('resources/dist/img/users/' . $picture); ?>" class="img-circle" alt="User Image">
              <p>
                <?= $this->session->fullname . '  ' . $employee_position; ?>
                <small>Started Since: <?= nice_date($date_hired, 'F d, Y'); ?> </small>
              </p>
            </li>

            <li class="user-footer">
              <div class="pull-left">
                <a href="<?php echo base_url('edit-profile.html'); ?>" class="btn btn-default btn-flat">Profile</a>
              </div>

              <div class="pull-right">
                <a href="<?php echo base_url('logout.html'); ?>" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>

    </div>
  </nav>
</header>