<!-- *************************************************************************************** -->

<div class="modal" id="overtime_request_form" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Overtime Application</h4>
      </div>
      <div class="modal-body">
        <div class="row">

        <?= form_open('attendance/request-overtime', array('class' => 'request_overtime')); ?>


          <div class="form-group col-xs-12">
          	<p> <span style="font-weight: bold;">Start: </span> <span class='main_start_time'></span> </p>
          	<p> <span style="font-weight: bold;">End: </span><span class='main_end_time'></span> </p>
          </div>


          <div class="form-group col-xs-6">
            <label>OT Start</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
                </div>
              <input type="time" id='ot_start_time' name='ot_start_time' class="form-control pull-right">
            </div>
          </div>
          <div class="form-group col-xs-6">
            <label>OT End</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
                </div>
              <input type="time" id='ot_end_time' name='ot_end_time' class="form-control pull-right">
            </div>
          </div>
          <div class="form-group col-xs-6">
            <label>Project Manager</label>
            <div>
              <select id='pm_id' name='pm_id' class="form-control">
<?php
	if($project_manager !== false) {

		echo '<option disabled selected>Select PM...</option>';
		
		foreach ($project_manager as $index => $pm) {

			$pm_id = $pm['id'];
			$pm_mi = substr($pm['middlename'] , 0, 1) . '.';
			$pm_fullname = $pm['firstname'] . ' ' . $pm_mi . ' ' . $pm['lastname'];

			echo "<option value='$pm_id'>$pm_fullname</option>";
		}
	}else {
		echo "<option value='0'>You don't have a PM yet.</option>";
	}
?>
              </select>
            </div>
          </div>
          <div>
            <div class="form-group col-xs-12">
              <textarea id='ot_reason' name='ot_reason' class="form-control" rows="5" placeholder="Your reason here..."></textarea>
              <input type='hidden' id='table_pk' name='table_pk' />
            </div>
          </div>

		<div id='overtime_application_msg' class="form-group col-xs-12">
		</div>

        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id='overtime_request_close'>Close</button>
         <button type="button" class="btn btn-danger" id='overtime_request_clear'>Clear</button>
         <input type='submit' class='btn btn-primary' value='Ok' />
      </div>

      <?= form_close(); ?>
    </div>
   </div>
</div>

<!-- *************************************************************************************** -->

<div class="content-wrapper">
	<div class="container">
		<section class="content-header">
			<div class="content-wrapper">
				<section class="content">
					<div class="row">
						<div class="col-md-4 col-sm-8 col-xs-12">
							<div class="info-box">
								<span class="info-box-icon bg-blue"><i class="fa fa-clock-o"></i></span>
								<div class="info-box-content">
									<span class="info-box-text">Total Regular Hours</span>
									<span class="info-box-number" id='total_timelog_reghours'>
										<?php
											if($attendance_total_hours['regular'] !== NULL) {
												echo $attendance_total_hours['regular'];
											}else { echo '0'; }
										?>
									</span>
								</div>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="info-box">
								<span class="info-box-icon bg-blue"><i class="fa fa-hourglass"></i></span>
								<div class="info-box-content">
									<span class="info-box-text">Total Overtime</span>
									<span class="info-box-number" id='total_timelog_overtime'>
										<?php
											if($attendance_total_hours['overtime'] !== NULL) {
												echo $attendance_total_hours['overtime'];
											}else { echo '0'; }
										?>
									</span>
								</div>
							</div>
						</div>
					</div>

					<?= form_open('attendance/timelog-search', array('class' => 'timelog_search')); ?>
					<div class="row">
						<div class="form-group col-md-4 col-sm-4 col-xs-12">
							<label>From</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="date" id='from' class="form-control pull-right" />
							</div>
						</div>
						
						<div class="form-group col-md-4 col-sm-4 col-xs-12">
							<label>To</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="date" id='to' class="form-control pull-right" />
							</div>
						</div>
						
						<div class="form-group col-md-4 col-sm-4 col-xs-12">
							<label>Search</label>
							<div class="input-group">
								<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</div>
					<?= form_close(); ?>

					<div class="row">
						<div class="col-xs-12">
							<div class="box box-primary">
								<div class="box-body">
									<table id='timelog_table' class="table table-hover">
										<thead>
											<tr>
												<td style='display: none;'></td>
												<th>Date</th>
												<th>Time In</th>
												<th>Time Out</th>
												<th>Total Time</th>
												<th>Action</th>
											</tr>
										</thead>
										
										<tbody>


<?php
										if(count($timelog)) { // leave, overtime, timelog

											foreach ($timelog as $index => $log) {

												if($log['type'] == 'leave') {

													echo <<<LEAVE
														<tr>
															<td style='display: none;'>{$log['timestamp']}</td>
															<td>{$log['date']}</td>
															<td>--</td>
															<td>--</td>
															<td>{$log['total_time']} hrs <span class='label bg-navy'>Leave</span></td>
															<td>
																<div class='btn-group'>
																	<a class='dropdown-toggle' data-toggle='dropdown'>
																		<span><i class='fa fa-fw fa-toggle-down'></i></span>
																		<span class='sr-only'>Toggle Dropdown</span>
																	</a>
																	<ul class='dropdown-menu' role='menu'>
																		<li><a onclick='show_leave_modal({$log["id"]})' href='#/'>View</a></li>
																	</ul>
																</div>
															</td>
														</tr>
LEAVE;

												}elseif($log['type'] == 'overtime') {

													$hr_word = ($log['hour'] > 1) ? ' hrs ' : ' hr ';
													$min_word = ($log['minute'] > 1) ? ' mins ' : ' min ';

													if($log['hour'] == 0 && $log['minute'] == 0) {
														$overtime_total_time = '<strong>NC</strong>';
													}elseif ($log['hour'] > 0 && $log['minute'] == 0) {
														$overtime_total_time = $log['hour'] . $hr_word;
													}elseif($log['hour'] == 0 && $log['minute'] > 0) {
														$overtime_total_time = $log['minute'] . ' ' . $min_word;
													}else {
														$overtime_total_time = $log['hour'] . $hr_word . $log['minute'] . $min_word;
													}

													// ------------------------------------------------------------------------------

													$tr_id = 'tr_overtime_request_' . $log['id'];

													// ------------------------------------------------------------------------------

													if($log['notification_status'] == 0 && $log['overtime_status'] != 'pending') {
														$seen_status = 'class="bg-gray"';
													}else { $seen_status = ''; }

													// ------------------------------------------------------------------------------

													$overtime_status = function($overtime_status) {
														$html_form = null;

														switch ($overtime_status) {
															case 'approved':
																$html_form = '<span class="label bg-teal">OT Approved</span>';
															break;

															case 'pending':
																$html_form = '<span class="label label-warning">OT Pending</span>';
															break;

															case 'declined':
																$html_form = '<span class="label label-danger">OT Declined</span>';
															break;
														}

														return $html_form;
													};

													// ------------------------------------------------------------------------------
?>

													<tr <?= $seen_status . " id='$tr_id'"; ?> >
														<td style='display: none;'> <?= $log['timestamp']; ?> </td>
														<td> <?= $log['start_date']; ?> </td>
														<td> <?= $log['start_time']; ?> </td>
														<td> <?= $log['end_time']; ?> </td>
														<td><?= $overtime_total_time . ' ' . $overtime_status($log['overtime_status']); ?>  </td>
														<td>
															<div class="btn-group">
																<a class="dropdown-toggle" data-toggle="dropdown">
																	<span><i class="fa fa-fw fa-toggle-down"></i></span>
																	<span class="sr-only">Toggle Dropdown</span>
																</a>
																
																<ul class="dropdown-menu" role="menu">

																<?php
																	if($log['overtime_status'] == 'pending') {

																		echo "<li><a onclick='show_overtime_modal({$log['id']})' href='#/'>View</a></li>";
																		echo "<li class='divider'></li>";
																		echo "<li><a onclick='cancel_overtime_request({$log['id']})' href='#/'>Cancel</a></li>";
																	
																	}elseif($log['overtime_status'] == 'declined') {

																		echo "<li><a onclick='show_overtime_modal({$log['id']})' href='#/'>View</a></li>";
																		echo '<li class="divider"></li>';
																		echo "<li><a onclick='resend_overtime_request({$log['id']})' href='#/'>Apply for Overtime</a></li>";
																	
																	}else {

																		echo "<li><a onclick='show_overtime_modal({$log['id']})' href='#/'>View</a></li>";
																	
																	}
																?>

																</ul>
															</div>
														</td>
													</tr>
<?php
												}elseif($log['type'] == 'timelog_overtime') {

													$hr_word = ($log['hour'] > 1) ? ' hrs ' : ' hr ';
													$min_word = ($log['minute'] > 1) ? ' mins ' : ' min ';

													if($log['hour'] == 0 && $log['minute'] == 0) {
														$timelog_total_time = '<strong>NC</strong>';
													}elseif ($log['hour'] > 0 && $log['minute'] == 0) {
														$timelog_total_time = $log['hour'] . $hr_word;
													}elseif($log['hour'] == 0 && $log['minute'] > 0) {
														$timelog_total_time = $log['minute'] . ' ' . $min_word;
													}else {
														$timelog_total_time = $log['hour'] . $hr_word . $log['minute'] . $min_word;
													}

													echo <<<TIMELOG_OVERTIME
															<tr>
																<td style='display: none;'> {$log['timestamp']} </td>
																<td> {$log['timein_date']} </td>
																<td> {$log['time_in']} </td>
																<td> {$log['time_out']} </td>
																<td> $timelog_total_time </td>
																<td>
																	<div class="btn-group" style="pointer-events: none; opacity: 0.4;">
																		<a class="dropdown-toggle" data-toggle="dropdown">
																			<span><i class="fa fa-fw fa-toggle-down"></i></span>
																			<span class="sr-only">Toggle Dropdown</span>
																		</a>
																	</div>
																</td>
															</tr>
TIMELOG_OVERTIME;

												}else {

													$hr_word = ($log['hour'] > 1) ? ' hrs ' : ' hr ';
													$min_word = ($log['minute'] > 1) ? ' mins ' : ' min ';

													if($log['hour'] == 0 && $log['minute'] == 0) {
														$timelog_total_time = '<strong>NC</strong>';
													}elseif ($log['hour'] > 0 && $log['minute'] == 0) {
														$timelog_total_time = $log['hour'] . $hr_word;
													}elseif($log['hour'] == 0 && $log['minute'] > 0) {
														$timelog_total_time = $log['minute'] . ' ' . $min_word;
													}else {
														$timelog_total_time = $log['hour'] . $hr_word . $log['minute'] . $min_word;
													}

													if(!is_null($log['time_out'])) {
														echo <<<TIMELOG_COMPLETE

															<tr>
																<td style='display: none;'> {$log['timestamp']} </td>
																<td> {$log['timein_date']} </td>
																<td> {$log['time_in']} </td>
																<td> {$log['time_out']} </td>
																<td> $timelog_total_time </td>
																<td>
																	<div class="btn-group">
																		<a class="dropdown-toggle" data-toggle="dropdown">
																			<span><i class="fa fa-fw fa-toggle-down"></i></span>
																			<span class="sr-only">Toggle Dropdown</span>
																		</a>
																		
																		<ul class="dropdown-menu" role="menu">
																			<li><a onclick='show_overtime_form({$log['id']})' href='#/'>Apply for Overtime</a></li>
																		</ul>
																	</div>
																</td>
															</tr>
TIMELOG_COMPLETE;
													}else {

														echo <<<TIMELOG_INCOMPLETE

															<tr>
																<td style='display: none;'> {$log['timestamp']} </td>
																<td> {$log['timein_date']} </td>
																<td> {$log['time_in']} </td>
																<td> -- </td>
																<td> $timelog_total_time </td>
																<td>
																	<div class="btn-group" style="pointer-events: none; opacity: 0.4;">
																		<a class="dropdown-toggle" data-toggle="dropdown">
																			<span><i class="fa fa-fw fa-toggle-down"></i></span>
																			<span class="sr-only">Toggle Dropdown</span>
																		</a>
																	</div>
																</td>
															</tr>
TIMELOG_INCOMPLETE;

													}

												}
											} // End of foreach

										}else {
											echo "
												<tr>
													<td style='display: none;'></td>
													<td><p class='error'>Log is empty</p></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											";
										}

?>


										<!-- *************************************************************************************** -->

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</section>
	</div>
</div>