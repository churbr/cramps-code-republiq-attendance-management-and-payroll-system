<div class="content-wrapper">
  <div class="container">
    <section class="content-header">
    </section>

    <section class="content">

      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-suitcase"></i></span>
            <div class="info-box-content">
              <h3>Holidays</h3>
            </div>
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">

              <table id="holiday_table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Holiday</th>
                    <th>Type</th>
                  </tr>
                </thead>

                <tbody>

<?php
  foreach ($holidays as $index => $holiday) {
    $type = ($holiday['type'] == 'regular') ? 'Regular Holiday' : 'Special Holiday';
?>

                  <tr>
                    <td> <?= nice_date($holiday['date'], 'F d'); ?> </td>
                    <td> <?= $holiday['name']; ?> </td>
                    <td> <?= $type; ?> </td>
                  </tr>

<?php
  }
?>

                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>

    </section>
    
  </div>
</div>