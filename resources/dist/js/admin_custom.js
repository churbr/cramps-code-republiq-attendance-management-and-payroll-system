//overtime time select
$(document).ready(function () {
    toggleFields();
    $(".user-type-select").change(function () {
    toggleFields();
    });

});

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap_other_deductions"); //Fields wrapper
    var add_button      = $(".add_field_button_other_deductions"); //Add button ID
    

    $('#type').on('change', function() {
    	type = $('#type').val();

    	if(type == 'project_manager') {
    		$('#project_manager_field').hide();
    	}else {
    		$("#project_manager_field").show();
    	}

    });


    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row" style="padding-bottom:15px;"><label class="col-sm-2 control-label">Name</label><div class="col-sm-4"><input type="text" class="form-control" name="mytext[]"/></div><label class="col-sm-1 control-label">Amount</label><div class="col-sm-3"><input type="number" class="form-control" name="mytext[]"/></div><a href="#" class="remove_field col-sm-1">Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});

function toggleFields() {
    if ($(".user-type-select").val() != 'project_manager')
        $(".project_manager").show();
    else
        $(".project_manager").hide();
}

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap_adjustments"); //Fields wrapper
    var add_button      = $(".add_field_button_adjustments"); //Add button ID
    
    var x = 1; //initial text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row" style="padding-bottom:15px;"><label class="col-sm-2 control-label">Name</label><div class="col-sm-4"><input type="text" class="form-control" name="mytext[]"/></div><label class="col-sm-1 control-label">Amount</label><div class="col-sm-3"><input type="number" class="form-control" name="mytext[]"/></div><a href="#" class="remove_field col-sm-1">Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });
    
    $('#payroll_mgt_quick_form').submit(function(e){
        var curdate = new Date($('#date_from').val());
        
        if (curdate.getDay()==1) {
            return true;
        }
        else{
            alert('From Date should be a Monday');
            return false;
        }
        
    });
    
    $('#payroll_overview_form').submit(function(e){
        var curdate = new Date($('#date_from').val());
        
        if (curdate.getDay()==1) {
            return true;
        }
        else{
            alert('From Date should be a Monday');
            return false;
        }
        
    });
});
//Start of change BORJA 3_13_2016
function triggerPDF(uid,url) {
    /*var data = [];
    data['uid']=uid;

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success:function(response) {			
           
        },        
        error:function(Err){
            alert(Err);
        }
    });*/
}
function updateMonthSelection(form) {
   if (form.value=='all') {
    $("#selectmonth").val("all");
   }
}
function validateMonthSelection(form) {
    if (form.value!='all'&&$("#selectyear").val()=='all') {
        form.value = 'all';
       alert('Cannot select specific Month data if no specific Year selected');
    }
}
function updateTo(from) {
    var curdate = new Date($('#date_from').val());
    var enddate = curdate;
    var targetdate = new Date();
    enddate.setDate(enddate.getDate() + 13);
            
    targetMonth = enddate.getMonth()<10 ? '0'+(enddate.getMonth()+1) : (enddate.getMonth()+1);
    targetDay = enddate.getDate()<10 ? '0'+enddate.getDate() : enddate.getDate();
    targetdate = enddate.getFullYear()+'-'+targetMonth+'-'+targetDay;
    $('#date_to').val(targetdate);
}
function updateEditHolidayTexts(id,name,type,date){
	$('#edit_holiday_name').html('You are about to edit '+name);
	$('#edit_holiday_title').html('Edit '+name);
	$('#holiday_name_edit').val(name);
	$('#holiday_date_edit').val(date);
	$('#holiday_id_edit').val(id);
	$('#holiday_type_edit option[value="'+type+'"]').attr("selected",true);
}
function updateRemoveHolidayTexts(id,name){
	$('#holiday_id_remove').val(id);
	$('#remove_holiday_name').html('You are about to remove '+name);
}
function addHoliday(form){
	var that = $(form),
				url		= that.attr('action'),
				method	= that.attr('method'),
				data	= {};

	that.find('[name]').each(function() {
		var that = $(this),
				name		= that.attr('name'),
				value		= that.val();
				data[name]	= value;
	});
			
	if(data['holiday_name'].length==0||data['holiday_date'].length==0){
		$('#add_holiday_message').html('Fill in all fields');
	}
	else{
		$.ajax({
			url: url,
			type: method,
			data: data,
			success:function(response) {               
				if(response==0){
					$('#add_holiday_message').html('Holiday added Successfully');
				}
				else if(response==1){
					$('#add_holiday_message').html('Date already used by another holiday');
				}
				else if(response==2){
					$('#add_holiday_message').html('Holiday addition failed');
				}
				else{
					$('#add_holiday_message').html('An error occured');
				}
			}
		});
       
	}
	return false;
}

function editHoliday(form){
	var that = $(form),
				url		= that.attr('action'),
				method	= that.attr('method'),
				data	= {};

	that.find('[name]').each(function() {
		var that = $(this),
				name		= that.attr('name'),
				value		= that.val();
				data[name]	= value;
	});

	if(data['holiday_name_edit'].length==0||data['holiday_date_edit'].length==0){
		$('#edit_holiday_message').html('Fill in all fields');		
	}
	else{		
		$.ajax({
			url: url,
			type: method,
			data: data,
			success:function(response) {			
				if(response==0){
					$('#edit_holiday_message').html('Holiday changed Successfully');
				}
				else if(response==1){
					$('#edit_holiday_message').html('Date already used by another holiday');
				}
				else if(response==2){
					$('#edit_holiday_message').html('Holiday modification failed');
				}
				else if(response==3){
					$('#edit_holiday_message').html('Holiday not found');
				}
				else{
					$('#edit_holiday_message').html('An error occured');
				}
			}
		});		
	}
	return false;
}
function setActivateModalText(action, id,name){
    if (action=='activate') {
        $('#activate_modal_text').html('You are about to '+action+': '+name);
    }
    else{
       $('#activate_modal_text').html('You are about to '+action+': '+name+'. <br/>Enter reason.'); 
    }
		
	$('#modal_emp_id').val(id);	
	$('#modal_emp_function').val(action);
    if (action=='deactivate') {
        $('#reason_div').show();
    }else{
        $('#reason_div').hide();
    }
}
function hideToast(){
	$('#admin-notification').hide();
}

function addAdjustmentFields(){
	//Dynamically add Other Adjustments fields on button click
	$('#adjustment_group').append('<div class="form-group"><label class="col-sm-2 control-label">Name</label><div class="col-sm-3"><input type="text" id="adjust_name[]" name="adjust_name[]" class="form-control" /></div><label class="col-sm-2 control-label">Amount</label><div class="col-sm-3"><input type="number" id="adjust_value[]" name="adjust_value[]" class="form-control" /><a href="#remove-od-prompt" data-toggle="modal" onclick="$(this).closest(\'.form-group\').remove();">Remove</a></div></div>');	
}

function addDeductionFields(){
	//Dynamically add Other Deductions fields on button click
	$('#deduction_group').append('<div class="form-group"><label class="col-sm-2 control-label">Name</label><div class="col-sm-3"><input type="text" id="deduct_name[]" name="deduct_name[]" class="form-control" /></div><label class="col-sm-2 control-label">Amount</label><div class="col-sm-3"><input type="number" id="deduct_value[]" name="deduct_value[]" class="form-control" /><a href="#remove-od-prompt" data-toggle="modal" onclick="$(this).closest(\'.form-group\').remove();">Remove</a></div></div>');	
}
//End of change BORJA 3_13_2016