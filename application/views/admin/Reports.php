Employee Management Wrapper -->
      <div class="employee-management-wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1 id="reports_header">
              Reports
              <small></small>
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
		  
		  <div class="row">
              <div class="col-md-10 col-xs-12">
                <div class="box box-danger">
                  <div class="box-body">
                    <div class="row">
                      <?= form_open('admin/reports', array('id' => 'report_view')); ?>
                      <div class="form-group col-md-5 col-xs-12">
                        <label>Month</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>                            
                          
						  <select id="selectmonth" name="selectmonth"class="form-control pull-right" onchange='validateMonthSelection(this)'>
							<?php
							  foreach($dropdown_months as $workarea){
								if($selected_month==$workarea['val']){
								  echo '<option selected value="'.$workarea['val'].'">'.$workarea['name'].'</option>';
								}
								else{
								  echo '<option value="'.$workarea['val'].'">'.$workarea['name'].'</option>';
								}
								
							  }
							?>
							
						  </select>						  
                        </div>
                      </div>
                      <div class="form-group col-md-5 col-xs-12">
                        <label>Year</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
						  <select id="selectyear" name="selectyear" class="form-control pull-right" onchange='updateMonthSelection(this)'>
							<?php
							  foreach($dropdown_years as $workarea){
								if($selected_year==$workarea['val']){
								  echo '<option selected value="'.$workarea['val'].'">'.$workarea['name'].'</option>';
								}
								else{
								  echo '<option value="'.$workarea['val'].'">'.$workarea['name'].'</option>';
								}
								
							  }
							?>
							
						  </select>	
                        </div>
                      </div>
                      <div class="form-group col-md-2 col-xs-12">
                        <label>Calculate</label>
                        <div class="input-group">
                          <button type="submit" class="btn btn-default"><i class="fa fa-calculator"></i></button>                          
                        </div>
                      </div>
                      <?= form_close(); ?>
                    </div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- /.col -->
            </div><!-- /.row -->
            <!-- Your Page Content Here -->
	<div id="campaign_keywords"></div>
            <div class="row">
              <div class="col-xs-12">
                <div class="box box-danger">
                  <div class="box-body">
                    <table id="example1" class="table table-hover">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Employee Name</th>
                          <th>Approved OT</th>
                          <th>OT Hours</th>
                          <th>Sick Leave</th>
                          <th>Vacation</th>
                          <th>Paternity</th>
                          <th>Maternity</th>
                          <!-- <th>Absences</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      	<?php
							foreach($report_details as $rept){
								if($rept['gender']=='male'){
									$rept['maternity'] = '--';
								}
								else{
									$rept['paternity'] = '--';
								}
								echo '<tr><td>'.$rept['id'].'</td><td>'.$rept['firstname'].' '.$rept['lastname'].'</td><td>'.$rept['approved_ot'].'</td><td>'.$rept['total_ot'].'</td><td>'.$rept['sickleave'].'</td><td>'.$rept['vacation'].'</td><td>'.$rept['paternity'].'</td><td>'.$rept['maternity'].'</td></td></tr>';
                // echo '<tr><td>'.$rept['id'].'</td><td>'.$rept['firstname'].' '.$rept['lastname'].'</td><td>'.$rept['approved_ot'].'</td><td>'.$rept['total_ot'].'</td><td>'.$rept['sickleave'].'</td><td>'.$rept['vacation'].'</td><td>'.$rept['paternity'].'</td><td>'.$rept['maternity'].'</td><td>'.$rept['absences'].'</td></tr>';
							}
						?>
                      </tbody>
                    </table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
              <div class="col-md-2 col-sm-8 col-xs-6">
                <div class="box">
                  <div class="box-body">
                    <div class="col-xs-12">
                      <button class="btn btn-block btn-default btn-sm print-reports"><i class="fa fa-fw fa-print"></i>Print</button>
                    </div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- /.col -->
            </div><!-- /.row -->

          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.employee-management-wrapper -->

    <script type="text/javascript">
    $('.print-reports').on('click',function(){
    printReports();
    })
    </script>

